<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
        if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$permission=array();
	$cl2=new mysql_class;
	//die("user id is:".$user_id);
	$canWrite=$se->detailAuth('doWrite');
	if($user_id==1)
		$canWrite='all';
	//echo($canWrite);
	/*else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("profileAdmin.php",$permission))
			$canWrite='limited';
		
	}*/
	function loadUser($id)
	{
		$u = new user_class((int)$id);
		$out = '----';
		if(isset($u->id))
			$out = $u->fname.' ' .$u->lname;
		return($out);
	}
	function mpdate($inp)
	{
		return(($inp != '' && $inp != '0000-00-00 00:00:00')?jdate("H:i d / m / Y",strtotime($inp)):'----');
	}
	function mpdate_back($inp)
	{
		return(audit_class::hamed_pdateBack($inp));
	}
	function loadPic($id)
	{
		$id = (int)$id;
		$out = '----';
		$my = new mysql_class;
		$my->ex_sql("select `pic` from `profile` where `id` = '$id'",$q);
		if(isset($q[0]))
		{
			if ($q[0]["pic"]!='')
			{
				$pic = $q[0]["pic"];
				$out = "<img src=\"$pic\" class=\"imgGallTmb pointer\" onclick=\"startUpload1('$id')\"/>";
				$out .= "<div id=\"fileupload1_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr1_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
			else
			{
				$out = "<button  style=\"margin:10px;\" onclick=\"startUpload('$id');\">بروزرسانی تصویر</button>";
				$out .= "<div id=\"fileupload_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
		}		
		return($out);
	}
	if(isset($_REQUEST['sendEmail']))
	{
		$defF = array('msg','sendEmail','sendSms','sendAll','all');
		$sendAll = ($_REQUEST['sendAll'] == 'true');
		$my = new mysql_class;
		if($sendAll)
		{
			$whereC = array();
			foreach($_REQUEST as $key=>$value)
				if(!in_array($key,$defF))
					$whereC[] = "`$key` = '$value'";
			$my->ex_sql("select `email`,`mob` from `profile` ".((count($whereC)>0)?' where '.implode(' and ',$whereC):''),$q);
		}
		else
		{
			$profile_ids = explode(',',$_REQUEST['profile_ids']);
			$my->ex_sql("select `email`,`mob` from `profile` ".((count($profile_ids)>0)?' where `id` in ('.implode(',',$profile_ids).')':''),$q);
		}
		foreach($q as $r)
		{
			if($_REQUEST['sendEmail'] == 'true')
				$e = new email_class($r['email'],'پیام از دارما',$_REQUEST['msg']);
			if($_REQUEST['sendSms'] == 'true')
				sms_class::send($_REQUEST['msg'],$r['mob']);
		}
		die('true');
	}
	$columnList = '';
	$visibleCols = isset($_REQUEST['cols'])?explode(',',$_REQUEST['cols']):array(1,2,4,9,11,12,13,14,15,17);
	$colNames = array('','کاربر','معرف','','اعتبار','تلفن ثابت','تلفن همراه','آدرس','کد پستی','تصویر','پست الکترونیک','تاریخ تولد','منطقه','شهر','نوع مشتری','دسته کاربر','کمیسیون زیردستان%','جنسیت');
        $gname = "gname_profile";
	$input =array($gname=>array('table'=>'profile','div'=>'main_div_profile'));
        $xgrid = new xgrid($input);
	$xgrid->whereClause[$gname] = " not(`user_id` in (select `id` from `user` where `user`='mehrdad'))";
	$xgrid->column[$gname][1]['cfunction'] = array('loadUser');
	$xgrid->column[$gname][1]['access'] = 'a';
	$xgrid->column[$gname][1]['search'] = 'list';
	$xgrid->column[$gname][1]['searchDetails'] = columnListLoader("user| `user` <> 'mehrdad'",array('id','fname','lname'));
	$xgrid->column[$gname][2]['clist'] = columnListLoader("user| `user` <> 'mehrdad'",array('id','fname','lname'));//array('loadUser');
	$xgrid->column[$gname][2]['search'] = 'list';
	$xgrid->column[$gname][2]['searchDetails'] = columnListLoader("user| `user` <> 'mehrdad'",array('id','fname','lname'));
//	$xgrid->column[$gname][2]['access'] = 'a';
	$xgrid->column[$gname][4]['cfunction'] = array('monize','umonize');
	$xgrid->column[$gname][4]['search'] = 'text';
	
	//if($canWrite=='all' || $canWrite=='limited')
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
	{
		$xgrid->column[$gname][9] = $xgrid->column[$gname][0];
		$xgrid->column[$gname][9]['cfunction'] = array('loadPic');
		$xgrid->column[$gname][9]['access'] = 'a';
	}
	
	$xgrid->column[$gname][11]['cfunction'] = array('mpdate','mpdate_back');
	$xgrid->column[$gname][12]['clist'] = columnListLoader('mantaghe');
	$xgrid->column[$gname][12]['search'] = 'list';
	$xgrid->column[$gname][12]['searchDetails'] = columnListLoader('mantaghe');
	$xgrid->column[$gname][13]['clist'] = columnListLoader('city');
	$xgrid->column[$gname][13]['search'] = 'list';
	$xgrid->column[$gname][13]['searchDetails'] = columnListLoader('city');
	$xgrid->column[$gname][14]['clist'] = columnListLoader('customer_type');
	$xgrid->column[$gname][14]['search'] = 'list';
	$xgrid->column[$gname][14]['searchDetails'] = columnListLoader('customer_type');
	$xgrid->column[$gname][15]['clist'] = columnListLoader('user_daste');
	$xgrid->column[$gname][15]['search'] = 'list';
	$xgrid->column[$gname][15]['searchDetails'] = columnListLoader('user_daste');
	$xgrid->column[$gname][17]['clist'] = array(1=>'مرد',2=>'زن');
	$xgrid->column[$gname][17]['search'] = 'list';
	$xgrid->column[$gname][17]['searchDetails'] = array(0=>'',1=>'مرد','2'=>'زن');
       	
	//if($canWrite=='all' || $canWrite=='limited')
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
	{
		$xgrid->canAdd[$gname] = TRUE;
		$xgrid->canDelete[$gname] = TRUE;
		$xgrid->canEdit[$gname]= TRUE;
	}
	foreach($xgrid->column[$gname] as $indx=>$col)
	{
		$checked = FALSE;
		if(in_array($indx,$visibleCols))
		{
			$xgrid->column[$gname][$indx]['name'] = $colNames[$indx];
			$checked = TRUE;
		}
		else
			$xgrid->column[$gname][$indx]['name'] = '';
		if(isset($colNames[$indx]) && $colNames[$indx] != '')
		{
			$columnList .= $colNames[$indx];
			if($canWrite=='never')
			{
				$columnList .= '<input class="selCols" type="checkbox" disabled="disabled" 
				id="sel_'.$col['fieldname'].'_'.$indx.'" '.(($checked)?'checked="checked"':'').'/>';
			}
			else
			$columnList .= '<input class="selCols" type="checkbox" id="sel_'.$col['fieldname'].'_'.$indx.'" '.(($checked)?'checked="checked"':'').'/>';
		}
	}
	
	if($columnList != '')
		if($canWrite!='never')
			$columnList .= '<button onclick="selectColumns();">انتخاب</button>';
		else
			$columnList .= '<button disabled="disabled" onclick="selectColumns();">انتخاب</button>';
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);
?>
<style>
	.imgGallTmb{width:50px;}
	#msgDiv
	{
		border : 1px #000000 solid;
		width : 400px;
	}
</style>
<script type="text/javascript" >
	var ggname = '<?php echo $gname; ?>';
	function startUpload(id)
	{
		$("#ifr_"+id).prop("src","upload_pic.php?id="+id+"&table=profile&");
		$("#fileupload_"+id).toggle();
	}
	function startUpload1(id)
	{
		$("#ifr1_"+id).prop("src","upload_pic.php?id="+id+"&table=profile&");
		$("#fileupload1_"+id).toggle();
	}
	function selectColumns()
	{
		var selFields = [];
		$(".selCols:checked").each(function(id,field){
			selFields.push(field.id.split('_')[field.id.split('_').length-1]);
			
		});
		//alert(selFields.join());
		console.log("f is:",selFields.join());
		gArgs[ggname].eRequest = {cols:selFields.join()};
		grid[ggname].init(gArgs[ggname]);
	}
	function loadPic(obj)
	{
		//alert(obj);
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load(obj,function(){
                        selectButton(obj);
                });
	}	
	function RPage()
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load('profileAdmin.php');
	}
	function sendMsg()
	{
		var pars = {msg:encodeURIComponent($("#msg").val()),sendEmail : $("#emailSend").prop('checked'),sendSms : $("#smsSend").prop('checked'),sendAll : $("#allSend").prop('checked')};
		if($("#allSend").prop('checked'))
		{
			//for(i in whereObject)
			if(typeof whereObj[ggname] != 'undefined')
			{
				for(i in whereObj[ggname])
					pars[i] = whereObj[ggname][i];
			}
			else
				pars['all'] = 'all';
		}
		else
		{
			var profile_ids = [];
			$(".ajaxgrid_checkSelect").each(function(id,field){
				if($(field).prop('checked'))
					profile_ids.push($("#gname_profile-span-id-"+field.id.split('-')[2]).html());
			});
			if(profile_ids.length > 0)
			{
				pars['profile_ids'] = profile_ids.join();
			}
			else
			{
				alert('حداقل یک نفر می بایست انتخاب شود');
				return(false);
			}
		}
		$("#msgDiv").append("<img src='../img/status_fb.gif' id='khoonMsg' />");
		$.post("profileAdmin.php",pars,function(result){
			//console.log(result);
			$("#khoonMsg").remove();
			alert('پیام با موفقیت ارسال گردید.');
		});
	}
        $(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
		var admin_acc='<?php echo($canWrite); ?>' ;

		if(admin_acc=='never')
		{
			$("#msgDiv").children().attr("disabled","disabled");
		}
		
        });
</script>
<div id="msgDiv" >
	متن پیام : <textarea id="msg" name="msg"></textarea><br/>
	پست الکترونیک : <input type="checkbox" id="emailSend" name="emailSend" checked="checked" /><br/>
	پیامک : <input type="checkbox" id="smsSend" name="smsSend" /><br/>
	ارسال به همه نتایج جستجو : <input type="checkbox" id="allSend" name="allSend" /><br/>
	<button onclick="sendMsg();">ارسال</button>
</div>
<div id="colSelector">
	<?php
		echo $columnList;
	?>
</div>
<div id="main_div_profile"></div>
