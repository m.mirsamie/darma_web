<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
        include_once '../simplejson.php';
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$isAdmin = $se->detailAuth('all');
	function loadUser($inp)
	{
		$user = new user_class($inp);
		return(isset($user->id)? $user->fname.' '.$user->lname:'حذف شده');
	}
	function tarikh($inp)
	{
		return($inp!='0000-00-00 00:00:00' ? jdate('H:i -- d / m / Y',strtotime($inp)):'نامعلوم');
	}
	function tarikhBack($inp)
	{
		return(audit_class::hamed_pdateBack($inp));
	}
        function loadKala($inp)
        {
            $tt = new kala_class($inp);
            return(isset($tt->id)?$tt->name:'--');
        }
        if(isset($_REQUEST['abarGroup_id']))
        {
            $abar_id = (int)$_REQUEST['abarGroup_id'];
            $ou = childFromParent($abar_id, 'kala_group','kala_abarGroup');
            die($ou);
        }
        if(isset($_REQUEST['miniGroup_id']))
        {
            $mini_id = (int)$_REQUEST['miniGroup_id'];
            $ou = childFromParent($mini_id,'kala_miniGroup','kala_group');
            die($ou);     
        }

        if(isset($_REQUEST['werc']) && strpos($_REQUEST['werc'],'Fake')===0)
        { 
            $werc = str_replace('Fake,','',$_REQUEST['werc']);
            $werc_arr= explode("|", $werc);
            $a_id = $werc_arr[0];
            $g_id = $werc_arr[1];
            $m_id = $werc_arr[2];
            $kala_ids = kala_class::loadByAllParent($a_id,$g_id,$m_id);
            $new_werc='';
            foreach( $kala_ids as $arr)
                $new_werc .=($new_werc==''?'':','). $arr['id'];
            if($new_werc!='')
                $new_werc="where id in($new_werc)";
            else
                $new_werc="where 1=0";
            $_REQUEST['werc'] = $new_werc;
        }
        if(isset($_REQUEST['a_id']))
        {
                $a_id = $_REQUEST['a_id'];
                $g_id = $_REQUEST['a_id'];
                $m_id = $_REQUEST['m_id'];
                $ou = kala_class::loadByAllParent($a_id,$g_id,$m_id);
                die(toJSON($ou));
        } 
        if(isset($_REQUEST['jamkol']))
	{
                $new_werc='';
                if($_REQUEST['quu']!='')
                {
                    $tt = explode(',',$_REQUEST['quu']);
                    if($tt[0]=='Fake')
                    {
                        $werc = str_replace('Fake,','',$_REQUEST['quu']);
                        $werc_arr= explode("|", $werc);
                        $a_id = $werc_arr[0];
                        $g_id = $werc_arr[1];
                        $m_id = $werc_arr[2];
                        $kala_ids = kala_class::loadByAllParent($a_id,$g_id,$m_id);
                        foreach( $kala_ids as $arr)
                            $new_werc .=($new_werc==''?'':','). $arr['id'];
                        if($new_werc!='')
                            $new_werc="where id in($new_werc)";
                        else
                            $new_werc="where 1=0";
                    }
                    else {
                        $new_werc=$_REQUEST['quu'];
                    }
                }
		$my = new mysql_class;
                $my->ex_sql("select sum(tedad) as te ,sum(ghimat) as ghi from factor_det $new_werc", $q);
                $tedad_jam = (int)$q[0]['te'];
                $ghimat_jam = (int)$q[0]['ghi'];
                $out=' تعداد: '.monize($tedad_jam)."   قیمت: ".monize($ghimat_jam);
		die($out);
	}
        $gname = 'grid1_report';
        $input =array($gname=>array('table'=>'factor_det','div'=>'main_div_report_grooh'));
        $xgrid = new xgrid($input);
        $xgrid->column[$gname][0]['name'] = '';
        $xgrid->column[$gname][1]['name'] = 'شماره فاکتور';
        $xgrid->column[$gname][2]['name'] = 'کالا';
        $xgrid->column[$gname][2]['cfunction'] = array('loadKala');
        $xgrid->column[$gname][3]['name'] = 'تعداد';
        $xgrid->column[$gname][4]['name'] = 'قیمت';
        $xgrid->column[$gname][5]['name'] ='توضیحات';
        $xgrid->column[$gname][6]['name'] = 'تخفیف مدیر';    
        $xgrid->column[$gname][7]['name'] ='تخفیف بسته';
        $xgrid->column[$gname][8]['name'] ='تخفیف تعداد';
        $xgrid->column[$gname][9]['name'] ='سقف تخفیف';
        $xgrid->column[$gname][10]['name'] ='';
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);
?>
<style>
	.bargashtFactor
	{
		color : red;
		font-weight : bold;
	}
	#jam
	{
		background: #eeeeee;
		padding : 10px;
		border : 2px solid #bbbbbb;
		font-size:20px;
	}
</style>
<script>
	var grid_gname = '<?php echo $gname; ?>';
        var kala=[];
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
		gArgs[grid_gname]['afterLoad'] = function(){
			jamkol();
		};
                load_kala();
        });
	function loadgroup(obj)
        {
            var abar_id = $(obj).val();
            $("#kala_group_span").html("<img src='../img/status_fb.gif'>");
            $.get("report_grooh.php?abarGroup_id="+abar_id,function(result){
                var out = '<select id="kala_group_id" onchange="load_miniGroup(this);" >';
                out+='<option value="-1" >گروه کالا</option>';
                out+=$.trim(result);
                out+='</select>';
                $("#kala_group_span").html(out);
                load_kala();
            });
        }
        function load_miniGroup(obj)
        {
            var mini_id = $(obj).val();
            $("#kala_miniGroup_span").html("<img src='../img/status_fb.gif'>");
            $.get("report_grooh.php?miniGroup_id="+mini_id,function(result){
                    var out = '<select id="kala_miniGroup_id" onclick="load_kala();" >';
                    out+='<option value="-1" >ریز گروه کالا</option>';
                    out+=$.trim(result);
                    out+='</select>';
                    $("#kala_miniGroup_span").html(out);
                    load_kala();
            });
        }
        function load_kala()
        {
            var kala_abarGroup_id= $("#kala_abarGroup_id").val();
            var kala_group_id= $("#kala_group_id").val();
            var kala_minGroup_id= $("#kala_miniGroup_id").val();
            $("#kala_span").html("<img src='../img/status_fb.gif'>");
            $.getJSON("report_grooh.php?a_id="+kala_abarGroup_id+"&g_id="+kala_group_id+"&m_id="+kala_minGroup_id,function(result){
                    var ou = '';
                    kala=result;
                    for(i in result)
                        ou+='<option value="'+result[i].id+'" >'+result[i].name+'</option>';
                    var out = '<select id="kala_id"  >';
                    out+='<option value="-1" > کالا</option>';
                    out+=ou;
                    out+='</select>';
                    $("#kala_span").html(out);
            });
            
        }
        var quu=''
        function search_kala()
        {
            var werc='';
            var kala_id =parseInt($.trim($("#kala_id").val()),10);
            var search_kala =kala;
            var kala_abarGroup_id = parseInt($("#kala_abarGroup_id").val(),10);
            var kala_group_id = parseInt($("#kala_group_id").val(),10);
            var kala_miniGroup_id = parseInt($("#kala_miniGroup_id").val());
            if(kala_id!=-1)
            {
                search_kala=[kala_id];
                werc= search_kala.join(',');
                werc = 'where kala_id in ('+werc+')';
            }
            else
                 werc= 'Fake,'+kala_abarGroup_id+'|'+kala_group_id+'|'+kala_miniGroup_id;
            quu = werc;
          //  werc= "where `persenel_id`='"+persenel_id+"' and `porsesh_id` in ("+result+")";
            whereClause[grid_gname] = encodeURIComponent(werc);
            grid[grid_gname].init(gArgs[grid_gname]);
        }
	function jamkol()
	{
		var p = {"jamkol":1,'quu':quu};
		$.get("report_grooh.php",p,function(result){
			$("#jam").html(result);
		});
	}
</script>
<div id="content">
        <div>
            <select id="kala_abarGroup_id" onchange="loadgroup(this);" >
                <?php
                    echo columnListToCombo(columnListLoader(kala_abarGroup),'دسته کالا');
                ?>
            </select>
            <span id="kala_group_span" >
                <select id="kala_group_id" >
                    <option value='-1' >گروه کالا</option>
                </select>
            </span>
            <span id="kala_miniGroup_span" >
                <select id="kala_miniGroup_id" >
                    <option value='-1' >ریز گروه کالا</option>
                </select>
            </span>
            <span id="kala_span" >
                <select id="kala_id" >
                    
                </select>
            </span>
            <button onclick="search_kala();" >
                    جستجو
            </button>
        </div>
	<div id="main_div_report_grooh">
	</div>
	<div id="jam">
	</div>
</div>
