<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id=(int)($_SESSION[$conf->app.'_user_id']);
	$permission=array();
	$cl2=new mysql_class;
	/*$canWrite='never';
	if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("takhfif.php",$permission))
			$canWrite='limited';
		
	}*/
	function add($gname,$table,$fields,$column)
	{
		$out = FALSE;
		$conf = new conf;
		$mysql = new mysql_class;
		$user_id = (int)$_SESSION[$conf->app.'_user_id'];
		$tarikh = date("Y-m-d H:i:s");
		$en = 1;
		$name = $fields['name'];
		$darsad = (int)$fields['darsad'];
		$ln = $mysql->ex_sqlx("insert into `$table` (`name`,`darsad`,`en`,`user_id`,`regdate`) values ('$name','$darsad',$en,$user_id,'$tarikh') ",FALSE);
		$id = $mysql->insert_id($ln);
		$mysql->close($ln);
		if($id>0)
			$out = TRUE;
		return($out);
		
	}
	function editUser($table,$id,$field,$val,$fn,$gname)
	{
		$id = (int)$id;
		if($field == 'pass')
			$val = md5($val);
		return($continue);
	}
	function del($table,$id,$gname)
	{
		$my = new mysql_class;
		$my->ex_sqlx("update `$table` set `en`=0 where `id`=$id");
		return(TRUE);
	}
	function loadUser($inp)
	{
		$out = new user_class($inp);
		return(isset($out->fname)?$out->fname.' '.$out->lname :'نامعلوم');
	}
	function loadDate($inp)
	{
		return(jdate("H:i -- d / m / Y",strtotime($inp)));
	}
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$mysql = new mysql_class;
	$msg ='';
	$isAdmin = $se->detailAuth('all');
        $gname = 'grid1';
        $input =array($gname=>array('table'=>'takhfif','div'=>'main_div_takhfif'));
        $xgrid = new xgrid($input);
	//$xgrid->alert = TRUE;
	$xgrid->whereClause[$gname] =' `en`=1 ';
        $xgrid->column[$gname][0]['name'] = '';
        $xgrid->column[$gname][1]['name'] = 'نام';
	$xgrid->column[$gname][1]['search'] = 'text';
	$xgrid->column[$gname][2]['name'] = 'درصد';
	$xgrid->column[$gname][3]['name'] = '';
	$xgrid->column[$gname][4]['name'] = 'ثبت کننده';
	$xgrid->column[$gname][4]['cfunction'] = array('loadUser');
	$xgrid->column[$gname][4]['access'] = 'q';
	$xgrid->column[$gname][5]['name'] = 'زمان ثبت';
	$xgrid->column[$gname][5]['access'] = 'q';
	$xgrid->column[$gname][5]['cfunction'] = array('loadDate');
	$xgrid->addFunction[$gname]='add';
	       		
	//if($canWrite=='all' || $canWrite=='limited')
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
       	{
		$xgrid->canAdd[$gname] = TRUE;
       		$xgrid->canDelete[$gname]=TRUE;
		$xgrid->canEdit[$gname]= TRUE;
	}
	$xgrid->deleteFunction[$gname] = 'del';
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);	
?>
<script>
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
        });
</script>
<div id="main_div_takhfif">
</div>
