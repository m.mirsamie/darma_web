<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	function loadUsers()
	{
		$conf = new conf;
		$out = array(-1=>'');
		$my = new mysql_class;
		$my->ex_sql("select id,fname,lname,user from user where en=1 and not (user like 'mehrdad%' ) and company_id = ".$conf->company_id." order by lname,fname",$q);
		foreach($q as $r)
			$out[(int)$r['id']] = $r['fname'].' '.$r['lname'].'['.$r['user'].']';
		return($out);
	}
	$permission=array();
	$cl2=new mysql_class;
	
	$mysql = new mysql_class;
	$msg ='';
	$isAdmin = $se->detailAuth('all');
	$gname = 'grid1';
	$input =array($gname=>array('table'=>'company_user','div'=>'main_div_company_user'));
	$xgrid = new xgrid($input);
//$xgrid->alert = TRUE;
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = 'مشتری حقوقی';
	$moshtari = columnListLoader('company_group');
	$xgrid->column[$gname][1]['clist'] = $moshtari;
	$xgrid->column[$gname][1]['search'] = 'list';
	$xgrid->column[$gname][1]['searchDetails'] = $moshtari;
	$xgrid->column[$gname][2]['name'] = 'کاربر';
	//$loadUser = columnListLoader('user| not (user like \'mehrdad%\' ) and company_id = '.$conf->company_id,array('id','lname','fname'));
	$xgrid->column[$gname][2]['clist'] = loadUsers();
	$xgrid->column[$gname][2]['search'] = 'list';
	$xgrid->column[$gname][2]['searchDetails'] = loadUsers();
	$xgrid->column[$gname][3]['name'] = 'مدیر است';
	$stat = array('-1'=>'','0'=>'خیر','1'=>'بله');
	$xgrid->column[$gname][3]['clist'] = $stat;
	$xgrid->column[$gname][3]['search'] = 'list';
	$xgrid->column[$gname][3]['searchDetails'] = $stat;
	$xgrid->canAdd[$gname] = TRUE;
	$xgrid->canDelete[$gname] = TRUE;
	$xgrid->canEdit[$gname] = TRUE;
	//if($canWrite=='all' || $canWrite=='limited')
	$out =$xgrid->getOut($_REQUEST);
	if($xgrid->done)
		die($out);	
?>
<script>
        var gname = '<?php echo $gname; ?>';
	$(document).ready(function(){
            var args=<?php echo $xgrid->arg; ?>;
            args[gname]['afterLoad']=function(a){
                $('select').select2({
                    dir: "rtl"
                });
            };
            intialGrid(args);
        });
</script>
<div id="main_div_company_user">
</div>
