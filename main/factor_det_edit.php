<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$factor_id = isset($_REQUEST['factor_id'])? (int)$_REQUEST['factor_id']:-1;
	$factor = new factor_class($factor_id);
	$info = '';
	if(isset($factor->user_id))
	{
		$user = new user_class($factor->user_id);
		$info = isset($user->fname)?$user->fname.' '.$user->lname:'---';
	}
	function loadUser($inp)
	{
		$user = new user_class($inp);
		return(isset($user->id)? $user->fname.' '.$user->lname:'حذف شده');
	}
	function edit1($table,$id,$field,$val,$fn,$gname)
	{
		$out = TRUE;
		$id = (int)$id;
		$f_det = new factor_det_class($id);
		$mysql = new mysql_class;
		if($field == 'tedad' || $field == 'toz' )
		{
			$mysql->ex_sqlx("update `$table` set `$field` = '$val' where `id` = $id");
			if($field == 'tedad')
			{
				$fac = new factor_det_class($id);
				if((int)$val>0)
				{
					$factor = new factor_class($fac->factor_id);
					$fac->refreshMablagh($val,$factor->user_id);
				}
				else
					$out = "FALSE|  جهت افزایش تعداد & سفارش جدید ثبت کنید";
			}
		}
		else
			$out = "FALSE| تغییر امکان پذیر نیست";
		return($out);
	}
	$mysql = new mysql_class;
	$gname = 'grid1';
	$input =array($gname=>array('table'=>'factor_det','div'=>'main_div_factor_det_edit'));
	$xgrid = new xgrid($input);
	for($i=0;$i<count($xgrid->column[$gname]);$i++)
		$xgrid->column[$gname][$i]['access']='a';
	$xgrid->eRequest[$gname] = array('factor_id'=>$factor_id);
	$xgrid->whereClause[$gname] = ' `factor_id`='.$factor_id;
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = '';
	$xgrid->column[$gname][2]['name'] = 'کالا';
	$xgrid->column[$gname][2]['clist'] = columnListLoader('kala');
	$xgrid->column[$gname][2]['access'] = null;
	$xgrid->column[$gname][3]['name'] = 'تعداد';
	$xgrid->column[$gname][3]['access'] = null;
	$xgrid->column[$gname][4]['name'] = 'قیمت';
	$xgrid->column[$gname][5]['access'] = null;
	$xgrid->column[$gname][5]['name'] = 'توضیحات';
	$xgrid->column[$gname][6]['name'] = 'تخفیف مدیر';
	$xgrid->column[$gname][7]['name'] = 'تخفیف بسته';
	$xgrid->column[$gname][8]['name'] = 'تخفیف تعداد';
	$xgrid->column[$gname][9]['name'] = 'سقف تخفیف';
	$xgrid->column[$gname][10]['name'] = 'تامین کننده';
	$xgrid->canEdit[$gname]= TRUE;
	$xgrid->editFunction[$gname] = 'edit1';
        //$xgrid->canAdd[$gname] = TRUE;
       // $xgrid->canDelete[$gname] = $canDo;
	$out =$xgrid->getOut($_REQUEST);
	if($xgrid->done)
			die($out);	
?>
<script>
	$(document).ready(function(){
		var ggname = '<?php echo $gname; ?>';
		var factor_id = <?php echo isset($factor_id)? $factor_id:-1; ?>;
		var args=<?php echo $xgrid->arg; ?>;
		intialGrid(args);
		gArgs[ggname]['afterEdit'] =function(e){
			 loadFactorDet(factor_id);
			return(e);
		};
        });
</script>
<div id="content" >
	<div align="right" style="border:solid 1px #333333;" class="round" >
		<h3>
		<?php 
			if(isset($factor->id) && $info!='')
			{			
				echo 'شماره فاکتور: '.$factor_id.' خریدار: '.$info.' تاریخ: '.jdate(" H:i d / m / Y",strtotime($factor->tarikh));
			}
		?>
		</h3>
	</div>
	<div id="main_div_factor_det_edit">
	</div>
	<div align="right" style="border:solid 1px #333333;margin-top:5px;" class="round" >
		<?php 
			$jam = $factor->jam();
			echo 'جمع مبلغ فاکتور بدون هزینه ارسال:'.$jam['kol'].'<br>مبلغ ارسال: '.$jam['ersal'].'<br> جمع کل:'.(string)($jam['kol']+$jam['ersal']);
		?>
	</div>
</div>
