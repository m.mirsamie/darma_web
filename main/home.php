<?php
	include_once("../kernel.php");
	include_once("../simplejson.php");
	//if(!isset($_REQUEST['kala_group_id']) || !isset($_REQUEST['q']))
		//die($conf->access_deny);
	$isSerach = FALSE;
	if(isset($_REQUEST['q']))
	{
		$isSerach = TRUE;
		$kala = toJSON(kala_class::search(trim($_REQUEST['q'])));
	}
	else if(isset($_REQUEST['kala_group_id']))
	{
		$kala_group_id = (int)$_REQUEST['kala_group_id'];
		$kala =toJSON(kala_class::loadByGroup($kala_group_id));
	}
?>
<style>
	#content button {font-family:tahoma;font-size:12px;}
	.line{text-decoration:line-through}
</style>
<script type="text/javascript" >
	var kala = <?php echo $kala; ?>;
	$(document).ready(function(){
		initial();
	});
	function drawKala(obj)
	{
		//var lin = "openDialog(\"kalaProfile.php?kala="+encodeURIComponent(JSON.stringify(obj))+"\",{width:600,height:400,title:\""+obj.name+"\"});";
		var lin = "openDialog(\"kalaProfile.php?kala_id="+obj.id+"\",{width:600,height:400,title:\""+obj.name+"\"});";
		var out="<div class='round blue' style='width:"+boxWidth+"px;height:180px;'>";
		out+="<table width='100%' height='100%' ><tr><td align='center' ><img src='"+(obj.pic=='' ? '../img/default.png':obj.pic)+"' width='70' height='70' ></td>";
		out+="<td><div class='div_"+obj.id+"' style='display:none' >تعداد:<input type='text' style='width:30px;'  id='tedad_"+obj.id+"' class='sefaresh_tedad' onkeypress='return numbericOnKeypress(event);' value='1' ></div></td></tr>";
		out+="<tr><td colspan='2' ><span class='pointer' onclick='"+lin+"' >"+obj.name+" <span id='khoon_"+obj.id+"' ></span></span></td></tr>";
		var st = parseInt(obj.mojoodi,10)>0? "<button class='kharid_"+obj.id+"' onclick='kharid("+obj.id+");' >خرید</button>": "<span style='color:#999999;' >عدم موجودی</span>";
		out+="<tr><td align='right' >"+st;
		out+="<div style='display:none;' class='div_"+obj.id+"' ><button onclick='sabad1("+obj.id+");' >سفارش</button><button onclick='kharid("+obj.id+");' >انـصــراف</button></div>";
		var takhfif_val =parseInt(obj.ghimat*(1-(parseFloat(obj.takhfif)/100)),10);
		var takhf = (trim(obj.takhfif)!="0" ?"<div style='font-size:10px;color:#093121;' >بیش از "+obj.tedad_baste+" عدد قیمت به"+takhfif_val+" کاهش می یابد </div>" : '');
		var ghimat_user = obj.ghimat_user!='0' ? "<div class='line' >"+obj.ghimat_user+"</div>":'';
		out+="</td><td>"+ghimat_user+"<div>"+obj.ghimat+"تومان</div>"+takhf+"</td></tr>";
		out+="</table></div>";
		return(out);
	}
	function kharid(id)
	{
		$(".div_"+id).toggle('slow');
		$(".kharid_"+id).toggle('slow');
	}
	function initial()
	{
		if(kala.length>0)
			$("#content").html(drawTable());
		else
			$("#content").html('کالا موجود نیست');
		$(".sefaresh_tedad").spinner({
			min:1
		});
	}
	function sabad1(id)
	{
		var tedad = $("#tedad_"+id).val();
		if(id>0 && parseInt(tedad)>0)
		{
			$("#khoon_"+id).html('<img src="../img/status_fb.gif" width="20" >');
			$.get("index.php?kala_id="+id+"&tedad="+tedad+"&",function(result){
				$("#khoon_"+id).html('');
				if(result='true')
				{
					//$("#tedad_"+id).val('');
					kharid(id);
					refreshSabad();
				}
				else
					alert('خطادر افزودن به سبد خرید');
			});
		}
		else
			alert('تعداد را وارد کنید');
	}
	function trim(str)
	{
		var out=str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		return out;
        }
</script>
<div id="content" style="font-family:tahoma;" >

</div>
<div>
	<?php
		if($isSerach)
			echo '<button style="font-family:tahoma;" onclick="removeTab();" >بستن</button';
	?>
</div>
