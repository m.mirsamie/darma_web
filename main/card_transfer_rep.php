<?php
    include_once('../kernel.php');
    $SESSION = new session_class;
    register_shutdown_function('session_write_close');
    session_start();
    include_once('../simplejson.php');
    if(!isset($_SESSION[$conf->app.'_user_id']))
    {
        die('جهت استفاده از این امکان می بایست ابتدا با کاربری خود از بالای سایت وارد شوید');
    }
    else
    {
        $user = new user_class((int)$_SESSION[$conf->app.'_user_id']);
        if(!isset($user->company_group_id) || $user->company_group_id <= 0)
        {
            die('جهت استفاده از این امکان می بایست کاربر برای یک فروشگاه باشد');
        }
    }
    function loadCard($tr_id)
    {
        $out = '----';
        $tr = new transactions_class((int)$tr_id);
        if(isset($tr->id))
        {
            $c = new cards_class($tr->cards_id);
            $card = (isset($c->shomare))?'['.$c->shomare.']':'';
            $my = new mysql_class;
            $my->ex_sql("select fname,lname from user_etebar left join user on (user_id=user.id) where transaction_id = $tr_id", $q);
            if(isset($q[0]))
            {
                $out = $q[0]['fname'].' '.$q[0]['lname'].$card;
            }
        }
        return($out);
    }
    function loadUser($tr_id)
    {
        $out = '----';
        $tr = new transactions_class((int)$tr_id);
        if(isset($tr->id))
        {
            $my = new mysql_class;
            $my->ex_sql("select fname,lname from user_etebar left join user on (user_sabt_id=user.id) where transaction_id = $tr_id", $q);
            if(isset($q[0]))
            {
                $out = $q[0]['fname'].' '.$q[0]['lname'];
            }
        }
        return($out);
    }
    function loadTarikh($inp)
    {
        return(($inp!='' && $inp!='0000-00-00 00:00:00')?jdate("Y/m/d",strtotime($inp)):'----');
    }
    function loadGhimat($ghimat)
    {
        return(monize($ghimat));
    }
    $sherkat = '----';
    $cpg = new company_group_class($user->company_group_id);
    if(isset($cpg->name))
        $sherkat = $cpg->name;
    $jam_kol = 0;
    $my = new mysql_class;
    $user_ids = array();
    $my->ex_sql("select user_id from company_user where company_group_id = ".$user->company_group_id, $q);
    foreach($q as $r)
        $user_ids[] = (int)$r['user_id'];
    $q = null;
    $ter_ids = array();
    if(count($user_ids)>0)
    {
        $my->ex_sql("select sum(etebar - min_etebar) as jam from profile where user_id in (".  implode(",", $user_ids).")", $q);
        if(isset($q[0]))
            $jam_kol = (int)$q[0]['jam'];
        $q = null;
        //echo "select transaction_id from user_etebar where user_sabt_id in (".  implode(",", $user_ids).") and transaction_id > 0<br/>\n";
        $my->ex_sql("select transaction_id from user_etebar where user_sabt_id in (".  implode(",", $user_ids).") and transaction_id > 0", $q);
        //var_dump($q);
        foreach($q as $r)
            if((int)$r['transaction_id']>0)
                $ter_ids[] = $r['transaction_id'];
    }
    //var_dump($_REQUEST);
    $jam = 0;
    if(isset($_REQUEST['werc']))
    {
        $my->ex_sql("select sum(mablagh) as jj from transactions ".$_REQUEST['werc']." and id in (".  implode(",", $ter_ids).")",$qq);
        if(isset($qq[0]))
            $jam = monize((int)$qq[0]['jj']);
    }
    //var_dump($ter_ids);
    //var_dump($user_ids);
    $gname = 'gridct';
    $input =array($gname=>array('table'=>'transactions','div'=>'ct_div'));
    $xgrid = new xgrid($input);
    if(count($ter_ids)>0)
        $xgrid->whereClause[$gname] = " id in (".  implode(",", $ter_ids).") order by tarikh desc";
    else
        $xgrid->whereClause[$gname] = " 1=0 ";
    $xgrid->column[$gname][0]['name'] = '';
    $xgrid->eRequest[$gname] = array("jam"=>$jam);
    $xgrid->column[$gname][1] = $xgrid->column[$gname][0];
    $xgrid->column[$gname][1]['name'] = 'کارت';
    $xgrid->column[$gname][1]['cfunction'] = array('loadCard');
    $xgrid->column[$gname][2]['name'] = 'تاریخ';
    $xgrid->column[$gname][2]['search'] = 'dateValue_minmax';
    $xgrid->column[$gname][2]['cfunction'] = array('loadTarikh');
    $xgrid->column[$gname][3] = $xgrid->column[$gname][0];
    $xgrid->column[$gname][3]['name'] = 'کاربر';
    $xgrid->column[$gname][3]['cfunction'] = array('loadUser');
    $xgrid->column[$gname][4]['name'] = 'مبلغ';
    $xgrid->column[$gname][4]['cfunction'] = array('loadGhimat');
    $xgrid->column[$gname][5]['name'] = '';
    $xgrid->column[$gname][6]['name'] = '';
    $xgrid->column[$gname][7]['name'] = '';
    $out =$xgrid->getOut($_REQUEST);
    if($xgrid->done)
        die($out);
    //echo $xgrid->whereClause[$gname];
?>
<div style="font-size:20px;"> جمع اعتبار شرکت 
    <?php echo $sherkat; ?>
     معادل 
    <?php echo monize($jam_kol); ?>
     ریال است
    <br/>
    جمع دریافتی در این بازه
    <span id="jam"></span>
    ریال
</div>
<div id="ct_div"></div>
<script>
    var tday = "<?php echo jdate("Y/m/d"); ?>";
    var tmar = "<?php echo jdate("Y/m/d",strtotime(date("Y-m-d").' + 1 day')); ?>";
    var start_time = 1;
    //$(document).ready(function(){
            var args=<?php echo $xgrid->arg; ?>;
            /*
            if(start_time === 1)
            {
            }
            else
            {
                args['gridct']['afterLoad']=null;
            }
            */
            args['gridct']['afterLoad']=function(a){
                start_time++; 
                if(start_time === 2)
                {
                    $("#tarikh-start").val(unFixNums(tday));
                    $("#tarikh-stop").val(unFixNums(tmar));
                    grid['gridct'].searchGrid('grid_search_gridct');
                }
                else
                    $("#jam").html(grid['gridct'].eRequest['jam']);
            };
            intialGrid(args);
    //});
</script>