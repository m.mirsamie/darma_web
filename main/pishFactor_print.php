<?php
	include_once("../kernel.php");
	include_once('../simplejson.php');
	$SESSION = new session_class;
	register_shutdown_function('session_write_close');
	session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
        die($conf->access_deny);
	$se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
	if(!$se->can_view)
			die($conf->access_deny);
	$factor_id = isset($_REQUEST['factor_id'])? (int)$_REQUEST['factor_id']:-1;
	$tmp = (isset($_REQUEST['typ']) && (int)$_REQUEST['typ']=='real')?'':'پیش';
	$user_id = isset($_REQUEST['user_id'])? (int)$_REQUEST['user_id']:(int)$_SESSION[$conf->app.'_user_id'];
	$use = new user_class($user_id);
	if(isset($_REQUEST['sid']))
	{
		$sid = (int)$_REQUEST['sid'];
		$factor_id = (int)$_REQUEST['factor_id'];
		$factor = new factor_class($factor_id);
		$svalue = (int)$_REQUEST['svalue'];
		$out = $factor->updateFactorDet($sid,$svalue);
		die(toJSON($out));
	}
	if(isset($_REQUEST['kala_id']))
	{
		$kala_id = (int)$_REQUEST['kala_id'];
		$factor_id = (int)$_REQUEST['factor_id'];
		$factor = new factor_class($factor_id);
		$out = $factor->removeFactorDet($kala_id);
		$out = array('kala_id'=>$kala_id,'status'=>$out);
		die(toJSON($out));
	}
	$ou = '<h1>شما به این فاکتور دسترسی ندارید</h1>';
	$f = new factor_class($factor_id);
	if(isset($f->id) && $f->user_id == $user_id)
	{
		$ou='
		<style>
		.pishfactorTable{
		width:100%;
		}
		.pishfactorTable td{
			border:solid 1px #333333;
			padding:3px;
		}
		.pishfactorTable th{
			border:solid 1px #333333;
			padding:3px;
		}
		</style>
	';
		$ou .="<h3 align='center'>
			".$tmp." فاکتور شماره: ".$factor_id.' '.$use->fname.' '.$use->lname.' '.enToPerNums(audit_class::idToCode($user_id))."</h3>";
		$ou .= factor_class::loadPishFactor($factor_id,$user_id,'pishFactor.php',TRUE,FALSE);
		//echo $ou;
		include("../mpdf/mpdf.php");
		$mpdf=new mPDF('ar'); 
		$mpdf->SetDirectionality('rtl');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($ou);
		$mpdf->Output();
	}
	else
		echo $ou;
	exit;
?>
