<?php
	include_once("../kernel.php");
	include_once("../simplejson.php");
	if(!isset($_REQUEST['kala']))
		die($conf->access_deny);
	//$kala_id = (int)$_REQUEST['kala_id'];
	$rq =str_replace("\\\"",'"',$_REQUEST['kala']);
	$kala =fromJSON($rq);//toJSON(kala_class($kala_id));
	$kala_gallery = toJSON(kala_gallery_class::loadByKala($kala->id));
?>
<html lang="fa">
	<head>
		<meta charset="utf-8">
		<title>
			<?php echo $kala->name; ?>
		</title>
		<link rel="stylesheet" href="../css/style.css">
		<style>
			#content button {font-family:tahoma;font-size:12px;}
			td,th{text-align:center;border:solid 1px #333333;padding:5px;}
		</style>
		<script src="../js/jquery.min.js"></script>
		<script src="../js/jquery.lightbox-0.5.min.js"></script>
		<script src="../js/inc.js" ></script>
		<link rel="stylesheet" href="../css/jquery.lightbox-0.5.css">
		<script type="text/javascript" >
			var kala = <?php echo $rq; ?>;
			var kala_gallery = <?php echo $kala_gallery; ?>;
			$(document).ready(function(){
				console.log(kala_gallery);
				$("#content").html(draw_kala(kala));
				$("#gallery").html(drawGall(kala_gallery));
				$("a.lbox").lightBox({
					imageBtnClose:'../img/lightbox-btn-close.gif',
					imageLoading:'../img/lightbox-ico-loading.gif',
					imageBtnPrev:'../img/lightbox-btn-prev.gif',
					imageBtnNext:'../img/lightbox-btn-next.gif'
				});
				
			});
			function draw_kala(obj)
			{
				var out ="<table class='kala' width='70%' style='margin-top:8px;' ><tr><th>نام</th><th>قیمت</td><th>تعداد هر بسته</th></tr><tr>";
				out+="<td>"+obj.name+"</td>";
				out+="<td>"+obj.ghimat+"</td>";
				out+="<td>"+obj.tedad_baste+"</td>";
				out+="</tr></table>";
				return(out);
			}
			
		</script>
	</head>
	<body>
	<div style="border-bottom:dashed 1px #333333;padding:5px;" ><img src="../img/darma.png"></div>
	<div id="content" style="font-family:tahoma;" >
		
	</div>
	<div id="gallery" ></div>
	</body>
</html>
