<?php
	include_once("../kernel.php");
	include_once("../simplejson.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
	$isAdmin = $se->detailAuth('all');
        if(!$se->can_view)
                die($conf->access_deny);
	function loadList($inp,$table)
	{
		$out = '<select id="'.$table.'_id" class="regdate" style="font-family:tahoma;" ><option value="-1" ></option>';
		$my = new mysql_class;
		$my->ex_sql("select `id`,`name` from `$table` order by `name` ",$q);
		foreach($q as $r)
			$out.='<option '.($inp==(int)$r['id']? 'selected="selected"':'').' value="'.$r['id'].'" >'.$r['name'].'</option>';
		$out .='</select>';
		return($out);
	}
	function loadJender($inp)
	{
		$out ='<select id="jender" class="regdate"  style="font-family:tahoma;" >';
		$out.='<option '.($inp==1? 'selected="selected"': '').' value="1" >مرد</option>';
		$out.='<option '.($inp==2? 'selected="selected"': '').' value="2" >زن</option>';
		$out .='</selcted>';
		return($out);
	}
	function loadPreUser($inp)
	{
		$out = '<select id="pre_user_id" class="regdate" style="font-family:tahoma;" ><option value="0" ></option>';
		$out .='<option value="-1" '.($inp==-1?'selected="selcted"':'').' >جستجو</option>';
		$out .='<option value="-2" '.($inp==-2?'selected="selcted"':'').' >روزنامه</option>';
		$out .='<option value="-3" '.($inp==-3?'selected="selcted"':'').' >شرکت دارما</option>';
		$out .='<option value="-4" '.($inp==-4?'selected="selcted"':'').' >دوستان</option>';
		$out .='<option value="-5" '.($inp==-5?'selected="selcted"':'').' >سایر</option>';
		$my = new mysql_class;
		$my->ex_sql("select `id`,`fname`,`lname` from `user` where `user`<>'mehrdad' order by `lname`,`fname` ",$q);
		foreach($q as $r)
			$out.='<option '.($inp==(int)$r['id']? 'selected="selected"':'').' value="'.$r['id'].'" >'.$r['lname'].' '.$r['fname'].'</option>';
		$out .='</select>';
		return($out);
	}
	$user_id = isset($_REQUEST['user_id']) ?(int)$_REQUEST['user_id']:(int)$_SESSION[$conf->app.'_user_id'];
	$user = new user_class($user_id);
	$prof = new profile_class($user_id);
	$source_user_id = isset($_REQUEST['source_user_id']) ?(int)$_REQUEST['source_user_id']:(int)$_SESSION[$conf->app.'_user_id'];
	$bool = TRUE;
	if(!isset($prof->id))
		$prof->id = profile_class::create($user_id);
	if(isset($_REQUEST['pcode']) || (isset($_REQUEST['cardnumber']) && !isset($_REQUEST['mablagh'])))
	{
		if(isset($_REQUEST['pcode']))
			$u_id=audit_class::codeToId($_REQUEST['pcode']);
		else
		{
			$card_tmp = new cards_class;
			$card_tmp->loadByShomare($_REQUEST['cardnumber']);
			if(!isset($card_tmp->user_id))
				die('nouser');
			$u_id = $card_tmp->user_id;
		}
		if((int)$u_id!=$source_user_id)
		{
			$us= new user_class($u_id);
			die(isset($us->id)?$us->fname.' '.$us->lname:'nouser');
		}
		die('same');
	}
	if(isset($_REQUEST['mablagh']))
	{
		$card_or_id = (int)$_REQUEST['id']>0;
		if($card_or_id)
			$u_id = audit_class::codeToId($_REQUEST['id']);
		else
		{
			$card_tmp = new cards_class;
			$card_tmp->loadByShomare($_REQUEST['cardnumber']);
			$u_id = $card_tmp->user_id;
		}
		$mablagh =abs((int)$_REQUEST['mablagh']);
		if($se->detailAuth('all') && isset($_REQUEST['source_user_id']))
			$user_sabt_id = (int)$_REQUEST['source_user_id'];
		else
			$user_sabt_id = (int)$_SESSION[$conf->app.'_user_id'];
		$own_profile = new profile_class($user_sabt_id);
		$des_profile = new profile_class($u_id);
		if(isset($own_profile->id) && isset($des_profile->id))
		{
			if($mablagh>($own_profile->etebar-$own_profile->min_etebar))
				die('etebar_err');
			else
			{
				profile_class::move($own_profile,$des_profile,$mablagh,$user_sabt_id);
				$user_id = (int)$_SESSION[$conf->app.'_user_id'];
				$tarikh = urlencode(jdate("Y/m/d",strtotime(date("Y-m-d"))));
				$typ =urlencode(isset($_REQUEST['cardnumber'])?'انتقال به کارت':'انتقال به شخص');
				$tt="/resid.php?from_user_id=$user_sabt_id&card_id=".($card_or_id?'':$_REQUEST['cardnumber'])."&typ=$typ&mablagh=$mablagh&tarikh=$tarikh&user_id=$user_id&to_user_id=$u_id&";
				$tmp1 = explode("php",$_SERVER['REQUEST_URI']);
				$tmp=explode("/",$tmp1[0]);
				$msg ="http://".$_SERVER['HTTP_HOST'];
				for($i=0;$i<count($tmp)-1;$i++)
					$msg.='/'.$tmp[$i];
				$msg = file_get_contents($msg.$tt);
			//	$die = array('tarikh'=>jdate("Y/m/d",strtotime(date("Y-m-d"))),'user_id'=>$user_id,'from_user_id'=>$user_sabt_id,'to_user_id'=>$u_id,'mablagh'=>$mablagh,'card'=>($card_or_id?'':$_REQUEST['cardnumber']));
				die($msg);
			}
		}
		else
			die('user_err');
	}
	if($prof->id>0)
	{
		$html = new htmlGenerator('profile',$prof->id);
		$html->notHtml=array('pre_user_id');
	}
	if(isset($_REQUEST['k_id']))
	{
		$id = (int)$_REQUEST['k_id'];	
		$qu = '';
		if($id>0)
		{
			$qu ='';
			$que ='';
			foreach($_REQUEST as $key=>$val)
				if($key!='k_id' && $key!='etebar' && $key!='pass')
					if($key!='fname' && $key!='lname' )
						$qu.=($qu==''?'':',')." `$key`='".($key=='tarikh_tavalod'?hamed_pdateBack2($val):$val)."'";
					else
						$que.=($que==''?'':',')." `$key`='$val'";
			if(trim($_REQUEST['mob'])!='')
				$que.=($que==''?'':',')." `user`='".$_REQUEST['mob']."'";
			$qu ="update `profile` set $qu where `user_id`=$id";
			$que ="update `user` set $que where `id`=$id";
			$m = new mysql_class;
			$m->ex_sqlx($qu);
			$m->ex_sqlx($que);
		}	
		die('done');
	}
	if(isset($_REQUEST['change_pass']) && (int)$_REQUEST['change_pass']==1)
	{
		$pass = $_REQUEST['pass'];
		$npass = $_REQUEST['npass'];
		$u_id = (int)$_REQUEST['u_id'];
		$my = new mysql_class;
		$my->ex_sql("select `id` from `user` where `id`=$u_id and `pass`='$pass'",$q);
		if(isset($q[0]) && count($q[0])>0)
			$out = $my->ex_sqlx("update `user` set `pass`='$npass' where `id`=$u_id");
		else
			$out = 'not_match';
		die($out);	
	}
	$year_t = perToEnNums(jdate("Y",strtotime($prof->tarikh_tavalod)));
	$month_t = perToEnNums(jdate("m",strtotime($prof->tarikh_tavalod)));
	$day_t = perToEnNums(jdate("d",strtotime($prof->tarikh_tavalod)));
        $year_tmp = '';
        $month_tmp = '';
        $day_tmp = '';
	$today_year = perToEnNums(jdate("Y"));
        for($i = 1300;$i <= $today_year;$i++)
                $year_tmp .= '<option value="'.$i.'" '.(($i==$year_t)?'selected':'').' >'.enToPerNums($i).'</option>';
        for($i = 1;$i <= 12;$i++)
                $month_tmp .= '<option value="'.$i.'" '.(($i==$month_t)?'selected':'').' >'.enToPerNums($i).'</option>';
        for($i = 1;$i <= 31;$i++)
                $day_tmp .= '<option value="'.$i.'" '.(($i==$day_t)?'selected':'').' >'.enToPerNums($i).'</option>';
?>
<script type="text/javascript" >
	var pcode=0;
	var mablagh=0;
	var u_id = <?php echo $user_id; ?>;
	$(document).ready(function(){
		$.each($(".dateValue"),function(id,field){
	                Calendar.setup({
        		        inputField     :    field.id,
                		button         :    field.id,
	                	ifFormat       :    "%Y/%m/%d",
	        	        dateType           :    'jalali',
        	        	weekNumbers    : false
	                });			
		});
		$('.dPass').dPassword({
			duration: 2000,
		});
	});
	function sabt_k(k_id)
	{
		var req='';
		var rmp='';
		var user_id = <?php echo $prof->id;?>;
		var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
		if(!email_regex.test($("#email").val()))
		{
			alert('آدرس ایمیل به درستی وارد نشده است');
			$("#email").val('');
		}
		else
		{
			$(".regdate").each(function(id,field) {
				rmp = $("#"+field.id).val();
				if($("#"+field.id).hasClass('dateValue'))
					rmp=$("#"+field.id).val();
				else if($("#"+field.id).hasClass('pass') && $("#"+field.id).val()!='')
					rmp=md5($("#"+field.id).val());
				if(($("#"+field.id).hasClass('pass') && $("#"+field.id).val()!='')|| !$("#"+field.id).hasClass('pass'))
	    				req+= field.id+"="+encodeURIComponent(rmp)+"&";
				
			});
			console.log(req);
			$("#khoon").html('<img src="../img/status_fb.gif" >');
			req="k_id="+k_id+"&"+req;
			$.get("profile.php?"+req,function(result){
				if(trim(result)=='done')
					$("#khoon").html('ثبت با موفقیت انجام شد');
			});
		}
	}
	function loadProfile(user_id)
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
        $("#body").load('profile.php?user_id='+user_id+'&');
	}
	function checkMobile(obj)
	{
		if(!isMobile($(obj).val()))
		{
			alert('تلفن همراه را درست وارد کنید');
			$(obj).val('');
		}
	}
	/*
	function enteghal()
	{
		pcode = $("#pcode").val();
		mablagh = $("#mablagh").val();
		if(mablagh=="")
		{
			alert('کد پروفایل را وارد کنید');
			return false;
		}
		else if(parseInt(mablagh,10)<=0)
		{
			alert('مبلغ صحیح وارد نشده است');
			return(false);
		}
		
		$("#msg_span").html('<img src="../img/status_fb.gif" >');
		$.get("profile.php?pcode="+pcode+"&",function(result){
			if(result=='same')
				$("#msg_span").html('انتقال اعتبار به خود امکان پذیر نمی باشد');
			else if(result=='nouser')
				$("#msg_span").html('چنین کاربری وجود ندارد');
			else if(result!='')
			{
				var txt = '<div class="notice" >ایا انتقال به '+result+' انجام شود؟</div>';
				txt+='<button onclick="setEteber();" >موافقم</button>';
				txt+='<button onclick="noEteber();" >خیر</button>';
				$("#msg_span").html(txt);
			}
		});	
	}
	function setEteber()
	{
		$("#khoon").html('<img src="../img/status_fb.gif" >');
		//console.log("profile.php?id="+pcode+"&mablagh="+mablagh+"&");
		$.get("profile.php?id="+pcode+"&mablagh="+mablagh+"&",function(result){
			$("#khoon").html('');
			//console.log(result);
			if(result=='done')
			{
				$("#msg_span").html('<div class="msg" >انتقال انجام شد</div>');
				setTimeout('refresh_p();',3000);
			}
			else if(result=='etebar_err')
				$("#msg_span").html('<div class="msg" >اعتبار شما جهت انتقال کافی نیست</div>');
			else if(result=='user_err')
				$("#msg_span").html('<div class="msg" >کاربر درست انتخاب نشده است</div>');
		});
	}
	*/
	function noEteber()
	{
		$("#msg_span").html('');
	}
	function md5(inp)
	{
		return(hex_md5(inp));
	}
	function add_etebar()
	{
		var tmp = $("#sharzh_val").val();
		var typ = $("#etebar_code").prop("checked")? 'etebar_code':'banki';
		if(typ=='banki')
		{
			startBank(tmp);
		}
		else
		{
			$("#msg_span").html('<img src="../img/status_fb.gif" >');
			$.get("profile.php?tmp="+tmp+"&typ="+typ+"&",function(result){
				//console.log("'"+result+"'");
				if(result=='true')
				{
					$("#msg_span").html('<div class="msg" >افزایش اعتبار با موفقیت انجام شد</div>');
					setTimeout('refresh_p();',3000);
				}
				else
					$("#msg_span").html('<div class="notice" >افزایش اعتبار با خطا مواجه گردید</div>');
			});
		}
	}
	function refresh_p()
	{
		if(typeof openDialog!='undefined')
			openDialog('profile.php?user_id='+u_id,'پروفایل کاربر');
		else
			loadprofile(u_id);
	}
	function cha()
	{
		
		if($("#etebar_code").prop("checked"))
			$("#sharzh_span").html('کد اعتبار را وارد کنید');
		else if($("#banki").prop("checked"))
			$("#sharzh_span").html('مبلغ');
		$("#sharzh_val").val('');
	}
	function changePass()
	{
		
		
		var pass = $("#pass").val();
		var npass = $("#npass").val();
		var npasst = $("#npasst").val();
		
		if($.trim(pass)=='')
			alert('رمز فعلی را وارد کنید');
		else if($.trim(npass)=='')
			alert('رمز جدید را وارد کنید');
		else if(npass!=npasst)
			alert('رمز های وارد شده یکسان نیست');
		else
		{
			pass = md5(pass);
			npass = md5(npass);
			$("#pass_khoon").html('<img src="../img/status_fb.gif" >');
			$.get("profile.php?change_pass=1&pass="+pass+"&npass="+npass+"&u_id="+u_id+"&",function(result){
				result = $.trim(result);
				var txt='خطای نامعلوم';
				if(result=='ok')
					txt='<div class="msg" >تغییر رمز با موفقیت انجام شد</div>';
				else if(result=='not_match')
					txt='<div class="notice" >رمز فعلی وارد شده درست نیست</div>';
				$("#pass_khoon").html(txt);
				$("#pass").val('');
				$("#npass").val('');
				$("#npasst").val('');			
			});
		}
	}
	function tarkh(dobj)
	{
		var t = $("#tarikh_tavalod");
		t.val($("#year_tmp").val()+'/'+$("#month_tmp").val()+'/'+$("#day_tmp").val());
	}
</script>
<div>
	<h3>
		نام کاربری:
		<?php echo $user->user;  ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	کد کاربری شما:
	<?php 
		echo audit_class::idToCode($user_id)." موجودی اعتبار قابل برداشت : ".monize($prof->etebar-$prof->min_etebar).'ریال';
	?>
	</h3>
</div>
<div id="content" >
	<div style="font-family:tahoma;font-size:12px;" >
		<table style="font-family:tahoma;font-size:12px;width:100%;" >
			<tr>
				<th class="round" colspan="6" style="color:#ffffff;background:#333333;padding:5px;">اطلاعات کاربری</th>
			</tr>
			<tr>
				<td align="left">نام:</td>
				<td align="right">
					<input type="text" class="regdate" value="<?php echo (isset($user->fname)?$user->fname:''); ?>" id="fname"  placeholder="نام"  >
				</td>
				<td align="left">نام خانوداگی:
				</td>
				<td align="right"><input type="text" class="regdate" value="<?php echo (isset($user->lname)?$user->lname:''); ?>" id="lname"  placeholder="نام خانوادگی"  >
				</td>
				<td align="left">نوع کاربر:
				</td>
				<td align="right">
					<?php echo loadList($prof->customer_type_id,'customer_type'); ?>
				</td>
			</tr>
			<tr>
				<td align="left">ایمیل:</td>
				<td align="right">
					<input type="text" class="regdate" id="email"  placeholder="ایمیل" value="<?php echo  (isset($prof->email)? $prof->email:''); ?>" >
				</td>
				<td align="left">
		تاریخ تولد:
				</td>
				<td align="right">
					<select onchange="tarkh(this);" id="day_tmp"><?php echo $day_tmp; ?></select>
					<select onchange="tarkh(this);" id="month_tmp"><?php echo $month_tmp; ?></select>
					<select onchange="tarkh(this);" id="year_tmp"><?php echo $year_tmp; ?></select>
					<input readonly="readonly" type="text" style="display:none;" class="regdate dateValue" value="<?php echo (isset($prof->tarikh_tavalod)?($prof->tarikh_tavalod=='0000-00-00 00:00:00'?'':jdate("Y/m/d",strtotime($prof->tarikh_tavalod))):''); ?>" id="tarikh_tavalod"  placeholder="تاریخ تولد"  >
				</td>
				<td align="left">تلفن همراه:
				</td>
				<td align="right"><input type="text" class="regdate" value="<?php echo (isset($prof->mob)? $prof->mob:''); ?>" id="mob" onblur="checkMobile(this);" placeholder="تلفن همراه"  onkeypress='return numbericOnKeypress(event);' ></td>
			</tr>
			<tr>
				<td align="left">شهر:
				</td>
				<td align="right">
					<?php //echo loadList((int)$prof->city_id,'city'); ?>
					<select id="city_id" name="city_id">
						<option value="1">مشهد</option>
					</select>
				</td>
				<td align="left">نشانی:
				</td>
				<td align="right" colspan="4" ><input type="text" class="regdate" value="<?php echo (isset($prof->addr)? $prof->addr:''); ?>" id="addr" placeholder="نشانی" style="width:90%;" >
				</td>
			</tr>
			<tr>
				<td align="left">کدپستی:
				</td>
				<td align="right"><input type="text" class="regdate" value="<?php echo (isset($prof->codePosti)? $prof->codePosti:''); ?>" id="codePosti" placeholder="کدپستی">
				</td>
				<td align="left">تلفن ثابت:
				</td>
				<td align="right"><input type="text" class="regdate" value="<?php echo (isset($prof->tel)? $prof->tel:''); ?>" id="tel" placeholder="تلفن ثابت" onkeypress='return numbericOnKeypress(event);' >
				</td>
				<?php if($isAdmin) { ?>
				<td align="left">منطقه:
				</td>
				<td align="right">
					<?php echo loadList($prof->mantaghe_id,'mantaghe'); ?>
				</td>
				<tr>
					<td align="left">دسته کاربر:
					</td>
					<td align="right">
						<?php echo loadList($prof->user_daste_id,'user_daste'); ?>
					</td>
					<td align="left">کمیسیون<br/>زیر دستان:
					</td>
					<td align="right">
						<input type="text" class="regdate" value="<?php echo (isset($prof->down_user_poorsant)? $prof->down_user_poorsant:''); ?>" id="down_user_poorsant" placeholder="کمیسیون" onkeypress="return numbericOnKeypress(event);" >
					</td>
					<td align="left">معرف:
					</td>
					<td align="right">
						<?php echo loadPreUser($prof->pre_user_id); ?>
					</td>
				</tr>
				<tr>
					<td align="left">جنسیت:
					</td>
					<td align="right">
						<?php echo loadJender($prof->jender); ?>
					</td>
				</tr>
				<?php } else { ?>
					<td align="left">جنسیت:
					</td>
					<td align="right">
						<?php echo loadJender($prof->jender); ?>
					</td>
				</tr>
				<?php } ?>
			</tr>
		</table>
	</div>
	<div id="khoon" style="color:red;" >
	</div>
	<div>
		<button onclick="sabt_k(<?php echo $user->id; ?>);" >بروز رسانی</button>
	</div>
	<table style="font-family:tahoma;font-size:12px;padding-top:20px;width:100%" >
		<tr>
				<th class="round" colspan="6" style="color:#ffffff;background:#333333;padding:5px;">تغییر رمز عبور</th>
			</tr>
		<tr>
			<td  align="left" >
				رمز فعلی:
			</td>
			<td>
				<input type="password" class="pass" value="" id="pass" name="pass" tabindex="0" accesskey placeholder="رمز فعلی"  >
			</td>
			<td align="left" >
				رمز جدید:
			</td>
			<td>
				<input type="password" class="pass"  id="npass" name="npass" tabindex="1" accesskey placeholder="رمز جدید"  >
			</td>
			<td align="left" >
				تکرار رمز جدید:
			</td>
			<td>
				<input type="password" class="pass" id="npasst" name="npasst" tabindex="2" accesskey  placeholder="تکرار رمز جدید"  >
			</td>
		</tr>
		<tr>
			<td colspan="6" >
				<span id="pass_khoon" width="25" ></span>
				<button onclick="changePass();" >تغییر رمز</button>
			</td>
		</tr>
	</table>
	<!--
	<table style="font-family:tahoma;font-size:12px;padding-top:20px;width:100%" >
		<tr>
				<th class="round" colspan="2" style="color:#ffffff;background:#333333;padding:5px;">انتقال اعتبار</th>
			</tr>
		<tr>
			<td>
				انتقال اعتبار به دیگر کاربران:
				<input type="text" id="pcode" placeholder="کد کاربرمورد نظر" >
				مبلغ:
				<input type="text" id="mablagh" placeholder="مبلغ" onkeypress='return numbericOnKeypress(event);' >
				ریال
			</td>
			<td align="left" >
				<button onclick="enteghal();" >انجام انتقال</button>
			</td>
		</tr>
		<tr>
			<th class="round" colspan="2" style="color:#ffffff;background:#333333;padding:5px;">افزایش اعتبار</th>
		</tr>
		<tr>
			<td>
				پرداخت آنلاین
				<input name="sharzh" id="banki" type="radio" onclick="cha()" checked="checked" >
				ورود کد اعتبار
				<input name="sharzh" id="etebar_code" onclick="cha()" type="radio" >
				<span id="sharzh_span" >مبلغ</span>
				<input type="text" id="sharzh_val" >
			</td>
			<td align="left" >
				<button onclick="add_etebar();" >افزایش</button>
			</td>
		</tr>
		<tr>
		<tr>
			<td colspan="2" >
				<span id="msg_span" ></span>
			<td>
		</tr>
	</table>
	-->
</div>
