<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
	$user_id=(int)($_SESSION[$conf->app.'_user_id']);
	$cl1=new user_class($user_id);
	$use_name=$cl1->user;
	//die("user is:".$use_name);
        if(!$se->can_view)
                die($conf->access_deny);
	$permission=array();
	$cl2=new mysql_class;
	//die("id is".$user_id);
	/*$canWrite='never';
	if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("log.php",$permission))
			$canWrite='limited';
		
	}*/
	function showPass($pass)
	{
		return('&nbsp;');
	}
	function changePass($pass)
	{
		return(md5($pass));
	}
	function loadProfile($inp)
	{
		$out = '<span class="notice pointer" onclick="loadProfile('.$inp.');" >پروفایل</span>';
		return($out);
	}
	function loadMali($id)
	{
		$main = isset($_REQUEST['main'])?'main=main&':'';
		$out = '<span class="msg pointer" onclick="loadCont(null,\'mali.php?user_id='.$id.'&'.$main.'\');" >گردش مالی</span>';
		return($out);
	}
	function addUser($gname,$table,$fields,$column)
	{
		global $user_id;
		$us_ch_id='do not exist';
		$mysql = new mysql_class;
		$flag=true;		
		$card_id = $fields['cards_id'];
		$ter_id = $fields['terminal_id'];
		$mysql->ex_sql("select user_id from cards where id='$card_id'",$q);
		if(count($q)>0)
			$us_ch_id=$q[0]['user_id'];
		$toz="  درج با شماره کارت : $card_id  و شماره کاربری : $us_ch_id   و شماره ترمینال : $ter_id  ";
		$extra="عملیات درج در جدول.";
		
		$mysql->ex_sql("select * from card_terminal where cards_id='$card_id' and terminal_id='$ter_id' ",$res);
		if(count($res)<=0)
		{
			$lo=log_class::add($user_id,$toz,$extra);
			if($lo=='true')
			{
				$ln = $mysql->ex_sqlx("insert into `card_terminal` (`cards_id`, `terminal_id`)
				 values ('$card_id','$ter_id')",FALSE);
				$user_id = $mysql->insert_id($ln);
				$mysql->close($ln);
				
			}
			else
				$flag=false;
		}
		else
			$flag='FALSE|  کارتی تکراری نمیتوانید برای ترمینال یکسان اعمال کنید.';
		return($flag);
	}
	function editUser($table,$id,$field,$val,$fn,$gname)
	{
		global $user_id;
		$flag=TRUE;
		$sq='';
		$id = (int)$id;
		//ard_id = $field['cards_id'];
		//$ter_id = $field['terminal_id'];
		$preval='-';
		if($field == 'pass')
			$val = md5($val);
		$mysql = new mysql_class;
		$mysql->ex_sql("select * from card_terminal where id='$id'",$qu1);
		//die("select * from card_terminal where id='$id'");
		if(count($qu1)>0)
		{
			$c_id=$qu1[0]['cards_id'];
			$t_id=$qu1[0]['terminal_id'];
			$preval=$qu1[0][$field];
			if($field=='cards_id')
			{
				$mysql->ex_sql("select * from card_terminal where id!='$id' and cards_id='$val' 
				and terminal_id='$t_id' ",$qu2);
				$sq="select * from card_terminal where id!='$id' and cards_id='$val' 
				and terminal_id='$t_id' ";
			}
			else
			{
				$mysql->ex_sql("select * from card_terminal where id!='$id' and terminal_id='$val' 
				and cards_id='$c_id' ",$qu2);
				$sq="select * from card_terminal where id!='$id' and terminal_id='$val' 
				and cards_id='$c_id' ";
			}
			//die($sq);
			if(count($qu2)>0)
				$flag="FALSE| ویرایش به مقادیر تکراری امکان پذیر نیست.";

			
		}
		if($flag===TRUE)
		{
			$toz="ویرایش فیلد $field از مقدار  $preval به مقدار $val.";
			$extra="عملیات ویرایش.";
		
			$lo=log_class::add($user_id,$toz,$extra);
			$mysql->ex_sqlx("update `$table` set `$field` = '$val' where `id` = $id");
		}
		return($flag);
	}
	function delUser($table,$id,$gname)
	{
		global $user_id;
		$user_prof = new profile_class($id);
		if(isset($user_prof->etebar) &&(int)$user_prof->etebar==0)
		{
			$card_id='-';
			$ter_id='-';
			$user_name='-';
			$my = new mysql_class;
			$my->ex_sql("select * from `$table` where `id`='$id' ",$res);
			if(count($res)>0)
			{
				$card_id=$res[0]['cards_id'];
				$ter_id=$res[0]['terminal_id'];
				$my->ex_sql("select user_id ,fname,lname from cards 
				left join user on(user_id=user.id) where cards.id='$card_id'",$res2);
				//die("select user_id ,fname,lname from cards 
				//left join user on(user_id=id) where id='$card_id'");
				if(count($res2)>0)
				{
					$user_name=$res2[0]['fname']." " .$res2[0]['lname'];
				}
			}
			$toz="حذف کارت با شماره ی کارت :$card_id و کاربر :$user_name و شماره ترمینال : $ter_id ";
			$extra="عملیات حذف .";
			$lo=log_class::add($user_id,$toz,$extra);
			if($lo=='true')
				$my->ex_sqlx("delete from `$table` where `id`=$id");
			$out = TRUE;
		}
		else
			$out='FALSE|این کاربر اعتبار مالی دارد';
		return($out);
	}
	function loadAcc($inp)
	{
		$div="<div class='msg pointer' onclick='loadAcc($inp);' >دسترسی </div>";
		return($div);
	}
	function loadCardsData()
	{
		$ms=new mysql_class;
		$out=array(0=>'');
		$ms->ex_sql("select fname,lname,shomare,cards.id cid from cards left join user on(user.id=user_id) ",$q);
		foreach($q as $res)
			$out[$res['cid']]=$res['fname']." " .$res['lname'] ." : " . $res['shomare'];
		return($out);
	}
        $gname = "gname_user";
	$isMain = isset($_REQUEST['main'])?TRUE:FALSE;
	$input =array($gname=>array('table'=>'card_terminal','div'=>'main_div_cards_terminal'));
        $xgrid = new xgrid($input);
	if($isMain)
		$xgrid->eRequest[$gname] = array('main'=>'main');
	//$xgrid->whereClause[$gname] = " ";
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = 'کارت';
	$xgrid->column[$gname][1]['search'] ='text';
	$xgrid->column[$gname][1]['clist'] = loadCardsData();//columnListLoader('cards',array('id','user_id','shomare'));
	$xgrid->column[$gname][2]['name'] = 'ترمینال';	
	$xgrid->column[$gname][2]['search'] ='text';
	$xgrid->column[$gname][2]['clist'] =columnListLoader('terminals',array('id','code'));
	//if($canWrite=='all' || $canWrite=='limited')
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
	{
		$xgrid->canAdd[$gname] = TRUE;
		$xgrid->canDelete[$gname] = TRUE;
		$xgrid->canEdit[$gname] = TRUE;
	}
	
       		
	$xgrid->addFunction[$gname] = 'addUser';
	$xgrid->editFunction[$gname] = 'editUser';
	$xgrid->deleteFunction[$gname] = 'delUser';
        
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);
?>
<script type="text/javascript" >
        $(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
		var ggname = '<?php echo $gname; ?>';
		gArgs[ggname]['afterEdit'] = function(tgname,r){
			if(r.split('|').length>1)
			{
				alert(r.split('|')[1]);
				grid['gname_user'].init(gArgs['gname_user']);
			}
			return(r);
		};
        });
	function loadProfile(user_id)
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load('profile.php?user_id='+user_id+'&');
	}
	function loadAcc(id)
	{
		$("#body").load('permission.php?user_id='+id);
	}
</script>
<div id="main_div_cards_terminal"></div>
