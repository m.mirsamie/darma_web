<?php
	include_once("../kernel.php");
	include_once("../simplejson.php");
	$city_woeids = array(
		"تهران"=>2251945,
		"مشهد"=>2254914,
		"شیراز"=>2255202,
		"اصفهان"=>2254572,
		"کرمان"=>2431862,
		"تبریز"=>2255239,
		"اهواز"=>2254294,
		"قم"=>2255062,
		"کرمانشاه"=>2254797,
		"رشت"=>2255086,
		"کرمان"=>2431862,
		"ارومیه"=>2242302,
		"زاهدان"=>2253501,
		"همدان"=>2254664,
		"اراک"=>2254333,
		"کرج"=>2254777,
		"یزد"=>2253355,
		"قزوین"=>20070200,
		"اردبیل"=>20070198,
		"بندر عباس"=>2218961,
		"زنجان"=>2255311,
		"خرم آباد"=>2254824,
		"سنندج"=>2255130,
		"گرگان"=>2217763,
		"ساری"=>2255151,
		"شهرکرد"=>2255184,
		"بندر بوشهر"=>2254463,
		"بجنورد"=>2220377,
		"بیرجند"=>2254447,
		"ایلام"=>2345775,
		"سمنان"=>2345784,
		"یاسوج"=>2255297
	);
	$weather_array = array(

		0=>	"توفان",//"tornado",
		1=>	"توفان های گرمسیری",//"tropical storm",
		2=>	"توفان شدید",//"hurricane",
		3=>	"رعد و برق شدید",//"severe thunderstorms",
		4=>	"رعد و برق",//"thunderstorms",
		5=>	"برفی و بارانی",//"mixed rain and snow", 
		6=>	"باران و تگرگ",//"mixed rain and sleet", 
		7=>	"برف و تگرگ",//"mixed snow and sleet",
		8=>	"نم نم باران",//"freezing drizzle",
		9=>	"نم نم باران",//"drizzle",
		10=>	"سرماریزه",//"freezing rain",
		11=>	"باران و رگبار",//"showers",
		12=>	"باران و رگبار",//"showers",
		13=>	"بارش برف",//"snow flurries",
		14=>	"رگبار و بارش برف",//"light snow showers",
		15=>	"بارش برف",//"blowing snow",
		16=>	"برف",//"snow",
		17=>	"تگرگ",//"hail",
		18=>	"برف و باران-بوران",//"sleet",
		19=>	"گرد و غبار",//"dust",
		20=>	"مه",//"foggy",
		21=>	"مه سنگین",//"haze",
		22=>	"مه غلیظ",//"smoky",
		23=>	"وزش باد",//"blustery",
		24=>	"بادی",//"windy",
		25=>	"سرد",//"cold",
		26=>	"ابری",//"cloudy",
		27=>	"آسمان پوشیده از ابر (شب)",//"mostly cloudy (night)",
		28=>	"آسمان پوشیده از ابر (شب)",//"mostly cloudy (day)",
		29=>	"کمی تا نیمه ابری(شب)",//"partly cloudy (night)",
		30=>	"کمی تا نمیه ابری(روز)",//"partly cloudy (day)",
		31=>	"صاف (شب)",//"clear (night)",
		32=>	"آفتابی",//"sunny",
		33=>	"صاف و بدون ابر (شب)",//"fair (night)",
		34=>	"صاف و بدون ابر (شب)",//"fair (day)",
		35=>	"باران و تگرگ",//"mixed rain and hail", 
		36=>	"گرم و آفتابی",//"hot", 
		37=>	"رعد و برق",//"isolated thunderstorms",
		38=>	"رعدو برق پراکنده",//"scattered thunderstorms",
		39=>	"رعدو برق پراکنده",//"scattered thunderstorms",
		40=>	"رگبار پراکنده",//"scattered showers",
		41=>	"بارش برف سنگین",//"heavy snow",
		42=>	"بارش برف و رگبار پراکنده",//"scattered snow showers",
		43=>	"برف سنگین",//"heavy snow",
		44=>	"کمی تا نیمه ابری",//"partly cloudy",
		45=>	"رگبار همراه با رعد و برق",//"thundershowers",
		46=>	"بارش برف",//"snow showers",
		47=>	"رگبار همراه با رعد و برق",//"isolated thundershowers",
		3200=>	"اطلاعات در دسترس نیست"//"not available",


	);
	$woeid = isset($_REQUEST["woeid"])?(int)$_REQUEST["woeid"]:2254914;
        $rss_url = "http://weather.yahooapis.com/forecastrss?w=$woeid&u=c";
	$result = file_get_contents($rss_url);
	$xml = simplexml_load_string($result);
	$xml->registerXPathNamespace('yweather', 'http://xml.weather.yahoo.com/ns/rss/1.0');
	$location = $xml->channel->xpath('yweather:location');
	foreach($xml->channel->item as $item){
		$current = (array)$item->xpath('yweather:condition');
		$forecast = $item->xpath('yweather:forecast');
		//var_dump($current);
		//var_dump($forecast);
	}
	$day_trans = array(
		"Sat"=>"شنبه",
		"Sun"=>"یک شنبه",
		"Mon"=>"دوشنبه",
		"Tue"=>"سه شنبه",
		"Wed"=>"چهارشنبه",
		"Thu"=>"پنج شنبه",
		"Fri"=>"جمعه"
	);
	$current[0] = (array)$current[0];
	$current[0]["@attributes"]["text"] = $weather_array[$current[0]["@attributes"]["code"]];
	foreach($forecast as $i=>$for)
	{
		$forecast[$i] = (array) $forecast[$i];
		$forecast[$i]["@attributes"]["day"] = $day_trans[$forecast[$i]["@attributes"]["day"]];
		$forecast[$i]["@attributes"]["date"] = jdate("Y/m/d",strtotime($forecast[$i]["@attributes"]["date"]));
	}
	//var_dump($current);
	$weatherData = array(
		"location"=>$location,
		"current" =>$current,
		"forecast"=>$forecast
	);
	die(toJSON($weatherData));
?>
