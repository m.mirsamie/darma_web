<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
?>
<script>
        
</script>
<style>
    .report_table
    {
        width: 50%;
    }
    .report_table td
    {
        border: #111 solid thin;
        font-weight: bold;
        padding: 5px;
    }
</style>
<div id="content" >
    <table class="report_table" >
        <tr>
            <td class="round pointer" >
                    <div onclick="loadCont(this,'factor_origin.php?main=main&');" >
گزارش فروش       
                    </div>
            </td>
            <td class="round pointer">
                    <div onclick="loadCont(this,'report_grooh.php?main=main&');" >
                        گزارش فروش گروه کالایی
                    </div>
            </td>
	    <td class="round pointer">
                    <div onclick="loadCont(this,'user_etebar.php?main=main&');" >
                        گزارش مالی
                    </div>
            </td>
        </tr>
    </table>
</div>
