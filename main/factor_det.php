<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$permission=array();
	$cl2=new mysql_class;
	//die("id is".$user_id);
	/*$canWrite='never';
	if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("factor_det.php",$permission))
			$canWrite='limited';
		
	}*/
	function loadTamin($id)
	{
		$t = new taminkonande_class((int)$id);
		$out = '----';
		if(isset($t->name))
			$out = $t->name;
		return($out);
	}
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$mysql = new mysql_class;
	$msg ='';
	$isAdmin = $se->detailAuth('all');
        $gname = 'grid1';
        $input =array($gname=>array('table'=>'factor_det','div'=>'main_div_factor_det'));
        $xgrid = new xgrid($input);
	//$xgrid->alert = TRUE;
        $xgrid->column[$gname][0]['name'] = '';
        $xgrid->column[$gname][1]['name'] = 'شماره فاکتور';
	$xgrid->column[$gname][1]['search'] = 'text';
	$xgrid->column[$gname][2]['name'] = 'کالا';
	$xgrid->column[$gname][2]['clist'] = columnListLoader('kala');
	$xgrid->column[$gname][2]['search'] = 'list';
	$xgrid->column[$gname][2]['searchDetails'] = columnListLoader('kala');
	$xgrid->column[$gname][3]['name'] = 'تعداد';
	$xgrid->column[$gname][4]['name'] = 'قیمت';
	$xgrid->column[$gname][5]['name'] = 'توضیحات';
	$xgrid->column[$gname][6]['name'] = 'تخفیف مدیر';
	$xgrid->column[$gname][7]['name'] = 'تخفیف بسته';
	$xgrid->column[$gname][8]['name'] = '';
	$xgrid->column[$gname][9]['name'] = 'سقف تخفیف';
	$xgrid->column[$gname][10]['name'] = 'تأمین کننده';
	$xgrid->column[$gname][10]['clist'] = columnListLoader('taminkonande');
	$xgrid->column[$gname][10]['search'] = 'list';
	$xgrid->column[$gname][10]['searchDetails'] = columnListLoader('taminkonande');
        //$xgrid->canEdit[$gname]= TRUE;
        //$xgrid->canAdd[$gname] = TRUE;
	//$xgrid->canDelete[$gname] = $canDo;
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);	
?>
<script>
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
        });
</script>
<div id="main_div_factor_det">
</div>
