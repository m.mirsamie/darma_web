<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die('{"error":"logout"}');
	include_once('../simplejson.php');
	$user_id=(int)($_SESSION[$conf->app.'_user_id']);
	$is_read = isset($_REQUEST['is_read'])?(int)$_REQUEST['is_read']:0;
	$rec_id = isset($_REQUEST['rec_id'])?(int)$_REQUEST['rec_id']:-1;
	if($rec_id>0)
	{
		$msg = $_REQUEST['msg'];
		$msg_id = msg_class::sendMsg($rec_id,$msg,$user_id);
		die("$msg_id");
	}
	$msgs = msg_class::loadUser($user_id,$is_read);
	$out = array("error"=>"","msgs"=>$msgs);
	die(toJSON($out));
?>
