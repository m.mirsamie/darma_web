<?php
	include_once('../kernel.php');
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
        if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$mostSales = '';
	$mostSaleKalas = kala_class::mostSale();
	foreach($mostSaleKalas as $kala_id)
	{
		$k = new kala_class($kala_id['kala_id']);
		$mostSales .= (($mostSales!='')?',':'').$k->name.'['.$kala_id['tedad'].']';
	}
	$mostLike = '';
	$almostDones = '';
	$mostLikeKala  = kala_class::mostRate();
	foreach($mostLikeKala as $kala_id)
        {
                $k = new kala_class($kala_id['kala_id']);
		$mostLike .= (($mostLike!='')?',':'').$k->name.'['.$kala_id['emtiaz'].']';
	}
	$almostDoneKala  = kala_class::almostDone();
	foreach($almostDoneKala as $kala_id)
        {
                $k = new kala_class($kala_id['kala_id']);
                $almostDones .= (($almostDones!='')?',':'').$k->name.'['.$kala_id['mojoodi'].']';
        }
?>
<style>
	.highL
	{
		color:red;
	}
</style>
<script>
	$(document).ready(function(){
		$(".gb").hide();
	});
	function lPage(inp)
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
		$("#body").load(inp);
	}

</script>
<div style="float:right;"  >
	<!-- <table width="400px" >
		<tr>
			<td > 
				<div class="pointer fastAcc" onclick="lPage('kala.php');" >
					<div>
						<img src="../img/manage_pro.png"/>
					</div>
				مدیریت محصولات
				</div>
			</td>
			<td>								
				<div class="pointer fastAcc" onclick="lPage('kala_group.php');">
					<div>
						<img src="../img/group_pro.png"/>
					</div>
				مدیریت گروه های کالا
				</div>
			</td>
			<td>
				<div class="pointer fastAcc" onclick="lPage('factor_det.php');">
					<div>
						<img src="../img/order_pro.png"/>
					</div>
				محصولات سفارش داده شده
				</div>
			</td>
		</tr>
		<tr>
			<td >
				<div class="pointer fastAcc" onclick="lPage('kala_galleryGrid.php');" >
					<div>
						<img src="../img/galleryIcon.png"/>
					</div>
				مدیریت تصاویر
				</div>
			</td>
			<td>
				<div class="pointer fastAcc" onclick="lPage('user.php');">
					<div>
						<img src="../img/user.png"/>
					</div>
				مدیریت کاربران
				</div>
			</td>
			<td>
				<div class="pointer fastAcc" onclick="lPage('user_daste.php');">
					<div>
						<img src="../img/users-icon.png"/>
					</div>
				مدیریت گروه های کاربری
				<div>
			</td>
		</tr>
		<tr>
			<td >
				<div class="pointer fastAcc" onclick="lPage('factor.php');" >
					<div>
						<img src="../img/sale_rep.png">				
					</div>
				گزارش فروش
				</div>
			</td>
			<td class="gb">
				<div class="pointer fastAcc"  onclick="lPage('gozaresh_factor_det_back.php');">
					<div>
						<img src="../img/report-icon.png">	
					</div>
				گزارش برگشت از خرید
				</div>
			</td>
			<td class="gb">
				<div class="pointer fastAcc" onclick="lPage('gozaresh_porforush.php');">
					<div>
						<img src="../img/best-seller.png">			
					</div>
				پرفروش ترین ها
				</div>
			</td>
		</tr>
	</table> -->
</div>
<div style="float:right;" >
	<table width="200px" class="fastAcc1">
		<tr>
			<td>
				<div>
				فروش‌امروز: <div class="msg highL"><?php echo factor_class::daramad(date("Y-m-d")); ?> ریال</div>
				</div>
			</td>
			<td>
				<div>
				پر فروش ترین کالا ها: <div class="msg highL"><?php echo $mostSales; ?></div>
				</div>
			</td>
		</tr>
		<tr>
                        <td>
                                <div>
                                بالاترین امتیاز ها: <div class="msg highL"><?php echo $mostLike; ?> ﺭیﺎﻟ</div>
                                </div>
                        </td>
                        <td>
                                <div>
                                درحال اتمام ها: <div class="msg highL"><?php echo $almostDones; ?></div>
                                </div>
                        </td>
                </tr>
		
	</table>
</div>
