<?php
	include_once('../kernel.php');
	include_once('../simplejson.php');
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
/*
	function loadZaman()
	{
		$conf = new conf;
		$out = '';
		$dayCount = ($conf->dayCount!='')?(int)$conf->dayCount:6;
		$i = 1;
		$days = array();
		while($dayCount > count($days))
		{
			$st = strtotime(date("Y-m-d ")." + $i day");
			$d = date("D",$st);
			if($d != "Fri")
				$days[] = array("st"=>$st,"d"=>(($i==0)?'امروز':jdate("l d F",$st)),"gd"=>$d);
			$i++;
		}
		foreach($days as $day)
			$out .= '<option value="'.$day['st'].'">'.$day['d'].'</option>';
		return($out);
	}
	function loadSaat()
	{
		$out =<<<SEL
			<select name="saat" id="saat" class="inp" >
			<!--
					<option value="-1">در اسرع وقت</option>
					<option value="-2">قبل از ظهر</option>
					<option value="-3">قبل از نیمه شب</option>
			-->
					<option value="8:00:00">بین ۸ تا ۱۱ قبل از ظهر</option>
					<option value="11:00:00">بین ١۱ تا ١۴</option>
					<option value="14:00:00">بین ١۴ تا ١۷</option>
					<option value="17:00:00">بین ١۷ تا ۲۰</option>
			</select>
SEL;
//		$out = '';
//		for($i
		return($out);
	}
*/
	$ersal = ($conf->ersal=='')?25000:(int)$conf->ersal;
	$ersalUpLimit = ($conf->ersalUpLimit=='')?750000:(int)$conf->ersalUpLimit;
	$loged = isset($_SESSION[$conf->app.'_user_id']);
	$factor_id = isset($_REQUEST['factor_id'])? (int)$_REQUEST['factor_id']: -1;
	$sabad = new sabad_class($factor_id);
	$user_id = ($loged)?(int)$_SESSION[$conf->app.'_user_id']:-1;
	$prof= new profile_class($user_id);
	$use = new user_class($user_id);
	if(!isset($_SESSION['sabad']))
		$_SESSION['sabad'] = $sabad;
	if($factor_id<=0)
		$sabad = $_SESSION['sabad'];
	$factor_id = $sabad->factor_id;
	if(isset($_REQUEST['nahve']))
	{
		$nahve = $_REQUEST['nahve'];
		$zaman = date("Y-m-d",$_REQUEST['zaman']);
		$makan = $_REQUEST['makan'];
		$saat = $_REQUEST['saat'];
		$ersal = (int)$_REQUEST['ersal'];
		$typ = $_REQUEST['typ'];
		$typ2 = isset($_REQUEST['typ2'])?$_REQUEST['typ2']:0;
		$mantaghe_id = (int)$_REQUEST['mantaghe_id'];
		$mablaghKol =0;
		foreach($sabad->kalas as $kal)
		{
			$k = new kala_class($kal['kala']);
			$takh = takhfif_class::get((int)$kal['kala'],$user_id,$kal['tedad']);
			$takhfif = $takh['takhfif'];
			$mablaghKol+= $kal['tedad']*$k->ghimat*(100-$takhfif)/100;
		}
		$ersal = $conf->ersal==''?25000:$conf->ersal;
		
		if($mantaghe_id==-2)
			$ersal = 0;
		else
		{
			if($mablaghKol > 300000 && $mablaghKol < 600000)
				$ersal = 15000;
			else if($mablaghKol >=600000 && $mablaghKol< $ersalUpLimit )
				$ersal = 7500;
			else if ($mablaghKol >= $ersalUpLimit )
				$ersal=0;
		}
		$mablaghKol+=$ersal;
		$mablagh=$mablaghKol;
		/*
		if($typ=='manzel')
		{
			$pardakht = $mablagh;
			$tasfie = -1;
			$type = 1;
		}
		else
		*/
		if($typ=='etebar')
		{
			if(($prof->etebar-$prof->min_etebar)>=$mablaghKol)
			{
				$pardakht = $mablagh;
				$tasfie =1;
				$type = 0;
			}
			else
			{
				$typ=$typ2;
				$mablagh=$mablaghKol-($prof->etebar-$prof->min_etebar);
				$pardakht = $prof->etebar-$prof->min_etebar;
				$tasfie =-1;
				$type = (($typ == 'naghd')?0:1);
			}
		}
		else if($typ=='naghd')
		{
			$pardakht = $mablagh;
			$tasfie =-1;
			$type = 0;
		}
		if(isset($type))
		{
			//---a صدور فاکتور
			$my = new mysql_class;
			$tarikh = date("Y-m-d H:i:s");
/*
			$ln = $my->ex_sqlx("insert into `factor` (`zamanTahvil`,`nahveTahvil`,`makaneTahvil`,`user_id`,`tarikh`,`en`,`jamKol`,`hazineErsal`,`isTasfie`,`typ`,`mantaghe_id`,`pardakht`) values ('$zaman $saat','$nahve','$makan',$user_id,'$tarikh',1,".($mablaghKol-$ersal).",$ersal,$tasfie,$type,$mantaghe_id,$pardakht)",FALSE);
			$factor_id = (int)$my->insert_id($ln);
			$my->close($ln);
*/
			//-----a کاهش اعتبار کاربر
			if($tasfie ==1 || $typ2=='naghd')
			{
				$toz = 'بابت فاکتور شماره '.$factor_id.' به تاریخ '.jdate("H:i Y / m / d",strtotime($tarikh));
				user_etebar_class::add($user_id,-1,$pardakht,$toz,$user_id);
				$my->ex_sqlx("update `profile` set `etebar`=".(($prof->etebar-$mablaghKol>=0)?$prof->etebar-$mablaghKol:0)." , `hajm_rialy_takhfif` =  `hajm_rialy_takhfif` + ".($mablaghKol)." where `user_id`=$user_id");
				$my->ex_sqlx("update factor set en=1,typ=".(($mantaghe_id==-2)?"1":"0").",isTasfie=1,jamKol=$mablaghKol where id=$factor_id");
				
				$delivery_sms = ($conf->delivery_sms!='')?$conf->delivery_sms:'09156000631';
				sms_class::send($toz,$delivery_sms);
                                $delivery_email = $conf->delivery_email==''?'hscomp2002@gmail.com':$conf->delivery_email;
                                $mail = new email_class($delivery_email,'دارما دلیوری',$toz);
				log_class::add($user_id,'دارما دلیوری سایت اعتباری'.' '.$toz,'delivery_site_etebari');
			}
			//----a موارد فاکتور ثبت می شوند
/*
			if($factor_id>0)
				foreach($sabad->kalas as $kal)
				{
					$k = new kala_class($kal['kala']);
					$takh = takhfif_class::get((int)$kal['kala'],$user_id,$kal['tedad']);
					$takhfif = $takh['takhfif'];
					$takhBaste = $takh['basteTakhfif']['value'];
					$takhModir = $takh['modirTakhfif']['value'];
					$takhTedad = $takh['tedadTakhfif']['value'];
					$takhSaghf = $takh['saghfTakhfif'];
					$ghimatKolKala = $kal['tedad']*$k->ghimat*(100-$takhfif)/100;
					$my->ex_sqlx("insert into `factor_det` (`factor_id`, `kala_id`, `tedad`, `ghimat`, `toz`, `takhfifModir`, `takhfifBaste`, `takhfifTedad`, `saghfTakhfif`,`taminkonande_id`) values ($factor_id,".$kal['kala'].",".$kal['tedad'].",$ghimatKolKala,'',$takhModir,$takhBaste,$takhTedad,$takhSaghf,".$k->taminkonande_id.")");
					if($tasfie ==1)
						$k->decMojoodi($kal['tedad']);	
				}
*/
			$ou = array('factor_id'=>$factor_id,'mablagh'=>$mablagh,'typ'=>$typ);
		}
		else
			$out = array('factor_id'=>-1);
		die(toJSON($ou));
	}
	if(isset($_REQUEST['sid']))
	{
		$user_id = isset($_SESSION[$conf->app.'_user_id'])? (int)$_SESSION[$conf->app.'_user_id']:-1;
		$sid = (int)$_REQUEST['sid'];
		$factor_id = (int)$_REQUEST['factor_id'];
		$factor = new factor_class($factor_id);
		$svalue = (int)$_REQUEST['svalue'];
		$out = $factor->updateFactorDet($sid,$svalue,$user_id);
		$sab = $_SESSION['sabad'];
		$sab->add($sid,$svalue,FALSE);
		$factor->refreshMablagh();
		die(toJSON($out));
	}
	if(isset($_REQUEST['kala_id']))
	{
		$kala_id = (int)$_REQUEST['kala_id'];
		$factor_id = (int)$_REQUEST['factor_id'];
		$factor = new factor_class($factor_id);
		$out = $factor->removeFactorDet($kala_id);
		$sab = $_SESSION['sabad'];
		$sab->remove($kala_id);
		$out = array('kala_id'=>$kala_id,'status'=>$out);
		die(toJSON($out));
	}

	$out = factor_class::loadPishFactor($factor_id,$user_id,'sabadPreview.php',FALSE,TRUE);
	if(isset($_REQUEST['refresh']))
		die($out);
	//$out = factor_class::loadPishFactor($factor_id,$user_id,'sabadPreview.php',FALSE,TRUE);
	$tedadKol = 0;
	$ghimatKol = 0;
	$ghimatKolKham = 0;
	$i = 0;
	foreach($sabad->kalas as $ii=>$sab)
	{
		$k = new kala_class($sab['kala']);
		$takh = takhfif_class::get((int)$sab['kala'],$user_id,$sab['tedad']);
		$takhfif = $takh['takhfif'];
		$takhBaste = $takh['basteTakhfif']['name'].'('.$takh['basteTakhfif']['value'].')';
		$takhModir = $takh['modirTakhfif']['name'].'('.$takh['modirTakhfif']['value'].')';
		$takhTedad = $takh['tedadTakhfif']['name'].'('.$takh['tedadTakhfif']['value'].')';
		$takhSaghf = $takh['saghfTakhfif'];
		//if($sab['tedad'] > $k->tedad_baste)
		//	$takhfif = $k->takhfif;
		$ghimatKolKala = $sab['tedad']*$k->ghimat*(100-$takhfif)/100;
		$ghimatKolKalaKham = $sab['tedad']*$k->ghimat_user;
		$tedadKol += $sab['tedad'];
		$ghimatKol += $ghimatKolKala;
		$ghimatKolKham += $ghimatKolKalaKham;
		$i++;
	}
/*
	$out .= '<tr>';
	$out .= '<th colspan="9" align="left">جمع بدون تخفیف</th>';
	$out .= '<th>'.$ghimatKolKham.'</th>';
	$out .= '</tr>';
	$out .= '<tr>';
	$out .= '<tr>';
	$out .= '<th colspan="9" align="left">سود حاصل از خرید از سامانه فروش دارما</th>';
	$out .= '<th>'.($ghimatKolKham-$ghimatKol).'</th>';
	$out .= '</tr>';
	$out .= '<tr>';
	$out .= '<th colspan="9" align="left">جمع اقلام</th>';
	$out .= '<th>'.$ghimatKol.'<input type="hidden" id="ersal" name="ersal" value="'.$ersal.'" /></th>';
	$out .= '</tr>';
*/
	if($ghimatKol < $ersalUpLimit)
	{
		if($ghimatKol > 300000 && $ghimatKol <= 600000)
			$ersal = 15000;
		else if($ghimatKol >600000)
			$ersal = 7500;
		else if ($ghimatKol>750000)
			$ersal=0;
/*
		$out .= '<tr>';
		$out .= '<th colspan="9" align="left">هزینه ارسال</th>';
		$out .= '<th>'.$ersal.'</th>';
		$out .= '</tr>';
*/
	}
/*
	$out .= '<tr>';
        $out .= '<th colspan="9" align="left">جمع کل</th>';
        $out .= '<th>'.($ghimatKol+$ersal).'</th>';
        $out .= '</tr>';
*/
	$goBank=FALSE;
	$tafaz = 0;
	$msg = 'است';
	if($ghimatKol+$ersal>$prof->etebar)
	{
		$goBank = TRUE;
		$msg = 'نیست';
		$tafaz = $ghimatKol+$ersal-$prof->etebar;
	}
?>
<script>
	var userLoged = <?php echo $loged?'true':'false';?>;
	var selfMantaghe_id = <?php echo (int)$prof->mantaghe_id; ?>;
	var fac_id = <?php echo isset($_REQUEST['factor_id'])>0? 'false':'true'; ?>;
	function sendToBank(factor_id)
	{
		alert('sending to bank . . .');
	}
	function purchase()
	{
		$("#pardakhtButt").prop("disabled",true);
		var typ1='0';
		if($("#manzel").prop("checked"))
			typ1='manzel';
		else if($("#etebar").prop("checked"))
			typ1='etebar';
		else if($("#naghd").prop("checked"))
			typ1='naghd';
		var vals = {
			nahve:$("#nahve").val(),
			zaman:$("#zaman").val(),
			saat :$("#saat").val(),
			makan:$("#makan").val()+(($("#girande_name").val()!='')?' '+$("#girande_name").val():'')+(($("#girande_mob").val()!='')?' '+$("#girande_mob").val():''),
			ersal:$("#ersal").val(),
			mantaghe_id:(($("#selfAddr").prop("checked"))?selfMantaghe_id:(($("#malAddr").prop("checked"))?-2:-1)),
			typ:typ1
		};
		if(!fac_id)
			vals['factor_id'] = <?php echo $factor_id; ?>;
		
		var gobank=<?php echo $goBank?'true':'false'; ?>;
		var tafaz = <?php echo $tafaz; ?>;
		var user_id = <?php echo $user_id; ?>;
		var cont=true;
		var goReallyBank = true;
		if(gobank && typ1=='etebar')
			if(!confirm('اعتبار شما کافی نیست آیا تمایل به افزایش اعتبار به میزان '+tafaz+'ریال دارید؟'))
			{
				if(!confirm('اعتبار شما به میزان '+tafaz+' ریال کم است ، آیا مایل به پرداخت آن درب منزل هستید؟'))
					cont=false;
				else
					goReallyBank = false;
			}
		if(!cont)
			return false;
		if(goReallyBank)
			vals['typ2'] = 'naghd';
		else
			vals['typ2'] = 'manzel';
		$("#pardakhtButt").after("<img class='pardakhtKhoon' src='../img/status_fb.gif' >");
		$.getJSON("sabadPreview.php",vals,function(result){
			$(".pardakhtKhoon").remove();
			if(result.factor_id=="-1")
			{
				alert('خطا در اطلاعات');
				return false;
			}
			else
			{
				var pr = "<a target='_blank' href=\"pishFactor_print.php?user_id="+user_id+"&factor_id="+result.factor_id+"&\" >مشاهده نسخه قابل چاپ</button>";
				if(result.typ=='naghd')
				{
					startBank(result.mablagh,result.factor_id);
				}
				else if(result.typ=='etebar')
				{
					var txt = 'خرید با موفقیت انجام شد شماره فاکتور شما '+result.factor_id+" می باشد";
					readOnly_all();
					$("#pardakht_div").html("<div class='msg' ><h1>"+txt+"</h1>"+pr+"</div>");
				}
				else if(result.typ=='manzel')
				{
					var txt = 'خرید با موفقیت انجام شد شماره فاکتور شما '+result.factor_id+" می باشد ، طبق درخواست کالا برای شما ارسال خواهد شد";
					readOnly_all();
					$("#pardakht_div").html("<div class='msg' ><h1>"+txt+"</h1>"+pr+"</div>");
				}
				emptySabad(true);
				
			}
			$("#pardakhtButt").prop("disabled",false);
		});
	}
	function readOnly_all()
	{
		$('input').attr('readonly', 'readonly');
		$('input').attr('disabled', true);
		$('textarea').attr('readonly', 'readonly');
		$('select').attr('disabled', true);
		$('radio').attr('disabled', true);
		$(".fact_tedad").spinner( "option", "disabled", true );
	}
	$(document).ready(function(){
		if(fac_id)
			refreshSabad();
		$(".fact_tedad").keyup(function(e){ 
			var code = e.which;
			if(code==13)
			{
				e.preventDefault();
				tedadChanged(e.currentTarget);
			}
		});
		$(".fact_tedad").spinner({
			change: function( event, ui ) 
			{
				tedadChanged(event.currentTarget);
			}
		});
		$(".fact_tedad").spinner({
			min:1
		});
		$("#selfAddr").click(function(){
			$("#girande_name").val('');
			$("#girande_mob").val('');
			$("#makan").val($("#makan_def").text());
			$("#digariDiv").hide();
		});
		$("#otherAddr").click(function(){
			$("#girande_name").val('');
			$("#girande_mob").val('');
			$("#makan").val('');
			$("#digariDiv").show();
                });
	});
	function refreshData()
	{
		var out = "";
		$.get("sabadPreview.php?refresh=1&factor_id=<?php echo $factor_id; ?>",function(result){
			if(result=='empty')
			{
				$("#pardakht_div").html("");
				$("#sss_div").html("");
			}
			else
				$("#sss_div").html("<table>"+result+"</table>");
			refreshSabad();
		});
		
	}
	function tedadChanged(obj)
	{
		$("#sss_div").html("درحال بروز رساني قيمت ها<img src='../img/status_fb.gif' >");
		var si = '';
		var val = 0;
		if(obj)
		{
			si = obj.id.split('_')[1];
			val = obj.value;
			$(obj).after("<img class='khoon_tedad_"+si+"' src='../img/status_fb.gif' >");
		}
		$.getJSON("sabadPreview.php",
			{
				user_id:<?php echo $user_id; ?>,
				factor_id:<?php echo $factor_id; ?>,
				sid:si,
				svalue:val
			},
			function(result){
				refreshData();
		});
	}
</script>
<style>
	#sss{width:100%;font-family:tahoma;font-size:12px;}
	#sss td{border:solid 1px #333333;padding:5px;}
	#sss th{border:solid 1px #333333;padding:5px;}
	#sabadPreview button{font-family:tahoma;font-size:12px;}
</style>
<div id="sabadPreview">
	<table>
		<tr>
			<td>
				<img src="../img/darma.png" width="50" >
			</td>
			<td>
				<h3 align="center">
					شماره پیش فاکتور:<?php echo $sabad->factor_id.' '.$use->fname.' '.$use->lname; ?> [<?php echo enToPerNums(audit_class::idToCode($user_id)); ?>]
				</h3>
			</td>
	</table>
	<div id="sss_div" >
	<table id="sss" >
	<?php echo $out; ?>
	</table>
	</div>
	<br/>
	<div align="right" id="pardakht_div" >
		<div>
			اعتبار قابل خرید شما:
			<b><?php echo $prof->etebar-$prof->min_etebar; ?></b>
			ریال می‌باشد که برای پرداخت این فاکتور کافی 
				<?php echo $msg; ?>
		</div>
                <?php 
                    if($tafaz==0)
                    {    
                ?>
                    خرید اعتباری	
                    <input type="radio" name="kharid" id="etebar" checked="checked" >
                <?php } ?>
                 
		خرید نقدی	
                <input type="radio" name="kharid" id="naghd" <?php echo $tafaz==0?'':'checked="checked"'; ?> >
		<!--
		پرداخت درب منزل
		<input type="radio" name="kharid" id="manzel" >
		-->
		<button id="pardakhtButt" onclick="purchase();">ادامه</button>
	</div>
</div>
