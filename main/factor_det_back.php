<?php
	include_once("../kernel.php");
	$SESSION = new session_class;
	register_shutdown_function('session_write_close');
	session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
	$se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
	if(!$se->can_view)
			die($conf->access_deny);
	$isAdmin = $se->detailAuth('all');
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$factor_id = isset($_REQUEST['factor_id'])? (int)$_REQUEST['factor_id']:-1;
	factor_det_back_class::insert($factor_id);
	$factor = new factor_class($factor_id);
	$info = '';
	if(isset($factor->user_id))
	{
		$user = new user_class($factor->user_id);
		$info = isset($user->fname)?$user->fname.' '.$user->lname:'---';
	}
	function loadUser($inp)
	{
		$user = new user_class($inp);
		return(isset($user->id)? $user->fname.' '.$user->lname:'حذف شده');
	}
	function edit1($table,$id,$field,$val,$fn,$gname)
	{
		$out = TRUE;
		$id = (int)$id;
		$f_det = new factor_det_back_class($id);
		$mysql = new mysql_class;
		if($field == 'tedad' || $field == 'toz' )
		{
			$mysql->ex_sqlx("update `$table` set `$field` = '$val' where `id` = $id");
			if($field == 'tedad')
			{
				$fac = new factor_det_back_class($id);
				if($fac->tedad>(int)$val && (int)$val>0)
				{
					$factor = new factor_class($fac->factor_id);
					$fac->refreshMablagh($val,$factor->user_id);
				}
				else
					$out = "FALSE|  جهت افزایش تعداد & سفارش جدید ثبت کنید";
			}
		}
		else
			$out = "FALSE| تغییر امکان پذیر نیست";
		return($out);
	}
	if(isset($_REQUEST['final']))
	{
		$factor_id = (int)$_REQUEST['factor_id'];
		$my = new mysql_class;
		die($my->ex_sqlx("update `factor_det_back` set `en`=1 where `factor_id`=$factor_id"));
	}
	if(isset($_REQUEST['admin']))
	{
		$factor_id = (int)$_REQUEST['factor_id'];
		$factor = new factor_class($factor_id);
		$factor->restoreFactorBack();
		die('restoreFactorBack');
	}
	$mysql = new mysql_class;
	$mysql->ex_sql("select `id` from `factor_det_back` where `en`=1 and `factor_id`=".$factor_id,$q);
	$readOnly = isset($q)? FALSE:TRUE;
	$gname = 'grid1';
	$input =array($gname=>array('table'=>'factor_det_back','div'=>'main_div_factor_det_back'));
	$xgrid = new xgrid($input);
	for($i=0;$i<count($xgrid->column[$gname]);$i++)
		$xgrid->column[$gname][$i]['access']='a';
	$xgrid->eRequest[$gname] = array('factor_id'=>$factor_id);
	$xgrid->whereClause[$gname] = ' `factor_id`='.$factor_id;
    $xgrid->column[$gname][0]['name'] = '';
    $xgrid->column[$gname][1]['name'] = '';
	$xgrid->column[$gname][2]['name'] = '';
	$xgrid->column[$gname][3]['name'] = 'تعداد';
	$xgrid->column[$gname][3]['access'] = null;
	$xgrid->column[$gname][4]['name'] = 'قیمت';
	$xgrid->column[$gname][5]['name'] = 'توضیحات';
	$xgrid->column[$gname][5]['access'] = null;
	$xgrid->column[$gname][6]['name'] = '';
	$xgrid->column[$gname][7]['name'] = '';
	$xgrid->column[$gname][8]['name'] = '';
	$xgrid->column[$gname][9]['name'] = '';
	$xgrid->column[$gname][10]['name'] = '';
	$xgrid->column[$gname][11]['name'] = '';
    $xgrid->canEdit[$gname]= $readOnly || $isAdmin;
	$xgrid->editFunction[$gname] = 'edit1';
	//$xgrid->editFunction[$gname] = 'edit1';
        //$xgrid->canAdd[$gname] = TRUE;
       // $xgrid->canDelete[$gname] = $canDo;
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);	
?>
<script>
	$(document).ready(function(){
		var ggname = '<?php echo $gname; ?>';
		var factor_id = <?php echo isset($factor_id)? $factor_id:-1; ?>;
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
			gArgs[ggname]['afterEdit'] =function(e){
			 loadFactorDet(factor_id);
			return(e);
		};
    });
	
	<?php if($isAdmin){ ?>
	function doChanges()
	{
		var factor_id = <?php echo $factor_id; ?>;
		$("#back_khoon").html('<img src="../img/status_fb.gif" >');
		$.get("factor_det_back.php?admin=1&factor_id="+factor_id+"&",function(result){
			$("#back_khoon").html('');
			console.log(result);
		});
	}
	<?php } else {?>
	function finalOk()
	{
		var factor_id = <?php echo $factor_id; ?>;
		$("#back_khoon").html('<img src="../img/status_fb.gif" >');
		$.get("factor_det_back.php?final=1&factor_id="+factor_id+"&",function(result){
			var txt='<span class="notice" >خطا در ثبت نهایی</span>';
			if($.trim(result)=='ok')
				txt='<span class="msg" >ثبت با موفقیت انجام شد</span>';
			$("#back_khoon").html(txt);
		});
	}
	<?php }?>
</script>
<div id="content" >
	<div align="right" style="border:solid 1px #333333;" class="round" >
		امکان ویرایش مقدار تعداد برای شما وجود دارد جهت ویرایش روی تعداد کلیک کنید و پس از ویرایش دکمه اینتر را فشار دهید و درانتها دکمه تایید نهایی را کلیک کنید
		<h3>
		<?php 
			if(isset($factor->id) && $info!='')		
				echo 'شماره فاکتور: '.$factor_id.' خریدار: '.$info.' تاریخ: '.jdate(" H:i d / m / Y",strtotime($factor->tarikh));
		?>
		</h3>
	</div>
	<div id="main_div_factor_det_back">
	</div>
	<div id="sabt_final" >
		<span id="back_khoon" style="width:25px;" ></span>
		<?php if($isAdmin){ ?>
			<button onclick="doChanges();" >اعمال تغییرات</button>
			<button onclick="doNotChange();" >عدم تایید</button>
		<?php } else{
			if($readOnly)
			{
		?>
			<button onclick="finalOk();" >تأیید نهایی</button>
		<?php }}?>
	</div>
</div>
