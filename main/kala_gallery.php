<?php
        include_once("../kernel.php");
	include_once("../simplejson.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	if(isset($_REQUEST['delete_ids']))
	{
		$del_ids =trim( $_REQUEST['delete_ids']);
		if($del_ids!='')
		{
			$my = new mysql_class;
			die($my->ex_sqlx("delete from `kala_gallery` where `id` in ($del_ids)"));
		}
		else
			die('no_item');
	}
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$kala_id = (isset($_REQUEST['kala_id']))?(int)$_REQUEST['kala_id']:-1;
	
	$kala_gallery = kala_gallery_class::loadByKala($kala_id);
	/*
	"<a class='lbox' href='../kargah_img/"+kargah[i].img+"' title='"+kargah[i].name+"' ><img width='60' src='../kargah_img/"+kargah[i].img+"' ></a>"
	*/
?>
<link rel="stylesheet" href="../css/jquery.lightbox-0.5.css">
<script src="../js/jquery.lightbox-0.5.min.js" ></script>
<script src="../js/inc.js" ></script>
<script>
	var kala_id = <?php echo $kala_id; ?>;
	var kala_gallery = <?php echo toJSON($kala_gallery); ?>;
	$(document).ready(function(){
		//var s = drawGall(kala_gallery);
		$("#main_div_gallery").html(drawGall(kala_gallery,'delete'));
		$("a.lbox").lightBox({
			imageBtnClose:'../img/lightbox-btn-close.gif',
			imageLoading:'../img/lightbox-ico-loading.gif',
			imageBtnPrev:'../img/lightbox-btn-prev.gif',
			imageBtnNext:'../img/lightbox-btn-next.gif'
		});
        });
	function loadGall(ka_id)
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load('kala_gallery.php?kala_id='+ka_id+'&');
	}
	function delet()
	{
		var del_ids='';
		$.each($(".gallery"),function(id,field){
			if($("#"+field.id).prop("checked"))
				del_ids+= (del_ids==''?'':',' )+field.id.split('_')[1];	
		});
		if(del_ids!='')
		{
			$("#khoon_gal").html("<img src='../img/status_fb.gif' >");
			$.get("kala_gallery.php?delete_ids="+del_ids+"&",function(result){
				//$("#khoon_gal").html('');
				if($.trim(result)=='ok')
				{
					$("#khoon_gal").html('حذف با موفقيت انجام شد');
					setTimeout(function(){ loadGall(kala_id); },4000);
				}
				else if(result=='no_item')
					$("#khoon_gal").html('هيچ تصويري انتخاب نشده است');
				else
					$("#khoon_gal").html('خطا در حذف');
			});
		}
		else
			alert('هيچ تصويري انتخاب نشده است');
	}
</script>

<div id="main_div_gallery" align="right" >
</div>
<div align="right">
	<button onclick="wopen('upload_gallery.php?kala_id=<?php echo $kala_id; ?>','',500,400);" >ارسال تصویر</button>
	<button onclick="delet();"  >حذف</button>
</div>
<div id="khoon_gal" ></div>
