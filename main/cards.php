<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
        if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$permission=array();
	$cl2=new mysql_class;
	/*$canWrite='never';
	if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("cards.php",$permission))
			$canWrite='limited';
		
	}*/
	function loadUser($id)
	{
		$u = new user_class((int)$id);
		$out = '----';
		if(isset($u->id))
			$out = $u->fname.' ' .$u->lname;
		return($out);
	}
	function mpdate($inp)
	{
		return(($inp != '' && $inp != '0000-00-00 00:00:00')?jdate("H:i Y/m/d",strtotime($inp)):'----');
	}
	function mpdate_back($inp)
	{
		return(audit_class::hamed_pdateBack($inp));
	}
	function loadPic($id)
	{
		$id = (int)$id;
		$out = '----';
		$my = new mysql_class;
		$my->ex_sql("select `pic` from `profile` where `id` = '$id'",$q);
		if(isset($q[0]))
		{
			if ($q[0]["pic"]!='')
			{
				$pic = $q[0]["pic"];
				$out = "<img src=\"$pic\" class=\"imgGallTmb pointer\" onclick=\"startUpload1('$id')\"/>";
				$out .= "<div id=\"fileupload1_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr1_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
			else
			{
				$out = "<button  style=\"margin:10px;\" onclick=\"startUpload('$id');\">بروزرسانی تصویر</button>";
				$out .= "<div id=\"fileupload_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
		}		
		return($out);
	}
	function pas1($inp)
	{

		return(' ');
	}
	function load_trans($inp)
	{
		//return($inp);
		$box="<div class='msg pointer' onclick=\"loadCont(this,'transaction.php?id='+$inp);\" >";
		$ms=new transactions_class($inp);
		$box.=isset($ms->tarikh)?mpdate($ms->tarikh):'0000-00-00 00:00:00';
		$box.="</div>";
		return($box);
	}
	function addCard($gname,$table,$fields,$column)
        {
                $conf = new conf;
		$user_sabt_id = (int)$_SESSION[$conf->app.'_user_id'];
		$card_new_mablagh = (trim($conf->card_new_mablagh)!='')?(int)$conf->card_new_mablagh:35000;
		$q1='';
		$q2='';
		$min_etebar=0;
		$passr = '';
		$fields['passr'] = $fields['pass'];
		$fields['saghf_hadie'] = ((int)$fields['saghf_hadie']==0)?-1:$fields['saghf_hadie'];
                foreach($fields as $key=>$val)
		{
			if($key=='saghf_hadie')
			{
				$val = abs($val);
				$min_etebar=$val;
			}
			else if($key=='user_id' && $val>0)
				$prof = new profile_class($val);
			else if($key == 'pass')
				$val = md5($val);
			$q1.=($q1==''?'(':',').$key;
			$q2.=($q2==''?'(':',')."'".$val."'";
		}
		$card_id = -1;
		//var_dump($prof);
		$tmpp = (isset($prof->min_etebar) && ($prof->etebar-$prof->min_etebar>=$card_new_mablagh));
		if(isset($prof->min_etebar) && ($prof->etebar-$prof->min_etebar>=$card_new_mablagh))
		{
			$mysql = new mysql_class;
			$q1.=',company_id)';
			$q2.=",'".$conf->company_id."')";
			$qu = "insert into cards $q1 value $q2";
			//echo $qu;
			$ln = $mysql->ex_sqlx($qu,FALSE);
			$card_id = $mysql->insert_id($ln);
			$mysql->close($ln);
			if((int)$card_id>0)
			{
				$mysql->ex_sqlx("update profile set min_etebar=min_etebar+$min_etebar where id=".$prof->id);
				if(isset($_REQUEST['card_etebar_dec']) && $_REQUEST['card_etebar_dec']=='yes')
					$out = $prof->decEtebar($card_new_mablagh,$user_sabt_id,'کاهش اعتبار بابت ثبت یک کارت به شماره '.$fields['shomare']);
			}
		}
		$out = ((int)$card_id>0)?TRUE:"false|اعتبار کافی نیست";
                return($out);
        }
	
	function editUser($table,$id,$field,$val,$fn,$gname)
	{
		global $user_id;
		$id = (int)$id;
		$mysql = new mysql_class;
		if($field == 'pass')
		{
			$valtmp = md5($val);
			$toz="ویرایش رمز عبور.";
			$extra="عملیات ویرایش";
			$lo=log_class::add($user_id,$toz,$extra);
			if($lo=='true')
			{
				$mysql->ex_sqlx("update `$table` set `$field` = '$valtmp' where `id` = $id");
				$mysql->ex_sqlx("update `$table` set `passr` = '$val' where `id` = $id");
				$flag=true;
			}
		}
		else if($field=='shomare')
		{
			$sh=$val;
			$mysql->ex_sql("select `id` from `cards` where `shomare`='$sh' and `id`!='$id'",$res);
			if(count($res)>0)
			{
				$flag="FALSE| شماره نمیتواند تکراری باشد.  ";
			}
			else
			{	$pre='----';
				$mysql->ex_sql("select shomare from cards where id='$id' " ,$res);
			
				if(count($res)>0)
				{
					if($res[0][$field]!='')
						$pre=$res[0]['shomare'];
							
				}
				$toz="ویرایش فیلد: $field   از  $pre به $val.";
				$extra="عملیات ویرایش";
				$lo=log_class::add($user_id,$toz,$extra);
				if($lo=='true')
				{
					$mysql->ex_sqlx("update `$table` set `$field` = '$val' where `id` = $id");
					$flag=true;
				}
				
			}
			
		}
		else if($field=='rfid')
		{
			$rfid=$val;
			$mysql->ex_sql("select `id` from `cards` where `rfid`='$rfid' and `id`!='$id'",$res);
                        if(count($res)>0)
                        {
                                $flag="FALSE| RFID تکراری می باشد ";
                        }
                        else
                        {       $pre='----';
                                $mysql->ex_sql("select rfid from cards where id='$id' " ,$res);
                                if(count($res)>0)
                                {
                                        if($res[0][$field]!='')
                                                $pre=$res[0]['rfid'];

                                }
                                $toz="ﻭیﺭﺍrfid: $field   ﺍﺯ  $pre ﺐﻫ $val.";
                                $extra="ﻊﻤﻟیﺎﺗ ﻭیﺭﺍیﺵ";
                                $lo=log_class::add($user_id,$toz,$extra);
                                if($lo=='true')
                                {
                                        $mysql->ex_sqlx("update `$table` set `$field` = '$val' where `id` = $id");
                                        $flag=true;
                                }

                        }
		}
		else
		{
			$pre='----';
			$nxt=$val;
			$mysql->ex_sql("select `cards`.`rfid`, `cards`.`user_id`,`cards`.`tarikh_sodor`,`cards`.`shomare`, 
			`cards`.`last_trans_kharid`,`cards`.`last_trans_enteghal`,
			`cards`.`expire_date`,`cards`.`pass`,`cards`.`passr`,`cards`.`wrong_count`,
			`cards`.`wrong_date`,`cards`.`en`,`cards`.`saghf_rooz`,`cards`.`saghf_hadie`,
			`cards`.`saghf_enteghal`,`cards`.`karmozd`,`cards`.`rooz_mablagh`,`cards`.`rooz_enteghal`,cards.address,cards.fname,cards.codemeli
			`user`.`fname` f_name,`user`.`lname`,`user`.`user`, `transactions`.`tarikh`
			from cards left join user on(cards.user_id=user.id) 
			left join transactions on (cards.`last_trans_kharid`=transactions.id) where cards.id='$id' " ,$res);
			
			if(count($res)>0)
			{
				if($res[0][$field]!='')
				{
					switch($field)
					{	
						case 'user_id':
							$pre=$res[0]['f_name']. ' ' .$res[0]['lname'];
							break;
						default:
							$pre=$res[0][$field];
					}
				}
			}
			if($field=='user_id')
			{
				$mysql->ex_sql("select fname,lname from user where id='$val'",$resval);
				$car = new cards_class($id);
				$pre_user_id = $car->user_id;
				if($val>0)
				{
					$new_prof = new profile_class($val);
					if($new_prof->etebar-$new_prof->min_etebar>=$car->saghf_hadie)
						$mysql->ex_sqlx("update profile set min_etebar=min_etebar+".$car->saghf_hadie." where id=".$new_prof->id);
					else
						return('false|سقف اعتبار کاربر مقصد کمتر از میزان تخصیص داده شده است');
				}
				if($pre_user_id>0)
				{
					$pre_prof= new profile_class($pre_user_id);
					$mysql->ex_sqlx("update profile set min_etebar=min_etebar-".$car->saghf_hadie." where id=".$pre_prof->id);
				}
				if(count($resval)>0)
				{
					$nxt=$resval[0]['fname'].' '.$resval[0]['lname'];
				}
			}
			if($field=='saghf_hadie')
			{
				$car = new cards_class($id);
				$val =($val==-1)?-1:abs($val);
				$dif = $val-($car->saghf_hadie==-1?0:$car->saghf_hadie);
				if($car->user_id>0)
				{
					$prof = new profile_class($car->user_id);
					if(($prof->etebar-$prof->min_etebar)>=$dif)
						$mysql->ex_sqlx("update profile set min_etebar=min_etebar+$dif where id=".$prof->id);
					else
						return("false|سقف اعتبار شما کمتر از میزانی است که تخصیص داده اید");
				}
			}
			if($field=='expire_date')
			{
				$val=mpdate_back($val);
			}
			//die("next is:".$nxt)
			$toz="ویرایش فیلد: $field  از  $pre به $nxt.";
			$extra="عملیات ویرایش";
			$lo=log_class::add($user_id,$toz,$extra);
			if($lo=='true')
			{
				
				$mysql->ex_sqlx("update `$table` set `$field` = '$val' where `id` = $id");
				$flag=true;
			}
		}
		return($flag);
	}
	function loadUserNamesAndUser()
	{
		$out = array(0=>'');
		$my = new mysql_class;
		$conf = new conf;
		$my->ex_sql("select id,fname,lname,user from user where not (user like 'mehrdad%') and company_id=".$conf->company_id." order by lname,fname,user",$q);
		foreach($q as $r)
			$out[(int)$r['id']] = $r['fname'].' '.$r['lname'].' ['.$r['user'].']';
		return($out);
	}
	if(isset($_REQUEST['sendEmail']))
	{
		$defF = array('msg','sendEmail','sendSms','sendAll','all');
		$sendAll = ($_REQUEST['sendAll'] == 'true');
		$my = new mysql_class;
		if($sendAll)
		{
			$whereC = array();
			foreach($_REQUEST as $key=>$value)
				if(!in_array($key,$defF))
					$whereC[] = "`$key` = '$value'";
			$my->ex_sql("select `email`,`mob` from `profile` ".((count($whereC)>0)?' where '.implode(' and ',$whereC):''),$q);
		}
		else
		{
			$profile_ids = explode(',',$_REQUEST['profile_ids']);
			$my->ex_sql("select `email`,`mob` from `profile` ".((count($profile_ids)>0)?' where `id` in ('.implode(',',$profile_ids).')':''),$q);
		}
		foreach($q as $r)
		{
			if($_REQUEST['sendEmail'] == 'true')
				$e = new email_class($r['email'],'پیام از دارما',$_REQUEST['msg']);
			if($_REQUEST['sendSms'] == 'true')
				sms_class::send($_REQUEST['msg'],$r['mob']);
		}
		die('true');
	}
	
	if(isset($_REQUEST['new_tedad']))
	{
		$tedad = (int)$_REQUEST['new_tedad'];
		$user_id =  (int)$_SESSION[$conf->app.'_user_id'];
		$user_sabt_id = $user_id;
		$out=user_card_sefaresh_class::add($user_id,$tedad,$user_sabt_id);
		$ou = $out?'done':'etebar_error';
		die($ou);
	}
	$columnList = '';
	$visibleCols = isset($_REQUEST['cols'])?explode(',',$_REQUEST['cols']):array(1,2,3,4,5,6,8,10,12,14);
	$colNames = array('','R.F.I.D','کاربر','تاریخ صدور','شماره','اخرین تراکنش خرید','اخرین تراکنش انتقال','','رمز کارت','','','','وضعیت','سقف روزانه','سقف اعتبار','سقف انتقال','کارمزد','میلغ روز','انتقال در روز'
,'کمپانی','نام و نام خانوادگی','کدملی','آدرس');
        $gname = "gname_cards";
	$gname_sefaresh="gname_sefaresh";
	$input =array($gname=>array('table'=>'cards','div'=>'main_div_cards'),$gname_sefaresh=>array('table'=>'user_card_sefaresh','div'=>'div_user_card_sefaresh'));
        $xgrid = new xgrid($input);
	$company_id = (trim($conf->company_id)!='')?(int)$conf->company_id:2;
	$user_wer = ($se->detailAuth('all') || $se->detailAuth('doWrite'))?') or (user_id<=0':" and id = $user_id ";
	$xgrid->whereClause[$gname] = " company_id = $company_id and  (`user_id` in (select `id` from `user` where `user`<>'mehrdad' $user_wer)) order by id desc";
	//$xgrid->column[$gname][1]['access'] = 'a';
	$xgrid->column[$gname][2]['clist'] = loadUserNamesAndUser();//columnListLoader("user| `user` <> 'mehrdad' and company_id = ".$conf->company_id,array('id','fname','lname'));
	$xgrid->column[$gname][2]['search'] = 'list';
	$xgrid->column[$gname][2]['searchDetails'] = columnListLoader("user|user <> 'mehrdad' and user <> 'mehrdad_naghshe'".(($se->detailAuth('all') || $se->detailAuth('doWrite'))?'':' and id = '.$user_id),array('id','fname','lname'));
	$xgrid->column[$gname][3]['cfunction'] =array('mpdate','mpdate_back');
	$xgrid->column[$gname][3]['search'] = 'dateValue';
	$xgrid->column[$gname][3]['access'] ='null';
	$xgrid->column[$gname][4]['search'] = 'text';
	$xgrid->column[$gname][5]['cfunction'] =array('load_trans');
	$xgrid->column[$gname][5]['access'] ='null';
	$xgrid->column[$gname][6]['cfunction'] =array('load_trans');
	$xgrid->column[$gname][6]['access'] ='null';
	$xgrid->column[$gname][7]['cfunction'] =array('mpdate','mpdate_back');
	$xgrid->column[$gname][8]['cfunction'] =array('pas1');
	$xgrid->column[$gname][5]['search'] = 'dateValue';
	$xgrid->column[$gname][6]['search'] = 'dateValue';
	$xgrid->column[$gname][12]['clist'] =array(0=>'غیر فعال',1=>'فعال');
	$xgrid->column[$gname][12]['search'] = 'list';
	$xgrid->column[$gname][12]['searchDetails'] = array(-1=>'',0=>'غیر فعال',1=>'فعال');
	$xgrid->column[$gname][17]['access'] ='null';
	$xgrid->column[$gname][18]['access'] ='null';
	$xgrid->column[$gname][19]['access'] ='null';
	$xgrid->column[$gname][19]['clist'] = columnListLoader('company');
	$canWrite = '';
	$exceptionFields = array(8,14,20,21,22);
       	$xgrid->canEdit[$gname]= TRUE;
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
		$xgrid->canAdd[$gname] = TRUE;
	else
		foreach($xgrid->column[$gname] as $indx => $col)
			if(!in_array($indx,$exceptionFields))
				$xgrid->column[$gname][$indx]['access'] = 'a';
	foreach($xgrid->column[$gname] as $indx=>$col)
	{
		$checked = FALSE;
		if(in_array($indx,$visibleCols))
		{
			$xgrid->column[$gname][$indx]['name'] = $colNames[$indx];
			$checked = TRUE;
		}
		else
			$xgrid->column[$gname][$indx]['name'] = '';
		if(isset($colNames[$indx]) && $colNames[$indx] != '')
		{
			$columnList .= $colNames[$indx];
			if($canWrite=='never')
			{
				$columnList .= '<input class="selCols" type="checkbox" disabled="disabled" 
				id="sel_'.$col['fieldname'].'_'.$indx.'" '.(($checked)?'checked="checked"':'').'/>';
			}
			else
				$columnList .= '<input class="selCols" type="checkbox" id="sel_'.$col['fieldname'].'_'.$indx.'" '.(($checked)?'checked="checked"':'').'/>';
		}
	}
	
	if($columnList != '')
		if($canWrite!='never')
			$columnList .= '<button onclick="selectColumns();">انتخاب</button>';
		else
			$columnList .= '<button disabled="disabled" onclick="selectColumns();">انتخاب</button>';
	$xgrid->editFunction[$gname] = 'editUser';
	$xgrid->addFunction[$gname]='addCard';
	$xgrid->eRequest[$gname] = array('card_etebar_dec'=>'no');
	$xgrid->whereClause[$gname_sefaresh] = " `user_id` in (select `id` from $conf->poolDB.`user` where `user`<>'mehrdad' $user_wer) order by tedad_sodoor asc ,tarikh_sabt desc";
	$xgrid->column[$gname_sefaresh][0]['name']='';
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
	{
		$xgrid->column[$gname_sefaresh][1]['name']='کاربر';
		$xgrid->column[$gname_sefaresh][1]['cfunction']=array('loadUser');
		$xgrid->column[$gname_sefaresh][1]['search']='list';
		$xgrid->column[$gname_sefaresh][1]['searchDetails'] = columnListLoader("user|user <> 'mehrdad' and user <> 'mehrdad_naghshe'",array('id','fname','lname'));
		$xgrid->canEdit[$gname_sefaresh]=TRUE;
		$xgrid->column[$gname_sefaresh][2]['access'] = 'a';
		$xgrid->column[$gname_sefaresh][3]['access'] = 'a';
	}
	else
		$xgrid->column[$gname_sefaresh][1]['name']='';
	$xgrid->column[$gname_sefaresh][2]['name']='تعداد';
	$xgrid->column[$gname_sefaresh][3]['name']='تاریخ ثبت درخواست';
	$xgrid->column[$gname_sefaresh][3]['cfunction']=array('mpdate');
	$xgrid->column[$gname_sefaresh][4]['name']='';
	$xgrid->column[$gname_sefaresh][5]['name']='تعداد صادر شده';
	//$xgrid->column[$gname][14]['clist'] = array(-1=>'غیر هدیه','100000'=>'100,000','200000'=>'200,000','500000'=>'500,000');
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);
	$prof = new profile_class($user_id);
	//echo $xgrid->whereClause[$gname];
?>
<style>
	.imgGallTmb{width:50px;}
	#msgDiv
	{
		border : 1px #000000 solid;
		width : 400px;
	}
</style>
<script type="text/javascript" >
	var ggname = '<?php echo $gname; ?>';
	function load_trans(id)
	{
		openDialog("transaction.php?id="+id,'تراکنش',{minWidth:800,minHeight:200},false);
		
	}
	function startUpload(id)
	{
		$("#ifr_"+id).prop("src","upload_pic.php?id="+id+"&table=profile&");
		$("#fileupload_"+id).toggle();
	}
	function startUpload1(id)
	{
		$("#ifr1_"+id).prop("src","upload_pic.php?id="+id+"&table=profile&");
		$("#fileupload1_"+id).toggle();
	}
	function selectColumns()
	{
		var selFields = [];
		$(".selCols:checked").each(function(id,field){
			selFields.push(field.id.split('_')[field.id.split('_').length-1]);
			
		});
		//alert(selFields.join());
		gArgs[ggname].eRequest = {cols:selFields.join()};
		grid[ggname].init(gArgs[ggname]);
	}
	function loadPic(obj)
	{
		//alert(obj);
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load(obj,function(){
                        selectButton(obj);
                });
	}	
	function RPage()
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load('profileAdmin.php');
	}
	function sendMsg()
	{
		var pars = {msg:encodeURIComponent($("#msg").val()),sendEmail : $("#emailSend").prop('checked'),sendSms : $("#smsSend").prop('checked'),sendAll : $("#allSend").prop('checked')};
		if($("#allSend").prop('checked'))
		{
			if(typeof whereObj[ggname] != 'undefined')
			{
				for(i in whereObj[ggname])
					pars[i] = whereObj[ggname][i];
			}
			else
				pars['all'] = 'all';
		}
		else
		{
			var profile_ids = [];
			$(".ajaxgrid_checkSelect").each(function(id,field){
				if($(field).prop('checked'))
					profile_ids.push($("#gname_profile-span-id-"+field.id.split('-')[2]).html());
			});
			if(profile_ids.length > 0)
			{
				pars['profile_ids'] = profile_ids.join();
			}
			else
			{
				alert('حداقل یک نفر می بایست انتخاب شود');
				return(false);
			}
		}
		$("#msgDiv").append("<img src='../img/status_fb.gif' id='khoonMsg' />");
		$.post("profileAdmin.php",pars,function(result){
			$("#khoonMsg").remove();
			alert('پیام با موفقیت ارسال گردید.');
		});
	}
	function sabt_new_card()
	{
		var obj = {"new_tedad":$("#new_tedad").val() };
		$("#new_vard_khoon").html('<img src="../img/status_fb.gif" >');
		console.log(obj);
		$.post("cards.php",obj,function(result){
			console.log(result);
			if($.trim(result)=='done')
			{
				$("#new_vard_khoon").html("ثبت با موفقیت انجام شد");
				alert("ثبت با موفقیت انجام شد");
				showCards();
			}
			else
				$("#new_vard_khoon").html("اعتبار شما کافی نیست");
		});
	}
	function view_clicked()
	{
		if($('#view_card_div').is(":visible"))
		{
			$('#view_card_div').hide();
			$("#view_card").html("مشاهده سفارش کارت");
		}
		else
		{
			$('#view_card_div').show();
                        $("#view_card").html("مخفی کردن سفارش کارت");
		}
	}
	
	function new_clicked()
        {
                if($('#new_card_div').is(":visible"))
                {
                        $('#new_card_div').hide();
                        $("#new_card").html("سفارش کارت جدید");
                }
                else
                {
                        $('#new_card_div').show();
                        $("#new_card").html("مخفی کردن سفارش کارت جدید");
                }
        }
        $(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
		var admin_acc='<?php echo($canWrite); ?>' ;
		if(admin_acc=='never')
			$("#msgDiv").children().attr("disabled","disabled");
        });
	function setNewCardEt(dobj)
	{
		var obj = $(dobj);
		//console.log(obj.prop('checked'));
		gArgs[ggname].eRequest['card_etebar_dec'] = (obj.prop('checked'))?'yes':'no';
		grid[ggname].eRequest['card_etebar_dec'] = (obj.prop('checked'))?'yes':'no';
	}
</script>
<div>
	اعتبار: <?php echo monize($prof->etebar); ?> ریال, اعتبار قابل خرید: <?php echo monize($prof->etebar-$prof->min_etebar); ?> ریال
	<button id="new_card" onclick="new_clicked();" >سفارش کارت جدید</button>
	<button id="view_card" onclick="view_clicked();" >مشاهده سفارش</button>
	<div id="new_card_div" style="display:none;" >
		<input type="nubmer" id="new_tedad" placeholder="تعداد" style="width:100px;" value="1" <?php echo ($se->detailAuth('all') || $se->detailAuth('doWrite'))?'':'readonly'; ?> >
		<button onclick="sabt_new_card()" >ثبت سفارش کارت</button>
		<span id="new_vard_khoon" ></span>
		<div class="notice" >
		توجه داشته باشید به ازای سفارش هر کارت مبلغ <?php echo trim($conf->card_new_mablagh)?$conf->card_new_mablagh:35000; ?> ریال از حساب شما کسر خواهد شد.
		</div>
	</div>
	<div id="view_card_div" style="display:none;"  >
		<div id="div_user_card_sefaresh" >
		</div>
	</div>
</div>
<div id="colSelector">
	<?php
		echo $columnList;
	?>
</div>
<div id="main_div_cards"></div>
<?php
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
	{
?>
<div>کارت جدید از اعتبار کم شود؟ <input type="checkbox" onclick="setNewCardEt(this);" id="card_etebar_dec" /></div>
<?php
	}
?>
