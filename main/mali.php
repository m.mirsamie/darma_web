<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
	include_once("../simplejson.php");
	$se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	function mpdate($inp)
	{
		return(($inp != '' && $inp != '0000-00-00 00:00:00')?jdate("H:i d / m / Y",strtotime($inp)):'----');
	}
	$user_id = isset($_REQUEST['user_id'])?(int)$_REQUEST['user_id']:(int)$_SESSION[$conf->app.'_user_id'];
	$iAm = ($user_id == (int)$_SESSION[$conf->app.'_user_id']);
	$this_user = new user_class((int)$_SESSION[$conf->app.'_user_id']);
	$company_group_id = $this_user->company_group_id;
	$is_master = $this_user->is_master;
	if(isset($_REQUEST['comment']))
	{
		$out = 'false';
		$my = new mysql_class;
		$comment = strip_tags(trim($_REQUEST['comment']));
		if($comment != '')
		{
			$ln = $my->ex_sqlx("insert into `comment_tbl` (`user_id`, `comment`, `reg_date`) values ($user_id,'$comment','".date("Y-m-d H:i:s")."')",FALSE);
			$id = $my->insert_id($ln);
			$my->close($ln);
			if($id > 0)
				$out = 'true';
			
		}
		die($out);
	}
	if(isset($_REQUEST['tmp']) && isset($_REQUEST['typ']) && $_REQUEST['typ']=='etebar_code')
	{
		//$mab = (int)$_REQUEST['tmp'];
		$kode = $_REQUEST['tmp'];
		$etebar = new etebar_class;
		$etebar->loadByKode($kode);
		$us_id = $_REQUEST['user_dest_id'];
		$user_sabt_id = $_SESSION[$conf->app.'_user_id'];
		$out= 'err';
		if($etebar->useIt($us_id,$user_sabt_id))
		{
			$tt = "/resid.php?from_user_id=$user_sabt_id&card_id=&typ=".urlencode('استفاده از پاداش')."&mablagh=".$etebar->mablagh."&tarikh=".urlencode(jdate("Y/m/d",strtotime(date("Y-m-d"))))."&user_id=$user_sabt_id&to_user_id=$us_id&padash_code=$kode";
			$tmp1 = explode("php",$_SERVER['REQUEST_URI']);
			$tmp=explode("/",$tmp1[0]);
			$msg ="http://".$_SERVER['HTTP_HOST'];
			for($i=0;$i<count($tmp)-1;$i++)
				$msg.='/'.$tmp[$i];
			$msg = file_get_contents($msg.$tt);
			$out = $msg;
		}
		die($out);
	}
	$u = new user_class($user_id);
	$p = new profile_class($user_id);
	$hasTer = $u->hasTerminal();
	$srcUser = ($se->detailAuth('all'))?$user_id:'';
?>
<script type="text/javascript" >
	var pcode=0;
	var cardnumber = '';
	var mablagh=0;
	var page ="<?php echo isset($_REQUEST['main'])? 'main': 'index'?>";
	var u_id = <?php echo $user_id; ?>;
	var srcUser = '<?php echo $srcUser; ?>';
	$(document).ready(function(){
		$.each($(".dateValue"),function(id,field){
	                Calendar.setup({
        		        inputField     :    field.id,
                		button         :    field.id,
	                	ifFormat       :    "%Y/%m/%d",
	        	        dateType           :    'jalali',
        	        	weekNumbers    : false
	                });			
		});
	});
	
// *****************************************     start of edit    ********************************
	function bring_details()
	{ 
	  $.get("mali2.php?user_id="+u_id+"&",function(data){$("#div1").html(data);});
	  $('#btn1').prop("value", " عدم نمایش ");
	  $('#btn1').attr("onClick", "del_details();");
	  $("#div1").show(300);
		
	}
	function bring_compDetails()
        { 
          $.get("mali2.php?user_id="+u_id+"&company_group_id=<?php echo $company_group_id; ?>&",function(data){$("#div1").html(data);});
          $('#btn3').prop("value", "عدم مشاهده");
          $('#btn3').attr("onClick", "del_compDetails();");
          $("#div1").show(300);
                
        }
	function bring_terDetails()
	{ 
	  $("#div1").load("terminal_details.php?user_id="+u_id+"&");
	  $('#btn2').prop("value", " عدم نمایش  ترمینال");
	  $('#btn2').attr("onClick", "del_terDetails();");
	  $("#div1").show(300);
		
	}
	function del_details()
	{      
		$("#div1").hide(300,function(){$("#div1").html('');});
		$('#btn1').prop("value", "مشاهده  جزیٔیات");
		$('#btn1').attr("onClick", "bring_details();");
	}
        function del_compDetails()
        {      
                $("#div1").hide(300,function(){$("#div1").html('');});
                $('#btn3').prop("value", "مشاهده جزئیات کاربران گروه");
                $('#btn3').attr("onClick", "bring_compDetails();");
        }
	function del_terDetails()
	{      
		$("#div1").hide(300,function(){$("#div1").html('');});
		$('#btn2').prop("value", "مشاهده جزئیات ترمینال");
		$('#btn2').attr("onClick", "bring_terDetails();");
	}

	function enteghal()
	{
		pcode = $.trim($("#pcode").val());
		cardnumber = $.trim($("#cardnumber").val());
		var p = {};
		if(pcode!='')
			p['pcode'] = pcode;
		else if(cardnumber!='')
			p['cardnumber'] = cardnumber;
		else
		{
			alert('حداقل یکی از کد کاربر یا شماره کارت مقصد را وارد کنید');
			return false;
		}
		p['source_user_id'] = srcUser;
		mablagh = $("#mablagh").val().replace(/,/g,'');
                /*
		if(pcode==="")
		{
			alert('کد پروفایل را وارد کنید');
			return false;
		}
		else*/ if(parseInt(mablagh,10)<=0)
		{
			alert('مبلغ صحیح وارد نشده است');
			return(false);
		}
		
		$("#msg_span").html('<img src="../img/status_fb.gif" >');
		$.get("profile.php",p,function(result){
                    
			if(result=='same')
				$("#msg_span").html('<div class="notice"  >انتقال اعتبار به خود امکان پذیر نمی باشد</div>');
			else if(result=='nouser')
				$("#msg_span").html('<div class="notice" > چنین کاربری وجود ندارد</div>');
			else if(result!='')
			{
				var txt = '<div class="notice" >ایا انتقال به '+result+' انجام شود؟</div>';
				txt+='<button onclick="setEteber_mali();" >موافقم</button>';
				txt+='<button onclick="noEteber();" >خیر</button>';
				$("#msg_span").html(txt);
			}
		});	
	}

	function setEteber_mali()
	{
		$("#msg_span").html('<img src="../img/status_fb.gif" >');
		$.get("profile.php?id="+pcode+"&cardnumber="+cardnumber+"&mablagh="+mablagh+"&"+srcUser,function(result){
			$("#msg_span").html('<img src="../img/status_fb.gif" >');
			if(result=='user_err')
			{
				$("#msg_span").html('<div class="notice" >کاربر درست انتخاب نشده است</div>');
			}
			else if(result=='etebar_err')
			{
				$("#msg_span").html('<div class="notice" >اعتبار شما جهت انتقال کافی نیست</div>');
			}
			else
			{
				//var mm = "resid.php?from_user_id="+result.from_user_id+"&card_id="+result.card+"&typ="+pm+"&mablagh="+result.mablagh+"&tarikh="+encodeURIComponent(result.tarikh)+"&user_id="+result.user_id+"&to_user_id="+result.to_user_id;
				$("#msg_span").html('<div class="msg" >انتقال انجام شد <a target="_blank"  href="'+result+'" >چاپ رسید</a><span onclick="refresh_p();" style="padding-right:5px;" class="pointer" >ادامه</span> </div>');
			}
		});
	}
	function noEteber()
	{
		$("#msg_span").html('');
	}
	function loadProfile(user_id)
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
	        $("#body").load('mali.php?user_id='+user_id+'&');
	}
	function refresh_p()
	{
		if(page=='index')
			openDialog('mali.php?user_id='+u_id,'پروفایل کاربر');
		else
			loadProfile(u_id);
	}
	function add_etebar()
	{
		var tmp = $("#sharzh_val").val();
		var typ = $("#etebar_code").prop("checked")? 'etebar_code':'banki';
		if(typ=='banki')
		{
			var mb = parseInt(tmp,10);
			if(mb>=1000 && !isNaN(mb) && typeof mb!='undefined')
			{
				$("#add_etebar_btn").after("<img src='../img/status_fb.gif' />");
				startBank(mb,-130);
			}
			else
				alert('مبلغ را به درستی وارد کنید');
		}
		else
		{
			$("#msg_span").html('<img src="../img/status_fb.gif" >');
			$.get("mali.php?tmp="+tmp+"&typ="+typ+"&user_dest_id="+u_id+"&",function(result){
				if(result!='err')
				{
					$("#msg_span").html('<div class="msg" >افزایش اعتبار با موفقیت انجام شد <a target="_blank" href="'+result+'" >رسید</a>&nbsp;<span class="pointer" onclick="refresh_p();"  ><u>ادامه</u></span></div>');
					//setTimeout('refresh_p();',3000);
				}
				else
					$("#msg_span").html('<div class="notice" >افزایش اعتبار با خطا مواجه گردید</div>');
			});
		}
	}

	function cha()
	{
		
		if($("#etebar_code").prop("checked"))
			$("#sharzh_span").html('کد اعتبار را وارد کنید');
		else if($("#banki").prop("checked"))
			$("#sharzh_span").html('مبلغ');
		$("#sharzh_val").val('');
	}
	

	function bring_comment()
	{	
		$("#scomment").attr("style","display:block");
		$("#btnp").attr("onClick","toggle_fun();");
	}
	function toggle_fun()
	{	
		$("#scomment").toggle(300);
		
	}
//****************************************    end of edit   *****************************************


       
</script>


<!--  ***********************************  start of edit ******************************************* -->
<div class="content">
<table style="font-family:tahoma;font-size:12px;padding-top:20px;width:100%" >
	<?php
		if($iAm || $se->detailAuth('all'))
		{
	?>
		<tr>
			<th class="round" colspan="2" style="color:#ffffff;background:#333333;padding:5px;">انتقال اعتبار</th>
		</tr>
		<tr>
		<div id="content" >
		<h2>
		<?php echo $u->fname.' '.$u->lname; ?><br/>اعتبار شما : <?php echo monize($p->etebar); ?> ریال , اعتبار قابل خرید: <?php echo monize($p->etebar-$p->min_etebar); ?> ریال
		</h2>
		
		</div>
		</tr>
		
		<tr>
			<td>انتقال اعتبار به دیگر کاربران:<input type="text" id="pcode" placeholder="کد کاربرمورد نظر" />
				<table width="100%">
					<tr>
						<td>
				یا<input type="text" id="cardnumber" placeholder="شماره کارت مقصد" />
				مبلغ:
				<input type="text" id="mablagh" placeholder="مبلغ" onkeypress='return numbericOnKeypress(event);' onkeyup="moneyField(this);" />
				ریال
						</td>
					</tr>
					<tr>
						<td>
							<span id="horoof_mablagh" ></span> ریال
						</td>
					</tr>
				</table>
			</td>
			<td align="left" >
				<button onclick="enteghal();" >انجام انتقال</button>
			</td>
		</tr>
	<?php
		}
	?>
		<tr>
			<th class="round" colspan="2" style="color:#ffffff;background:#333333;padding:5px;">افزایش اعتبار</th>
		</tr>
		<tr>
			<td>پرداخت آنلاین<input name="sharzh" id="banki" type="radio" onclick="cha()" checked="checked" >
				ورود کد اعتبار
				<input name="sharzh" id="etebar_code" onclick="cha()" type="radio" >
				<span id="sharzh_span" >مبلغ</span>
				<input type="text" id="sharzh_val" >
			</td>
			<td align="left" >
				<button id="add_etebar_btn" onclick="add_etebar();" >افزایش</button>
			</td>
		</tr>
		<tr>
		<tr>
			<td colspan="2" >
				<span id="msg_span" ></span>
			</td></tr>
		<tr><td><input style="font-family:tahoma;" type="button" id="btn1" value="مشاهده جزیٔیات" onClick="bring_details();"><?php if($hasTer){ ?><input style="font-family:tahoma;" type="button" id="btn2" value="مشاهده جزیٔیات ترمینال" onClick="bring_terDetails();"><?php } ?><?php if($is_master){ ?><input style="font-family:tahoma;" type="button" id="btn3" value="مشاهده جزئیات کاربران گروه" onClick="bring_compDetails();"><?php } ?></td></tr>
		<tr><td><div id="div1" style="display:none;"></div></td></tr>
		</tr>
	</table>
<!-- **************************************    end of edit ******************************************* -->
</div>
</div>
