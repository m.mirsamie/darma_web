<?php
	include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = isset($_REQUEST['user_id']) ?(int)$_REQUEST['user_id']:(int)$_SESSION[$conf->app.'_user_id'];
	$user = new user_class($user_id);
	$prof = new profile_class($user_id);
	$bool = TRUE;
	if(!isset($prof->id))
		$prof->id = profile_class::create($user_id);
	if(isset($_REQUEST['pcode']))
	{
		$u_id=audit_class::codeToId($_REQUEST['pcode']);
		if((int)$u_id!=(int)$_SESSION[$conf->app.'_user_id'])
		{
			$us= new user_class($u_id);
			die(isset($us->id)?$us->fname.' '.$us->lname:'nouser');
		}
		die('same');
	}
	if(isset($_REQUEST['mablagh']))
	{
		$u_id = audit_class::codeToId($_REQUEST['id']);
		$mablagh = (int)$_REQUEST['mablagh'];
		$own_profile = new profile_class((int)$_SESSION[$conf->app.'_user_id']);
		$des_profile = new profile_class($u_id);
		if(isset($own_profile->id) && isset($des_profile->id))
		{
			if($mablagh>$own_profile->etebar)
				die('etebar_err');
			else
			{
				profile_class::move($own_profile,$des_profile,$mablagh);
				die('done');
			}
		}
		else
			die('user_err');
	}
	if(isset($_REQUEST['tmp']) && isset($_REQUEST['typ']) && $_REQUEST['typ']=='etebar_code')
	{
		//$mab = (int)$_REQUEST['tmp'];
		$etebar_id = audit_class::codeToId($_REQUEST['tmp']);
		$etebar = new etebar_class($etebar_id);
		$user_id = $_SESSION[$conf->app.'_user_id'];
		die($etebar->useIt($user_id)?'true':'false');
	}
	if($prof->id>0)
	{
		$html = new htmlGenerator('profile',$prof->id);
		$html->notHtml=array('pre_user_id');
	}
	if(isset($_REQUEST['k_id']))
	{
		$id = (int)$_REQUEST['k_id'];	
		$qu = '';
		if($id>0)
		{
			$qu ='';
			$que ='';
			foreach($_REQUEST as $key=>$val)
				if($key!='k_id' && $key!='etebar')
					if($key!='fname' && $key!='lname' && $key!='pass')
						$qu.=($qu==''?'':',')." `$key`='$val'";
					else
						$que.=($que==''?'':',')." `$key`='$val'";
			$qu ="update `profile` set $qu where `user_id`=$id";
			$que ="update `user` set $que where `id`=$id";
			$m = new mysql_class;
			$m->ex_sqlx($qu);
			$m->ex_sqlx($que);
		}	
		die('done');
	}
?>
<script type="text/javascript" >
	var pcode=0;
	var mablagh=0;
	var u_id = <?php echo $user_id; ?>;
	$(document).ready(function(){
		
	});
	function sabt_k(k_id)
	{
		var req='';
		var rmp='';
		var user_id = <?php echo $prof->id;?>;
		$(".regdate").each(function(id,field) {
			rmp = $("#"+field.id).val();
			if($("#"+field.id).hasClass('dateValue'))
				rmp=jToM($("#"+field.id).val());
			if($("#"+field.id).hasClass('pass') && $("#"+field.id).val()!='')
				rmp=md5($("#"+field.id).val());
			if(($("#"+field.id).hasClass('pass') && $("#"+field.id).val()!='')|| !$("#"+field.id).hasClass('pass'))
    				req+= field.id+"="+rmp+"&";
		});
		$("#khoon").html('<img src="../img/status_fb.gif" >');
		$.get("profile.php?k_id="+k_id+"&"+req,function(result){
			//console.log(result);
			if(trim(result)=='done')
				$("#khoon").html('ثبت با موفقیت انجام شد');
		});
	}
	function loadProfile(user_id)
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load('profile.php?user_id='+user_id+'&');
	}
	function checkMobile(obj)
	{
		if(!isMobile($(obj).val()))
		{
			alert('تلفن همراه را درست وارد کنید');
			$(obj).val('');
		}
	}
	function enteghal()
	{
		pcode = $("#pcode").val();
		mablagh = $("#mablagh").val();
		if(mablagh=="")
		{
			alert('کد پروفایل را وارد کنید');
			return false;
		}
		else if(parseInt(mablagh,10)<=0)
		{
			alert('مبلغ صحیح وارد نشده است');
			return(false);
		}
		
		$("#msg_span").html('<img src="../img/status_fb.gif" >');
		$.get("profile.php?pcode="+pcode+"&",function(result){
			if(result=='same')
				$("#msg_span").html('انتقال اعتبار به خود امکان پذیر نمی باشد');
			else if(result=='nouser')
				$("#msg_span").html('چنین کاربری وجود ندارد');
			else if(result!='')
			{
				var txt = '<div class="notice" >ایا انتقال به '+result+' انجام شود؟</div>';
				txt+='<button onclick="setEteber();" >موافقم</button>';
				txt+='<button onclick="noEteber();" >خیر</button>';
				$("#msg_span").html(txt);
			}
		});	
	}
	function setEteber()
	{
		$("#khoon").html('<img src="../img/status_fb.gif" >');
		$.get("profile.php?id="+pcode+"&mablagh="+mablagh+"&",function(result){
			$("#khoon").html('');
			if(result=='done')
			{
				$("#msg_span").html('<div class="msg" >انتقال انجام شد</div>');
				console.log(u_id);
				setTimeout('refresh_p();',3000);
			}
			else if(result=='etebar_err')
				$("#msg_span").html('<div class="msg" >اعتبار شما جهت انتقال کافی نیست</div>');
			else if(result=='user_err')
				$("#msg_span").html('<div class="msg" >کاربر درست انتخاب نشده است</div>');
		});
	}
	function noEteber()
	{
		$("#msg_span").html('');
	}
	function md5(inp)
	{
		return(hex_md5(inp));
	}
	function add_etebar()
	{
		var tmp = $("#sharzh_val").val();
		var typ = $("#etebar_code").prop("checked")? 'etebar_code':'banki';
		if(typ=='banki')
		{
			startBank(tmp);
		}
		else
		{
			$("#msg_span").html('<img src="../img/status_fb.gif" >');
			$.get("profile.php?tmp="+tmp+"&typ="+typ+"&",function(result){
				//console.log("'"+result+"'");
				if(result=='true')
				{
					$("#msg_span").html('<div class="msg" >افزایش اعتبار با موفقیت انجام شد</div>');
					setTimeout('refresh_p();',3000);
				}
				else
					$("#msg_span").html('<div class="notice" >افزایش اعتبار با خطا مواجه گردید</div>');
			});
		}
	}
	function refresh_p()
	{
		if(typeof openDialog!='undefined')
			openDialog('profile.php?user_id='+u_id,'پروفایل کاربر');
		else
			loadprofile(u_id);
	}
	function cha()
	{
		
		if($("#etebar_code").prop("checked"))
			$("#sharzh_span").html('کد اعتبار را وارد کنید');
		else if($("#banki").prop("checked"))
			$("#sharzh_span").html('مبلغ');
		$("#sharzh_val").val('');
	}
</script>
<div>
	<h3>
		نام کاربری:
		<?php echo $user->user;  ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	کد کاربری شما:
	<?php echo audit_class::idToCode($user_id);  ?>
	</h3>
</div>
<div id="content" >
	<div style="font-family:tahoma;font-size:12px;" >
		<table style="font-family:tahoma;font-size:12px;width:100%;" >
			<tr>
				<th class="round" colspan="6" style="color:#ffffff;background:#333333;padding:5px;">اطلاعات کاربری</th>
			</tr>
			<tr>
				<td align="left">نام:</td>
				<td align="right">
					<input type="text" class="regdate" value="<?php echo (isset($user->fname)?$user->fname:''); ?>" id="fname"  placeholder="نام"  >
				</td>
				<td align="left">نام خانوداگی:
				</td>
				<td align="right"><input type="text" class="regdate" value="<?php echo (isset($user->lname)?$user->lname:''); ?>" id="lname"  placeholder="نام خانوادگی"  >
				</td>
				<td align="left">رمز عبور:
				</td>
				<td align="right"><input type="text" class="regdate pass" value="" id="pass"  placeholder="خالی باشد رمز قبلی می‌ماند"  ></td>
			</tr>
			<tr>
				<td align="left">ایمیل:</td>
				<td align="right">
					<input type="text" class="regdate" value="" id="email"  placeholder="ایمیل"  >
				</td>
				<td align="left">
		تاریخ تولد:
				</td>
				<td align="right"><input type="text" class="regdate dateValue" value="<?php echo (isset($user->lname)?$prof->tarikh_tavalod:''); ?>" id="tarikh_tavalod"  placeholder="تاریخ تولد"  >
				</td>
				<td align="left">تلفن همراه:
				</td>
				<td align="right"><input type="text" class="regdate" value="<?php echo (isset($prof->mob)? $prof->mob:''); ?>" id="mob" onblur="checkMobile(this);" placeholder="تلفن همراه"  onkeypress='return numbericOnKeypress(event);' ></td>
			</tr>
			<tr>
				<td align="left">نشانی:
				</td>
				<td align="right"><input type="text" class="regdate" value="<?php echo (isset($prof->addr)? $prof->addr:''); ?>" id="addr" placeholder="نشانی">
				</td>
				<td align="left">کدپستی:
				</td>
				<td align="right"><input type="text" class="regdate" value="<?php echo (isset($prof->codePosti)? $prof->codePosti:''); ?>" id="codePosti" placeholder="کدپستی">
				</td>
				<td align="left">تلفن ثابت:
				</td>
				<td align="right"><input type="text" class="regdate" value="<?php echo (isset($prof->tel)? $prof->tel:''); ?>" id="tel" placeholder="تلفن ثابت" onkeypress='return numbericOnKeypress(event);' >
				</td>
			</tr>
		</table>
	</div>
	<div id="khoon" style="color:red;" >
	</div>
	<div>
		<button onclick="sabt_k(<?php echo $user->id; ?>);" >بروز رسانی</button>
	</div>
	<table style="font-family:tahoma;font-size:12px;padding-top:20px;width:100%" >
		<tr>
				<th class="round" colspan="2" style="color:#ffffff;background:#333333;padding:5px;">انتقال اعتبار</th>
			</tr>
		<tr>
			<td>
				انتقال اعتبار به دیگر کاربران:
				<input type="text" id="pcode" placeholder="کد کاربرمورد نظر" >
				مبلغ:
				<input type="text" id="mablagh" placeholder="مبلغ" onkeypress='return numbericOnKeypress(event);' >
				ریال
			</td>
			<td align="left" >
				<button onclick="enteghal();" >انجام انتقال</button>
			</td>
		</tr>
		<tr>
			<th class="round" colspan="2" style="color:#ffffff;background:#333333;padding:5px;">افزایش اعتبار</th>
		</tr>
		<tr>
			<td>
				پرداخت آنلاین
				<input name="sharzh" id="banki" type="radio" onclick="cha()" checked="checked" >
				ورود کد اعتبار
				<input name="sharzh" id="etebar_code" onclick="cha()" type="radio" >
				<span id="sharzh_span" >مبلغ</span>
				<input type="text" id="sharzh_val" >
			</td>
			<td align="left" >
				<button onclick="add_etebar();" >افزایش</button>
			</td>
		</tr>
		<tr>
		<tr>
			<td colspan="2" >
				<span id="msg_span" ></span>
			<td>
		</tr>
	</table>
</div>
