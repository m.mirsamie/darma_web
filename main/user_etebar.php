<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
        if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	function loadUser($id)
	{
		$user = new user_class($id);
		return(isset($user->id)?$user->fname.' '.$user->lname:'----');
	}
	function loadDate($inp)
	{
		return($inp!='0000-00-00 00:00:00'?jdate("H:i:s Y/m/d",strtotime($inp)):'----');
	}
	function loadTrans($tid)
	{
		$tid = (int)$tid;
		return($tid>0?$tid:'----');
	}
        $gname = "gname_user_etebar";
	$input =array($gname=>array('table'=>'user_etebar','div'=>'main_div_user_etebar'));
        $xgrid = new xgrid($input);
	$xgrid->whereClause[$gname] = " company_id = ".$conf->company_id." order by regdate desc";
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = 'کاربر';
	$xgrid->column[$gname][1]['cfunction'] = array('loadUser');
	$xgrid->column[$gname][1]['search'] = 'list';
	$xgrid->column[$gname][1]['searchDetails'] = columnListLoader('user| company_id = '.$conf->company_id,array('id','fname','lname'));
	$xgrid->column[$gname][2]['name'] = 'اعمال کننده';
	$xgrid->column[$gname][2]['cfunction'] = array('loadUser');
	$xgrid->column[$gname][3]['name'] = 'کاهش/افزایش';
	$xgrid->column[$gname][3]['clist'] = array(-1=>'کاهش',1=>'افزایش');
	$xgrid->column[$gname][3]['search'] = 'list';
        $xgrid->column[$gname][3]['searchDetails'] = array(0=>'همه',-1=>'کاهش',1=>'افزایش');
	$xgrid->column[$gname][4]['name'] = 'مبلغ';
	$xgrid->column[$gname][4]['cfunction'] = array('monize');
	$xgrid->column[$gname][5]['name'] = 'نوضیحات';
	$xgrid->column[$gname][5]['search'] = 'text';
	$xgrid->column[$gname][6]['name'] = 'تاریخ';
	$xgrid->column[$gname][6]['cfunction'] = array('loadDate');
	$xgrid->column[$gname][6]['search'] = 'dateValue_minmax';
	$xgrid->column[$gname][7]['name'] = 'شماره تراکنش';
	$xgrid->column[$gname][7]['cfunction'] = array('loadTrans');
	$xgrid->column[$gname][8]['name'] = '';
        $xgrid->canAdd[$gname] = FALSE;
        $xgrid->canDelete[$gname] = FALSE;
        $xgrid->canEdit[$gname] = FALSE;
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);
?>
<script type="text/javascript" >
        $(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
        });
</script>
<div id="main_div_user_etebar"></div>
