<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$permission=array();
	$cl2=new mysql_class;
	$canWrite='never';
	if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;
				}

			}
		}
		if(in_array("factor_origin.php",$permission))
			$canWrite='limited';
		
	}
	$isAdmin = $se->detailAuth('all');
	function loadUser($inp)
	{
		$user = new user_class($inp);
		return(isset($user->id)? $user->fname.' '.$user->lname:'حذف شده');
	}
	function tarikh($inp)
	{
		return($inp!='0000-00-00 00:00:00' ? jdate('H:i -- d / m / Y',strtotime($inp)):'نامعلوم');
	}
	function tarikhBack($inp)
	{
		return(audit_class::hamed_pdateBack($inp));
	}
	function dariaft($inp)
	{
		$out='نامعلوم';
		if((int)$inp==0)
			$out = '<div class="msg" >اینترنتی</div>';
		else if((int)$inp==1)
			$out = '<div class="notice" >درب منزل</div>';
		return($out);
	}
	function edit($table,$id,$field,$val,$fn,$gname)
	{
		$out = TRUE;
		$id = (int)$id;
		$mysql = new mysql_class;
		if($field == 'isTasfie')
		{
			$f = new factor_class((int)$id);
			if($f->isTasfie == -1)
			{
				$mysql->ex_sqlx("update `$table` set `$field` = '$val' where `id` = $id");
				if($val==1)
				{
					$mysql->ex_sql("select `user_id`,`jamKol` from `factor` where `id` = $id",$q);
					if(isset($q[0]))
					{
						$p = new profile_class((int)$q[0]['user_id']);
						$u = new user_class((int)$q[0]['user_id']);
						if(isset($p->pre_user_id) && $p->pre_user_id > 0)
						{
							$pp = new profile_class((int)$p->pre_user_id);
							if(isset($pp->down_user_poorsant))
							{
								$ae = ((int)$q[0]['jamKol'])*((int)$pp->down_user_poorsant/100);
								$pp->addEtebar($ae,'بابت خرید '.$u->fname.' '.$u->lname.' فاکتور شماره '.$id);
							}
						}
					}
				}
			}
			else
				$out = "FALSE| تغییر وضعیت پرداخت پس از تسویه و یا ابطال امکان پذیر نیست";
		}
		else
			$mysql->ex_sqlx("update `$table` set `$field` = '$val' where `id` = $id");
		return($out);
	}
	function det($inp)
	{
		$factor = new factor_class($inp);
		$out = '---';
		if(isset($factor->isTasfie) && (int)$factor->isTasfie==0)
			$out='<span class="msg pointer" onclick="loadFactorDet('.$inp.');" >مشاهده</span>';
		return($out);
	}
	function factor_back($inp)
	{
		$factor = new factor_class($inp);
		$out ='--';
		//if((int)$factor->status==3)
			$out = '<div class="msg pointer" onclick="loadFactorDetBack('.$inp.');" >فرم‌برگشت</div>';
		return($out);
	}
	function loadShomare($id)
	{
		$conf = new conf;
		$se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
		$out = $id;
		if($se->detailAuth('all'))
		{
			$my = new mysql_class;
			$my->ex_sql("select `id` from `factor_det_back` where `factor_id` = $id and `en` = 1 limit 1",$q);
			if(isset($q[0]))
				$out = "<u><span class='bargashtFactor pointer' onclick=\"showBargashtFactor($id);\">$id</span></u>";
		}
		return($out);
	}
        $gname = 'grid1';
        $input =array($gname=>array('table'=>'factor','div'=>'main_div_factor_origin'));
        $xgrid = new xgrid($input);
	//$xgrid->echoQuery = TRUE;
	for($i=0;$i<count($xgrid->column[$gname]);$i++)
		$xgrid->column[$gname][$i]['access']='a';
	$xgrid->whereClause[$gname] = ' `en`=1 '.((!$isAdmin)?" and `user_id` = $user_id ":'').' order by tarikh desc';
	//$xgrid->echoQuery = TRUE;
	$jam_mablagh = 0;
	$jam_hazine_ersal = 0;
	$my = new mysql_class;
	if(isset($_REQUEST['jamkol']))
	{
		$req = $_REQUEST;
		$wer = '';
		foreach($req as $key=>$value)
			if($key != 'jamkol')
			{
				if(strpos($key,"-start")!==FALSE)
					 $wer .= (($wer!='')?' and ':'').'`'.str_replace('-start','',$key).'` >= \''.$value.'\'';
				else if(strpos($key,"-stop")!==FALSE)
					$wer .= (($wer!='')?' and ':'').'`'.str_replace('-stop','',$key).'` <= \''.$value.'\'';
				else
					$wer .= (($wer!='')?' and ':'').'`'.$key.'` = \''.$value.'\'';
			}
		if($wer!='')
			$wer = "where $wer";
		$my->ex_sql("select sum(jamKol) as jkol , sum(hazineErsal) as hazer from factor ".$wer,$q);
		if(isset($q[0]))
		{
			$jam_mablagh = (int)$q[0]['jkol'];
			$jam_hazine_ersal = (int)$q[0]['hazer'];
		}
		$out = 'جمع کل : '.monize($jam_mablagh).'ریال , هزینه ارسال: '.monize($jam_hazine_ersal).'ریال';
		//$out .= '<br/>select sum(jamKol) as jkol , sum(hazineErsal) as hazer from factor '.$wer;
		die($out);
	}
	else
	{
		$wer = $xgrid->whereClause[$gname];
		$my->ex_sql("select sum(jamKol) as jkol , sum(hazineErsal) as hazer from factor where ".$wer,$q);
		if(isset($q[0]))
		{
			$jam_mablagh = (int)$q[0]['jkol'];
			$jam_hazine_ersal = (int)$q[0]['hazer'];
		}
	}
        $xgrid->column[$gname][0]['name'] = 'شماره فاکتور';
	$xgrid->column[$gname][0]['cfunction'] = array('loadShomare');
	$xgrid->column[$gname][0]['search']='exact';
        $xgrid->column[$gname][1]['name'] = 'خریدار';
	
	$xgrid->column[$gname][1]['cfunction'] = array('loadUser');
	$xgrid->column[$gname][2]['name'] = 'تاریخ سفارش';
	$xgrid->column[$gname][2]['cfunction'] = array('tarikh','tarikhBack');
	$xgrid->column[$gname][2]['search'] = 'dateValue_minmax';
	//$xgrid->column[$gname][2]['sort'] =TRUE;
	$xgrid->column[$gname][3]['name'] = '';
	$xgrid->column[$gname][4]['name'] = 'زمان تحویل';
	$xgrid->column[$gname][4]['cfunction'] = array('tarikh','tarikhBack');
	$xgrid->column[$gname][4]['search'] = 'dateValue_minmax';
	$xgrid->column[$gname][5]['name'] = 'نحوه تحویل';
	$xgrid->column[$gname][6]['name'] = 'مکان تحویل';
	$xgrid->column[$gname][7]['name'] = 'مبلغ';
	$xgrid->column[$gname][8]['name'] = 'هزینه ارسال';
	$xgrid->column[$gname][9]['name'] = 'توضیحات';
	$xgrid->column[$gname][9]['access'] = null;
	$xgrid->column[$gname][10]['name'] = 'وضعیت';
	$xgrid->column[$gname][10]['clist'] =array(0=>'پرداخت نشده',1=>'تسویه',2=>'باطل شده');
	$xgrid->column[$gname][10]['access'] = null;
	$xgrid->column[$gname][10]['search'] = 'list';
	$xgrid->column[$gname][10]['searchDetails'] =array(-1=>'',0=>'پرداخت نشده',1=>'تسویه',2=>'باطل شده');
	$xgrid->column[$gname][11]['name'] = 'پرداخت';
	//$xgrid->column[$gname][11]['cfunction'] = array('dariaft');
	$xgrid->column[$gname][11]['access'] = 'a';
	$xgrid->column[$gname][11]['clist'] = array(0=>'اینترنتی',1=>'درب منزل');
	$xgrid->column[$gname][11]['search'] = 'list';
	$xgrid->column[$gname][11]['searchDetails'] = array(-1=>'',0=>'اینترنتی',1=>'درب منزل');
	$xgrid->column[$gname][12]['name'] = 'موقعیت';
	$xgrid->column[$gname][12]['clist'] =array(1=>'ارسال نشده',2=>'ارسال شده',3=>'تحویل مشتری شده');
	$xgrid->column[$gname][12]['access'] = null;
	$xgrid->column[$gname][12]['search'] = 'list';
	$xgrid->column[$gname][12]['searchDetails'] =array(0=>'همه',1=>'ارسال نشده',2=>'ارسال شده',3=>'تحویل مشتری شده');
	$xgrid->column[$gname][13]['name'] = 'منطقه';
	$xgrid->column[$gname][13]['clist'] = columnListLoader('mantaghe');
	$xgrid->column[$gname][13]['access'] = null;
	$xgrid->column[$gname][14]['name'] = 'موزع';
	$xgrid->column[$gname][14]['clist'] = columnListLoader('transporter');
	$xgrid->column[$gname][15]['name'] = 'پرداخت شده';
        $xgrid->column[$gname][16]['name'] = 'نوع خرید';
        $order_type = array(0=>'همه',1=>'سایت', 2=>'آندروید',3=>'آیفون'  );
        $xgrid->column[$gname][16]['clist'] =$order_type;
        $xgrid->column[$gname][16]['search']='list';
        $xgrid->column[$gname][16]['searchDetails']=$order_type;
	if($isAdmin)
	{
		$xgrid->column[$gname][17] = $xgrid->column[$gname][0];
		$xgrid->column[$gname][17]['name'] = 'جزئیات';
		$xgrid->column[$gname][17]['cfunction'] = array('det');
		$xgrid->column[$gname][17]['access'] = 'q';
	}
	else
	{
		if($se->detailAuth('all') || $se->detailAuth('doWrite'))
		{
			$xgrid->column[$gname][1]['search'] = 'list';
			$xgrid->column[$gname][1]['searchDetails'] =columnListLoader('user|company_id='.$conf->company_id,array('id','lname'));
			$xgrid->column[$gname][14]['search'] = 'list';
			$xgrid->column[$gname][14]['searchDetails'] = columnListLoader('transporter');
			$xgrid->column[$gname][14]['access'] = null;
			$xgrid->column[$gname][17] = $xgrid->column[$gname][0];
			$xgrid->column[$gname][17]['name'] = 'فرم برگشت';
			$xgrid->column[$gname][17]['cfunction'] = array('factor_back');
			$xgrid->column[$gname][17]['access'] = 'q';
			$xgrid->canEdit[$gname]=TRUE;
		}
		
	}
		     
	$xgrid->editFunction[$gname] = 'edit';
        $xgrid->canAdd[$gname] = FALSE;
	$xgrid->canDelete[$gname] = FALSE;
       // $xgrid->canDelete[$gname] = $canDo;
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);	
?>
<style>
	.bargashtFactor
	{
		color : red;
		font-weight : bold;
	}
	#jam
	{
		background: #eeeeee;
		padding : 10px;
		border : 2px solid #bbbbbb;
		font-size:20px;
	}
</style>
<script>
	var gname = '<?php echo $gname; ?>';
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
		gArgs[gname]['afterLoad'] = function(){
			jamkol();
		};
        });
	function loadFactorDet(factor_id)
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
	        $("#body").load('factor_det_edit.php?factor_id='+factor_id+'&');
	}
	function loadFactorDetBack(factor_id)
	{
		openDialog('factor_det_back.php?factor_id='+factor_id+"&",{title:'برگشت فاکتور '+factor_id,width:950,height:700});
	}
	function showBargashtFactor(factor_id)
	{
		openDialog('factor_det_back.php?admin=true&factor_id='+factor_id+"&",{title:'برگشت فاکتور '+factor_id,width:950,height:700});
	}
	function jamkol()
	{
		var p = {"jamkol":1,"en":1<?php echo !$isAdmin?',"user_id":'.$user_id:''; ?>};
		var tmp = whereObj[gname];
		for(i in tmp)
		{
			if(i.indexOf('-start') >0 || i.indexOf('-stop') >0)
				p[i] = JalaliDate.jalaliToGregorian(tmp[i].split('/')[0],tmp[i].split('/')[1],tmp[i].split('/')[2]).join('-');
			else	
				p[i] = tmp[i];
		}
		$.get("factor_origin.php",p,function(result){
			$("#jam").html(result);
		});
	}
</script>
<div id="content">
	<div id="main_div_factor_origin">
	</div>
	<div id="jam">
	<?php
		echo 'جمع کل : '.monize($jam_mablagh).'ریال , هزینه ارسال: '.monize($jam_hazine_ersal).'ریال';
	?>
	</div>
</div>
