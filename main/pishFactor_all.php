<?php
	include_once("../kernel.php");
	$SESSION = new session_class;
	register_shutdown_function('session_write_close');
	session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
	$se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
	if(!$se->can_view)
			die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$isAdmin = $se->detailAuth('all');
	function loadUser($inp)
	{
		$user = new user_class($inp);
		return(isset($user->id)? $user->fname.' '.$user->lname:'حذف شده');
	}
	function tarikh($inp)
	{
		return($inp!='0000-00-00 00:00:00' ? jdate('H:i -- d / m / Y',strtotime($inp)):'نامعلوم');
	}
	function tarikhBack($inp)
	{
		return(audit_class::hamed_pdateBack($inp));
	}
	function dariaft($inp)
	{
		$out='نامعلوم';
		if((int)$inp==0)
			$out = '<div class="msg" >اینترنتی</div>';
		else if((int)$inp==1)
			$out = '<div class="notice" >درب منزل</div>';
		return($out);
	}
	function loadPish($inp)
	{
		$out = '<span class="pointer msg" onclick="openDialog(\'sabadPreview.php?factor_id='.$inp.'&\',\'مشاهده پیش فاکتور\',{width:800,height:500})" >ادامه خرید</span>';
		return($out);
	}
	$ptarikh = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." -3 day"));
    $gname = 'grid1';
    $input =array($gname=>array('table'=>'factor','div'=>'main_div_factor1'));
    $xgrid = new xgrid($input);
	//$xgrid->alert = TRUE;
	for($i=0;$i<count($xgrid->column[$gname]);$i++)
		$xgrid->column[$gname][$i]['access']='a';
	$xgrid->whereClause[$gname] = ' `en`=0 '.((!$isAdmin)?" and `user_id` = $user_id and `tarikh`>='$ptarikh'":'');
	//$xgrid->echoQuery = TRUE;
    $xgrid->column[$gname][0]['name'] = 'شماره فاکتور';
    $xgrid->column[$gname][1]['name'] = '';
	$xgrid->column[$gname][2]['name'] = 'تاریخ سفارش';
	$xgrid->column[$gname][2]['cfunction'] = array('tarikh','tarikhBack');
	//$xgrid->column[$gname][2]['sort'] =TRUE;
	$xgrid->column[$gname][3] = $xgrid->column[$gname][0];
	$xgrid->column[$gname][3]['name'] = 'ادامه خرید';
	$xgrid->column[$gname][3]['cfunction'] = array('loadPish');
	$xgrid->column[$gname][3]['access']='a';
	$xgrid->column[$gname][4]['name'] = '';
	$xgrid->column[$gname][5]['name'] = '';
	$xgrid->column[$gname][6]['name'] = '';
	$xgrid->column[$gname][7]['name'] = '';
	$xgrid->column[$gname][8]['name'] = '';
	$xgrid->column[$gname][9]['name'] = '';
	$xgrid->column[$gname][10]['name'] = '';
	$xgrid->column[$gname][11]['name'] = '';
	$xgrid->column[$gname][12]['name'] = '';
	$xgrid->column[$gname][13]['name'] = '';
	$xgrid->column[$gname][14]['name'] = '';
	$xgrid->column[$gname][15]['name'] = '';
	$xgrid->column[$gname][16]['name'] = '';
	$xgrid->column[$gname][17]['name'] = '';
 
       // $xgrid->canDelete[$gname] = $canDo;
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);	
?>
<style>
	.bargashtFactor
	{
		color : red;
		font-weight : bold;
	}
</style>
<script>
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
        });
	function loadFactorDet(factor_id)
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
	        $("#body").load('factor_det_edit.php?factor_id='+factor_id+'&');
	}
	function loadFactorDetBack(factor_id)
	{
		openDialog('factor_det_back.php?factor_id='+factor_id+"&",{title:'برگشت فاکتور '+factor_id,width:950,height:700});
	}
	function showBargashtFactor(factor_id)
	{
		openDialog('factor_det_back.php?admin=true&factor_id='+factor_id+"&",{title:'برگشت فاکتور '+factor_id,width:950,height:700});
	}
</script>
<div id="content">
	<div id="main_div_factor1">
	</div>
</div>
