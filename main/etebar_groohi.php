<?php
        include_once("../kernel.php");
	$zamime = '';
	$script = '';
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	$se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
	$msg='';
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	function getKode($length = 16) 
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++)
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		return $randomString;
	}
	if(isset($_FILES['imageFile']))
	{
		$tmp_target_path = "../img";
		$ext = explode('.',basename( $_FILES['imageFile']['name']));
		$ext = $ext[count($ext)-1];
		if(strtolower($ext)=='jpg' || strtolower($ext)=='png')
		{
			$target_path =$tmp_target_path."/".basename( $_FILES['imageFile']['name']).'.'.$ext;
			if(move_uploaded_file($_FILES['imageFile']['tmp_name'], $target_path))
				$zamime = $target_path;
		}	
	}
	if(isset($_REQUEST['group_name']))
	{
                $group_name = $_REQUEST['group_name'];
                $group_mablagh = (int)$_REQUEST['group_mablagh'];
                $group_tedad = (int)$_REQUEST['group_tedad'];
		$ok = FALSE;
		$mablagh_kol = $group_mablagh * $group_tedad;
		if(!$se->detailAuth('all'))
		{
			$pr = new profile_class($user_id);
			if($mablagh_kol>0 &&  ($pr->etebar-$pr->min_etebar)>=$mablagh_kol)
			{
				$toz="کاهش اعتبار بایت  تولید پاداش  به مبلغ $group_mablagh به تعداد $group_tedad ";
				$pr->decEtebar($mablagh_kol,$user_id,$toz);	
				$ok = TRUE;
			}
			else
			{
				$ok = FALSE;
				$script='alert("موجودی شما کافی نیست");';
			}
		}
		else
			$ok = $se->detailAuth('all');
		if($ok && $mablagh_kol>0)
		{
			$q1 = "insert into `etebar` (`name`,`mablagh`,`creator_user_id`,`kode`,`zamime`) values ";
			$q = '';
			for($i = 0;$i < $group_tedad;$i++)
			{
				$kode = getKode();
				if(etebar_class::repeatKode($kode))
					$q .=(($q != '')?',':'') . "('$group_name',$group_mablagh,$user_id,'$kode','$zamime')";
				else
					$i--;
			}
			$my = new mysql_class;
			$my->ex_sqlx($q1.$q);
			$tarikh=urlencode(hamed_pdateBack2(date("Y-m-d H:i:s")));
			$typ =urlencode("تولید پاداش به نام $group_name");
			$tt="/resid.php?from_user_id=$user_id&card_id=&typ=$typ&mablagh=$mablagh_kol&tarikh=$tarikh&user_id=$user_id&to_user_id=-1&";
			$tmp = explode("/",$_SERVER['REQUEST_URI']);
			$msg ="http://".$_SERVER['HTTP_HOST'];
			for($i=0;$i<count($tmp)-1;$i++)
				$msg.='/'.$tmp[$i];
			$msg = file_get_contents($msg.$tt);
			$msg = "<div><a target='_blank' href='$msg' >چاپ رسید</a> <span class='pointer' onclick='window.parent.RPage();' ><u> ادامه</span></u></div>";
			$script ="";// "window.parent.RPage();";
		}
	}
?>
<html>
        <head>
                <script src="../js/jquery.min.js"></script>
                <script>
			$(document).ready(function(){
				<?php echo $script; ?>	
			});
                        function send1()
                        {
                                document.getElementById("frm25").submit();
                        }
                </script>
		<link rel="stylesheet" href="../css/style.css" >	
        </head>
        <body dir="rtl">
		<form method="post" enctype="multipart/form-data" name='frm25' id='frm25'>
		<table width="100%">
			<tr style="background:#000000;color:#ffffff;">
				<th colspan="7">
					ثبت گروهی
				</th>
			</tr>
			<tr>
				<td>
					نام اعتبار : 
				</td>
				<td>
					<input id="group_name" name="group_name" placeholder="نام اعتبار" />
				</td>
				<td>
					مبلغ :
				</td>
				<td>
					<input id="group_mablagh" name="group_mablagh" placeholder="مبلغ اعتبار" />
				</td>
			</tr>
			<tr>
				<td>
					تعداد :
				</td>
				<td>
					<input id="group_tedad" name="group_tedad" placeholder="تعداد" />
				</td>
				<td>
		                        <input type="file" id="imageFile" name="imageFile"/>
				</td>
				<td>
					<button>
						ثبت
					</button>
				</td>
			</tr>
		</table>
		</form>
		<div>
			<?php echo $msg; ?>
		</div>
		<?php if(!$se->detailAuth('all')) { ?>
		<div class="notice"  >
			در صورت تعریف  پاداش به همان میزان از اعتبار شما کسر خواهد شد
		</div>
		<?php }  ?>
	</body>
</html>
