<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
//	if(!isset($_SESSION[$conf->app.'_user_id']))
//                die('شما به این صفحه دسترسی ندارید');
	function mpdate($inp)
	{
		return(($inp != '' && $inp != '0000-00-00 00:00:00')?jdate("H:i d / m / Y",strtotime($inp)):'----');
	}
	function loadMande($id)
	{
		$f = new user_etebar_class((int)$id);
		$out = 0;
		$conf = new conf;
		$user_id = $f->user_id;//isset($_REQUEST['user_id'])?(int)$_REQUEST['user_id']:(int)$_SESSION[$conf->app.'_user_id'];
		$my = new mysql_class;
		$my->ex_sql("select sum(`typ`*`mablagh`) as `mande` from `user_etebar` where `user_id` = $user_id and `regdate` <= '".$f->regdate."' order by `regdate`",$q);
		if(isset($q[0]))
			$out = (int)$q[0]['mande'];
		return(monize($out));
	}
	function loadUser($id)
	{
		$user = new user_class((int)$id);
		return(isset($user->id)?$user->fname.' '.$user->lname:'----');
	}
	function loadPos($in)
	{
		return($in == -1?"کاهش":"افزایش");
	}
	function loadCompany($id)
	{
		$c = new company_class($id);
		return(isset($c->id)?$c->name:'----');
	}
	$wer = "1=0";
        $gname = "gname_mali";
	$input = array($gname=>array('table'=>'user_etebar','div'=>'main_div_mali'));
        $xgrid = new xgrid($input);
	$xgrid->eRequest[$gname] = array('terminal_id'=>$_REQUEST['terminal_id']);
	if(isset($_REQUEST['terminal_id']))
		$wer = " transaction_id in (select id from ".$conf->poolDB.".transactions where ter_id = ".$_REQUEST['terminal_id'].") ";
	$xgrid->whereClause[$gname] = "$wer order by `regdate` desc";
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = 'کاربر';
	$xgrid->column[$gname][1]['cfunction'] = array('loadUser');
	$xgrid->column[$gname][1]['search'] = 'list';
	$xgrid->column[$gname][1]['searchDetails'] = columnListLoader('user|not user like \'mehrdad%\' and company_id = '.$conf->company_id,array('id','fname','lname'));
	$xgrid->column[$gname][2]['name'] = '';
	$xgrid->column[$gname][3]['name'] = 'افزایش/کاهش';
	$xgrid->column[$gname][3]['search'] = 'list';
	$xgrid->column[$gname][3]['searchDetails'] = array(0=>'همه',-1=>'کاهش',1=>'افزایش');
	$xgrid->column[$gname][3]['cfunction'] = array('loadPos');
	$xgrid->column[$gname][4]['name'] = 'مبلغ(ریال)';
	$xgrid->column[$gname][4]['cfunction'] = array('monize');
	$xgrid->column[$gname][5]['name'] = 'توضیحات';
	$xgrid->column[$gname][6]['name'] = 'تاریخ ثبت';
	$xgrid->column[$gname][6]['cfunction'] = array('mpdate');
	$xgrid->column[$gname][6]['search'] = 'dateValue_minmax';
	$xgrid->column[$gname][7] = $xgrid->column[$gname][0];
	$xgrid->column[$gname][7]['name'] = 'مانده(ریال)';
	$xgrid->column[$gname][7]['cfunction'] = array('loadMande');
	$xgrid->column[$gname][8]['name'] = 'کمپانی';
	$xgrid->column[$gname][8]['cfunction'] = array('loadCompany');
	$xgrid->column[$gname][8]['search'] = 'list';
	$xgrid->column[$gname][8]['searchDetails'] = columnListLoader('company');
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);
?>
<!doctype html>
<html lang="fa">
	<head>
		<meta charset="utf-8">
		<title><?php echo $conf->title;  ?></title>
		<link rel="stylesheet" href="../css/jquery-ui.css">
		<link rel="stylesheet" href="<?php echo (isset($kala_abarGroup->css) && trim($kala_abarGroup->css)!='')?trim($kala_abarGroup->css):'../css/style.css';?>">
		<link rel="stylesheet" href="../css/jquery.lightbox-0.5.css">
<!--		<link rel="stylesheet" href="../css/jquery.tooltip.css"> -->
		<link rel="stylesheet" href="../css/xgrid.css">
                <link rel="stylesheet" type="text/css" media="all" href="../js/cal/skins/aqua/theme.css" title="Aqua" />
		<script src="../js/jquery.min.js"></script>
<!--
		<script src="../js/jquery.slides.min.js"></script>
-->
		<script src="../js/jquery.cycle.all.js"></script>
		<script src="../js/jquery.lightbox-0.5.min.js"></script>
		<script src="../js/jquery-ui.js"></script>
		<script src="../js/date.js" ></script>
		<script src="../js/inc.js"></script>
		<script src="../js/md5.js"></script>
		<script src="../js/tmp.js" ></script>
		<script src="../js/grid.js"></script>
		<script src="../js/weather_translation.js"></script>
                <script type="text/javascript" src="../js/cal/jalali.js"></script>
                <script type="text/javascript" src="../js/cal/calendar.js"></script>
                <script type="text/javascript" src="../js/cal/calendar-setup.js"></script>
                <script type="text/javascript" src="../js/cal/lang/calendar-fa.js"></script>	
		<script src="../js/gistfile1.js"></script>
		<script type="text/javascript" >
			$(document).ready(function(){
				var args=<?php echo $xgrid->arg; ?>;
				intialGrid(args);
			});
		</script>
		<style>
			#timer
			{
				border : 1px #eaeaea solid;
                                background : #ffffff;
                                height : 50px;
			}
			.ui-tabs { direction: rtl; }
			.ui-tabs .ui-tabs-nav li.ui-tabs-selected,
			.ui-tabs .ui-tabs-nav li.ui-state-default {float: right; }
			.ui-tabs .ui-tabs-nav li a { float: right; }
			.leftborderBlue{
				border-left:solid 1px #a7d7f9;
				width:200px;
			}
			li
			{
				font-family:tahoma;
			}
			.sabad_mojoodi
			{
				font-size:12px;
				color:red;
			}
			.shegeftKalaSelected
			{
				color:red;
			}
			div#wrapper {
			    margin:0px;
			    width:800px;
			    background:#FFF;
			}
			div#mainContent {
			    width:560px;
			    padding:5px;
			    float:left;
			}
			div#sideBar {
			    width:230px;
			    padding:0px;
			    margin-left:0px;
			    float:left;
			}
			.clear {
			    clear:both;
			}
			.stick {
			    position:fixed;
			    top:0px;
			}
			.selecetd_brand{
				border : red 5px solid;
			}
		</style>
	</head>
	<body >
		<div id="content" >
			<div id="main_div_mali"></div>
		</div>
	</body>
</html>
