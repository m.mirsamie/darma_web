<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
        function loadUserNamesAndUser()
	{
            $out = array(0=>'');
            $my = new mysql_class;
            $conf = new conf;
            $my->ex_sql("select id,fname,lname,user from user where not (user like 'mehrdad%') and company_id=".$conf->company_id." order by lname,fname,user",$q);
            foreach($q as $r)
                    $out[(int)$r['id']] = $r['fname'].' '.$r['lname'].' ['.$r['user'].']';
            return($out);
	}
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$permission=array();
	$cl2=new mysql_class;
	
	$mysql = new mysql_class;
	$msg ='';
	$isAdmin = $se->detailAuth('all');
	$gname = 'grid1';
	$input =array($gname=>array('table'=>'company_group','div'=>'main_div_company_group'));
	$xgrid = new xgrid($input);
//$xgrid->alert = TRUE;
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = 'نام';
	$xgrid->column[$gname][1]['search'] = 'text';
        $xgrid->column[$gname][2]['name'] = 'بازاریاب';
        $xgrid->column[$gname][2]['clist'] = loadUserNamesAndUser();
        $xgrid->column[$gname][3]['name'] = 'کمیسیون بازاریاب';
        $xgrid->column[$gname][4]['name'] = 'کمیسیون دارما';
        
	$xgrid->canAdd[$gname] = TRUE;
	$xgrid->canEdit[$gname] = TRUE;
	$out =$xgrid->getOut($_REQUEST);
	if($xgrid->done)
		die($out);	
?>
<script>
        var gname = '<?php echo $gname; ?>';
	$(document).ready(function(){
            var args=<?php echo $xgrid->arg; ?>;
            args[gname]['afterLoad']=function(a){
                $('select').select2({
                    dir: "rtl"
                });
            };
            intialGrid(args);
        });
</script>
<div id="main_div_company_group">
</div>
