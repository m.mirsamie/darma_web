<?php
	include_once('../kernel.php');
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	include_once('../simplejson.php');
	$kala_abarGroup_id =-1;
	$special =kala_class::loadSpecial(10,$kala_abarGroup_id);
	$mostSale = kala_class::loadMostSale(10,$special,$kala_abarGroup_id);
	$tmpArr = array_merge($special,$mostSale);
	$last = kala_class::loadLast(10,$tmpArr,$kala_abarGroup_id);
	$home = toJSON(array('special'=>$special,'mostSale'=>$mostSale,'last'=>$last));
	
?>
<!doctype html>
<html lang="fa">
	<head>
		<meta charset="utf-8">
		<title><?php echo $conf->title;  ?></title>
		<link rel="stylesheet" href="../css/jquery-ui.css">
		<link rel="stylesheet" href="../css/jquery.lightbox-0.5.css">
                <link rel="stylesheet" type="text/css" media="all" href="../js/cal/skins/aqua/theme.css" title="Aqua" />
		<link rel="stylesheet" href="../css/style.css">
		<script src="../js/jquery.min.js"></script>
		<script src="../js/jquery.cycle.all.js"></script>
		<script src="../js/jquery.lightbox-0.5.min.js"></script>
		<script src="../js/jquery-ui.js"></script>
		<script src="../js/tmp.js"></script>
		<script type="text/javascript" >
			var home_kala = <?php echo $home; ?>;
			$(document).ready(function(){
				initial();
			});
			function drawKala(obj)
			{
				//var lin = "openDialog(\"kalaProfile.php?kala="+encodeURIComponent(JSON.stringify(obj))+"\",{width:600,height:400,title:\""+obj.name+"\"});";
				var lin = "openDialog(\"kalaProfile.php?kala_id="+obj.id+"\",{width:600,height:400,title:\""+obj.name+"\"});";
				var out="<div class='round blue' style='width:"+boxWidth+"px;height:250px;'>";
				out+="<table width='100%' height='100%' ><tr><td align='center' colspan='2' class='td_img_"+obj.id+"' ><img class='pointer' onclick='"+lin+"' src='"+(obj.pic=='' ? '../img/default.png':obj.pic)+"' width='100' height='100' ></td>";
				out+="<td colspan='2' class='td_tedad_"+obj.id+"' style='display:none'><div>تعداد:<input type='text' style='width:30px;'  id='tedad_"+obj.id+"' class='sefaresh_tedad' onkeypress='return numbericOnKeypress(event);' value='1' ></div></td></tr>";
				out+="<tr><td colspan='2' ><span style='color:#3a4009' class='pointer' onclick='"+lin+"' ><b>"+obj.name+" </b><span id='khoon_"+obj.id+"' ></span></span></td></tr>";
				var takhfif_val =parseInt(obj.ghimat*(1-(parseFloat(obj.takhfif)/100)),10);
				var takhf = (trim(obj.takhfif)!="0" ?"<div style='font-size:10px;color:#093121;' >بیش از "+obj.tedad_baste+" عدد قیمت به"+takhfif_val+" کاهش می یابد </div>" : '');
				var ghimat_user = obj.ghimat_user!='0' ? " <span class='line' >"+obj.ghimat_user+"</span> ":'';
				out+="</td><td align='right' ><div>قیمت:&nbsp;"+ghimat_user+" <span> "+obj.ghimat+"&nbsp;ریال</span></div>";
				out+="<div>تعداد در بسته: "+obj.tedad_baste+"</div>";
				out+="<div>قیمت بسته: "+takhfif_val+" ریال</div>";
				out+="</td></tr>";
				out+="<tr><td align='right' ><span style='font-family:bYekan,tahoma;' >کد کالا:"+obj.id+"</span></td></tr>";
				var st = parseInt(obj.mojoodi,10)>0? "<span class='pointer kharid_"+obj.id+" kala_kharid' onclick='kharid("+obj.id+");' >اضافه به سبد خرید</span> <span class='pointer' style='font-size:10px;color:#5f612a;border:solid 1px #e5debc;padding:3px;' onclick='"+lin+"' >نمایش</span>": "<span style='color:#999999;font-size:10px;' >عدم موجودی</span>";
				out+="<tr><td align='right' >"+st;
				out+="&nbsp;<span style='display:none;' class='div_"+obj.id+"' ><span onclick='sabad1("+obj.id+");' class='kala_kharid pointer' >سفارش</span>&nbsp;<span class='kala_kharid pointer' onclick='kharid("+obj.id+");' >انصراف</span></span>";
				out+="</td></tr>";
				out+="</table></div>";
				return(out);
			}
			function kharid(id)
			{
				$(".td_tedad_"+id).toggle('fast');
				$(".td_img_"+id).toggle('fast');
				$(".div_"+id).toggle('fast');
				$(".kharid_"+id).toggle('fast');
			}
			function initial()
			{
				if(typeof home_kala.last!='undefined')
					$("#content").html(writeHome());
				else
					$("#content").html('کالا موجود نیست');
				$(".sefaresh_tedad").spinner({
					min:1
				});
			}
			function sabad1(id)
			{
				var tedad = $("#tedad_"+id).val();
				if(id>0 && parseInt(tedad)>0)
				{
					$("#khoon_"+id).html('<img src="../img/status_fb.gif" width="20" >');
					$.get("index.php?kala_id="+id+"&tedad="+tedad+"&",function(result){
						$("#khoon_"+id).html('');
						//console.log(result);
						if(result='true')
						{
							//$("#tedad_"+id).val('');
							kharid(id);
							refreshSabad();
						}
						else
							alert('خطادر افزودن به سبد خرید');
					});
				}
				else
					alert('تعداد را وارد کنید');
			}
			function writeHome()
			{
				var out="<table style='width:100%;' class='home_table' >";
				if(home_kala.special.length>0)
				{
					out+="<tr><th>کالاهای پیشنهادی ویژه دارما</th></tr>";
					out+="<tr><td>"+drawTable(home_kala.special)+"</td></tr>";
				}
				if(home_kala.mostSale.length>0)
				{
					out+="<tr><th>پر فروش ترین ها</th></tr>";
					out+="<tr><td>"+drawTable(home_kala.mostSale)+"</td></tr>";
					if(home_kala.mostSale.length>10)
						out+="<tr><td style='text-align:left;'><a href='mostSale.php' target='_blank'>بیشتر</a></td></tr>";
				}
				if(home_kala.last.length>0)
				{
					out+="<tr><th>آخرین کالاهای اضافه شده</th></tr>";
					out+="<tr><td>"+drawTable(home_kala.last)+"</td></tr>";
				}
				out +="</table>";
				return(out);
			}
			function trim(str)
			{
				var out=str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
				return out;
			}
		</script>
		<style>
			#timer
			{
				border : 1px #eaeaea solid;
                                background : #ffffff;
                                height : 50px;
			}
			.ui-tabs { direction: rtl; }
			.ui-tabs .ui-tabs-nav li.ui-tabs-selected,
			.ui-tabs .ui-tabs-nav li.ui-state-default {float: right; }
			.ui-tabs .ui-tabs-nav li a { float: right; }
			.leftborderBlue{
				border-left:solid 1px #a7d7f9;
				width:200px;
			}
			li
			{
				font-family:tahoma;
			}
			.sabad_mojoodi
			{
				font-family:tahoma;
				font-size:20px;
				color:red;
				background:#000000;
				width:100%;
				text-align:left;
			}
			.shegeftKalaSelected
			{
				color:red;
			}
			div#wrapper {
			    margin:0px;
			    width:800px;
			    background:#FFF;
			}
			div#mainContent {
			    width:560px;
			    padding:5px;
			    float:left;
			}
			div#sideBar {
			    width:230px;
			    padding:0px;
			    margin-left:0px;
			    float:left;
			}
			.clear {
			    clear:both;
			}
			.stick {
			    position:fixed;
			    top:0px;
			}
			#content button {font-family:tahoma;font-size:12px;}
		</style>
	</head>
	<body>
		<div id="content" style="font-family:tahoma;" ></div>
	</body>
</html>
