<?php
	include_once("../kernel.php");
	include_once("../simplejson.php");
	$SESSION = new session_class;
	register_shutdown_function('session_write_close');
	session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die('<script>window.location="admin_login.php";</script>');//die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
               die('<script>window.location="admin_login.php";</script>');//die($conf->access_deny);
	$user_id=trim((int)$_SESSION[$conf->app.'_user_id']);
	$user_obj = new user_class($user_id);
	$user_user=$user_obj->user;
	//die("user is:".$user_obj->user);
	$menu_out = '';
	$mysql=new mysql_class;
	/*$mysql->ex_sql("select * from `menu_table` where show_tag='1' ",$q);
	foreach($q as $r)
	{
		$firstMenu = $r['link_address'];	
		$name = $r['name'];
		//$menu_out .="<button onclick=\"loadCont(this,'$firstMenu?main=main&');\">$name</button>";
		

	}
	$men_itm='('.implode(',',$men_arr).')';*/
	//die("select * from access left join menu_table on(`access`.`page_name`=`menu_table`.`link_address`) where `page_name` in (select link_address from menu_table) and `group_id`='$user_id' order by `menu_table`.`id` ");
	$mysql->ex_sql("select * from access left join menu_table on(`access`.`page_name`=`menu_table`.`link_address`) where `page_name` in (select link_address from menu_table) and `group_id`='$user_id' order by `menu_table`.`order` ",$arr_btn);
	if(count($arr_btn)>0)
	{
		for($i=0;$i<count($arr_btn);$i++)
		{
			$link=$arr_btn[$i]['page_name'];
			$name=$arr_btn[$i]['name'];
			if($link=='admin_login.php')
			{
				$menu_out .= "";
			}
			else if($link=='index.php')
			{
				$menu_out .= "<button class='fastAcc2' onclick=\"window.location='index.php';\">$name</button>";
			}
			else
				$menu_out .= "<button class='fastAcc2' onclick=\"loadCont(this,'$link?main=main&');\">$name</button>";	
		}

	}
	$menu_out .="<button class='fastAcc2' onclick=\"window.location='admin_login.php?main=main';\">خروج</button>";
?>
<!doctype html>
<html lang="fa">
	<head>
		<meta charset="utf-8">
		<title><?php echo $conf->title;  ?></title>
		<link rel="stylesheet" href="../css/jquery-ui.css">
		<link rel="stylesheet" href="../css/style.css">
		<link rel="stylesheet" href="../css/colorpicker.css">
		<link rel="stylesheet" href="../css/xgrid.css">
                <link rel="stylesheet" href="../css/select2.css">
                <link rel="stylesheet" type="text/css" media="all" href="../js/cal/skins/aqua/theme.css" title="Aqua" />
		<script src="../js/jquery.min.js"></script>
		<script src="../js/jquery-ui.js"></script>
		<script src="../js/grid.js"></script>
		<script src="../js/date.js" ></script>
		<script src="../js/inc.js"></script>
		<script src="../js/colorpicker.js"></script>
		<script src="../js/md5.js"></script>
                <script type="text/javascript" src="../js/cal/jalali.js"></script>
                <script type="text/javascript" src="../js/cal/calendar.js"></script>
                <script type="text/javascript" src="../js/cal/calendar-setup.js"></script>
                <script type="text/javascript" src="../js/cal/lang/calendar-fa.js"></script>
                <script src="../js/select2.min.js"></script>
		<style>
			button
			{
				-webkit-border-radius: 4px;
				-moz-border-radius: 4px;
				border-radius: 4px;
				border:1px solid black;
				cursor : pointer;
			}
			button:hover
			{
				background-color:#fefefe;
			}
		</style>
		<script>	
			function selectButton(obj)
			{
				if(obj)
				{
					var qobj = $(obj);
					$("button").css("border","#000000 solid 1px");
					qobj.css("border","dashed #666666 1px");
				}
			}
			function loadIndex()
			{
				window.location = 'index.php';
			}
			function loadCont(obj,addr)
			{
				//alert(addr);
				if(typeof obj == 'undefined')
					obj = document.getElementById('fastAccess');
				whereObj = {};
				$("#body").html("<img src='../img/status_fb.gif' >");
				$("#body").load(addr,function(){
                                        selectButton(obj);
                                });
			}
			function openDialog(addr,prop,fn)
			{
				$("#dialog").html("<img src='../img/status_fb.gif' alt='Loading . . .'/>");
				if($("#dialog").dialog)
				{
					if($("#dialog").dialog("isOpen"))
						$("#dialog").dialog("close");
					for(i in prop)
						$("#dialog").dialog("option",i,prop[i]);
					$("#dialog").dialog("open");
					$("#dialog").load(addr,function(){
						if(typeof fn == "function")
							fn();
					});
				}
			}
			$(document).ready(function(){
				loadCont(null,'fastAccess.php');
				$('#dialog').dialog({
						autoOpen : false,
						show: "slide",
						/*hide: "drop",*/
						modal: false,
						resizable: false,
						minWidth :200,
						minHeight : 200,
						position : 'center',
						closeOnEscape: true,
						beforeClose: function(event, ui) {
						return(true);
					}
				});
			});
		</script>
	</head>
	<body>
                <div>
			<table style="width:100%;" >
				<tr>
					<td style="width:80%;" >
                       				<img src="../img/darma.png">
					</td>
					<td style="width:20%;" class="blue" >
						<div style="margin:10px;">
						کاربر:
							<b><?php echo $user_obj->fname.' '.$user_obj->lname; ?></b>
						</div>
						<div style="margin:10px;">
								کد مشتری:
							<b><?php echo audit_class::idToCode($user_obj->id); ?></b>
						</div>
					</td>
				</tr>
			</table>
                </div>
		<div align="center">
			<div id="menu" align="right" style="padding:6px;" >
				<?php 
					echo $menu_out;
				?>
				<!--<button id='fastAccess' onclick="loadCont(this,'fastAccess.php');">دسترسی سریع</button>
				<button onclick="loadCont(this,'kala_abarGroup.php');">دسته های اصلی کالا</button>
				<button onclick="loadCont(this,'kala_miniGroup.php');">ریز گروه کالا</button>
				<button onclick="loadCont(this,'takhfif.php');">لیست تخفیف‌ها</button>
				<button onclick="loadCont(this,'takhfif_set.php');">اعمال تخفیف</button>-->
				<!--<button onclick="loadCont(this,'manage_group.php?main=main&');">مدیریت گروه ها</button>
				<button onclick="loadCont(this,'vahed.php');">تعریف واحد</button>
				<button onclick="loadCont(this,'city.php');">شهر</button>
				<button onclick="loadCont(this,'taminkonande.php');">تأمین کنندگان</button>
				<button onclick="loadCont(this,'etebar.php');">پاداش</button>
				<button onclick="loadCont(this,'profileAdmin.php');">مشتریان</button>
				<button onclick="loadCont(this,'slideShow.php');">اسلایدشو</button>
				<button onclick="loadCont(this,'brand.php');">برندها</button>
				<button onclick="loadCont(this,'comment.php');">نظرات</button>
				<button onclick="loadCont(this,'mantaghe.php');">منطقه</button>
				<button onclick="loadCont(this,'transporter.php');">موزع ها</button>
				<button onclick="loadCont(this,'back_restore.php');">پشتيبان</button>
				<button onclick="loadIndex(this);">خرید کالا</button>
				<button onclick="window.location='admin_login.php';">خروج</button>-->
			</div>
			<div id="body">
			</div>
		</div>
                <div id="dialog"></div>
	</body>
</html>
