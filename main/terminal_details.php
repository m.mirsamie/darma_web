<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
	function mpdate($inp)
	{
		return(($inp != '' && $inp != '0000-00-00 00:00:00')?jdate("H:i d / m / Y",strtotime($inp)):'----');
	}
	function loadMande($id)
	{
		$f = new user_etebar_class((int)$id);
		$out = 0;
		$conf = new conf;
		$user_id = isset($_REQUEST['user_id'])?(int)$_REQUEST['user_id']:(int)$_SESSION[$conf->app.'_user_id'];
		$my = new mysql_class;
		$my->ex_sql("select sum(`typ`*`mablagh`) as `mande` from `user_etebar` where `user_id` = $user_id and `regdate` <= '".$f->regdate."' order by `regdate`",$q);
		if(isset($q[0]))
			$out = (int)$q[0]['mande'];
		return(monize($out));
	}
	function loadUser($id)
	{
		$u = new user_class((int)$id);
		return(isset($u->id)?$u->fname.' '.$u->lname:'----');
	}
	$user_id = isset($_REQUEST['user_id'])?(int)$_REQUEST['user_id']:(int)$_SESSION[$conf->app.'_user_id'];
	$u = new user_class($user_id);
	$p = new profile_class($user_id);
        $gname = "gname_mali";
	$input = array($gname=>array('table'=>'user_etebar','div'=>'main_div_mali'));
	$user_ters_arr = array(-100);
	$my = new mysql_class;
	$my->ex_sql("select id from terminals where user_id = $user_id",$q);
	foreach($q as $r)
		$user_ters_arr[] = (int)$r['id'];
	$user_ters = implode(',',$user_ters_arr);
		$sum = 0;
	$my = new mysql_class;
	$wer = '';
	if(isset($_REQUEST['jamkol']))
	{
		$req = $_REQUEST;
		foreach($req as $key=>$value)
			if($key != 'jamkol')
			{
				if(strpos($key,"-start")!==FALSE)
					 $wer .= (($wer!='')?' and ':'').'`'.str_replace('-start','',$key).'` >= \''.$value.'\'';
				else if(strpos($key,"-stop")!==FALSE)
					$wer .= (($wer!='')?' and ':'').'`'.str_replace('-stop','',$key).'` <= \''.$value.'\'';
				else
					$wer .= (($wer!='')?' and ':'').'`'.$key.'` = \''.$value.'\'';
			}
	}
	$wer = $wer . ((trim($wer)!='')?' and ':'')." transaction_id in (select id from ".$conf->poolDB.".transactions where ter_id in ($user_ters)) order by `regdate` desc";
	$query = "select sum(typ * mablagh) as s from user_etebar ".((trim($wer)!='')?'where ':'')." $wer";
	$my->ex_sql($query,$q);
	if(isset($q[0]))
		$sum = (int)$q[0]['s'];
	$alamat = ($sum<0)?'کاهش':'';
	if(isset($_REQUEST['jamkol']))
	{
		//die($query);
		die($alamat.monize(abs($sum)));
	}
        $xgrid = new xgrid($input);
	$xgrid->eRequest[$gname] = array('user_id'=>$user_id);
	$xgrid->whereClause[$gname] = $wer;
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = 'کاربر';
	$xgrid->column[$gname][1]['cfunction'] = array('loadUser');
	$xgrid->column[$gname][2]['name'] = '';
	$xgrid->column[$gname][3]['name'] = 'افزایش/کاهش';
	$xgrid->column[$gname][3]['search'] = 'list';
	$xgrid->column[$gname][3]['searchDetails'] = array(0=>'همه',-1=>'کاهش',1=>'افزایش');
	$xgrid->column[$gname][3]['clist'] = array(-1=>'کاهش',1=>'افزایش');
	$xgrid->column[$gname][4]['name'] = 'مبلغ(ریال)';
	$xgrid->column[$gname][4]['cfunction'] = array('monize');
	$xgrid->column[$gname][5]['name'] = 'توضیحات';
	$xgrid->column[$gname][6]['name'] = 'تاریخ ثبت';
	$xgrid->column[$gname][6]['cfunction'] = array('mpdate');
	$xgrid->column[$gname][6]['search'] = 'dateValue_minmax';
	$xgrid->column[$gname][7] = $xgrid->column[$gname][0];
	$xgrid->column[$gname][7]['name'] = '';
	$xgrid->column[$gname][8]['name'] = 'کمپانی';
	$xgrid->column[$gname][8]['clist'] = columnListLoader('company');
	$xgrid->column[$gname][8]['search'] = 'list';
	$xgrid->column[$gname][8]['searchDetails'] = columnListLoader('company');
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);
?>
<script type="text/javascript" >
	var gname = '<?php echo $gname; ?>';
        $(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
		gArgs[gname]['afterLoad'] = function(){
                        jamkol();
                };
        });
	function jamkol()
	{
		var p = {"jamkol":1};
		var tmp = whereObj[gname];
		for(i in tmp)
		{
			if(i.indexOf('-start') >0 || i.indexOf('-stop') >0)
				p[i] = JalaliDate.jalaliToGregorian(tmp[i].split('/')[0],tmp[i].split('/')[1],tmp[i].split('/')[2]).join('-');
			else	
				p[i] = tmp[i];
		}
		$.get("terminal_details.php",p,function(result){
			$("#sum").html(result);
		});
	}
</script>

<div id="content" >
	<div id="sumDiv" style="text-align:center;font-size:20px;background-color:#eeeeee;border:1px #000000 solid;">جمع : <span id="sum"><?php echo $alamat.monize(abs($sum)); ?></span>ریال</div>
	<div id="main_div_mali"></div>
</div>
