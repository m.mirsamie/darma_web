<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id=(int)($_SESSION[$conf->app.'_user_id']);
	$permission=array();
	$cl2=new mysql_class;
	/*$canWrite='never';
	if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("kala_abarGroup.php",$permission))
			$canWrite='limited';
		
	}
*/
	function loadCss($id)
	{
		$kala_abarGroup_class = new kala_abarGroup_class((int)$id);
		$out = '<input id="kalaColor_'.$id.'" value="'.$kala_abarGroup_class->loadColor().'" class="kalaColorPicker" style="background:#'.$kala_abarGroup_class->loadColor().';" />';
		$out .= '<button onclick="updateCss('.$id.');">ثبت</button>';
		//$out = '<textarea>'.$kala_abarGroup_class->loadColor().'</textarea>';
		return($out);
	}
	function del($table,$id,$gname)
	{
		$mysql = new mysql_class;
		$mysql->ex_sql("select count(`id`) as `cid` from `kala_group` where `kala_abarGroup_id`=$id",$q);
		if((int)$q[0]['cid']>0)
			$out = 'false|اين گروه شامل زير گروه مي باشد لذا حذف امکان پذير نيست';
		else
		{
			$mysql->ex_sqlx("delete from `$table` where `id`=$id");
			$out = TRUE;
		}
		return($out);
	}
	function hiddenKalaAbarGrp($kala_abarGroup_id)
	{
		$kala_abarGroup_id = (int)$kala_abarGroup_id;
		return("<div class='pointer notice' onclick='hiddenKalaAbar($kala_abarGroup_id);'>مخفی سازی</div><div class='pointer msg' onclick='showKalaAbar($kala_abarGroup_id);'>نمایش</div>");
	}
	function hiddenKalaAbarGrpDo($kala_abarGroup_id)
	{
		$my = new mysql_class;
		$my->ex_sqlx("update kala left join kala_miniGroup on (kala_miniGroup_id=kala_miniGroup.id) left join kala_group on (kala_group_id=kala_group.id) left join kala_abarGroup on (kala_abarGroup_id=kala_abarGroup.id) set kala.en = -1  where kala_abarGroup.id=$kala_abarGroup_id");
	}
	function showKalaAbarGrpDo($kala_abarGroup_id)
        {
                $my = new mysql_class;
                $my->ex_sqlx("update kala left join kala_miniGroup on (kala_miniGroup_id=kala_miniGroup.id) left join kala_group on (kala_group_id=kala_group.id) left join kala_abarGroup on (kala_abarGroup_id=kala_abarGroup.id) set kala.en = 1  where kala_abarGroup.id=$kala_abarGroup_id");
        }
	if(isset($_REQUEST['hide']))
	{
		$kala_abarGroup_id = (int)$_REQUEST['kala_abarGroup_id'];
		hiddenKalaAbarGrpDo($kala_abarGroup_id);
		die('true');
	}
	if(isset($_REQUEST['show']))
        {
                $kala_abarGroup_id = (int)$_REQUEST['kala_abarGroup_id'];
                showKalaAbarGrpDo($kala_abarGroup_id);
                die('true');
        }
	if(isset($_REQUEST['col']))
	{
		$k = new kala_abarGroup_class((int)$_REQUEST['id']);
		$k->setColor($_REQUEST['col']);
		die('true');
	}
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$mysql = new mysql_class;
	$msg ='';
	$isAdmin = $se->detailAuth('all');
        $gname = 'grid1';
        $input =array($gname=>array('table'=>'kala_abarGroup','div'=>'main_div_kalaAbargrp'));
        $xgrid = new xgrid($input);
	//$xgrid->alert = TRUE;
	$xgrid->column[$gname][0]['name'] = '';
        $xgrid->column[$gname][1]['name'] = 'نام';
	$xgrid->column[$gname][1]['search'] = 'text';
	$xgrid->column[$gname][2]['name'] = '';
	$xgrid->column[$gname][3]['name'] = 'css';
	$xgrid->column[$gname][4]['name'] = 'ترتیب';
        $xgrid->column[$gname][5]['name'] = 'نام لاتین';
	$xgrid->column[$gname][6] = $xgrid->column[$gname][0];
	$xgrid->column[$gname][6]['name'] = 'مخفی کردن';
	$xgrid->column[$gname][6]['access'] = 'a';
	$xgrid->column[$gname][6]['cfunction'] = array('hiddenKalaAbarGrp');
	//$xgrid->column[$gname][3]['cfunction'] = array('loadCss');
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
	{
		$xgrid->canEdit[$gname]= TRUE;
        	$xgrid->canAdd[$gname] = TRUE;
       		$xgrid->canDelete[$gname] = TRUE;
	}
	$xgrid->deleteFunction[$gname] = 'del';
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);	
?>
<script>
	function hiddenKalaAbar(kala_abarGroup_id)
	{
		if(confirm('آیا مخفی سازی انجام شود؟'))
			$.get("kala_abarGroup.php?hide=hide&kala_abarGroup_id="+kala_abarGroup_id+"&",function(result){
				alert('مخفی سازی با موفقیت انجام شد');
			});
	}
	function showKalaAbar(kala_abarGroup_id)
        {
		if(confirm('آیا نمایش دادن انجام شود؟'))
			$.get("kala_abarGroup.php?show=show&kala_abarGroup_id="+kala_abarGroup_id+"&",function(result){
				alert('نمایش با موفقیت اعمال شد');
			});
        }
	function updateCss(id)
	{
		var col = $("#kalaColor_"+id).val();
		$("#kalaColor_"+id).after("<img src='../img/status_fb.gif' id='kalaColorKhoon_"+id+"' />");
		$.get("kala_abarGroup.php?id="+id+"&col="+col+"&",function(result){
			$("#kalaColorKhoon_"+id).remove();
		});
	}
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
		var ggname = '<?php echo $gname; ?>';
		var colorPickerElement;
                intialGrid(args);
		gArgs[ggname]['afterLoad'] = function(){
			$(".kalaColorPicker").focus(function(){
				colorPickerElement = this;
			});
			$(".kalaColorPicker").ColorPicker({
				onSubmit: function(hsb, hex, rgb, el) {
					$(el).val(hex);
					$(el).ColorPickerHide();
				},
				onChange : function(hsb, hex, rgb) {
					var el = colorPickerElement;
                                        $(el).val(hex);
					$(el).css("background","#"+hex);
                                },
				onBeforeShow: function () {
					$(this).ColorPickerSetColor(this.value);
				}
			});
		};
        });
</script>
<div id="main_div_kalaAbargrp">
</div>
