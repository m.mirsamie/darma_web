<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	function loadUser($id)
	{
		$u = new user_class((int)$id);
		return(isset($u->id)?$u->fname.' '.$u->lname:'----');
	}
	function hamed_pdate($dt)
	{
		return(($dt!='0000-00-00 00:00:00')?jdate("Y/m/d H:i",strtotime($dt)):'----');
	}
	function convertMsg($msg)
	{
		return(str_replace("\n",'<br/>',$msg));
	}
	function deConvertMsg($msg)
	{
		return(str_replace('<br/>',"\n",$msg));
	}
	$user_id=(int)($_SESSION[$conf->app.'_user_id']);
	$permission=array();
	$cl2=new mysql_class;
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$mysql = new mysql_class;
        $gname = 'grid1';
        $input =array($gname=>array('table'=>'msg','div'=>'main_div_kalaAbargrp'));
        $xgrid = new xgrid($input);
	$xgrid->whereClause[$gname] = ((!$se->detailAuth('all'))?" reciever_user_id=$user_id ":'')." order by send_date desc";
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = 'پیام';
	$xgrid->column[$gname][1]['cfunction'] = array('convertMsg','deConvertMsg');
	$xgrid->column[$gname][2]['name'] = 'فرستنده';
	$xgrid->column[$gname][2]['cfunction'] = array('loadUser');
	$xgrid->column[$gname][2]['search'] = 'list';
	$xgrid->column[$gname][2]['searchDetails'] = columnListLoader('user',array('id','fname','lname'));
	if($se->detailAuth('all'))
	{
		$xgrid->column[$gname][3]['name'] = 'گیرنده';
		$xgrid->column[$gname][3]['cfunction'] = array('loadUser');
	}
	else
		$xgrid->column[$gname][3]['name'] = '';
	$xgrid->column[$gname][4]['name'] = '';
	$xgrid->column[$gname][5]['name'] = 'تاریخ ارسال';
	$xgrid->column[$gname][5]['cfunction'] = array('hamed_pdate');
	$xgrid->column[$gname][6]['name'] = '';
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);	
?>
<script>
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
		var ggname = '<?php echo $gname; ?>';
		var colorPickerElement;
                intialGrid(args);
        });
</script>
<div id="main_div_kalaAbargrp">
</div>
