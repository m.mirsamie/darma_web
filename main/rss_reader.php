<?php
	//$xml=("http://news.google.com/news?ned=us&topic=h&output=rss");
	include_once("../kernel.php");
	$rss_url = (trim($conf->rss_url)!='')?trim($conf->rss_url):"http://news.google.com/news?ned=us&topic=h&output=rss";
	$xml=($rss_url);
	$xmlDoc = new DOMDocument();
	$xmlDoc->load($xml);
	$channel=$xmlDoc->getElementsByTagName('channel')->item(0);
	$channel_title = $channel->getElementsByTagName('title')
	->item(0)->childNodes->item(0)->nodeValue;
	$channel_link = $channel->getElementsByTagName('link')
	->item(0)->childNodes->item(0)->nodeValue;
	$channel_desc = $channel->getElementsByTagName('description')
	->item(0)->childNodes->item(0)->nodeValue;

	//output elements from "<channel>"
/*
	echo("<p><a target='_blank' href='" . $channel_link
	  . "'>" . $channel_title . "</a>");
	echo("<br>");
	echo($channel_desc . "</p>");
*/
	//get and output "<item>" elements
	$x=$xmlDoc->getElementsByTagName('item');
	$items = array();
	for ($i=0; $i<=10; $i++) {
		if(is_object($x->item($i))){
		
			$item_title=$x->item($i)->getElementsByTagName('title')
			->item(0)->childNodes->item(0)->nodeValue;
			$item_link=$x->item($i)->getElementsByTagName('link')
			->item(0)->childNodes->item(0)->nodeValue;
			$item_desc=$x->item($i)->getElementsByTagName('description')
			->item(0)->childNodes->item(0)->nodeValue;
			$item = array(
				'title' => $item_title,
				'link'  => $item_link,
				'desc'  => $item_desc
			);
			$items[] = $item;
			echo ("<div><p><a target='_blank' href='" . $item_link
			. "'>" . $item_title . "</a>");
			echo ("<br>");
			echo ($item_desc . "</p></div>");
		}
	}
	$rssData = array(
		'channel'=> array(
			'title' => $channel_title,
			'link'  => $channel_link,
			'desc'  => $channel_desc
		),
		'items'  => $items
	);
	//var_dump($rssData);
?>
