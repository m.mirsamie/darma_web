<?php

	include_once("../kernel.php");
	include_once("../simplejson.php");
	$SESSION = new session_class;
	register_shutdown_function('session_write_close');
	session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die('<script>window.location="admin_login.php";</script>');//die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
       	if(!$se->can_view)
               die('<script>window.location="admin_login.php";</script>');//die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$permission=array();
	$cl2=new mysql_class;
	/*$canWrite='never';
	if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("comment.php",$permission))
			$canWrite='limited';
		
	}*/
	function mpdate($inp)
	{
		return(($inp != '' && $inp != '0000-00-00 00:00:00')?jdate("Y/m/d",strtotime($inp)):'----');
	}
	function mpdateBack($inp)
	{
		return(audit_class::hamed_pdateBack($inp));
	}
	function loadUser($id)
	{
		$u = new user_class((int)$id);
		return($u->fname.' '.$u->lname);
	}
	$user = new user_class((int)$_SESSION[$conf->app.'_user_id']);
	$grid= 'grid2' ;
	$input=array($grid=>array("table"=>"comment_tbl","div"=>"main_div_comment"));
	$xgrid=new xgrid($input);
	$xgrid->column[$grid][0]['name'] = '';
	$xgrid->column[$grid][1]['name'] = 'کاربر';
	$xgrid->column[$grid][1]['cfunction'] = array('loadUser');//columnListLoader("user|`user` <> 'mehrdad'",array('id','fname','lname'));
	$xgrid->column[$grid][1]['access'] = 'majid';
	$xgrid->column[$grid][2]['name'] = 'ایده';
	$xgrid->column[$grid][2]['access'] = 'majid';
	$xgrid->column[$grid][3]['name'] = 'تاریخ ثبت';
	$xgrid->column[$grid][3]['access'] = 'majid';
	$xgrid->column[$grid][3]['cfunction'] = array('mpdate','mpdateBack');
	$xgrid->column[$grid][4]['name'] = 'وضعیت';
	$xgrid->column[$grid][4]['clist'] = array(0=>'خوانده نشده',1=>'خوانده شده');
       //if($canWrite=='all' || $canWrite=='limited')
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
	{
		$xgrid->canAdd[$grid] = TRUE;
		$xgrid->canDelete[$grid] = TRUE;
		$xgrid->canEdit[$grid]= TRUE;
	}
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);
	
?>
<script type="text/javascript" >
        $(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
        });
</script>
<div id="main_div_comment">

</div>
