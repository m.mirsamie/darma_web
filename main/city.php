<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$permission=array();
	$cl2=new mysql_class;
	/*$canWrite='never';
	if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("city.php",$permission))
			$canWrite='limited';
		
	}*/
	$mysql = new mysql_class;
	$msg ='';
	$isAdmin = $se->detailAuth('all');
	$gname = 'grid1';
	$input =array($gname=>array('table'=>'city','div'=>'main_div_city'));
	$xgrid = new xgrid($input);
//$xgrid->alert = TRUE;
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = 'نام';
	$xgrid->column[$gname][1]['search'] = 'text';
	//if($canWrite=='all' || $canWrite=='limited')
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
	{
		$xgrid->canAdd[$gname] = TRUE;
		$xgrid->canDelete[$gname] = TRUE;
		$xgrid->canEdit[$gname]= TRUE;
	}
	$out =$xgrid->getOut($_REQUEST);
	if($xgrid->done)
			die($out);	
?>
<script>
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
        });
</script>
<div id="main_div_city">
</div>
