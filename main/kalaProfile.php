<?php
	include_once("../kernel.php");
	include_once("../simplejson.php");
	//if(!isset($_REQUEST['kala']))
		//die($conf->access_deny);
	$kala_id = (int)$_REQUEST['kala_id'];
        $mojood = (isset($_REQUEST['mojood']));
	$kala = new kala_class($kala_id);
	$barnd = new brand_class((int)$kala->brand_id);
	$kala->brand=isset($barnd->name)? $barnd->name:'';
	$kala_gallery = toJSON(kala_gallery_class::loadByKala($kala->id));
?>

		<style>
			#content1 button {font-family:tahoma;font-size:12px;}
			.kala td{text-align:right;padding:5px;}
			.kala th{text-align:right;padding:5px;}
		</style>
		<script type="text/javascript" >
			var kala = <?php echo toJSON($kala); ?>;
			var kala_gallery = <?php echo $kala_gallery; ?>;
			//console.log(kala_gallery);
			$(document).ready(function(){
				$("#content1").html(draw_kala(kala));
				$("#gallery").html(drawGall(kala_gallery));
				$("a.lbox").lightBox({
					imageBtnClose:'../img/lightbox-btn-close.gif',
					imageLoading:'../img/lightbox-ico-loading.gif',
					imageBtnPrev:'../img/lightbox-btn-prev.gif',
					imageBtnNext:'../img/lightbox-btn-next.gif'
				});
				$(".sefaresh_tedad").spinner({
					min:1
				});	
			});
			function draw_kala(obj)
			{
				var out ="<table class='kala' width='100%' style='margin-top:8px;' >";
				//var pic = obj.pic!=''?"<img src='"+obj.pic+"' width='90' >":'';
				var pic =(kala_gallery.length>0 && kala_gallery[0].pic!='')?"<img src='../img/kala_pic/"+kala_gallery[0].pic+"' width='250' >":'';
				if(pic=='')
					pic = obj.pic!=''?"<img src='"+obj.pic+"' width='90' >":'';
				out+="<tr><td style='color:green;font-size:16px;' ><b>"+obj.name+"</b></td><td style='text-align:left;' rowspan='5' >"+pic+"</td></tr>";
				//out+="<tr><td><span style='text-decoration:line-through' >"+obj.ghimat_user+"</span></td></tr>";
				out+="<tr><td>قیمت:<span style='text-decoration:line-through' >"+(parseInt(obj.ghimat_user,10)>0?obj.ghimat_user:"")+"</span> "+(parseInt(obj.ghimat,10)>0?obj.ghimat:"")+"</td></tr>";
				out+="<tr><td>تعداد در هر بسته:"+obj.tedad_baste+"</td></tr>";
				out+="<tr><td>تخفیف به ازای خرید بیش از یک بسته:"+parseInt((obj.ghimat*obj.takhfif/100),10)+" ريال </td></tr>";
				if(obj.garanty_name!='')
					out+="<tr><td>اطلاعات گارانتی:"+obj.garanty_name+"<br> نشانی: "+obj.garanty_addr+"<br> تلفن: "+obj.garanty_tell+"</td></tr>";
				if(typeof obj.brand!='undefined' &&  obj.brand!='')
					out+="<tr><td>برند:"+obj.brand+"</td></tr>";
				out+="<tr><td>وزن: "+obj.vazn+"</td></tr>";
				if(obj.toz!='')
					out+="<tr><td>توضيحات: "+obj.toz+"</td></tr>";
				//out+="<tr><td><!-- <button onclick='showGall();' >عدم مشاهده گالری</button> --> </td>";
                                <?php
                                if($mojood)
                                {
                                ?>
				out+="<td style='text-align:left;' >تعداد:<input type='text' style='width:30px;'  id='tedadp_"+obj.id+"' class='sefaresh_tedad' onkeypress='return numbericOnKeypress(event);' value='1' ><span id='khoon1_"+obj.id+"' ></span><button onclick='sabad2("+obj.id+");' >سفارش</button></td></tr>";
                                <?php
                                }
                                ?>
				out+="</table>";
				return(out);
			}
			function sabad2(id)
			{
				var tedad = $("#tedadp_"+id).val();
				if(id>0 && parseInt(tedad)>0)
				{
					$("#khoon1_"+id).html('<img src="../img/status_fb.gif" width="20" >');
					$.get("index.php?kala_id="+id+"&tedad="+tedad+"&",function(result){
						$("#khoon1_"+id).html('');
						if(result=='true')
						{
							//$("#tedadp_"+id).val('');
							kharid(id);
							refreshSabad();
						}
						else
							alert('خطادر افزودن به سبد خرید');
					});
				}
				else
					alert('تعداد را وارد   ');
			}
			function showGall()
			{
				$("#gallery").toggle("slow");
			}
		</script>
	<div>
	<div id="content1" style="font-family:tahoma;" ></div>
	<div id="gallery" ></div>
