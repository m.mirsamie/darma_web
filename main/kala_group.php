<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$permission=array();
	$cl2=new mysql_class;
	//die("id is".$user_id);
	/*$canWrite='never';
	if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("kala_group.php",$permission))
			$canWrite='limited';
		
	}*/
	function loadGallery($id)
	{
		$id = (int)$id;
		$out = '----';
		$my = new mysql_class;
		$my->ex_sql("select `pic` from `kala_group` where `id` = '$id'",$q);
		if(isset($q[0]))
		{
			if (trim($q[0]["pic"])!='')
			{
				$pic = $q[0]["pic"];
				$out = "<img src=\"$pic\" class=\"imgGallTmb pointer\" onclick=\"startUpload1('$id')\"/>";
				$out .= "<div id=\"fileupload1_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr1_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
			else
			{
				$out = "<button  style=\"margin:10px;\" onclick=\"startUpload('$id');\">بروزرسانی تصویر</button>";
				$out .= "<div id=\"fileupload_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
		}		
		return($out);
	}
	function hiddenKalaGrp($kala_abarGroup_id)
	{
		$kala_abarGroup_id = (int)$kala_abarGroup_id;
		return("<div class='pointer notice' onclick='hiddenKalaAbar($kala_abarGroup_id);'>مخفی سازی</div><div class='pointer msg' onclick='showKalaAbar($kala_abarGroup_id);'>نمایش</div>");
	}
	function hiddenKalaGrpDo($kala_abarGroup_id)
	{
		$my = new mysql_class;
		$my->ex_sqlx("update kala left join kala_miniGroup on (kala_miniGroup_id=kala_miniGroup.id) left join kala_group on (kala_group_id=kala_group.id) set kala.en = -1  where kala_group.id=$kala_abarGroup_id");
	}
	function showKalaGrpDo($kala_abarGroup_id)
        {
                $my = new mysql_class;
                $my->ex_sqlx("update kala left join kala_miniGroup on (kala_miniGroup_id=kala_miniGroup.id) left join kala_group on (kala_group_id=kala_group.id) set kala.en = 1  where kala_group.id=$kala_abarGroup_id");
        }
	if(isset($_REQUEST['hide']))
	{
		$kala_abarGroup_id = (int)$_REQUEST['kala_abarGroup_id'];
		hiddenKalaGrpDo($kala_abarGroup_id);
		die('true');
	}
	if(isset($_REQUEST['show']))
        {
                $kala_abarGroup_id = (int)$_REQUEST['kala_abarGroup_id'];
                showKalaGrpDo($kala_abarGroup_id);
                die('true');
        }

	$mysql = new mysql_class;
	$msg ='';
	$isAdmin = $se->detailAuth('all');
        $gname = 'grid1';
        $input =array($gname=>array('table'=>'kala_group','div'=>'main_div_kalagrp'));
        $xgrid = new xgrid($input);
	//$xgrid->alert = TRUE;
        $xgrid->column[$gname][0]['name'] = '';
        $xgrid->column[$gname][2]['name'] = 'نام';
	$xgrid->column[$gname][2]['search'] = 'text';
	$xgrid->column[$gname][1]['name'] = 'دسته';
	$xgrid->column[$gname][1]['clist'] = columnListLoader('kala_abarGroup');
	$xgrid->column[$gname][3]['name'] = 'توضیحات';
	$xgrid->column[$gname][4]['name'] = '';
	$xgrid->column[$gname][5]['name'] = 'ترتیب';
	$xgrid->column[$gname][6] = $xgrid->column[$gname][0];
	$xgrid->column[$gname][6]['name'] = 'آیکون';
	$xgrid->column[$gname][6]['cfunction'] = array('loadGallery');
	$xgrid->column[$gname][6]['access'] = 'a';
	$xgrid->column[$gname][7] = $xgrid->column[$gname][0];
        $xgrid->column[$gname][7]['name'] = 'مخقی سازی';
        $xgrid->column[$gname][7]['cfunction'] = array('hiddenKalaGrp');
        $xgrid->column[$gname][7]['access'] = 'a';
        //if($canWrite=='all' || $canWrite=='limited')
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
	{
		$xgrid->canAdd[$gname] = TRUE;
		$xgrid->canEdit[$gname] = TRUE;
		
	}
	//if($se->detailAuth('all') || $se->detailAuth('doWrite'))
       	//	$xgrid->canEdit[$gname] = TRUE;
       // $xgrid->canDelete[$gname] = $canDo;
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);	
?>
<script>
	function hiddenKalaAbar(kala_abarGroup_id)
	{
		if(confirm('آیا مخفی سازی انجام شود؟'))
			$.get("kala_group.php?hide=hide&kala_abarGroup_id="+kala_abarGroup_id+"&",function(result){
				alert('مخفی سازی با موفقیت انجام شد');
			});
	}
	function showKalaAbar(kala_abarGroup_id)
        {
		if(confirm('آیا نمایش دادن انجام شود؟'))
			$.get("kala_group.php?show=show&kala_abarGroup_id="+kala_abarGroup_id+"&",function(result){
				alert('نمایش با موفقیت اعمال شد');
			});
        }
	function RPage()
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load('kala_group.php');
	}
	function startUpload(id)
	{
		$("#ifr_"+id).prop("src","upload_pic.php?table=kala_group&id="+id+"&");
		$("#fileupload_"+id).toggle();
	}
	function startUpload1(id)
	{
		$("#ifr1_"+id).prop("src","upload_pic.php?table=kala_group&id="+id+"&");
		$("#fileupload1_"+id).toggle();
	}
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
        });
</script>
<div id="main_div_kalagrp">
</div>
