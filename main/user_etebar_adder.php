<?php
	session_start();
	$current_dir =  __DIR__;
	$dir_sep = DIRECTORY_SEPARATOR;
	$dirs = explode($dir_sep,$current_dir);
	unset($dirs[count($dirs)-1]);
	$main_path = implode($dir_sep,$dirs);
	date_default_timezone_set("Asia/Tehran");
	include_once($main_path.'/class/mysql_class.php');
	include_once($main_path.'/class/conf.php');
	include_once($main_path.'/class/session_class.php');
	include_once($main_path.'/class/user_class.php');
	include_once($main_path.'/class/user_etebar_class.php');
	include_once($main_path.'/class/profile_class.php');
	include_once($main_path.'/class/email_class.php');
	include_once($main_path.'/class/company_class.php');
	include_once($main_path.'/class/company_etebar_class.php');
        include_once($main_path.'/jdf.php');
	if(count($argv) == 8)
	{
		$user_id = (int)trim($argv[1]);
		$typ = (int)trim($argv[2]);
		$mablagh = (int)trim($argv[3]);
		$toz = trim($argv[4]);
		$user_sabt_id = (int)trim($argv[5]);
		$is_enteghal = (strtoupper(trim($argv[6]))=='TRUE')?TRUE:FALSE;
		$transaction_id = (int)trim($argv[7]);
		//echo ("user_id=$user_id,typ=$typ,mablagh=$mablagh,toz=$toz,user_sabt_id=$user_sabt_id");
		$u = user_etebar_class::add($user_id,$typ,$mablagh,$toz,$user_sabt_id,$is_enteghal);
		$my = new mysql_class;
		$my->ex_sqlx("update user_etebar set transaction_id=$transaction_id where id=$u");
		echo $u;
	}
	else
	{
		echo "-1";
	}
?>
