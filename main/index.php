<?php
	include_once('../kernel.php');
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	include_once('../simplejson.php');
        if(isset($_REQUEST['regCode']))
        {
                $uid = audit_class::codeToId($_REQUEST['regCode']);
                $usr = new user_class((int)$uid);
                if(isset($usr->en) && $usr->en == 0)
                {
			$my = new mysql_class;
                        $my->ex_sqlx("update `user` set `en` = 1,login_count=login_count+1 where `id` = $uid");
                        $_SESSION[$conf->app.'_user_id'] = $usr->id;
                }
        }
	$agent = strtolower($_SERVER['HTTP_USER_AGENT']);
	$isWindows = (strpos($agent,'windows')!==FALSE);
	$floatWindowTopDef = $isWindows?170:140;
	$loged = isset($_SESSION[$conf->app.'_user_id']);
	$userName = '';
	$jender = '';
	if($loged)
	{
		$user_id = (int)$_SESSION[$conf->app.'_user_id'];
		$user = new user_class($user_id);
		$poro = new profile_class($user_id);
		$jender = ((int)$poro->jender==1)?'آقای':'خانم';
		$userName = isset($user->fname)?$jender.' '.$user->fname.' '.$user->lname.' کد مشتری : '.audit_class::idToCode($user_id):'کاربر عزیز';
	}
	if(isset($_REQUEST["userName"]))
		die($userName);
	$userName = '<span id="userName" class="blue1" '.($loged?'':'style="display:none;"').'>'.$userName.'</span>';
	$loginImg = $loged?'out.png':'pass.png';
	$loginTit = $loged?'خروج':'ورود کاربران';
	$ozviatImg = $loged?'Login.png':'user.png';
	$ozviatTit = $loged?'پروفایل':'عضویت';
	$url_name = isset($_REQUEST['url_name'])?$_REQUEST['url_name']:'';
	if($url_name == '' )
	{
		$kala_abarGroup = new kala_abarGroup_class;
                $kala_abarGroup->loadByUrlName('supermarket');
	}
	else
        {
            $kala_abarGroup = new kala_abarGroup_class;
            $kala_abarGroup->loadByUrlName($url_name);
            
        }
	$daste = $kala_abarGroup->id;
	$sabad = new sabad_class;
	//////////////////////date
	$timezone = 0;//برای 3:30 عدد 12600 و برای 4:30 عدد 16200 را تنظیم کنید
	$now = date("Y-m-d", time()+$timezone);
	$time = date("H:i:s", time()+$timezone);
	list($year, $month, $day) = explode('-', $now);
	list($hour, $minute, $second) = explode(':', $time);
	$timestamp = mktime($hour, $minute, $second, $month, $day, $year);
	$jalali_date = $userName.'<span class="blue1">'.jdate("تاریخ: Y/m/d",$timestamp).'</span>';
	//////////////////////
	if(isset($_SESSION['sabad']) && !(isset($_REQUEST['sabad'])))
		$sabad = $_SESSION['sabad'];
	else
		$_SESSION['sabad'] = $sabad;
	if(isset($_REQUEST['kala_id']))
	{
		$kala_id = (int)$_REQUEST['kala_id'];
		$tedad = (int)$_REQUEST['tedad'];
		$sabad->user_id = isset($user_id)?$user_id:-1;
		$out = ($sabad->add($kala_id,$tedad))?'true':'false';
		die($out);
	}
	if(isset($_REQUEST['mablagh']) && isset($_REQUEST['factor_id']))
	{
		$out = array('rahgiri'=>0,'pay_code'=>array(),'status'=>FALSE);
		$mablagh = (int)$_REQUEST['mablagh'];
		$factor_id = (int)$_REQUEST['factor_id'];
		if($factor_id >0)
		{
			$factorTest = new factor_class($factor_id);
			$factorTest->jam();
			if(isset($user_id) && $user_id > 0 && isset($factorTest->id) && $mablagh > 0 && $factorTest->id>0 && $factorTest->jamKol>0 && ($factorTest->jamKol+$factorTest->hazineErsal)==$mablagh)
			{
				$pardakht_id =  pardakht_class::add($user_id,date("Y-m-d H:i:s"),$mablagh,$factor_id);
				$pardakht = new pardakht_class($pardakht_id);
				$out['rahgiri'] = $pardakht->getBarcode();
				$out['pay_code'] = pay_class::ps_pay($pardakht_id,$mablagh);
				$out['status'] = TRUE;
			}
		}
		else if($factor_id == -130)
		{
			$pardakht_id =  pardakht_class::add($user_id,date("Y-m-d H:i:s"),$mablagh);
			$pardakht = new pardakht_class($pardakht_id);
			$out['rahgiri'] = $pardakht->getBarcode();
			$out['pay_code'] = pay_class::ps_pay($pardakht_id,$mablagh);
			$out['status'] = TRUE;
		}
		die(toJSON($out));
	}
	$showSlide = FALSE;
	if(!isset($_REQUEST['daste']) || (isset($_REQUEST['daste']) && $_REQUEST['daste']=='home'))
		$showSlide = TRUE;
	$jsabad = toJSON($sabad);
	if(isset($_REQUEST['refreshSabad']))
		die($jsabad);
	$tabs = '';
	$kg = array();
	$kagi = -1;
	if(isset($kala_abarGroup->id))
	{
		$kagi = $kala_abarGroup->id;
		$kg = kala_group_class::getAll($url_name,TRUE);
		foreach($kg as $kalaGroup)
			$tabs .= '<li><a href="loadKala.php?kala_group_id='.$kalaGroup['id'].'&" ><span>'.$kalaGroup['name'].'</span></a></li>';
	}
	else
	{
		$tabs = '<li><a href="loadKala.php?home=1&" ><span>صفحه اصلی</span></a></li>';
	}
	$sabad_count = count($sabad->kalas);
	$sabad_jam_kol = monize($sabad->jam_kol); 
	$showIcon1 = (count($sabad->kalas)<=0)?'block':'none';
	$showIcon = (count($sabad->kalas)>0)?'block':'none';
	$slideShowContentAll =  slideShow_class::getAll($kagi);
	$kalaSlide = kala_class::getTimeLimit();
	$slideShowContent = $slideShowContentAll['img'];
	$men = new menu_class($loged,$userName);
	$menu = $men->menu;
	$city_woeids = array(
		"تهران"=>2251945,
		"مشهد"=>2254914,
		"شیراز"=>2255202,
		"اصفهان"=>2254572,
		"کرمان"=>2431862,
		"تبریز"=>2255239,
		"اهواز"=>2254294,
		"قم"=>2255062,
		"کرمانشاه"=>2254797,
		"رشت"=>2255086,
		"کرمان"=>2431862,
		"ارومیه"=>2242302,
		"زاهدان"=>2253501,
		"همدان"=>2254664,
		"اراک"=>2254333,
		"کرج"=>2254777,
		"یزد"=>2253355,
		"قزوین"=>20070200,
		"اردبیل"=>20070198,
		"بندر عباس"=>2218961,
		"زنجان"=>2255311,
		"خرم آباد"=>2254824,
		"سنندج"=>2255130,
		"گرگان"=>2217763,
		"ساری"=>2255151,
		"شهرکرد"=>2255184,
		"بندر بوشهر"=>2254463,
		"بجنورد"=>2220377,
		"بیرجند"=>2254447,
		"ایلام"=>2345775,
		"سمنان"=>2345784,
		"یاسوج"=>2255297
	);
	$cities = '';
	foreach($city_woeids as $name=>$value)
		$cities .= "<option value='$value' ".(($name=='مشهد')?'selected':'').">$name</option>";
?>
<!doctype html>
<html lang="fa">
<head>
	<meta charset="utf-8">
	<title><?php echo $conf->title;  ?></title>
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/jquery-ui.css">
	<link rel="stylesheet" href="<?php echo (isset($kala_abarGroup->css) && trim($kala_abarGroup->css)!='')?trim($kala_abarGroup->css):'../css/erstyle.css';?>">
	<link rel="stylesheet" href="../css/jquery.lightbox-0.5.css">
<!--		<link rel="stylesheet" href="../css/jquery.tooltip.css"> -->
		<link rel="stylesheet" href="../css/xgrid.css">
                <link rel="stylesheet" type="text/css" media="all" href="../js/cal/skins/aqua/theme.css" title="Aqua" />
		<script src="../js/jquery.min.js"></script>
<!--
		<script src="../js/jquery.slides.min.js"></script>
-->
	<script src="../js/jquery.cycle.all.js"></script>
	<script src="../js/jquery.lightbox-0.5.min.js"></script>
	<script src="../js/jquery-ui.js"></script>
<!--		<script src="../js/jquery.tooltip.js"></script>-->
		<script src="../js/date.js" ></script>
		<script src="../js/inc.js"></script>
		<script src="../js/md5.js"></script>
		<script src="../js/tmp.js" ></script>
		<script src="../js/grid.js"></script>
		<script src="../js/weather_translation.js"></script>
                <script type="text/javascript" src="../js/cal/jalali.js"></script>
                <script type="text/javascript" src="../js/cal/calendar.js"></script>
                <script type="text/javascript" src="../js/cal/calendar-setup.js"></script>
                <script type="text/javascript" src="../js/cal/lang/calendar-fa.js"></script>	
		<script src="../js/gistfile1.js"></script>
		<script type="text/javascript" src="../js/jcarousellite_1.0.1.pack.js" ></script>
		<script>
		var sabad = <?php echo $jsabad; ?>;
		var tabCount = <?php echo count($kg); ?>;
		var logedIn = <?php echo $loged?'true':'false'; ?>;
		var ps_payPage = '<?php echo $conf->ps_payPage; ?>';
		var slideTitles = <?php echo toJSON($slideShowContentAll['title']); ?>;
		var kalaSlide = <?php echo toJSON($kalaSlide); ?>;
		var currentSec = <?php echo strtotime(date("Y-m-d H:i:s")); ?>;
		var daste = '<?php echo $daste; ?>';
		var startSec = [];//<?php //echo strtotime(date("Y-m-d 23:59:59"))-strtotime(date("Y-m-d H:i:s")); ?>;
		var openFactor = false;
		var kalaSlideIndex = 1;
		var shegeftText = '<?php echo $conf->shegeftText; ?>';
		var floatWindowTopDef = <?php echo $floatWindowTopDef; ?>;
		var floatWindowTop =  floatWindowTopDef;
		var kharid_disabled = <?php echo ($conf->kharid_disabled === TRUE)?'true':'false'; ?>;
		var open_popup = <?php echo ($conf->open_popup===TRUE)?'true':'false'; ?>;
	</script>
	<script src="../js/index.js"></script>
	<script>
		function popup(data) 
		    {
			var mywindow = window.open('', 'my div', 'height=400,width=600');
			mywindow.document.write('<html><head><meta charset="utf-8"><title>darma</title>');
			/*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
			mywindow.document.write('</head><body style="width:8cm;direction:rtl;font-family:tahoma;font-size:12px;" >');
			mywindow.document.write('<div style="width:7cm;border:solid 1px #ddd;" >'+data+'</div>');
			mywindow.document.write('</body></html>');

			mywindow.document.close(); // necessary for IE >= 10
			mywindow.focus(); // necessary for IE >= 10

			mywindow.print();
			mywindow.close();

			return true;
		    }
/*
			$(document).ready(function() {
			    var s = $("#sticker");
			    var pos = s.position();                   
			    $(window).scroll(function() {
				var windowpos = $(window).scrollTop();
				if (windowpos >= pos.top) {
				    s.addClass("stick");
				} else {
				    s.removeClass("stick");
				}
			    });
			});
*/
		</script>
		
		<!-- Brands -->
		<script type="text/javascript">
		var menu_data = <?php echo toJSON(kala_group_class::getMini()); ?>;
		function getKalaMini(id)
		{
			var out = [];
			if(typeof menu_data[id] != 'undefined')
				out = menu_data[id];
			return(out);
		}
		</script>
		
	<style>
			#timer
			{
				border : 1px #eaeaea solid;
                                background : #ffffff;
                                height : 50px;
			}
			.ui-tabs { direction: rtl; }
			.ui-tabs .ui-tabs-nav li.ui-tabs-selected,
			.ui-tabs .ui-tabs-nav li.ui-state-default {float: right; }
			.ui-tabs .ui-tabs-nav li a { float: right; }
			.leftborderBlue{
				border-left:solid 1px #a7d7f9;
				width:200px;
			}
			li
			{
				font-family:tahoma;
			}
			.sabad_mojoodi
			{
				font-size:12px;
				color:red;
			}
			.shegeftKalaSelected
			{
				color:red;
			}
			div#wrapper {
			    margin:0px;
			    width:800px;
			    background:#FFF;
			}
			div#mainContent {
			    width:560px;
			    padding:5px;
			    float:left;
			}
			div#sideBar {
			    width:230px;
			    padding:0px;
			    margin-left:0px;
			    float:left;
			}
			.clear {
			    clear:both;
			}
			.stick {
			    position:fixed;
			    top:0px;
			}
			.selecetd_brand{
				
			}
		</style>
</head>
<body>

	<!-- Header -->
	<div class="erhdr">
		<div class="erlogo"><a href="http://darma.ir"><img src="../images/logo.png" alt="سامانه فروش دارما، فروشگاه های زنجیره ای دارما"></a></div>
		<div class="ersearch"><input id="searchItem" name="searchItem" placeholder="جستجو" /></div>
		<div class="ertopmenu"><?php echo $menu ?></div>
		
		<div class="clear"></div>
	</div>
	
	<!-- Main Cat Menu -->
	<div class="ermaincatmenu">
		<?php 
		$abar = kala_abarGroup_class::getAll($daste);
		echo $abar['abar'];
		?>
		
		<div class="erlinks">
			<ul>
				<li><a target="_blank" href="../img/broshur.png">کاتالوگ دارما</a></li>
				<li><a target="_blank" href="https://www.dropbox.com/s/lsnebovcvq3syj6/Darma_Web.apk">همراه مارکت دارما</a></li>
			</ul>
		</div>
	</div>
	
	<!-- Cat Menu -->
	<div class="ercatmenu">
		<?php 
		$abar = kala_abarGroup_class::getAll($daste);
		//echo $abar['abar'];
		echo $abar['group'];
		?>
	</div>
		
		
		<?php 
		/*
		<div id="headerDiv">
			<table class="headerTable" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<?php echo $menu; ?>
				</tr>
				<tr>
					<td class="rightBannerTd" rowspan="3">
					</td>
					<td class="middleBannerTd">
                                        </td>
					<td class="leftBannerTd" align="left" >
						<table>
						<tr>
						<td style="font-size:20px;color:blue;">
							<a href="../img/broshur.png" target="_bank" >بروشور دارما</a>
						</td>
						<td>
							<img src="../img/barcode.gif" height="50px" />
						</td>
						<td style="font-size:20px;color:blue;">
							<a href="https://www.dropbox.com/s/lsnebovcvq3syj6/Darma_Web.apk" target="_blank">همراه مارکت دارما >>></a>
						</td>
						<td>
						<a href="https://www.dropbox.com/s/lsnebovcvq3syj6/Darma_Web.apk" target="_blank"><img title="دریافت نسخه آندرویدی" src="../img/android.png" width="50px" /></a>
						</td>
						<td>
							&nbsp;
						</td>
						<td>
						<span style="font-weight:bold;color:#FFFFFF;font-size:14px;" >
						چی لازم دارید؟
						</span>
						</td>
						<td>
						<input id="searchItem" name="searchItem" placeholder="جستجو" style="width:200px;margin-left:20px;" />
						</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td id="right" colspan="3">
						<div id="drawer" align="right">
							<table class="drawerTable">
								<tr>
<!--
									<td <?php echo (($daste == '' ||$daste == 'home')?"class='kalaAbarSelector drawerTable_active'":"class='kalaAbarSelector'"); ?> id="k_home" >
										
									</td>
-->
									<?php
										$abar = kala_abarGroup_class::getAll($daste);
										echo $abar['abar'];
									?>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</div>
		*/
		?>
		
	<?php 
	/*
	<div id="wrapper" style="padding-top:100px;">
		<div id="mainContent">
		<!--Content for your main div - like a blog post-->
		</div>
		<div id="sideBar">		    
		</div>
		<div class="clear"></div>
	</div>
	<div  align="center" style="display:none;" >
		<table style="margin-right:80px"  >
			<tr>
				<td>
					<div id="news" style="width:190px;border:solid 1px #3dc200;height:297px;padding:5%;" >
						<marquee behavior="scroll" direction="up" scrollamount="2" height="297" width="190">
							<div id="news_chd" >
							</div>
						</marquee>
					</div>
				</td>
				<td>
					<div id="slideShowDiv"  >
						<div id="slideShow" style="width:800px;float:left;" >
								<?php echo $slideShowContent; ?>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</div>
	
	<div id="shegeftDiv" align="center" >
		<div id="shegeft"  >
			<table border="0" width="100%">
				<tr>
					<td id="shegeftSelectors"></td>
					<td id="shegeftContainer"></td>
				</tr>
			</table>
		</div>
	</div>
	*/
	?>
	
	<div class="erwrapper">
	
		<div id="sticker" class="stickerClass">تمامی کالا ها و خدمات این فروشگاه، حسب مورد دارای مجوزهای لازم از مراجع مربوطه می باشند. فعالیت های این سایت تابع قوانین و مقررات جمهوری اسلامی ایران است</div>
	
		<div id="body_div" class="ercontent">
			<div class="ercontext">
				<div id="kalaDiv"></div>
			</div>
		</div>
		<div id="body_div_left" class="ersidebar">
			<?php echo $slideShowContent; ?>
		</div>
		<div class="clear"></div>
	</div>
	
	<div class="clear"></div>
	
	<div id="brands_container" class="erbrands" align="center">
		<div class="erbrandstext">
			با کلیک روی هر یک از برندهای زیر لیست محصولات آن را مشاهده کنید.
			<a href="#" id="brands_show" onclick="loadBrands();return false;">نمایش تمام برندها</a>
		</div>
		<!-- <marquee behavior="scroll" direction="right" scrollamount="2" scrolldelay="50" truespeed="truespeed" height="114" width="98%"> -->
			<div id="brands"><?php echo brand_class::loadAll(); ?></div>
		<!-- </marquee> -->
	</div>
		<div id="bottomDiv">
		</div>
		<div id="footer">
		</div>
		<div id="dialog"></div>
		<div id="kalaMiniDiv" style="display:none;"></div>
		<div id="rightFloatDiv" class="floatDiv">
			
		</div>
		<div id="leftFloatDiv" class="floatDiv">
			<table width="100%">
				<tr>
					<td id="sabad_td1" align = "center" style="display:<?php echo $showIcon1; ?>;">
						<img id="SabdEmpty" src="../img/bg.png" style="width:100px;" title="سبد شما خالی است!"/>
					</td>
					<td id="sabad_td" align = "center" style="display:<?php echo $showIcon; ?>;">
						<table border=0 width="100%">
							<tr>
								<td class="sabad_mojoodi pointer" style="text-align:center;">
									تعداد
								</td>
                                                                <td class="sabad_mojoodi pointer" style="text-align:center;">
									جمع (ریال)
								</td>
							</tr>
							<tr>
								<td class="sabad_mojoodi pointer" style="text-align:center;">
									<span id="sabad_count"  onclick="continSabad();" title="جهت ادامه خرید کلیک کنید">
									<?php
	                                                                        echo $sabad_count;
                                                                        ?>
									</span>
								</td>
								<td class="sabad_mojoodi pointer" style="text-align:center;">
									<span id="sabad_jam_kol"  onclick="continSabad();" title="جهت ادامه خرید کلیک کنید">
									<?php
	                                                                        echo $sabad_jam_kol;
                                                                        ?>
									</span>
								</td>
							</tr>
							<tr>
								<td align="center">
									<img id="StoreImg" src="../img/Store.png" style="width:70px;" class="pointer" onclick="continSabad();" title="جهت ادامه خرید کلیک کنید"/>
								</td>
								<td align="center">
									<img src="../img/cancel.png" class="pointer" style="width:30px;" onclick="emptySabad();" title="جهت خالی کردن سبد خرید کلیک کنید"/>
								</td>
							</tr>
							<tr>
								<td style="color:#ffffff;text-align:center;" class="pointer" onclick="continSabad();">

									ادامه خرید
								</td>
								<td style="color:#ffffff;text-align:center;" class="pointer" onclick="emptySabad();">
									انصراف
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<!--mouseover="viewTitles();" mouseout="hideTitles();"-->
		<!--  
		<div id="kalaGroupDiv" style="background:url('../img/b.png');"><?php echo $abar['group'] ?>
			<div id="kalaGroupTitle" style="display:none;"></div>
		</div>
		-->
		
		<!-- Footer -->
		<div id="footerDiv" class="erfooter">
			<div class="erfooterwrap">
				<div class="erfooterlinks">
					<div class="erfootertitle"><span>لینکهای مفید</span></div>
					<a href="#" onclick="openDialog('pos_report_auth.php',{title:'گزارش ترمینال ها',width:850,height:550});">ترمینال</a>
					<a href="http://darmafood.com/index.php" target="_blank">بازرگاني</a>
					<a href="http://darmafood.com/index.php/%D9%82%D9%88%D8%A7%D9%86%DB%8C%D9%86-%D9%88-%D9%85%D9%82%D8%B1%D8%B1%D8%A7%D8%AA" target="_blank">قوانين و مقررات</a>
					<a href="http://darmafood.com/index.php/%D8%B1%D8%A7%D9%87%D9%86%D9%85%D8%A7%DB%8C-%D8%AE%D8%B1%DB%8C%D8%AF" target="_blank">راهنما</a>
					<a href="http://darmafood.com/index.php/%D8%A7%D8%B9%D9%84%D8%A7%D9%85-%D9%85%D8%B4%DA%A9%D9%84-%DA%AF%D8%A7%D8%B1%D8%A7%D9%86%D8%AA%DB%8C" target="_blank">اعلام مشكل</a>
					<a href="http://darmafood.com/index.php/%D8%AA%D9%85%D8%A7%D8%B3-%D8%A8%D8%A7-%D9%85%D8%A7" target="_blank">تماس با ما</a>
                                        <a href="#" onclick="openDialog('card_transfer.php',{title:'گزارش ترمینال ها',width:850,height:550});" >انتقال کارتی</a>
					<a href="#" id="ozviatTitle"><span class="pointer ozviat" onclick="showOzviat(document.getElementById('ozviatIc'));return false;"><?php echo $ozviatTit; ?></span></a>
					<a href="#" id="loginTitle"><span class="pointer vorood" onclick="showLogin(document.getElementById('loginIc'));return false;"><?php echo $loginTit; ?></span></a>
				</div>
				
				<div class="erfooterweather">
					<div class="erfootertitle"><span>پیش بینی آب و هوا</span></div>
					<div class="erweatherselect">آب و هوای <select onchange="loadWeather(this);" id="city_woeid"><?php echo $cities; ?></select></div> 
					<div class="erweatherinfo" id="weather_sp" ><img src="../img/status_fb.gif" ></div>
				</div>
				
				<div class="erfooterlinks">
					<div class="erfootertitle"><span>سایتهای مرتبط</span></div>
					<?php echo $conf->links; ?>
				</div>
				
				<div class="erenamad">
					<div class="erfootertitle"><span>نماد اعتماد الکترونیکی</span></div>
					<div class="erenamadcontent">
						<iframe src="enamad.htm" frameborder="0" scrolling="no" allowtransparency="true" style="width: 130px; height:150px;"></iframe>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		
		<div class="ersubfooter">
			کلیه حقوق مادی و معنوی این سایت متعلق به دارما می باشد.
			<br />
			Darma.ir - © 2.13 - 2014
		</div>
		
			<?php 
			/*
			<div style="margin-right:80px;margin-left:80px;border:solid 1px #999"  >
				<table>
					<tr>
						<td valign="top" >
							<table class="footer"  width="100%" >
								<tr>
									<td valign="top"  >
										سایتهای مرتبط					
									</td>
								</tr>
								<tr>
									<td>
										<?php echo $conf->links; ?>
									</td>
								</tr>
							</table>
						</td>
						<td valign="top"  >
							<table class="footer"  width="100%"  >
								<tr>
									<td valign="top"  >
										<table>
										<tr>
										<td>
										آب و هوای 
										</td>
										<td>
										<select onchange="loadWeather(this);" id="city_woeid"><?php echo $cities; ?></select>
										</td>
										</tr>
										</table>
									</td>   
								</tr>
								<tr>
									<td>
										<span id="weather_sp" ><img src="../img/status_fb.gif" ></span>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		*/
		?>
		<div id="msg_div" style="display:none;background:#ffffff;border:#eeeeee 3px solid;width:250px;height:100px;overflow-y:scroll;">
			<div id="msg_header" style="background:#eeeeee;height:20px;text-align:center;">
				<button onclick="newMsg();" style="height:15px;">
                                        جدید
                                </button>
				<button onclick="viewMsgs();" style="height:15px;">
                                        همه
                                </button>
				&nbsp;
				پیام ها
				&nbsp;
				&nbsp;
				&nbsp;
				&nbsp;
				<span style="display:none;" class="pointer" onclick="closeMsg();">
					X
				</span>
			</div>
			<div id="msg_body" style="padding:10px;">
			</div>
		</div>
	</body>
</html>
