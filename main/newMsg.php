<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
	$user_id=(int)($_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	if(isset($_REQUEST['rec_id']))
	{
		$out = -1;
		if(checkMobile(trim($_REQUEST['rec_id'])))
		{
			$pp = new profile_class;
			$pp->loadByMobile(trim($_REQUEST['rec_id']));
			if(isset($pp->user_id) && (int)$pp->user_id>0)
			{
				$rec_id = (int)$pp->user_id;
				$out = msg_class::sendMsg($rec_id,$_REQUEST['msg'],$user_id);
			}
		}
		else
		{
			$rec_id = audit_class::codeToId((int)$_REQUEST['rec_id']);
			$utmp = new user_class($rec_id);
			if($rec_id>0 && isset($utmp->id))
				$out = msg_class::sendMsg($rec_id,$_REQUEST['msg'],$user_id);
		}
		die("$out");
	}
	$cl1 = new user_class($user_id);
	$users = '';
	$my = new mysql_class;
	$my->ex_sql("select id,fname,lname from user order by lname,fname",$q);
	foreach($q as $r)
		$users .= '<option value="'.$r['id'].'">'.$r['fname'].' '.$r['lname'].'</option>';
?>
<script type="text/javascript" >
        $(document).ready(function(){
        });
	function sendMsg()
	{
		var rec_id = $("#rec_id").val();
		var msg = $("#msg").val();
		$.get("newMsg.php",{"rec_id":rec_id,"msg":msg},function(result){
			var out = parseInt(result,10);
			if(out>0 && !isNaN(out))
			{
				alert('پیام ارسال گردید');
				closeDialog();
			}
			else
				alert('خطا در ارسال پیام');
		});
	}
</script>
<div id="main_div_user" align="center">
	<h1>پیام از <?php echo $cl1->fname.' '.$cl1->lname; ?></h1>
	<h3>به (کد کاربر یا تلفن همراه):<input id="rec_id" /></h3>
	<textarea cols="40" rows="8" id="msg" ></textarea><br/>
	<button onclick="sendMsg();">ارسال</button>
</div>
