<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
        if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$permission=array();
	$cl2=new mysql_class;
	/*$canWrite='never';
	if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("etebar.php",$permission))
			$canWrite='limited';
		
	}*/
	$sabt="<button onclick='group_sabt();' disabled='disabled'>ثبت</button>";
	//if($canWrite=='limited' || $canWrite=='all')
	if(true)
		$sabt="<button onclick='group_sabt();'>ثبت</button>";
	function etebarCode($id)
	{
		$e = new etebar_class((int)$id);
		$cls = ($e->en == 1)?'msg':'notice';
		$st = ($e->en == 1)?'':'style="text-decoration: line-through"';
		return('<div class="'.$cls.'" '.$st.'>'.audit_class::newIdToCode($id).'</div>');
	}
	function getKode($length = 16) 
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++)
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		return $randomString;
	}
	function loadGallery($id)
	{
		$id = (int)$id;
		$out = '----';
		$my = new mysql_class;
		$my->ex_sql("select `zamime` as `img` from `etebar` where `id` = '$id'",$q);
		$out = var_export($q,TRUE);
		if(isset($q[0]))
		{
			if ($q[0]["img"]!='')
			{
				$pic = $q[0]["img"];
				$out = "<img style=\"width:60px;\" src=\"$pic\" class=\"imgGallTmb pointer\" onclick=\"startUpload1('$id')\"/><a class='msg' href='$pic' target='_blank' >مشاهده</a>";
				$out .= "<div id=\"fileupload1_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr1_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
			else
			{
				$out = "<button  style=\"margin:10px;\" onclick=\"startUpload('$id');\">بروزرسانی ضمیمه</button>";
				$out .= "<div id=\"fileupload_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
		}		
		return($out);
	}
	function delPadash($table,$id,$gname)
	{
		$et = new etebar_class($id);
		$out = FALSE;
		if(isset($et->id))
		{
			if($et->user_etebar_id>0)
				$out = 'false|این اعتبار قبلا به شخصی تخصیص داده شده است';
			else
			{
				$my = new mysql_class;
				$my->ex_sqlx("delete from etebar where id=$id");
				$out = TRUE;
			}
		}
		else
			$out='false|این رکورد قبلا حذف شده بوده است ';
		return($out);
	}
	function editUser($table,$id,$field,$val,$fn,$gname)
	{
		$out = FALSE;
		$etebar = new etebar_class($id);
		if(isset($etebar->id) && $etebar->$field <= 0)
		{
			$us_id = (int)$val;
			$conf = new conf;
			$user_sabt_id = $_SESSION[$conf->app.'_user_id'];
			//echo "change etebar ...\n";
			$out = $etebar->useIt($us_id,$user_sabt_id);
		}
		return($out);
	}
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	if(isset($_REQUEST['group_name']))
	{
		$zamime = '';
		$group_name = $_REQUEST['group_name'];
		$group_mablagh = (int)$_REQUEST['group_mablagh'];
		$group_tedad = (int)$_REQUEST['group_tedad'];
		$q1 = "insert into `etebar` (`name`,`mablagh`,`creator_user_id`,`kode`,`zamime`) values ";
		$q = '';
		for($i = 0;$i < $group_tedad;$i++)
		{
			$kode = getKode();
			if(etebar_class::repeatKode($kode))
					$q .=(($q != '')?',':'') . "('$group_name',$group_mablagh,$user_id,'$kode','$zamime')";
			else
					$i--;
		}
		$my = new mysql_class;
		$my->ex_sqlx($q1.$q);
		die('true');
	}
	$users = columnListLoader("user| `user` <> 'mehrdad' ",array('id','fname','lname'));
        $gname = "gname_etebar";
	$input =array($gname=>array('table'=>'etebar','div'=>'main_div_etebar'));
        $xgrid = new xgrid($input);
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = 'نام اعتبار';
	$xgrid->column[$gname][1]['search'] = 'text';
	$xgrid->column[$gname][2]['name'] = 'مبلغ(ریال)';
	$xgrid->column[$gname][3]['name'] = '';
	$xgrid->column[$gname][4]['name'] = 'کاربر ثبت کننده';
	$xgrid->column[$gname][4]['access'] = 'a';
	$xgrid->column[$gname][4]['clist'] = $users;
	$xgrid->column[$gname][5]['name'] = 'کاربر اعتبار گیرنده';
//	if(!$se->detailAuth('all') && !$se->detailAuth('doWrite'))
//		$xgrid->column[$gname][5]['access'] = 'a';
	$xgrid->column[$gname][5]['clist'] = $users;
	$xgrid->column[$gname][6]['name'] = 'استفاده شده';
	$xgrid->column[$gname][6]['access'] = 'a';
	$xgrid->column[$gname][6]['clist'] = array(1=>'مصرف نشده',2=>'استفاده شده'); 
	$xgrid->column[$gname][6]['search'] = 'list';
	$xgrid->column[$gname][6]['searchDetails'] = array(0=>'',1=>'مصرف نشده',2=>'استفاده شده');
	//$xgrid->column[$gname][] = $xgrid->column[$gname][0];
	$xgrid->column[$gname][7]['name'] = 'کد اعتبار';
	$xgrid->column[$gname][7]['access'] = 'a';
	$xgrid->column[$gname][8] = $xgrid->column[$gname][0];
	$xgrid->column[$gname][8]['name'] = 'ضمیمه';
	$xgrid->column[$gname][8]['cfunction'] = array('loadGallery');
	$xgrid->column[$gname][8]['access'] = 'a';
	//$xgrid->column[$gname][7]['cfunction'] = array('etebarCode');
	//$xgrid->column[$gname][7]['access'] = 'a';
	//if($canWrite=='all' || $canWrite=='limited')
//	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
//	{
/*
		$xgrid->canAdd[$gname] = TRUE;
		$xgrid->canDelete[$gname] = TRUE;
		$xgrid->deleteFunction[$gname] = 'delPadash';
*/
	        $xgrid->canEdit[$gname] = TRUE;
//	}
	$xgrid->editFunction[$gname] = 'editUser';
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);
?>
<script type="text/javascript" >
	var gname = '<?php echo $gname; ?>';
        $(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
        });
	function group_sabt()
	{
		var vals = {
			group_name : $("#group_name").val(),
			group_mablagh : parseInt($("#group_mablagh").val(),10),
			group_tedad : parseInt($("#group_tedad").val(),10)
		};
		if(vals.group_tedad > 0 && vals.group_name != '' && vals.group_mablagh > 0)
		{
			$("#group_tedad").after('<img src="../img/status_fb.gif" class="khh" />');
			$.get("etebar.php",vals,function(result){
				$(".khh").remove();
				if(result == 'true')
				{
					grid[gname].init(gArgs[gname]);
				}
				else
					alert('خطا در بروزرسانی');
			});
		}
		else
			alert('لطفا اطلاعات را به درستی وارد کنید');
	}
	function startUpload(id)
	{
		$("#ifr_"+id).prop("src","upload_pic.php?id="+id+"&table=etebar");
		$("#fileupload_"+id).toggle();
	}
	function startUpload1(id)
	{
		$("#ifr1_"+id).prop("src","upload_pic.php?id="+id+"&table=etebar");
		$("#fileupload1_"+id).toggle();
	}
	function RPage()
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load('etebar.php');
	}
</script>
<div id="groohi" style="border:1px solid #000000;width:60%;" class="round">
	<iframe src="etebar_groohi.php" style="width:100%;height:100%;border:none;"></iframe>
<!--
	<table>
		<tr style="background:#000000;color:#ffffff;">
			<th colspan="7">
				ثبت گروهی
			</th>
		</tr>
		<tr>
			<td>
				نام اعتبار : 
			</td>
			<td>
				<input id="group_name" name="group_name" placeholder="نام اعتبار" />
			</td>
			<td>
				مبلغ :
			</td>
			<td>
				<input id="group_mablagh" name="group_mablagh" placeholder="مبلغ اعتبار" />
			</td>
		</tr>
		<tr>
			<td>
				تعداد :
			</td>
			<td>
				<input id="group_tedad" name="group_tedad" placeholder="تعداد" />
			</td>
			<td>
				<?php echo $sabt; ?>		
			</td>
		</tr>
	</table>
-->
</div>
<div id="main_div_etebar"></div>
