<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	//$user_id=(int)($_SESSION[$conf->app.'_user_id']);
	$permission=array();
	$cl2=new mysql_class;
	function hiddenKalaMiniGrp($kala_abarGroup_id)
	{
		$kala_abarGroup_id = (int)$kala_abarGroup_id;
		return("<div class='pointer notice' onclick='hiddenKalaAbar($kala_abarGroup_id);'>مخفی سازی</div><div class='pointer msg' onclick='showKalaAbar($kala_abarGroup_id);'>نمایش</div>");
	}
	function hiddenKalaMiniGrpDo($kala_abarGroup_id)
	{
		$my = new mysql_class;
		$my->ex_sqlx("update kala left join kala_miniGroup on (kala_miniGroup_id=kala_miniGroup.id) set kala.en = -1  where kala_miniGroup.id=$kala_abarGroup_id");
	}
	function showKalaMiniGrpDo($kala_abarGroup_id)
        {
                $my = new mysql_class;
                $my->ex_sqlx("update kala left join kala_miniGroup on (kala_miniGroup_id=kala_miniGroup.id) set kala.en = 1  where kala_miniGroup.id=$kala_abarGroup_id");
        }
	if(isset($_REQUEST['hide']))
	{
		$kala_abarGroup_id = (int)$_REQUEST['kala_abarGroup_id'];
		hiddenKalaMiniGrpDo($kala_abarGroup_id);
		die('true');
	}
	if(isset($_REQUEST['show']))
        {
                $kala_abarGroup_id = (int)$_REQUEST['kala_abarGroup_id'];
                showKalaMiniGrpDo($kala_abarGroup_id);
                die('true');
        }
	/*$canWrite='never';
	if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("kala_miniGroup.php",$permission))
			$canWrite='limited';
		
	}*/
	$mysql = new mysql_class;
	$msg ='';
	$isAdmin = $se->detailAuth('all');
	$gname = 'grid1';
	$input =array($gname=>array('table'=>'kala_miniGroup','div'=>'main_div_miniGroup'));
	$xgrid = new xgrid($input);
//$xgrid->alert = TRUE;
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = 'گروه کالا';
	$xgrid->column[$gname][1]['clist'] = columnListLoader('kala_group');
	$xgrid->column[$gname][2]['name'] = 'نام';
	$xgrid->column[$gname][2]['search'] = 'text';
	$xgrid->column[$gname][3]['name']='';
	$xgrid->column[$gname][4] = $xgrid->column[$gname][0];
	$xgrid->column[$gname][4]['name']='مخفی سازی';
	$xgrid->column[$gname][4]['cfunction'] = array('hiddenKalaMiniGrp');
        $xgrid->column[$gname][4]['access'] = 'a';
	
	//if($canWrite=='limited' || $canWrite=='all')
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
	{
		$xgrid->canAdd[$gname] = TRUE;
		$xgrid->canDelete[$gname] = TRUE;
		$xgrid->canEdit[$gname]= TRUE;
	}
	$out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);	
?>
<script>
	function hiddenKalaAbar(kala_abarGroup_id)
	{
		if(confirm('آیا مخفی سازی انجام شود؟'))
			$.get("kala_miniGroup.php?hide=hide&kala_abarGroup_id="+kala_abarGroup_id+"&",function(result){
				alert('مخفی سازی با موفقیت انجام شد');
			});
	}
	function showKalaAbar(kala_abarGroup_id)
        {
		if(confirm('آیا نمایش دادن انجام شود؟'))
			$.get("kala_miniGroup.php?show=show&kala_abarGroup_id="+kala_abarGroup_id+"&",function(result){
				alert('نمایش با موفقیت اعمال شد');
			});
        }
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
        });
</script>
<div id="main_div_miniGroup">
</div>
