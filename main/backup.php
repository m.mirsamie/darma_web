<?php
	include_once("../kernel.php");
	$SESSION = new session_class;
	register_shutdown_function('session_write_close');
	session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
        die('<script>window.location="admin_login.php";</script>');//die($conf->access_deny);
    $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
    if(!$se->can_view)
        die('<script>window.location="admin_login.php";</script>');
	$conf = new conf;
	$host = $conf->host;
	$db =$conf->db;
	$user = $conf->user;
	$pass = $conf->pass;
	$date = date("Y-m-d H:i:s");
	$date= str_replace('/','-',$date);
	$date= str_replace(' ','-',$date);
	$date= str_replace(':','-',$date);
	$command = "mysqldump -u $user $db -p'$pass' > db/backup-$date.sql ";
	system($command);
	system("cd db ; tar -czf backup-$date.tar.gz backup-$date.sql ");
	unlink("db/backup-$date.sql");
?>
<div id="content" >
	<?php echo "<h1><a href='db/backup-$date.tar.gz' >دانلود</a></h1>"; ?>
</div>
