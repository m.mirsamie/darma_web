<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	function loadUsers()
	{
		$conf = new conf;
		$out = array(-1=>'');
		$my = new mysql_class;
		$my->ex_sql("select id,fname,lname,user from user where en=1 and not (user like 'mehrdad%' ) and company_id = ".$conf->company_id." order by lname,fname",$q);
		foreach($q as $r)
			$out[(int)$r['id']] = $r['fname'].' '.$r['lname'].'['.$r['user'].']';
		return($out);
	}
	function loadUsersF($id)
	{
		$u = new user_class((int)$id);
		return(isset($u->id)?$u->fname.' '.$u->lname:'----');
	}
	function loadMoshtari($id)
	{
		$u = new company_group_class((int)$id);
		return(isset($u->id)?$u->name:'----');
	}
	function jamUser($id)
	{
		$out = 0;
		if(isset($_REQUEST['azt']))
		{
			$tmp = new company_user_class($id);
			$my = new mysql_class;
			$azt = date("Y-m-d 00:00:00",strtotime(audit_class::hamed_pdateBack($_REQUEST['azt'])));
			$tat = date("Y-m-d 00:00:00",strtotime(audit_class::hamed_pdateBack($_REQUEST['tat'])));
			$user_id = $tmp->user_id;
			$my->ex_sql("select sum(mablagh) as kk from user_etebar where user_id = $user_id and regdate >= '$azt' and regdate <= '$tat' and typ = -1",$q);
			if(isset($q[0]))
				$out = monize((int)$q[0]['kk']);
			//$out = "select sum(mablagh) as kk from user_etebar where user_id = $user_id and regdate >= '$azt' and regdate <= '$tat' and typ = -1";
		}
		return($out);
	}
        function loadMali($id)
	{
		//$u = new user_class($id);
		$p = new profile_class($id);
		$etebar = (isset($p->etebar))?monize($p->etebar).(($p->min_etebar>0)?' - '.monize($p->min_etebar):''):'----';
		$main = isset($_REQUEST['main'])?'main=main&':'';
		$out = '<span class="msg pointer" onclick="loadCont(null,\'mali.php?user_id='.$id.'&'.$main.'\');" >گردش مالی [<span class="user_pool" >'.$etebar.'</span> ریال]</span>';
		return($out);
	}
	$permission=array();
	$cl2=new mysql_class;
	
	$mysql = new mysql_class;
	$msg ='';
	$isAdmin = $se->detailAuth('all');
	$gname = 'grid1';
	$input =array($gname=>array('table'=>'company_user','div'=>'main_div_company_user'));
	$xgrid = new xgrid($input);
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = 'مشتری حقوقی';
	$moshtari = columnListLoader('company_group');
	$xgrid->column[$gname][1]['cfunction'] = array('loadMoshtari');
	$xgrid->column[$gname][1]['search'] = 'list';
	$xgrid->column[$gname][1]['searchDetails'] = $moshtari;
	$xgrid->column[$gname][2]['name'] = 'کاربر';
	$xgrid->column[$gname][2]['cfunction'] = array('loadUsersF');
	$xgrid->column[$gname][2]['search'] = 'list';
	$xgrid->column[$gname][2]['searchDetails'] = loadUsers();
	$xgrid->column[$gname][3]['name'] = '';//'مدیر است';
/*
	$stat = array('-1'=>'','0'=>'خیر','1'=>'بله');
	$xgrid->column[$gname][3]['clist'] = $stat;
	$xgrid->column[$gname][3]['search'] = 'list';
	$xgrid->column[$gname][3]['searchDetails'] = $stat;
*/
	$xgrid->canAdd[$gname] = FALSE;
	$xgrid->canDelete[$gname] = FALSE;
	$xgrid->canEdit[$gname] = FALSE;
	$xgrid->column[$gname][] = $xgrid->column[$gname][0];
	$xgrid->column[$gname][4]['name'] = 'جمع(ریال)';
	$xgrid->column[$gname][4]['cfunction'] = array('jamUser');
        
        $xgrid->column[$gname][] = $xgrid->column[$gname][2];
	$xgrid->column[$gname][5]['name'] = 'جزییات حساب';
	$xgrid->column[$gname][5]['cfunction'] = array('loadMali');
	$xgrid->pageRows[$gname]=99999;
	$out =$xgrid->getOut($_REQUEST);
	if($xgrid->done)
		die($out);	
?>
<script>
	var gname = '<?php echo $gname; ?>';
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                args[gname]['afterLoad']=function(a){
                    loadJam();
                };
                intialGrid(args);
        });
	function searchCompRep()
	{
		gArgs[gname].eRequest = {
			'azt' : $("#azt").val(),
			'tat' : $("#tat").val()
		};
		grid[gname].init(gArgs[gname]);
	}
        function loadJam()
        {
            var sum=0;
            $.each($(".user_pool"),function(id,feild){
                sum+=parseInt((umonize($(feild).html())),10);
            });
            $("#hs_edtebar_div").html('<h1> جمع کلیه اعتبارات '+monize2(sum)+' ریال </h1>');
        }
</script>
<div id="serach_div">
	<input class="dateValue" id="azt" />
	<input class="dateValue" id="tat" />
	<button onclick="searchCompRep();">انتخاب تاریخ</button>
</div>
<div id="hs_edtebar_div" class="round" style="text-align: right;padding: 10px;margin: 5px;" ></div>
<div id="main_div_company_user">
</div>
