<?php
	include_once('../kernel.php');
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	include_once('../simplejson.php');
        if(isset($_REQUEST['regCode']))
        {
                $uid = audit_class::codeToId($_REQUEST['regCode']);
                $usr = new user_class((int)$uid);
                if(isset($usr->en) && $usr->en == 0)
                {
			$my = new mysql_class;
                        $my->ex_sqlx("update `user` set `en` = 1,login_count=login_count+1 where `id` = $uid");
                        $_SESSION[$conf->app.'_user_id'] = $usr->id;
                }
        }
	$agent = strtolower($_SERVER['HTTP_USER_AGENT']);
	$isWindows = (strpos($agent,'windows')!==FALSE);
	$floatWindowTopDef = $isWindows?170:140;
	$loged = isset($_SESSION[$conf->app.'_user_id']);
	$userName = '';
	$jender = '';
	if($loged)
	{
		$user_id = (int)$_SESSION[$conf->app.'_user_id'];
		$user = new user_class($user_id);
		$poro = new profile_class($user_id);
		$jender = ((int)$poro->jender==1)?'آقای':'خانم';
		$userName = isset($user->fname)?$jender.' '.$user->fname.' '.$user->lname.' کد مشتری : '.audit_class::idToCode($user_id):'کاربر عزیز';
	}
	if(isset($_REQUEST["userName"]))
		die($userName);
	$userName = '<br/><span id="userName" class="blue1" '.($loged?'':'style="display:none;"').'>'.$userName.'</span><br/><br/>';
	$loginImg = $loged?'out.png':'pass.png';
	$loginTit = $loged?'خروج':'ورود کاربران';
	$ozviatImg = $loged?'Login.png':'user.png';
	$ozviatTit = $loged?'پروفایل':'عضویت';
	$daste = isset($_REQUEST['daste'])?$_REQUEST['daste']:'';
	if($daste == '' || $daste == 'home')
	{
		$kala_abarGroup = new kala_abarGroup_class(1);
		$daste = 1;
	}
	else
		$kala_abarGroup = new kala_abarGroup_class((int)$daste);
	$sabad = new sabad_class;
	//////////////////////date
	$timezone = 0;//برای 3:30 عدد 12600 و برای 4:30 عدد 16200 را تنظیم کنید
	$now = date("Y-m-d", time()+$timezone);
	$time = date("H:i:s", time()+$timezone);
	list($year, $month, $day) = explode('-', $now);
	list($hour, $minute, $second) = explode(':', $time);
	$timestamp = mktime($hour, $minute, $second, $month, $day, $year);
	$jalali_date = $userName.'<span class="blue1">'.jdate("تاریخ: Y/m/d",$timestamp).'</span>';
	//////////////////////
	if(isset($_SESSION['sabad']) && !(isset($_REQUEST['sabad'])))
		$sabad = $_SESSION['sabad'];
	else
		$_SESSION['sabad'] = $sabad;
	if(isset($_REQUEST['kala_id']))
	{
		$kala_id = (int)$_REQUEST['kala_id'];
		$tedad = (int)$_REQUEST['tedad'];
		$sabad->user_id = isset($user_id)?$user_id:-1;
		$out = ($sabad->add($kala_id,$tedad))?'true':'false';
		die($out);
	}
	if(isset($_REQUEST['mablagh']) && isset($_REQUEST['factor_id']))
	{
		$out = array('rahgiri'=>0,'pay_code'=>array(),'status'=>FALSE);
		$mablagh = (int)$_REQUEST['mablagh'];
		$factor_id = (int)$_REQUEST['factor_id'];
		if($factor_id >0)
		{
			$factorTest = new factor_class($factor_id);
			$factorTest->jam();
			if(isset($user_id) && $user_id > 0 && isset($factorTest->id) && $mablagh > 0 && $factorTest->id>0 && $factorTest->jamKol>0 && ($factorTest->jamKol+$factorTest->hazineErsal)==$mablagh)
			{
				$pardakht_id =  pardakht_class::add($user_id,date("Y-m-d H:i:s"),$mablagh,$factor_id);
				$pardakht = new pardakht_class($pardakht_id);
				$out['rahgiri'] = $pardakht->getBarcode();
				$out['pay_code'] = pay_class::ps_pay($pardakht_id,$mablagh);
				$out['status'] = TRUE;
			}
		}
		else if($factor_id == -130)
		{
			$pardakht_id =  pardakht_class::add($user_id,date("Y-m-d H:i:s"),$mablagh);
			$pardakht = new pardakht_class($pardakht_id);
			$out['rahgiri'] = $pardakht->getBarcode();
			$out['pay_code'] = pay_class::ps_pay($pardakht_id,$mablagh);
			$out['status'] = TRUE;
		}
		die(toJSON($out));
	}
	$showSlide = FALSE;
	if(!isset($_REQUEST['daste']) || (isset($_REQUEST['daste']) && $_REQUEST['daste']=='home'))
		$showSlide = TRUE;
	$jsabad = toJSON($sabad);
	if(isset($_REQUEST['refreshSabad']))
		die($jsabad);
	$tabs = '';
	$kg = array();
	$kagi = -1;
	if(isset($kala_abarGroup->id))
	{
		$kagi = $kala_abarGroup->id;
		$kg = kala_group_class::getAll($daste,TRUE);
		foreach($kg as $kalaGroup)
			$tabs .= '<li><a href="loadKala.php?kala_group_id='.$kalaGroup['id'].'&" ><span>'.$kalaGroup['name'].'</span></a></li>';
	}
	else
	{
		$tabs = '<li><a href="loadKala.php?home=1&" ><span>صفحه اصلی</span></a></li>';
	}
	$sabad_count = count($sabad->kalas);
	$sabad_jam_kol = monize($sabad->jam_kol); 
	$showIcon1 = (count($sabad->kalas)<=0)?'block':'none';
	$showIcon = (count($sabad->kalas)>0)?'block':'none';
	$slideShowContentAll =  slideShow_class::getAll($kagi);
	$kalaSlide = kala_class::getTimeLimit();
	$slideShowContent = $slideShowContentAll['img'];
	$men = new menu_class($loged,$userName);
	$menu = $men->menu;
	$city_woeids = array(
		"تهران"=>2251945,
		"مشهد"=>2254914,
		"شیراز"=>2255202,
		"اصفهان"=>2254572,
		"کرمان"=>2431862,
		"تبریز"=>2255239,
		"اهواز"=>2254294,
		"قم"=>2255062,
		"کرمانشاه"=>2254797,
		"رشت"=>2255086,
		"کرمان"=>2431862,
		"ارومیه"=>2242302,
		"زاهدان"=>2253501,
		"همدان"=>2254664,
		"اراک"=>2254333,
		"کرج"=>2254777,
		"یزد"=>2253355,
		"قزوین"=>20070200,
		"اردبیل"=>20070198,
		"بندر عباس"=>2218961,
		"زنجان"=>2255311,
		"خرم آباد"=>2254824,
		"سنندج"=>2255130,
		"گرگان"=>2217763,
		"ساری"=>2255151,
		"شهرکرد"=>2255184,
		"بندر بوشهر"=>2254463,
		"بجنورد"=>2220377,
		"بیرجند"=>2254447,
		"ایلام"=>2345775,
		"سمنان"=>2345784,
		"یاسوج"=>2255297
	);
	$cities = '';
	foreach($city_woeids as $name=>$value)
		$cities .= "<option value='$value' ".(($name=='مشهد')?'selected':'').">$name</option>";
?>
<!doctype html>
<html lang="fa">
	<head>
		<meta charset="utf-8">
		<title><?php echo $conf->title;  ?></title>
		<link rel="stylesheet" href="../css/jquery-ui.css">
		<link rel="stylesheet" href="<?php echo (isset($kala_abarGroup->css) && trim($kala_abarGroup->css)!='')?trim($kala_abarGroup->css):'../css/style.css';?>">
		<link rel="stylesheet" href="../css/jquery.lightbox-0.5.css">
<!--		<link rel="stylesheet" href="../css/jquery.tooltip.css"> -->
		<link rel="stylesheet" href="../css/xgrid.css">
                <link rel="stylesheet" type="text/css" media="all" href="../js/cal/skins/aqua/theme.css" title="Aqua" />
		<script src="../js/jquery.min.js"></script>
<!--
		<script src="../js/jquery.slides.min.js"></script>
-->
		<script src="../js/jquery.cycle.all.js"></script>
		<script src="../js/jquery.lightbox-0.5.min.js"></script>
		<script src="../js/jquery-ui.js"></script>
<!--		<script src="../js/jquery.tooltip.js"></script>-->
		<script src="../js/index.js"></script>
		<script src="../js/date.js" ></script>
		<script src="../js/inc.js"></script>
		<script src="../js/md5.js"></script>
		<script src="../js/tmp.js" ></script>
		<script src="../js/grid.js"></script>
		<script src="../js/weather_translation.js"></script>
                <script type="text/javascript" src="../js/cal/jalali.js"></script>
                <script type="text/javascript" src="../js/cal/calendar.js"></script>
                <script type="text/javascript" src="../js/cal/calendar-setup.js"></script>
                <script type="text/javascript" src="../js/cal/lang/calendar-fa.js"></script>	
		<script src="../js/gistfile1.js"></script>
		<script>
			var sabad = <?php echo $jsabad; ?>;
			var tabCount = <?php echo count($kg); ?>;
			var logedIn = <?php echo $loged?'true':'false'; ?>;
			var ps_payPage = '<?php echo $conf->ps_payPage; ?>';
			var slideTitles = <?php echo toJSON($slideShowContentAll['title']); ?>;
			var kalaSlide = <?php echo toJSON($kalaSlide); ?>;
			var currentSec = <?php echo strtotime(date("Y-m-d H:i:s")); ?>;
			var daste = '<?php echo $daste; ?>';
			var startSec = [];//<?php //echo strtotime(date("Y-m-d 23:59:59"))-strtotime(date("Y-m-d H:i:s")); ?>;
			var openFactor = false;
			var kalaSlideIndex = 1;
			var shegeftText = '<?php echo $conf->shegeftText; ?>';
			var floatWindowTopDef = <?php echo $floatWindowTopDef; ?>;
			var floatWindowTop =  floatWindowTopDef;
		</script>
		<script>
/*
			$(document).ready(function() {
			    var s = $("#sticker");
			    var pos = s.position();                   
			    $(window).scroll(function() {
				var windowpos = $(window).scrollTop();
				if (windowpos >= pos.top) {
				    s.addClass("stick");
				} else {
				    s.removeClass("stick");
				}
			    });
			});
*/
		</script>
		<style>
			#timer
			{
				border : 1px #eaeaea solid;
                                background : #ffffff;
                                height : 50px;
			}
			.ui-tabs { direction: rtl; }
			.ui-tabs .ui-tabs-nav li.ui-tabs-selected,
			.ui-tabs .ui-tabs-nav li.ui-state-default {float: right; }
			.ui-tabs .ui-tabs-nav li a { float: right; }
			.leftborderBlue{
				border-left:solid 1px #a7d7f9;
				width:200px;
			}
			li
			{
				font-family:tahoma;
			}
			.sabad_mojoodi
			{
				font-size:12px;
				color:red;
			}
			.shegeftKalaSelected
			{
				color:red;
			}
			div#wrapper {
			    margin:0px;
			    width:800px;
			    background:#FFF;
			}
			div#mainContent {
			    width:560px;
			    padding:5px;
			    float:left;
			}
			div#sideBar {
			    width:230px;
			    padding:0px;
			    margin-left:0px;
			    float:left;
			}
			.clear {
			    clear:both;
			}
			.stick {
			    position:fixed;
			    top:0px;
			}
			.selecetd_brand{
				border : red 5px solid;
			}
		</style>
	</head>
	<body >
		
		<div id="headerDiv">
			<table class="headerTable" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<?php echo $menu; ?>
				</tr>
				<tr>
					<td class="rightBannerTd" rowspan="3">
					</td>
					<td class="middleBannerTd">
                                        </td>
					<td class="leftBannerTd" align="left" >
						<table>
						<tr>
						<td>
							<img title="سایت جدید دارما" style="cursor:pointer;" onclick="window.location='index_new.php';" src="../img/new.png" height="50px" />
						</td>
						<td>
							<img src="../img/barcode.gif" height="50px" />
						</td>
						<td style="font-size:20px;color:blue;">
							<a href="https://www.dropbox.com/s/lsnebovcvq3syj6/Darma_Web.apk" target="_blank">همراه مارکت دارما >>></a>
						</td>
						<td>
						<a href="https://www.dropbox.com/s/lsnebovcvq3syj6/Darma_Web.apk" target="_blank"><img title="دریافت نسخه آندرویدی" src="../img/android.png" width="50px" /></a>
						</td>
						<td>
							&nbsp;
						</td>
						<td>
						<span style="font-weight:bold;color:#FFFFFF;font-size:14px;" >
						چی لازم دارید؟
						</span>
						</td>
						<td>
						<input id="searchItem" name="searchItem" placeholder="جستجو" style="width:200px;margin-left:20px;" />
						</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td id="right" colspan="3">
						<div id="drawer" align="right">
							<table class="drawerTable">
								<tr>
<!--
									<td <?php echo (($daste == '' ||$daste == 'home')?"class='kalaAbarSelector drawerTable_active'":"class='kalaAbarSelector'"); ?> id="k_home" >
										
									</td>
-->
									<?php
										$abar = kala_abarGroup_class::getAll($daste);
										echo $abar['abar'];
									?>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</div>
<div id="wrapper">
		  <div id="mainContent">
		    <!--Content for your main div - like a blog post-->
		  </div>
		  <div id="sideBar">		    
		  </div>
		  <div class="clear"></div>
		</div>
		<div  align="center" >
			<table style="margin-right:80px"  >
				<tr>
					<td>
						<div id="news" style="width:190px;border:solid 1px #3dc200;height:297px;padding:5%;" >
							<marquee behavior="scroll" direction="up" scrollamount="2" height="297" width="190">
								<div id="news_chd" >
								</div>
							</marquee>
						</div>
					</td>
					<td>
						<div id="slideShowDiv"  >
							<div id="slideShow" style="width:800px;float:left;" >
									<?php echo $slideShowContent; ?>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div id="shegeftDiv" align="center" >
			<div id="shegeft"  >
				<table border="0" width="100%">
					<tr>
						<td id="shegeftSelectors"></td>
						<td id="shegeftContainer"></td>
					</tr>
				</table>
			</div>
		</div>
		<div id="sticker" class="stickerClass">تمامی کالا ها و خدمات این فروشگاه، حسب مورد دارای مجوزهای لازم از مراجع مربوطه می باشند. فعالیت های این سایت تابع قوانین و مقررات جمهوری اسلامی ایران است</div>
		<div id="brands_container" align="center">
			<div id="brands_show" style="background:#eeeeee;float:left;padding:10px;cursor:pointer;" onclick="loadBrands();">همه</div>
			<marquee behavior="scroll" direction="left" scrollamount="2" height="104" width="1600">
				<div id="brands"><?php echo brand_class::loadAll(); ?>
				</div>
			</marquee>
		</div>
		<div id="body_div" style="overflow: scroll;height:700px; ">
			<div id="kalaDiv">
			</div>
		</div>
		<div id="bottomDiv">
		</div>
		<div id="footer">
		</div>
		<div id="dialog"></div>
		<div id="kalaMiniDiv" style="display:none;"></div>
		<div id="rightFloatDiv" class="floatDiv">
			<iframe src="enamad.htm" frameborder="0" scrolling="no" allowtransparency="true" style="width: 200px; height:150px;"></iframe>
		</div>
		<div id="leftFloatDiv" class="floatDiv">
			<table width="100%">
				<tr>
					<td id="sabad_td1" align = "center" style="display:<?php echo $showIcon1; ?>;">
						<img id="SabdEmpty" src="../img/bg.png" style="width:100px;" title="سبد شما خالی است!"/>
					</td>
					<td id="sabad_td" align = "center" style="display:<?php echo $showIcon; ?>;">
						<table border=0 width="100%">
							<tr>
								<td class="sabad_mojoodi pointer" style="text-align:center;">
									تعداد
								</td>
                                                                <td class="sabad_mojoodi pointer" style="text-align:center;">
									جمع (ریال)
								</td>
							</tr>
							<tr>
								<td class="sabad_mojoodi pointer" style="text-align:center;">
									<span id="sabad_count"  onclick="continSabad();" title="جهت ادامه خرید کلیک کنید">
									<?php
	                                                                        echo $sabad_count;
                                                                        ?>
									</span>
								</td>
								<td class="sabad_mojoodi pointer" style="text-align:center;">
									<span id="sabad_jam_kol"  onclick="continSabad();" title="جهت ادامه خرید کلیک کنید">
									<?php
	                                                                        echo $sabad_jam_kol;
                                                                        ?>
									</span>
								</td>
							</tr>
							<tr>
								<td align="center">
									<img id="StoreImg" src="../img/Store.png" style="width:70px;" class="pointer" onclick="continSabad();" title="جهت ادامه خرید کلیک کنید"/>
								</td>
								<td align="center">
									<img src="../img/cancel.png" class="pointer" style="width:30px;" onclick="emptySabad();" title="جهت خالی کردن سبد خرید کلیک کنید"/>
								</td>
							</tr>
							<tr>
								<td style="color:#ffffff;text-align:center;" class="pointer" onclick="continSabad();">

									ادامه خرید
								</td>
								<td style="color:#ffffff;text-align:center;" class="pointer" onclick="emptySabad();">
									انصراف
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<!--mouseover="viewTitles();" mouseout="hideTitles();"-->
		<div id="kalaGroupDiv" style="background:url('../img/b.png');"><?php echo $abar['group'] ?>
			<div id="kalaGroupTitle" style="display:none;"></div>
		</div>
		<br/>
		<div id="footerDiv" style="background:#d5ebbc;color:#333;padding:10px;">
			<div style="margin-right:80px;margin-left:80px;border:solid 1px #999"  >
				<table>
					<tr>
						<td valign="top" >
							<table class="footer"  width="100%" >
								<tr>
									<td valign="top"  >
										سایتهای مرتبط					
									</td>
								</tr>
								<tr>
									<td>
										<?php echo $conf->links; ?>
									</td>
								</tr>
							</table>
						</td>
						<td valign="top"  >
							<table class="footer"  width="100%"  >
								<tr>
									<td valign="top"  >
										<table>
										<tr>
										<td>
										آب و هوای 
										</td>
										<td>
										<select onchange="loadWeather(this);" id="city_woeid"><?php echo $cities; ?></select>
										</td>
										</tr>
										</table>
									</td>   
								</tr>
								<tr>
									<td>
										<span id="weather_sp" ><img src="../img/status_fb.gif" ></span>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div id="msg_div" style="display:none;background:#ffffff;border:#eeeeee 3px solid;width:250px;height:100px;overflow-y:scroll;">
			<div id="msg_header" style="background:#eeeeee;height:20px;text-align:center;">
				<button onclick="newMsg();" style="height:15px;">
                                        جدید
                                </button>
				<button onclick="viewMsgs();" style="height:15px;">
                                        همه
                                </button>
				&nbsp;
				پیام ها
				&nbsp;
				&nbsp;
				&nbsp;
				&nbsp;
				<span style="display:none;" class="pointer" onclick="closeMsg();">
					X
				</span>
			</div>
			<div id="msg_body" style="padding:10px;">
			</div>
		</div>
	</body>
</html>
