<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$permission=array();
	$cl2=new mysql_class;
	//die("id is".$user_id);
	//$canWrite='';
	/*if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("kala.php",$permission))
			$canWrite='limited';
		
	}*/
	function loadBrand()
	{
		$out = array(0=>'');
		$mysql = new mysql_class;
		$mysql->ex_sql("select `id`,`name` from `kala_miniGroup` order by `name`",$q);
		foreach($q as $r)
			$out[$r['id']] = $r['name'];
		return $out;
	}
	function loadVahed()
	{
		$out = array();
		$mysql = new mysql_class;
		$mysql->ex_sql("select `id`,`name` from `vahed` order by `name` ",$q);
		foreach($q as $r)
			$out[$r['id']] = $r['name'];
		return $out;
	}
	function loadGallery($id)
	{
		$id = (int)$id;
		$out = '----';
		$my = new mysql_class;
		$my->ex_sql("select `pic` from `kala` where `id` = '$id'",$q);
		if(isset($q[0]))
		{
			if ($q[0]["pic"]!='')
			{
				$pic = $q[0]["pic"];
				$out = "<img src=\"$pic\" class=\"imgGallTmb pointer\" onclick=\"startUpload1('$id')\"/>";
				$out .= "<div id=\"fileupload1_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr1_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
			else
			{
				$out = "<button  style=\"margin:10px;\" onclick=\"startUpload('$id');\">بروزرسانی تصویر</button>";
				$out .= "<div id=\"fileupload_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
		}
		return($out);
	}
	function tarikh($inp)
	{
		$out = ($inp=='0000-00-00 00:00:00' || $inp=='')? '':jdate("Y/m/d",strtotime($inp));
		return($out);
	}
	function utarikh($inp)
	{
		$out =hamed_pdateBack2($inp).' 23:59:59';
		return($out);
	}
	$columnList = '';
	$visibleCols = isset($_REQUEST['cols'])?explode(',',$_REQUEST['cols']):array(1,2,3,4,5,6,8,9,23,26,28);
	$colNames = array('','ریز گروه‌کالا','نام','واحد','موجودی','وزن','توضیحات','','قیمت دارما','قیمت مصرف کننده','تعداد در هر بسته','تخفیف بسته‌ای','سقف تخفیف','تخفیف به ازای','میزان تخفیف به ازا','امتیاز','پیشنهاد ویژه','شگفت انگیز','تأمین کننده','گارانتی کننده','آدرس گارانتی','تلفن گارانتی','برند','نمایش','انبار','پرفروش','بارکد','','تصویر');
	$mysql = new mysql_class;
	$msg ='';
	$isAdmin = $se->detailAuth('all');
	$canDo = ($se->detailAuth('all') || $se->detailAuth('edit')) && (isset($_REQUEST['edit']) && $_REQUEST['edit']='edit');
        $gname = 'grid1';
        $input =array($gname=>array('table'=>'kala','div'=>'main_div_kala'));
        $xgrid = new xgrid($input);
	$xgrid->column[$gname][1]['clist'] = loadBrand();//$xgrid->arrayToObject(loadBrand());
	$xgrid->column[$gname][1]['search'] = 'list';
	$xgrid->column[$gname][1]['searchDetails'] = loadBrand();
	$xgrid->column[$gname][2]['search'] = 'text';
	$xgrid->column[$gname][3]['clist'] = loadVahed();
	$xgrid->column[$gname][7]['access'] = 's';
	//$xgrid->column[$gname][15]['search'] = 'text';
	//$xgrid->column[$gname][15]['access'] = 's';
	$xgrid->column[$gname][16]['clist'] = array(0=>'نیست',1=>'است');
	$xgrid->column[$gname][17]['cfunction'] =array('tarikh','utarikh');
	$xgrid->column[$gname][18]['clist'] = columnListLoader('taminkonande');
	$xgrid->column[$gname][22]['clist'] = columnListLoader('brand');
	$xgrid->column[$gname][23]['clist'] = array(-1=>'مخفی',1=>'نمایش',-2=>'مخفی-هزینه');
	$xgrid->column[$gname][23]['search'] = 'list';
	$xgrid->column[$gname][23]['searchDetails'] = array(0=>'',-1=>'مخفی',1=>'نمایش',-2=>'مخفی-هزینه');
	$xgrid->column[$gname][26]['search'] = 'text';
	$xgrid->column[$gname][] = $xgrid->column[$gname][0];
	$xgrid->column[$gname][28]['cfunction'] = array('loadGallery');
	$xgrid->column[$gname][28]['access'] = 'a';
	$xgrid->column[$gname][28]['search']='text';
	$xgrid->column[$gname][28]['sort']=TRUE;
	//$xgrid->echoQuery=TRUE;
	$xgrid->column[$gname][1]['searchDetails'] = loadBrand();
	//if($canWrite=='all' || $canWrite=='limited')
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
	{
		$xgrid->canAdd[$gname] = TRUE;
		$xgrid->canEdit[$gname] = TRUE;
	}
	
	foreach($xgrid->column[$gname] as $indx=>$col)
	{
		$checked = FALSE;
		if(in_array($indx,$visibleCols))
		{
			$xgrid->column[$gname][$indx]['name'] = $colNames[$indx];
			$checked = TRUE;
		}
		else
			$xgrid->column[$gname][$indx]['name'] = '';
		if(isset($colNames[$indx]) && $colNames[$indx] != '')
		{
			$columnList .= $colNames[$indx];
			$columnList .= '<input class="selCols" type="checkbox" id="sel_'.$col['fieldname'].'_'.$indx.'" '.(($checked)?'checked="checked"':'').'/>';
		}
	}
	if($columnList != '')
		$columnList .= '<button onclick="selectColumns();">انتخاب</button>';
	$out =$xgrid->getOut($_REQUEST);
	if($xgrid->done)
        	die($out);	
?>
<script>
	var ggname = '<?php echo $gname; ?>';
	
	function selectColumns()
	{
		var selFields = [];
		$(".selCols:checked").each(function(id,field){
			selFields.push(field.id.split('_')[field.id.split('_').length-1]);
		});
		gArgs[ggname].eRequest = {cols:selFields.join()};
		grid[ggname].init(gArgs[ggname]);
	}
	function loadPic(obj)
	{
		//alert(obj);
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load(obj,function(){
                        selectButton(obj);
                });
	}	
	function startUpload(id)
	{
		$("#ifr_"+id).prop("src","upload_pic.php?id="+id+"&");
		$("#fileupload_"+id).toggle();
	}
	function startUpload1(id)
	{
		$("#ifr1_"+id).prop("src","upload_pic.php?id="+id+"&");
		$("#fileupload1_"+id).toggle();
	}
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
        });
	function RPage()
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load('kala.php');
	}
</script>
<style>
	.imgGallTmb{width:50px;}
</style>
<div id="colSelector">
	<?php
		echo $columnList;
	?>
</div>
<div id="main_div_kala">
</div>
