<?php
	include_once("../kernel.php");
	$SESSION = new session_class;
	register_shutdown_function('session_write_close');
	session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
	$se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
	if(!$se->can_view)
        die('<script>window.location="admin_login.php";</script>');
	function mpdate($inp)
	{
		return(($inp != '' && $inp != '0000-00-00 00:00:00')?jdate("H:i d / m / Y",strtotime($inp)):'----');
	}
	$user_id = isset($_REQUEST['user_id'])?(int)$_REQUEST['user_id']:(int)$_SESSION[$conf->app.'_user_id'];
	if(isset($_REQUEST['comment']))
	{
		$out = 'false';
		$my = new mysql_class;
		$comment = trim($_REQUEST['comment']);
		$dep_id = (int)$_REQUEST['dep_id'];
		$subject = trim($_REQUEST['subject']);
		die(comment_tbl_class::add($user_id,$comment,$dep_id,$subject));
	}
	$u = new user_class($user_id);
	$p = new profile_class($user_id);
?>
<script type="text/javascript" >
	var pcode=0;
	var mablagh=0;
	var u_id = <?php echo $user_id; ?>;
	$(document).ready(function(){
		$("#scomment").show();
		$("#msg").hide()
	});
	
// *****************************************     start of edit    ********************************
	function bring_comment()
	{	
		$("#scomment").attr("style","display:block");
		$("#btnp").attr("onClick","toggle_fun();");
	}
	function toggle_fun()
	{	
		$("#scomment").toggle(300);
	}
	function sender_comment()
	{
		if($("#comment").val()=="")
			alert("درج پیغام خالی امکان پذیر نیست");
		else
		{
			$("#btns").after("<img src='../img/status_fb.gif' class='khoonBtns' />");
			var frm = $("#scomment_frm").serialize();
			$.get("comment_user.php?"+frm+"&",function(result){
				$(".khoonBtns").remove();
				if(result == 'true')
				{
					$("#scomment").hide();
					$("#msg").show()
					$("#msg").html('ثبت با موفقیت انجام شد');
				}
				else
					alert('خطا درثبت');
			});
		}
	}
//****************************************    end of edit   *****************************************


       
</script>
<style>
	#scomment div{ padding:5px;}
</style>

<!--  ***********************************  start of edit ******************************************* -->
<div class="content">
	<table style="font-family:tahoma;font-size:12px;padding-top:20px;width:100%" >
		<tr>
			<td>
				<div id="scomment" align="right" >
					<form id="scomment_frm" > 
						<div>
							موضوع:
						</div>
						<div>
							<input id="subject" name="subject"  >
						</div>
						<div>
							واحد مرتبط:
						</div>
						<div>
							<select id="dep_id" name="dep_id"  >
								<?php echo dep_class::loadCombo(); ?>
							</select>
						</div>
						<div>
							لطفا پیشنهاد خود را در کادر زیر وارد نمایید.
						</div>
						<div>
							<textarea id="comment" name="comment"  rows="10" cols="20"  ></textarea>
						</div>
						<div>
							<input style="font-family:tahoma" type="button" id="btns" value="ارسال" onclick="sender_comment();">
						</div>
					</form>
				</div>
				<div id="msg" >	</div>
			</td>
		</tr>
	</table>
<!-- **************************************    end of edit ******************************************* -->
</div>
</div>
