<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id=(int)($_SESSION[$conf->app.'_user_id']);
	$permission=array();
	$cl2=new mysql_class;
	/*$canWrite='never';
	if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("takhfif_set.php",$permission))
			$canWrite='limited';
		
	} */
	function add($gname,$table,$fields,$column)
	{
		$out = FALSE;
		$fi = '';
		$va ='';
		$conf = new conf;
		$mysql = new mysql_class;
		$fields['user_reg_id']= (int)$_SESSION[$conf->app.'_user_id'];
		$fields['tarikh'] =  date("Y-m-d H:i:s");
		foreach($fields as $key=>$val)
		{
			$fi.=($fi=='' ? '':',')."`$key`";
			$va.=($va=='' ? '':',')."'$val'";
		}
		$qu = '('.$fi.') values ('.$va.')';
		$ln = $mysql->ex_sqlx("insert into `$table` $qu",FALSE);
		$id = $mysql->insert_id($ln);
		$mysql->close($ln);
		if($id>0)
			$out = TRUE;
		return($out);
		
	}
	function editUser($table,$id,$field,$val,$fn,$gname)
	{
		//
	}
	function del($table,$id,$gname)
	{
		$my = new mysql_class;
		$my->ex_sqlx("update `$table` set `en`=0 where `id`=$id");
		return(TRUE);
	}
	function edit($inp)
	{
		$out = new user_class($inp);
		return(isset($out->fname)?$out->fname.' '.$out->lname :'نامعلوم');
	}
	function loadUser($inp)
	{
		$out = new user_class($inp);
		return(isset($out->id)?$out->fname.' '.$out->lname:'نامعلوم');
	}
	function loadDate($inp)
	{
		return(jdate("H:i -- d / m / Y",$inp));
	}
	function loadUserN()
	{
		$out = array();
		$m = new mysql_class;
		$m->ex_sql("select `id`,`fname`,`lname`,`user_daste_id` from `user` order by `lname`,`fname`",$q);
		foreach($q as $r)
		{
			$daste = new user_daste_class((int)$r['user_daste_id']);
			if(isset($daste->id))
				$out[$r['id']] = $r['lname'].' '.$r['fname'].'('.$daste->name.')';
		}
		return($out);
	}
	function loadTakhfif()
	{
		$out = array();
		$m = new mysql_class;
		$m->ex_sql("select `id`,`name`,`darsad` from `takhfif` where `en`=1 order by `name`",$q);
		foreach($q as $r)
			$out[$r['id']] = $r['name'].'('.$r['darsad'].')';
		return($out);
	}
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	
	$gname = 'grid1';
	$table = isset($_REQUEST['takhfif_typ']) ? $_REQUEST['takhfif_typ']:'takhfif';
	$input =array($gname=>array('table'=>$table,'div'=>'main_div_takhfif_det'));
	$xgrid = new xgrid($input);
	if(!isset($_REQUEST['takhfif_typ']))
		$xgrid->start =FALSE;// 
	else
	{
		$xgrid->eRequest[$gname]=array('takhfif_typ'=>$table);
		if($table=='takhfif_g_g')
		{
			$xgrid->column[$gname][1]['name']='گروه کالا';
			$xgrid->column[$gname][1]['clist'] = columnListLoader('kala_group');
			$xgrid->column[$gname][2]['name']='گروه کاربری';
			$xgrid->column[$gname][2]['clist'] = columnListLoader('user_daste');
		}
		else if($table=='takhfif_g_u')
		{
			$xgrid->column[$gname][1]['name']='گروه کالا';
			$xgrid->column[$gname][1]['clist'] = columnListLoader('kala_group');
			$xgrid->column[$gname][2]['name']='کاربر';
			$xgrid->column[$gname][2]['clist'] = loadUserN();
		}
		else if($table=='takhfif_k_g')
		{
			$xgrid->column[$gname][1]['name']='کالا';
			$xgrid->column[$gname][1]['clist'] = columnListLoader('kala');
			$xgrid->column[$gname][2]['name']='گروه کاربری';
			$xgrid->column[$gname][2]['clist'] = columnListLoader('user_daste');
		}
		else if($table=='takhfif_k_u')
		{
			$xgrid->column[$gname][1]['name']='کالا';
			$xgrid->column[$gname][1]['clist'] = columnListLoader('kala');
			$xgrid->column[$gname][2]['name']='کاربر';
			$xgrid->column[$gname][2]['clist'] = loadUserN();
		}
	}
	$xgrid->column[$gname][0]['name']='';
	$xgrid->column[$gname][3]['name']='تخفیف';
	$xgrid->column[$gname][3]['clist'] = loadTakhfif();
	$xgrid->column[$gname][4]['name']='ثبت کننده';
	$xgrid->column[$gname][4]['cfunction']=array('loadUser');
	$xgrid->column[$gname][4]['access']='a';
	$xgrid->column[$gname][5]['name']='تاریخ ثبت';
	$xgrid->column[$gname][5]['cfunction']=array('loadDate');
	$xgrid->column[$gname][5]['access']='a';
	$xgrid->addFunction[$gname] = 'add';
		
	//if($canWrite=='all' || $canWrite=='limited')
	if($se->detailAuth('all') || $se->detailAuth('doWrite'))
	{
		$xgrid->canAdd[$gname] = TRUE;
		$xgrid->canDelete[$gname]=TRUE;
		$xgrid->canEdit[$gname]= TRUE;
	}
	//$xgrid->deleteFunction[$gname] = 'del';
	$out =$xgrid->getOut($_REQUEST);
	if($xgrid->done)
	        die($out);
	
?>
<script>
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
		var table = '<?php echo $table; ?>';
		$("#takhfif_typ option").each(function () {
		    if ($(this).val() ==table)
			$(this).prop('selected', 'selected');
		});
        });
	function loadGrid1(inp)
	{
		var tt = $("#"+inp).val();
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load("takhfif_set.php?takhfif_typ="+tt);
	}
</script>
<div>
	انتخاب نحوه تخفیف:
	<select id="takhfif_typ" >
		<option value="takhfif_g_g" >گروه کالا به گروه کاربر-4</option>
		<option value="takhfif_g_u" >گروه کالا به کاربر-3</option>
		<option value="takhfif_k_g" >کالا به گروه کاربر-2</option>
		<option value="takhfif_k_u" >کالا به کاربر-1</option>
	</select>
	<button onclick="loadGrid1('takhfif_typ');" >انتخاب</button>
</div>
<div id="main_div_takhfif_det">
</div>
