<?php
    include_once('../kernel.php');
    $SESSION = new session_class;
    register_shutdown_function('session_write_close');
    session_start();
    include_once('../simplejson.php');
    if(!isset($_SESSION[$conf->app.'_user_id']))
    {
        die('جهت استفاده از این امکان می بایست ابتدا با کاربری خود از بالای سایت وارد شوید');
    }
    else
    {
        $user = new user_class((int)$_SESSION[$conf->app.'_user_id']);
        if(!isset($user->company_group_id) || $user->company_group_id <= 0)
        {
            die('جهت استفاده از این امکان می بایست کاربر برای یک فروشگاه باشد');
        }
    }
    if(isset($_REQUEST['card_number']))
    {
        $out = array("res"=>array(),"err"=>'');
        $c = new cards_class();
        $mablagh =isset($_REQUEST['card_mablagh'])?(int)$_REQUEST['card_mablagh']:0;
        $c->loadByShomare($_REQUEST['card_number']);
        if(isset($c->id))
        {
            if($c->pass == md5($_REQUEST['card_pass']))
            {
                $p = new profile_class($c->user_id);
                $u = new user_class($c->user_id);
                unset($u->conf);
                $res = array(
                    "user" => $u,
                    "profile"=>$p,
                    "card"=>$c
                );
                if($_REQUEST['mode']=='ten-report')
                {
                    $out['err'] = '';
                    $out['res' ] = $c->loadTransaction();
                    die(toJSON($out));
                }    
                else if(isset($_REQUEST['card_mablagh']))
                {
                    if($mablagh>0)
                    {
                        if($mablagh < ($p->etebar - $p->min_etebar))
                        {
                            $user_sabt_id = (int)$_SESSION[$conf->app.'_user_id'];
                            $typ = -1;
                            $toz = 'خرید از طریق سامانه خرید کارتی سایت به مبلغ '.monize($mablagh).' ریال با کارت شماره '.$_REQUEST['card_number'];
                            $etebar_id = user_etebar_class::virtualPosAdd($c->user_id,$typ,$mablagh,$toz,$user_sabt_id);
                            $my = new mysql_class();
			    $tarikh = date("Y-m-d H:i:s");
                            $ln = $my->ex_sqlx("insert into transactions (`cards_id`, `tarikh`, `function_id`, `mablagh`, `ter_id`, `status`, `typ`) values (".$c->id.",'$tarikh',1,'$mablagh',0,1,0)",FALSE);
                            $t_id = $my->insert_id($ln);
                            $my->close($ln);
                            $my->ex_sqlx("update user_etebar set transaction_id=$t_id where id=$etebar_id");
                            $my->ex_sqlx("update `profile` set `etebar`=`etebar`-$mablagh  where `id`=".$p->id);
                            $p->etebar -= $mablagh;
                            $out['err'] = '';
                            $res['transaction_id'] = $t_id;
                            $res['tarikh']= jdate("H:i d / m / Y",strtotime(date("Y-m-d H:i")));
                            log_class::add($u->id,'پرداخت کارتی از طریق سایت کارت شماره  '.$c->shomare.' مبلغ '.$mablagh,'site card pos ');
                        }
                        else 
                        {
                            log_class::add($u->id,'خطا در پرداخت کارتی در سایت به علت کمبود موجودی به شماره '.$c->shomare.' مبلغ '.$mablagh,'mojoodi error on site card pos ');
                            $out['err'] = 'موجودی حساب کافی نمی باشد';
                        }
                    }
                    else
                    {
                        $out['err'] = 'مبلغ وارد شده صحیح نمی باشد';
                    }
                }
                $out['err'] = '';
                $out['res' ] = $res;
                $out['mablagh' ] = $mablagh;
            }
            else 
            {
                log_class::add($_SESSION[$conf->app.'_user_id'],'رمز عبور در پرداخت کارتی از سایت اشتباه وارد شده برای شماره '.$c->shomare,'pass error for site card pos ');
                $out['err'] = 'رمز عبور اشتباه است';
            }
        }
        else
        {
            $out['err'] = 'کارت مورد نظر پیدا نشد';
        }
        die(toJSON($out));
    }
?>
<style>
    .crad_kharid{
        text-align: center;
    }
    .crad_kharid input{
        margin : 5px;
    }
</style>
<script>
    function checkClick(dobj)
    {
        if($(dobj).prop('checked')===true)
        {
            $("#card_mablagh").prop('disabled',false);
        }
        else
            $("#card_mablagh").prop('disabled',true);
    }
    function sendCardInfo()
    {
        if($("#card_number").val().trim() === '' || $("#card_pass").val().trim() === '')
        {
            alert('لطفا شماره کارت و رمز آن را به درستی وارد کنید');
            return false;
        }
        var p = {
            "card_number" : $("#card_number").val(),
            "card_pass"   : $("#card_pass").val(),
            "mode" : $('input[name=card_kharid]:checked').val()
        };
        var mab = parseInt($("#card_mablagh").val(),10);
        if($('input[name=card_kharid]:checked').val()==='kharid' && !isNaN(mab) && mab >= 1000)
            p['card_mablagh'] = $("#card_mablagh").val();
        else if($('input[name=card_kharid]:checked').val()==='kharid' && isNaN(mab))
        {
            alert('لطفا مبلغ را به صحت وارد کنید');
            return(false);
        }
        else if($('input[name=card_kharid]:checked').val()==='kharid' && !isNaN(mab) && mab < 1000)
        {
            alert('مبلغ می بایست حداقل ۱۰۰۰ ریال باشد');
            return(false);
        }
        $("#response").html('<img src="../img/status_fb.gif" />');
        $("#card_kharid_btn").prop("disabled",true);
        $.getJSON("card_transfer.php",p,function(res){
            $("#card_kharid_btn").prop("disabled",false);
            if(res.err!=='')
            {
                $("#response").html('<div class="resid_div">'+res.err+'</div><button>چاپ</button>');
            }
            else if($('input[name=card_kharid]:checked').val()==='ten-report')
            {
                console.log(res);
                if(res.res.length===0)
                    $("#response").html('نتیجه ای موجود نیست');
                else
                {
                    $("#response").html( '<div class="resid_div">'+drawTen(res.res)+'</div><button>چاپ</button>');
                }    
            }    
            else
            {
                var mablagh = res.mablagh;
                res= res.res;
                var out ='<div style="border: 1px solid #ddd;border-radius: 4px;padding: 10px;margin: 5px;" >'+ ((res.profile.jender===1)?'آقای ':'خانم ');
                out+=res.user.fname+' '+res.user.lname+'</div>';
                if(typeof res.transaction_id !== 'undefined')
                {
                    out+='<div style="border: 1px solid #ddd;border-radius: 4px;padding: 10px;margin: 5px;">';
                    out+='<div style="padding: 5px;" > مبلغ خرید: '+ monize2(mablagh)+' ریال</div>';
                    out += '<div style="padding: 5px;" >خرید شما با موفقیت انجام گرفت ، کد پیگیری شما   '+res.transaction_id+'</div>';
                    out+='<div style="padding: 5px;" >تاریخ: '+res.tarikh+'</div>';
                    out+='</div>';
                }
                out += '<div style="border: 1px solid #ddd;border-radius: 4px;padding: 10px;margin: 5px;" > موجودی قابل برداشت شما'+monize2(res.profile.etebar - res.profile.min_etebar)+' ریال'+'</div>';
                $("#response").html('<div class="resid_div">'+out+'</div><button>چاپ</button>');
            }
            $("#response button").click(function(){
                //$(".resid_div").printElement({printMod:"popup"});
                popup($(".resid_div").html());
            });
        }).fail(function(){
            $("#card_kharid_btn").prop("disabled",false);
            $("#response").html('');
            alert('در ارتباط با سرور مشکلی پیش آمد');
        });
    }
    function drawTen(inp)
    {
        var tt;
        var out='<table style="width:100%" ><tr>';
        out+='<th style="border: 1px solid #ddd;border-radius: 4px;padding:5px;margin: 5px;" >ردیف</th><th style="border: 1px solid #ddd;border-radius: 4px;padding:5px;margin: 5px;" >تاریخ</th><th style="border: 1px solid #ddd;border-radius: 4px;padding:5px;margin: 5px;">مبلغ</th><th style="border: 1px solid #ddd;border-radius: 4px;padding:5px;margin: 5px;">نوع</th>';
        for(var i in inp)
        {
            tt = parseInt(inp[i].typ,10)===1?'افزایش':'کاهش';
            out+='<tr><th style="border: 1px solid #ddd;border-radius: 4px;padding:5px;margin: 5px;" >'+(parseInt(i,10)+1)+'</th><th style="border: 1px solid #ddd;border-radius: 4px;padding:5px;margin: 5px;" >'+ inp[i].regdate+'</th><th style="border: 1px solid #ddd;border-radius: 4px;padding:5px;margin: 5px;">'+monize2(inp[i].mablagh)+'</th><th style="border: 1px solid #ddd;border-radius: 4px;padding:5px;margin: 5px;">'+tt+'</th></tr>';
        }    
        out+='</table>';
        return out;
    }
</script>
<div class="crad_kharid">
    <input id="card_number" placeholder="شماره کارت "/><br/>
    <input type="password" id="card_pass" placeholder="رمز کارت " /><br/>
    <input id="card_mablagh" placeholder="مبلغ درصورت خرید"/><br/>
    <!--<input type="checkbox" id="card_kharid" checked  onclick="checkClick(this);"/>خرید انجام شود<br/>-->
    مانده: <input checked="checked" type="radio" class="card_kharid" name="card_kharid" value="mojoodi" >
    خرید: <input type="radio" class="card_kharid" name="card_kharid" value="kharid" >
    ده گردش آخر: <input type="radio" class="card_kharid" name="card_kharid" value="ten-report" >
    <!--
    <select id="card_kharid" >
        <option value="mojoodi">مانده</option>
        <option value="kharid">خرید</option>
        <option value="ten-report">ده گردش آخر</option>
    </select>
    -->
    <button id="card_kharid_btn" onclick="sendCardInfo();">ادامه</button>
    <button onclick="openDialog('card_transfer_rep.php');">گزارش فروشگاه</button>
</div>
<div id="response" style="width:9cm;">
</div>
