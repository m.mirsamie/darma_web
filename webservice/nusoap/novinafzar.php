<?php
//call library 
include_once('../kernel.php');
require_once ('../class/nusoap/nusoap.php'); 
//using soap_server to create server object 
$server = new soap_server; 

//register a function that works on server 
$server->register('get_message',array("data"=>"xsd:array"),array("return"=>"xsd:string")); 

// create the function 
function get_message($input_data) 
{ 
	if(!$input_data)
		return new soap_fault('Client','','NoData'); 
	$result = "Your data is :<br/>\n".var_export($input_data,TRUE); 
	return $result; 
} 
// create HTTP listener 
$post = file_get_contents('php://input');
$server->service($post); 
exit(); 
?> 
