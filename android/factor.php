<?php
        include_once("../kernel.php");
	$SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	include_once("../simplejson.php");
	$user =isset($_REQUEST['user'])?trim(strip_tags($_REQUEST['user'])):'_';
	$pass =isset($_REQUEST['pass'])?trim(strip_tags($_REQUEST['pass'])):'';
	$is_ersal = isset($_REQUEST['ersal']);
	$my = new mysql_class;
	$my->ex_sql("select pass,id from `user` where `user`='$user'",$q);
	if(!(count($q)>0) && !$is_ersal)
	{
		$out = array('peigiri'=>'','err'=>'خطای عدم احراز هویت','takhfif'=>0);
		die(toJSON($out));
	}
	$user_id = count($q)>0? (int)$q[0]['id']:-1;
	$_SESSION[$conf->app.'_user_id'] = $user_id;
	$prof= new profile_class($user_id);
	$use = new user_class($user_id);
	$err = '';
	$peigiri='';
	$takh_kol = 0;
        $store_tahvil = isset($_REQUEST['store_tahvil'])? ((int)$_REQUEST['store_tahvil']>0):FALSE;
	if(((isset($q[0]) && $q[0]['pass']==$pass && (int)$q[0]['id']>0) || $is_ersal) && $conf->kharid_disabled!==TRUE)
	{
		if($is_ersal)
		{
                        if($store_tahvil)
                        {
                            $peigiri = 0;
                        }
                        else
                        {    
                            $mablaghKol=0;
                            $ersal = ($conf->ersal=='')?25000:(int)$conf->ersal;
                            $ersalUpLimit = ($conf->ersalUpLimit=='')?750000:(int)$conf->ersalUpLimit;
                            $sabad_tmp = $_REQUEST['factor'];
                            $sabad=array();
                            $sabad_tmp = explode(",",$sabad_tmp);
                            foreach($sabad_tmp as $ca)
                            {
                                    $tmp = explode("|",$ca);
                                    $sabad[] = array('id'=>$tmp[0],'tedad'=>$tmp[1]);
                            }
                            foreach($sabad as $kal)
                            {
                                    $k = new kala_class($kal['id']);
                                    $mablaghKol+= $kal['tedad']*$k->ghimat;
                                    $takh = takhfif_class::get((int)$kal['id'],$user_id,$kal['tedad']);
                                    $takhfif = $takh['takhfif'];
                                    $takh_kol += $takhfif*$kal['tedad']*$k->ghimat/100;
                            }
                            $ersal = $conf->ersal==''?25000:$conf->ersal;
                            if($mablaghKol > 300000 && $mablaghKol < 600000)
                                    $ersal = 15000;
                            else if($mablaghKol >=600000 && $mablaghKol< $ersalUpLimit )
                                    $ersal = 7500;
                            else if ($mablaghKol >= $ersalUpLimit )
                                    $ersal=0;
                            $peigiri = $ersal;
                        }
		}
		else
		{
			$sabad_tmp = $_REQUEST['factor'];
			$sabad=array();
			$sabad_tmp = explode(",",$sabad_tmp);
			foreach($sabad_tmp as $ca)
			{
				$tmp = explode("|",$ca);
				$sabad[] = array('id'=>$tmp[0],'tedad'=>$tmp[1]);
			}
			if(count($sabad)==0)
			{
				die(toJSON(array('peigiri'=>'-1','err'=>'هیچ کالایی انتخاب نشده است','takhfif'=>$takh_kol)));
			}
			$ersal = ($conf->ersal=='')?25000:(int)$conf->ersal;
			$ersalUpLimit = ($conf->ersalUpLimit=='')?750000:(int)$conf->ersalUpLimit;
			$mantaghe_id = -1;
			$mablaghKol =0;
			foreach($sabad as $kal)
			{
				$k = new kala_class($kal['id']);
				$takh = takhfif_class::get((int)$kal['id'],$user_id,$kal['tedad']);
				$takhfif = $takh['takhfif'];
				$mablaghKol+= $kal['tedad']*$k->ghimat*(100-$takhfif)/100;
				$takh_kol += $takhfif*$kal['tedad']*$k->ghimat/100;
			}
                        if($store_tahvil)
                        {
                            $ersal=0;
                        }
                        else
                        {
                            $ersal = $conf->ersal==''?25000:$conf->ersal;
                            if($mablaghKol > 300000 && $mablaghKol < 600000)
                                    $ersal = 15000;
                            else if($mablaghKol >=600000 && $mablaghKol< $ersalUpLimit )
                                    $ersal = 7500;
                            else if ($mablaghKol >= $ersalUpLimit )
                                    $ersal=0;
                        }    
			
			$mablaghKol+=$ersal;
			$mablagh=$mablaghKol;
			if(($prof->etebar-$prof->min_etebar)>=$mablaghKol )
			{
				$my = new mysql_class;
				$tarikh = date("Y-m-d H:i:s");
				$addr = (isset($_REQUEST['addr']) && strip_tags($_REQUEST['addr'])!='' )?strip_tags($_REQUEST['addr']):$prof->addr;
				$nahve = isset($_REQUEST['nahve'])?(int)strip_tags($_REQUEST['nahve']):1;
				if($nahve==2 || $store_tahvil)
					$addr = 'محل فروشگاه دارما';
				$addr.=' -آندروید';
                                $mantaghe_id = $store_tahvil?-2:-1;
				$ln = $my->ex_sqlx("insert into factor (user_id,tarikh,en,zamanTahvil,makaneTahvil,jamKol,hazineErsal,isTasfie,typ,order_type,mantaghe_id) values($user_id,'$tarikh',1,'$tarikh','$addr',$mablagh,$ersal,1,0,2,$mantaghe_id)",FALSE);
				$factor_id = $my->insert_id($ln);
				$my->close($ln);
				foreach($sabad as $kal)
				{
					$k = new kala_class((int)$kal['id']);
					$my->ex_sqlx("insert into `factor_det` (`factor_id`, `kala_id`, `tedad`, `ghimat`, `toz`, `takhfifModir`, `takhfifBaste`, `takhfifTedad`, `saghfTakhfif`, `taminkonande_id`) values (".$factor_id.",".($k->id).",".$kal['tedad'].",".$k->ghimat.",'',0,0,0,12,".$k->taminkonande_id.")");
				}
				$toz = 'بابت فاکتور شماره '.$factor_id.' به تاریخ '.jdate("H:i Y / m / d",strtotime($tarikh)).' - آندروید';
	/*
				user_etebar_class::add($user_id,-1,0,$toz,$user_id);
				$my->ex_sqlx("update `profile` set `etebar`=".(($prof->etebar-$mablaghKol>=0)?$prof->etebar-$mablaghKol:0)." , `hajm_rialy_takhfif` =  `hajm_rialy_takhfif` + ".($mablaghKol)." where `user_id`=$user_id");
	*/
				$prof->decEtebar($mablagh,$user_id,$toz);
                                $delivery_sms = ($conf->delivery_sms!='')?$conf->delivery_sms:'09156000631';
                                sms_class::send($toz.' '.$use->fname.' '.$use->lname,$delivery_sms);
                                $delivery_email = $conf->delivery_email==''?'hscomp2002@gmail.com':$conf->delivery_email;
                                $mail = new email_class($delivery_email,'دارما دلیوری',$toz.' '.$use->fname.' '.$use->lname);
				$peigiri = $factor_id;
				log_class::add($user_id,'دارما دلیوری آندروید'.' '.$toz,'delivery_android');
			}
			else
				$err='خطای کمبود موجودی'.$prof->etebar.','.$prof->min_etebar.','.$mablaghKol;
		}
	}
	else if($conf->kharid_disabled===TRUE)
		$err = 'در حال حاضر خرید غیر فعال می باشد . لطفا بعدا سعی نمایید.';
	else 
		$err = 'خطای عدم احراز هویت';
	$out = array('peigiri'=>$peigiri,'err'=>$err,'takhfif'=>$takh_kol);
	die(toJSON($out));
?>
