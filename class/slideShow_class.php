<?php
	class slideShow_class
	{
		public function __construct($id=-1)
		{
			if((int)$id > 0)
			{
				$mysql = new mysql_class;
				$mysql->ex_sql("select * from `slideShow` where `id` = $id",$q);
				if(isset($q[0]))
				{
					$r = $q[0];
					$this->id=$r['id'];
					$this->order=$r['order'];
					$this->img=$r['img'];
					$this->title=$r['title'];
					$this->kala_id=$r['kala_id'];
				}
			}
		}
		public function getAll($kala_abarGroup_id=-1)
		{
			$out = array('img'=>'','title'=>array());
			$my = new mysql_class;
			$kala_abarGroup_id=(int)$kala_abarGroup_id;
			$wer = ($kala_abarGroup_id>0)?" where kala_abarGroup_id = $kala_abarGroup_id ":'';
			$my->ex_sql("select * from `slideShow` $wer order by `order`,`id`",$q);
			foreach($q as $r)
			{
				$class = 'class="left_banner"';
				$cl = '';
				if((int)$r['kala_id']>0)
				{
					$k = new kala_class((int)$r['kala_id']);
					$class = 'class="left_banner tooltipItem pointer" title="'.$k->name.'"';
					$cl = 'onclick="showKala('.((int)$r['kala_id']).');"';
					$out['title']['slideImg_'.((int)$r['kala_id'])] = $r['title'];
				}
				//style="width:800px;height:300px;"
				$out['img'] .= '<img   id="slideImg_'.((int)$r['kala_id']).'" '.$class.'src="'.$r['img'].'" '.$cl.' />';
			}
			return($out);
		}
	}
?>
