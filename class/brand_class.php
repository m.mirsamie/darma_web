<?php
	class brand_class
	{
		public function __construct($id=-1)
		{
			if((int)$id > 0)
			{
				$mysql = new mysql_class;
				$mysql->ex_sql("select * from `brand` where `id` = $id",$q);
				if(isset($q[0]))
				{
					$r = $q[0];
					$this->id=$r['id'];
					$this->name=$r['name'];
					$this->pic=$r['pic'];
					$this->link=$r['link'];
				}
			}
		}
		public function loadAll($scroll=TRUE)
		{
			$my = new mysql_class;
			$my->ex_sql("select * from `brand` ",$q);
			if($scroll)
			{
				$out = '<ul>';
				foreach($q as $r)
					//$out .= "<td><img height='100px' id='brand_".$r['id']."' src='".$r['pic']."' class='pointer' onclick='loadBrand(".$r['id'].");' title='".$r['name']."' /></td>";
					$out .= "<li><img height='100px' id='brand_".$r['id']."' src='".$r['pic']."' class='pointer' onclick='loadBrand(".$r['id'].");' title='".$r['name']."' /></li>";
				$out .= '</ul>';
			}
			else
			{
				$out = '<table>';
				$out .= "<tr>";
				foreach($q as $i=>$r)
				{
					if($i % 10 == 0)
	                                        $out .= "</tr><tr>";
					$out .= "<td><img height='100px' id='brand_".$r['id']."' src='".$r['pic']."' class='pointer' onclick='loadBrand(".$r['id'].");' title='".$r['name']."' /></td>";
				}
				$out .= "</tr>";
				$out .= '</table>';
			}
			return($out);
		}
	}
?>
