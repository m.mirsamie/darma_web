<?php
	class dep_class
	{
		public function __construct($id=-1)
		{
			if((int)$id > 0)
			{
				$mysql = new mysql_class;
				$mysql->ex_sql("select * from `dep` where `id` = $id",$q);
				if(isset($q[0]))
				{
					$r = $q[0];
					$this->id=$r['id'];
					$this->name=$r['name'];
					$this->emails=$r['emails'];
				}
			}
		}
		public function loadCombo($selected=-1)
		{
			$out = '';
			$selected = (int)$selected;
			$my = new mysql_class;
			$my->ex_sql("select * from `dep` order by `name`",$q);
			foreach($q as $r)
				$out .= '<option value="'.$r['id'].'"'.(($selected==(int)$r['id'])?' selected':'').'>'.$r['name'].'</option>';
			return($out);
		}
	}
?>
