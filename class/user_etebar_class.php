<?php
	class user_etebar_class
	{
		public function __construct($id=-1)
		{
			if((int)$id > 0)
			{
				$mysql = new mysql_class;
				$mysql->ex_sql("select * from `user_etebar` where `id` = $id",$q);
				if(isset($q[0]))
				{
					$r = $q[0];
					$this->id=(int)$r['id'];
					$this->user_id=(int)$r['user_id'];
					$this->user_sabt_id=(int)$r['user_sabt_id'];
					$this->typ=(int)$r['typ'];
					$this->mablagh=(int)$r['mablagh'];
					$this->toz=$r['toz'];
					$this->regdate=$r['regdate'];
				}
			}
		}
		public function add($user_id,$typ,$mablagh,$toz,$user_sabt_id,$is_enteghal=FALSE)
		{
			$conf = new conf;
			$company_user = new user_class;
			$company_user->getIncomeUser();
			$user_id = (int)$user_id;
			$typ = (int)$typ;
			$mablagh = (int)$mablagh;
			$tarikh = date("Y-m-d H:i:s");
			$my = new mysql_class;
			$user = new user_class($user_id);
			$user_sabt = new user_class($user_sabt_id);
			if(isset($user_sabt->id))
                        {
                            $user = $user_sabt;
                        }
			//var_dump($user);
			$ln = $my->ex_sqlx("insert into `user_etebar` (`user_id`,`user_sabt_id`, `typ`, `mablagh`, `toz`, `regdate`,`company_id`) values ($user_id,$user_sabt_id,$typ,$mablagh,'$toz','$tarikh',".$conf->company_id.")",FALSE);
			$out = $my->insert_id($ln);
			$my->close($ln);
			company_etebar_class::buyFromCompany($user->company_id,$mablagh);
			if(!$is_enteghal && isset($company_user->id) && $user_id!=$company_user->id && $typ==-1)
			{
				//user_etebar_class::add($company_user->id,1,$mablagh,'افزایش  به علت '.$toz,$user_sabt_id);
				$p_company = new profile_class($company_user->id);
				$p_company->addEtebar($mablagh,$user_sabt_id,'افزایش اعتبار بابت '.$toz);
			}
			if(!isset($company_user->id) || (isset($company_user->id) && $user_id!=$company_user->id))
			{
				$fname_u = isset($user->fname)?$user->fname:'';
				$lname_u = isset($user->lname)?$user->lname:'';
				$fname_s = isset($user_sabt->fname)?$user_sabt->fname:'';
				$lname_s = isset($user_sabt->lname)?$user_sabt->lname:'';
				$text = "تاریخ: ".jdate("Y/m/d H:i:s",strtotime($tarikh))." مبلغ: $mablagh کاربر: ".$fname_u.' '.$lname_u." ثبت کننده:".$fname_s.' '.$lname_s."  توضیحات: $toz" ;
//				$mail = new email_class($conf->sanad_emails==''?'darmafood@gmail.com':$conf->sanad_emails,'دارما تراکنش جدید',$text);                
			}
			return($out);
		}
		public function virtualPosAdd($user_id,$typ,$mablagh,$toz,$user_sabt_id,$is_enteghal=FALSE)
		{
			$conf = new conf;
                        $is_enteghal=FALSE;
			$company_user = new user_class;
			$company_user->getIncomeUser();
			$user_id = (int)$user_id;
			$typ = (int)$typ;
			$mablagh = (int)$mablagh;
			$tarikh = date("Y-m-d H:i:s");
			$my = new mysql_class;
			$user = new user_class($user_id);
			$user_sabt = new user_class($user_sabt_id);
			if(!isset($user_sabt->id))
                            return(-1);
                        $cg = new company_group_class($user_sabt->company_group_id);
                        if(!isset($cg->id))
                            return(-2);
                        //------------------Bazaryab---------------------------
                        $bmablagh = 0;
                        if((int)$cg->user_bazaryab_id>0 && (int)$cg->comision_bazaryab>0)
                        {
                            $btoz = "کمیسیون تراکنش از ".$cg->name;
                            $bmablagh = (int)((int)$cg->comision_bazaryab*$mablagh/100);
                            $my->ex_sqlx("insert into `user_etebar` (`user_id`,`user_sabt_id`, `typ`, `mablagh`, `toz`, `regdate`,`company_id`) values (".$cg->user_bazaryab_id.",$user_sabt_id,1,$bmablagh,'$btoz','$tarikh',".$conf->company_id.")");
                        }
                        //------------------Darma------------------------------
                        $dmablagh = (int)(abs((int)$cg->comision_darma)*$mablagh/100);
			company_etebar_class::buyFromCompany($user->company_id,$mablagh);
			if(!$is_enteghal && isset($company_user->id) && $user_id!=$company_user->id && $typ==-1)
			{
                            $p_company = new profile_class($company_user->id);
                            $p_company->addEtebar($dmablagh,$user_sabt_id,'افزایش اعتبار بابت '.$toz);
			}
                        //------------------Company Group----------------------
                        $fmablagh = $mablagh - ($bmablagh+$dmablagh);
                        $my->ex_sqlx("insert into `user_etebar` (`user_id`,`user_sabt_id`, `typ`, `mablagh`, `toz`, `regdate`,`company_id`) values ($user_sabt_id,$user_sabt_id,1,$fmablagh,'$toz','$tarikh',".$conf->company_id.")");
                        //------------------khodet yaroo-----------------------
			$ln = $my->ex_sqlx("insert into `user_etebar` (`user_id`,`user_sabt_id`, `typ`, `mablagh`, `toz`, `regdate`,`company_id`) values ($user_id,$user_sabt_id,$typ,$mablagh,'$toz','$tarikh',".$conf->company_id.")",FALSE);
			$out = $my->insert_id($ln);
			$my->close($ln);
                        //$user = $user_sabt;
                        /*
			if(!isset($company_user->id) || (isset($company_user->id) && $user_id!=$company_user->id))
			{
				$fname_u = isset($user->fname)?$user->fname:'';
				$lname_u = isset($user->lname)?$user->lname:'';
				$fname_s = isset($user_sabt->fname)?$user_sabt->fname:'';
				$lname_s = isset($user_sabt->lname)?$user_sabt->lname:'';
				$text = "تاریخ: ".jdate("Y/m/d H:i:s",strtotime($tarikh))." مبلغ: $mablagh کاربر: ".$fname_u.' '.$lname_u." ثبت کننده:".$fname_s.' '.$lname_s."  توضیحات: $toz" ;
				$mail = new email_class($conf->sanad_emails==''?'darmafood@gmail.com':$conf->sanad_emails,'دارما تراکنش جدید',$text);                
			}
                        */
			return($out);
		}
	}
?>
