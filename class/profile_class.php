<?php
	class profile_class
	{
		public function __construct($user_id=-1)
		{
			if((int)$user_id > 0)
			{
				$mysql = new mysql_class;
				$mysql->ex_sql("select * from `profile` where `user_id` = $user_id",$q);
				if(isset($q[0]))
				{
					$r = $q[0];
					$this->id=$r['id'];
					$this->user_id=$r['user_id'];
					$this->pre_user_id=$r['pre_user_id'];
					$this->hajm_rialy_takhfif=$r['hajm_rialy_takhfif'];
					$this->etebar=(int)$r['etebar'];
					$this->min_etebar=(int)$r['min_etebar'];
					$this->mob=$r['mob'];
					$this->addr=$r['addr'];
					$this->codePosti=$r['codePosti'];
					$this->tel=$r['tel'];
					$this->pic=$r['pic'];
					$this->email=$r['email'];
					$this->tarikh_tavalod=$r['tarikh_tavalod'];
					$this->mantaghe_id=$r['mantaghe_id'];
					$this->city_id=$r['city_id'];
					$this->customer_type_id=$r['customer_type_id'];
					$this->user_daste_id=(int)$r['user_daste_id'];
					$this->down_user_poorsant = (int)$r['down_user_poorsant'];
					$this->jender = (int)$r['jender'];
				}
				else
				{
					$mysql->ex_sqlx("insert into `profile` (`user_id`,`email`) values ($user_id,'noemail_$user_id')");
					$mysql->ex_sql("select * from `profile` where `user_id` = $user_id",$q);
	                                if(isset($q[0]))
        	                        {
						$r = $q[0];
						$this->id=$r['id'];
						$this->user_id=$r['user_id'];
						$this->pre_user_id=$r['pre_user_id'];
						$this->hajm_rialy_takhfif=$r['hajm_rialy_takhfif'];
						$this->etebar=$r['etebar'];
						$this->min_etebar=$r['min_etebar'];
						$this->mob=$r['mob'];
						$this->addr=$r['addr'];
						$this->codePosti=$r['codePosti'];
						$this->tel=$r['tel'];
						$this->pic=$r['pic'];
						$this->email=$r['email'];
						$this->tarikh_tavalod=$r['tarikh_tavalod'];
						$this->mantaghe_id=$r['mantaghe_id'];
						$this->city_id=$r['city_id'];
						$this->customer_type_id=$r['customer_type_id'];
						$this->user_daste_id=(int)$r['user_daste_id'];
						$this->down_user_poorsant = (int)$r['down_user_poorsant'];
						$this->jender = (int)$r['jender'];
					}
				}
			}
		}
		public function loadByMobile($mobile)
		{
			$mysql = new mysql_class;
			$mysql->ex_sql("select * from `profile` where `mob` = '$mobile'",$q);
			if(isset($q[0]))
			{
				$r = $q[0];
				$this->id=$r['id'];
				$this->user_id=$r['user_id'];
				$this->pre_user_id=$r['pre_user_id'];
				$this->hajm_rialy_takhfif=$r['hajm_rialy_takhfif'];
				$this->etebar=(int)$r['etebar'];
				$this->min_etebar=(int)$r['min_etebar'];
				$this->mob=$r['mob'];
				$this->addr=$r['addr'];
				$this->codePosti=$r['codePosti'];
				$this->tel=$r['tel'];
				$this->pic=$r['pic'];
				$this->email=$r['email'];
				$this->tarikh_tavalod=$r['tarikh_tavalod'];
				$this->mantaghe_id=$r['mantaghe_id'];
				$this->city_id=$r['city_id'];
				$this->customer_type_id=$r['customer_type_id'];
				$this->user_daste_id=(int)$r['user_daste_id'];
				$this->down_user_poorsant = (int)$r['down_user_poorsant'];
				$this->jender = (int)$r['jender'];
			}
		}
		public function create($user_id)
		{
			$my = new mysql_class;
			$ln = $my->ex_sqlx("insert into `profile` (`user_id`) values ($user_id)",FALSE);
			$id =(int) $my->insert_id($ln);
			$my->close($ln);
			return($id);
		}
		public function move($own,$des,$mablagh,$user_sabt_id)
		{
			$out = FALSE;
                        $conf = new conf;
			$mablagh = (int)$mablagh;
			if($mablagh>0)
			{
				$my = new mysql_class;
                                $sp_kala_id = (trim($conf->enteghal_id)!='')?(int)$conf->enteghal_id:914;
                                $sp_kala = new kala_class($sp_kala_id);
				$my->ex_sqlx("update `profile` set `etebar`=".($own->etebar-($mablagh+$sp_kala->ghimat))." where `id`=".$own->id);
				$my->ex_sqlx("update `profile` set `etebar`=".($des->etebar+$mablagh)." where `id`=".$des->id);
				$userOwn = new user_class($own->user_id);
				$userDes = new user_class($des->user_id);
				$tozOwn = 'انتقال اعتبار به '.$userDes->fname.' '.$userDes->lname;
				$tozDes = 'انتقال اعتبار از '.$userOwn->fname.' '.$userOwn->lname;
				user_etebar_class::add($own->user_id,-1,$mablagh+$sp_kala->ghimat,$tozOwn,$user_sabt_id,TRUE);
				user_etebar_class::add($des->user_id,1,$mablagh,$tozDes,$user_sabt_id,TRUE);
                                //$this->addEtebar($sp_kala->ghimat,$user_sabt_id);
                                $company_user = new user_class;
				$company_user->getIncomeUser();
				$p_company = new profile_class($company_user->id);
				$p_company->addEtebar($sp_kala->ghimat,$user_sabt_id,'افزایش اعتبار بابت '.$tozOwn);
                                $tmp=array();
                                $tmp[]= array('id'=>$sp_kala_id,'mablagh'=>$sp_kala->ghimat,'tedad'=>1);
                                $fac = new factor_class;
                                $fac->newSpecialFactor($own->user_id, $tmp, 'کاهش بابت انتقال وجه');
                                
				$out = TRUE;
			}
			return($out);
		}
		public function addEtebar($mablagh,$user_sabt_id,$toz='')
		{
			$out = FALSE;
                        $mablagh = (int)$mablagh;
                        if($mablagh>0)
                        {
				$out = TRUE;
				$my = new mysql_class;
				$my->ex_sqlx("update `profile` set `etebar`=`etebar`+$mablagh  where `id`=".$this->id);
				$tozDes = ($toz != '')?$toz:' افزایش اعتبار.';
				user_etebar_class::add($this->user_id,1,$mablagh,$tozDes,$user_sabt_id);
			}
			return($out);
		}
		public function decEtebar($mablagh,$user_sabt_id,$toz='')
		{
			$out = FALSE;
                        $mablagh = (int)$mablagh;
                        if($mablagh>0 && ($this->etebar-$this->min_etebar)>=$mablagh)
                        {
				$out = TRUE;
				$my = new mysql_class;
				$my->ex_sqlx("update `profile` set `etebar`=`etebar`-$mablagh  where `id`=".$this->id);
				$tozDes = ($toz != '')?$toz:' کاهش اعتبار.';
				user_etebar_class::add($this->user_id,-1,$mablagh,$tozDes,$user_sabt_id);
			}
			return($out);
		}
	}
?>
