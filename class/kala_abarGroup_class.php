<?php
	class kala_abarGroup_class
	{
		public function __construct($id=-1)
		{
			if((int)$id > 0)
			{
				$mysql = new mysql_class;
				$mysql->ex_sql("select * from `kala_abarGroup` where `id` = $id",$q);
				if(isset($q[0]))
				{
					$r = $q[0];
					$this->id=$r['id'];
					$this->name=$r['name'];
					$this->en=$r['en'];
					$this->css=$r['css'];
					$this->ord=$r['ord'];
                                        $this->url_name=$r['url_name'];
				}
			}
		}
		public function loadByUrlName($u)
		{
			if(trim($u)!='')
			{
				$mysql = new mysql_class;
				$mysql->ex_sql("select * from `kala_abarGroup` where `url_name` = '$u'",$q);
				if(isset($q[0]))
				{
					$r = $q[0];
					$this->id=$r['id'];
					$this->name=$r['name'];
					$this->en=$r['en'];
					$this->css=$r['css'];
					$this->ord=$r['ord'];
                                        $this->url_name=$r['url_name'];
				}
			}
		}
		public function getAll($url_name = '')
		{
			$out =array('abar'=>'','group'=>'','mini'=>array());
			$mysql = new mysql_class;
			$mysql->ex_sql("select `id`,`name`,`url_name` from `kala_abarGroup` order by `ord`,`name`",$q);
			foreach($q as $r)
				$out['abar'].='<li class="kalaAbarSelector'.(($r['id']==$url_name)?' drawerTable_active':'').'"  id="k_'.$r['url_name'].'" ><a href="'.$r['url_name'].'">'.$r['name'].'</a></li>';
			$out['abar']='<ul class="erabarcatmenu"  >'.$out['abar'].'</ul>';
			if($url_name != '' && $url_name != 'home')
			{
				$kgr = kala_group_class::loadByAbar($url_name);
				if(count($kgr)>0)
				{
					$out['group'] = '<ul class="topMenu_abar">';
					//$out['group'] = '<table class="topMenu_abar"><tr>';
					foreach($kgr as $i=>$it)
					{
						$riz =kala_miniGroup_class::loadByGroup($it['id']);
						$txt = (trim($it['pic'])!='')?'<img  src="'.$it['pic'].'" class="kalaGroupPic" /><span  class="kalaGroupTitle" >'.$it['name'].'</span>':$it['name'].'</span>';
						$out['group'] .= '<li class="kalaSelector"   id="kalaGroup_'.$it['id'].'">'.$txt.$riz.'</li>';
						//$txt = (trim($it['pic'])!='')?'<img onclick="loadKala('.$it['id'].');" src="'.$it['pic'].'" class="kalaGroupPic" /></td></tr><tr onclick="loadKala('.$it['id'].');" class="kalaGroupTitle" style="display:none;"><td>'.$it['name']:$it['name'];
						//$out['group'] .= '<td><table width="100%"><tr><td class="kalaSelector"   id="kalaGroup_'.$it['id'].'">'.$txt.'</td></tr></table></td>';
					}
					$out['group'] .= '</ul>';
				}
			}
			return($out);
		}
		public function loadColor()
		{
			$out = '';
			if(isset($this->css) && file_exists($this->css))
			{
				$file = file($this->css);
				$founded = FALSE;
				foreach($file as $indx=>$line)
				{
					$line = trim($line);
					if($line != '')
					{
						if($line == '/*mainColor*/')
							$founded = TRUE;
						else if($founded && strpos($line,'color')!==FALSE)
						{
							$tmp = explode(':',$line);
							if(count($tmp) == 2)
							{
								$founded = FALSE;
								$tmp1 = explode(';',$tmp[1]);
								$out = str_replace('#','',trim($tmp1[0]));
								return($out);
							}
						}
					}
				}
			}
			return($out);
		}
		public function setColor($color)
		{
			$ok = TRUE;
			if(strpos($color,'#') === FALSE)
				$color = '#'.$color;
			if(isset($this->css) && $color!='#')
			{
				if(!file_exists($this->css))
				{
					$ok = copy('../css/style.css','../css/kalaAbarGroupCss_'.$this->id.'.css');
					if($ok)
					{
						$this->css = '../css/kalaAbarGroupCss_'.$this->id.'.css';
						$my = new mysql_class;
						$my->ex_sqlx("update `kala_abarGroup` set `css` = '".$this->css."' where `id` = ".$this->id);
					}
				}
				if($ok)
				{
					$file = file($this->css);
					$founded = FALSE;
					$oFile = '';
					foreach($file as $indx=>$line)
					{
						$line = trim($line);
						if($line != '')
						{
							if(trim($line) == '/*mainColor*/')
							{
								$founded = TRUE;
							}
							else if($founded && strpos($line,'color')!==FALSE)
							{
								$founded = FALSE;
								$line = 'color : '.$color.';';
							}
							$oFile .= $line."\n";
						}
					}
					$f = fopen($this->css,'w+');
					fwrite($f,$oFile);
					fclose($f);
				}
			}
		}
	}
?>
