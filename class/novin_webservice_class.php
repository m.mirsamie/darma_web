<?php
	class novin_webservice_class
	{
		public $client;
		public $stat = FALSE;
		public $UserACCID = 10;
		public $HID = 5;
		function __construct($uName,$pass,$wsdl = "http://www.npsms.ir/products.asmx?wsdl")//,$UserACCID,$HID)
		{
			$this->client = new SoapClient($wsdl);
			//$this->UserACCID = $UserACCID;
			//$this->HID = $HID;
			$uName = ($uName == '') ? "darma" : $uName;
			$pass = ($pass == '') ? "darma3151492M" : $pass;
			$client = $this->client;
			$param = array(
				"uName" => $uName,
				"pass"  => $pass
				//"darma","darma3151492M"
/*
				"uName" => "darma",
				"pass"  => "darma3151492M"
*/
/*
				"uName" => "hosein",
				"pass"  => "0000"
*/
			);
			$result = $client->login($param);
			$this->stat = ($result->LoginResult===TRUE);
		}
		function getDepots($limit=999)
		{
			$client = $this->client;
			$param = array(
				"Top" => (int)$limit,
				"JustActive"  => TRUE
			);
			$depots_tmp = $client->Depot_List($param);
			$depots = array();
			if(isset($depots_tmp->Depot_ListResult))
				$depots = $depots_tmp->Depot_ListResult->WebDepot;
			return($depots);
		}
		function getAllProducts($limit=99999999)
		{
			$client = $this->client;
			$param = array(
				"Top"         => (int)$limit,
				"Name"        => '',
				"Code"        => '',
				"Desc"        => '',
				"GroupID"     => 0,
				"JustActive"  => FALSE
			);
			$all_products_tmp = $client->Product_List($param);
			$all_products = array();
			if(isset($all_products_tmp->Product_ListResult) && isset($all_products_tmp->Product_ListResult->webProducts))
				$all_products = $all_products_tmp->Product_ListResult->webProducts;
			return($all_products);
		}
		function getProductsTill($date)
		{
			$client = $this->client;
			$date = date("Y-m-d",strtotime($date));
			$param = array(
				"UpdateDate"  => $date
			);
			$new_products_tmp =  $client->Product_List_Modified($param);
			$new_products = array();
			if(isset($new_products_tmp->Product_List_ModifiedResult) && isset($new_products_tmp->Product_List_ModifiedResult->webProducts))
				$new_products = $new_products_tmp->Product_List_ModifiedResult->webProducts;
			return($new_products);
		}
		function sendFactor($factor_desc,$items,$user_id=-1)
		{
			$factor_ref = -1;
			$client = $this->client;
			$pishFactor = array(
				"ID"                 => 0,
				"BazaryabHID"        => 0,
				"UserACCID"          => $this->UserACCID,
				"User_Department_ID" => 0,
				"ID2"                => 0,
				"Date"               => date("Y-m-d H:i:s"),
				"HID"                => (((int)$user_id>0)?$user_id:$this->HID),
				"Discount"           => 0,
				"Description"        => $factor_desc,
				"Keraye"             => 0,
				"Maliat"             => 0,
				"Bazaryab"           => 0,
				"IsRead"             => FALSE,
				"Type"               => 0,
				"KerayeHID"          =>0,
				"KerayeHType"        =>0,
				"BazaryabHType"      =>0,
				"MaliatHID"          =>0,
				"MaliatHType"        =>0,
				"FactorType"         =>0,
				"PriceType"          =>0,
				"ServerPC_ID"        =>0,
				"ServerPC_ReadTime"   =>null
			);
			$pishFactor = (object) $pishFactor;
			$PishFactorItems = $items;
			$param = array(
                                "pishFactor"      => $pishFactor,
				"PishFactorItems" => $PishFactorItems,
				"UserACCID"       => $this->UserACCID,
				"BazaryabHID"     => 0
                        );
			$factor_ref_tmp = $client->Pishfactor_Total_Save($param);
			if(isset($factor_ref_tmp->Pishfactor_Total_SaveResult))
				$factor_ref = $factor_ref_tmp->Pishfactor_Total_SaveResult;
			return($factor_ref);
		}
	}
?>
