<?php
	class sabad_class
	{
		public $kalas = array();
		public $user_id = -1;
		public $factor_id = -1;
		public $jam_kol = 0;
		public function __construct($factor_id = -1)
		{
			$factor_id = (int)$factor_id;
			if($factor_id > 0)
			{
				$my = new mysql_class;
				$my->ex_sql("select `kala_id`,`tedad` from `factor_det` where `factor_id` = $factor_id",$q);
				foreach($q as $r)
					$this->kalas[] = array("kala"=>(int)$r['kala_id'],"tedad"=>(int)$r['tedad']);
				$this->factor_id = $factor_id;
			}
		}
		public function remove($kala_id)
		{
			$kala_id = (int)$kala_id;
			$indx_f = -1;
			foreach($this->kalas as $indx=>$kala)
				if($kala["kala"] == $kala_id)
					$indx_f = $indx;
			$tmpKalas = $this->kalas;
			$this->kalas = array();
			foreach($tmpKalas as $indx=>$kala)
				if($indx != $indx_f)
					$this->kalas[] = $kala;
		}
		public function add($kala_id,$tedad,$database = TRUE)
		{
			$kala_id = (int)$kala_id;
			$tedad = (int)$tedad;
			$k = new kala_class($kala_id);
			$out = (isset($k->id) && $tedad > 0);
			if($out)
			{
				$my = new mysql_class;
				$mantaghe_id = -1;
				if($this->user_id > 0)
				{
					$p = new profile_class($this->user_id);
					if(isset($p->mantaghe_id))
						$mantaghe_id = $p->mantaghe_id;

				}
				$factor_new = FALSE;
				if(count($this->kalas) == 0 && $database)
				{
					$ln = $my->ex_sqlx("insert into `factor` (`user_id`, `tarikh` , `zamanTahvil`, `nahveTahvil`, `makaneTahvil`, `jamKol`, `hazineErsal`, `toz`, `isTasfie`, `typ`, `status`, `mantaghe_id`, `transporter_id`) values (".$this->user_id.",'".date("Y-m-d h:i:s")."','','','',0,0,'',0,0,0,$mantaghe_id,-1)",FALSE);
					$this->factor_id = $my->insert_id($ln);
					$my->close($ln);
					$factor_new = TRUE;
				}
				else if($this->user_id > 0 && $database)
					$my->ex_sqlx("update `factor` set `user_id` = ".$this->user_id." where `id`= ".$this->factor_id);
				$found = FALSE;
				foreach($this->kalas as $indx=>$kal)
					if((int)$kal['kala'] == (int)$k->id)
					{
						$found = TRUE;
						$this->kalas[$indx]['tedad'] += $tedad;
						if($database)
							$my->ex_sqlx("update `factor_det` set `ghimat` = `ghimat`*".$this->kalas[$indx]['tedad']."/`tedad` , `tedad` = ".$this->kalas[$indx]['tedad']." where `kala_id` = ".$this->kalas[$indx]['kala']." and `factor_id` = ".$this->factor_id);
					}
				if(!$found)
				{
					$takh = takhfif_class::get((int)$k->id,$this->user_id,$tedad);
					$takhfif = $takh['takhfif'];
					$takhBaste = $takh['basteTakhfif']['value'];
					$takhModir = $takh['modirTakhfif']['value'];
					$takhTedad = $takh['tedadTakhfif']['value'];
					$takhSaghf = $takh['saghfTakhfif'];
                		        $mablagh = $tedad*$k->ghimat*(100-$takhfif)/100;
					$this->jam_kol += $mablagh;
					if($database)
					{
						$my->ex_sqlx("insert into `factor_det` (`factor_id`, `kala_id`, `tedad`, `ghimat`, `toz`, `takhfifModir`, `takhfifBaste`, `takhfifTedad`, `saghfTakhfif`, `taminkonande_id`) values (".$this->factor_id.",".((int)$k->id).",$tedad,$mablagh,'',$takhModir,$takhBaste,$takhTedad,$takhSaghf,".$k->taminkonande_id.")");
//						if($factor_new)
//							$my->ex_sqlx("update `factor` set `jamKol` = `jamKol`+$mablagh where `id` = ".$this->factor_id);
					}
					$this->kalas[] = array('kala'=>(int)$k->id,'tedad'=>$tedad);
				}
			}
			return($out);
		}
	}
?>
