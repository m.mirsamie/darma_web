<?php
	class kala_group_class
	{
		public function __construct($id=-1)
		{
			if((int)$id > 0)
			{
				$mysql = new mysql_class;
				$mysql->ex_sql("select * from `kala_group` where `id` = $id",$q);
				if(isset($q[0]))
				{
					$r = $q[0];
					$this->id=(int)$r['id'];
					$this->name=$r['name'];
					$this->toz=$r['toz'];
					$this->en=(int)$r['en'];
					$this->order=(int)$r['order'];
					$this->kala_abarGroup_id=(int)$r['kala_abarGroup_id'];
					$this->pic=$r['pic'];
				}
			}
		}
		public function loadByAbar($abar_id)
		{
			$out=array();
			$my =new mysql_class;
			$my->ex_sql("select `id`,`name`,`pic` from `kala_group` where `kala_abarGroup_id`=$abar_id order by `order`",$q);
			foreach($q as $r)
				$out[]=array('id'=>$r['id'],'name'=>$r['name'],'pic'=>$r['pic']);
			return($out);
		}
		public function getMini()
		{
			$out = new stdClass;
			$my =new mysql_class;
                        $my->ex_sql("select `id`,`name`,`kala_group_id` from `kala_miniGroup` order by `kala_group_id`,`name`",$q);
			foreach($q as $r)
			{
				if(!isset($out->$r['kala_group_id']))
					$out->$r['kala_group_id'] = array();
				array_push($out->$r['kala_group_id'], array("id"=>(int)$r['id'],"name"=>$r['name']));
			}
			return($out);
		}
		public function getAll($daste,$nameIdOnly = FALSE)
		{
			$daste = (int)$daste;
			$out = array();
			$my = new mysql_class;
			$d = array($daste);
			/*			
$shart = ((int)$daste>0)?'where `id`='.(int)$daste:'';
			$my->ex_sql("select `id` from `kala_abarGroup` $shart",$q);
			foreach($q as $r)
				$d[] = (int)$r['id'];
			*/
			if(count($d)>0)
			{
				$q = null;
			//	$my->ex_sql("select ".($nameIdOnly?'`id`,`name`':'*')." from `kala_group` order by `name`",$q);	
				$wer = $daste==0? '':"where `kala_abarGroup_id` in (".implode(',',$d).")";
				$my->ex_sql("select ".($nameIdOnly?'`id`,`name`':'*')." from `kala_group` $wer  order by `order`,`id` ",$q);
				foreach($q as $r )
				{
					if($nameIdOnly)
						$out[] = array("id"=>(int)$r['id'],"name"=>$r['name']);
					else
					{
						$tmp = new kala_group_class;
						$tmp->id = (int)$r['id'];
						$tmp->name=$r['name'];
						$tmp->toz=$r['toz'];
						$tmp->en=(int)$r['en'];
						$tmp->order=(int)$r['order'];
						$tmp->pic=$r['pic'];
						$out[] = $tmp;
					}
				}
			}
			return($out);
		}
	}
?>
