<?php
	class factor_det_class
	{
		public function __construct($id=-1)
		{
			if((int)$id > 0)
			{
				$mysql = new mysql_class;
				$mysql->ex_sql("select * from `factor_det` where `id` = $id",$q);
				if(isset($q[0]))
				{
					$r = $q[0];
					$this->id=$r['id'];
					$this->factor_id=$r['factor_id'];
					$this->kala_id=$r['kala_id'];
					$this->tedad=$r['tedad'];
					$this->ghimat=$r['ghimat'];
					$this->toz=$r['toz'];
					$this->takhfifModir=$r['takhfifModir'];
					$this->takhfifBaste=$r['takhfifBaste'];
					$this->takhfifTedad=$r['takhfifTedad'];
					$this->saghfTakhfif=$r['saghfTakhfif'];
					$this->taminkonande=$r['taminkonande'];
				}
			}
		}
		public function getlastTedad($user_id,$kala_id)
		{
			$out = -1;
			$my = new mysql_class;
			$my->ex_sql("select `id` from `factor_det` where `takhfifTedad` > 0 order by `id` limit 1",$q);
			if(isset($q[0]))
				$out = (int)$q[0]['id'];
			return($out);
		}
		public function refreshMablagh($tedad,$user_id)
		{
			if(isset($this->id))
			{
				$k = new kala_class($this->kala_id);
				$takh = takhfif_class::get($this->kala_id,$user_id,$tedad);
				$takhfif = $takh['takhfif'];
				$takhBaste = $takh['basteTakhfif']['value'];
				$takhModir = $takh['modirTakhfif']['value'];
				$takhTedad = $takh['tedadTakhfif']['value'];
				$takhSaghf = $takh['saghfTakhfif'];
				$ghimatKolKala = $tedad*$k->ghimat*(100-$takhfif)/100;
				$my = new mysql_class;
				$my->ex_sqlx("update `factor_det` set `tedad` = $tedad , `ghimat` = $ghimatKolKala , `takhfifModir` = $takhModir, `takhfifBaste` = $takhBaste , `takhfifTedad` = $takhTedad where `id` = ".$this->id);
				$my->ex_sqlx("update `factor` set `jamKol` = `jamKol`+ $ghimatKolKala where `id` = ".$this->factor_id);
			}
		}
		public function insert($factor_id)
		{
				$out = -1;
				if($factor_id>0)
				{
					$mysql = new mysql_class;
					$mysql->ex_sql("select `id` from `factor_det_back` where `factor_id` = $factor_id and `en`=1",$q);
					if(isset($q[0]))
					{
						$mysql->ex_sqlx("delete from `factor_det` where `factor_id` = $factor_id");
						$mysql->ex_sqlx("insert into `factor_det` (select null,`factor_id`,`kala_id`,`tedad`,`ghimat`,`toz`,`takhfifModir`,`takhfifBaste`,`takhfifTedad`,`saghfTakhfif`,`taminkonande_id` from `factor_det_back` where `factor_id`=$factor_id)");
						$mysql->ex_sqlx("delete from `factor_det_back` where `factor_id` = $factor_id");
						$out = TRUE;
					}
				}
				return($out);
		}
	}
?>
