<?php
	class user_class
	{
		public function __construct($id=-1)
		{
			$this->conf = new conf;
			if((int)$id > 0)
			{
				$mysql = new mysql_class;
				$mysql->ex_sql("select * from `user` where `id` = $id",$q);
				if(isset($q[0]))
				{
					$r = $q[0];
					$this->fname=$r['fname'];
					$this->lname=$r['lname'];
					$this->id=(int)$r['id'];
					$this->user=$r['user'];
					$this->pass=$r['pass'];
					$this->group_id=(int)$r['group_id'];
					$this->en = (int)$r['en'];
					$this->isEnable = (int)$r['isEnable'];
					$this->company_id = $r['company_id'];
					$this->login_count = $r['login_count'];
					$this->isOwn = ($this->conf->company_id == $this->company_id);
					$this->allEtebar = 0;
					if(!$this->isOwn)
					{
						switch((int)trim($this->company_id))
						{
							case 1:
								$c1_db = $this->conf->company_1;
								$mysql->doTransalet = FALSE;
								$mysql->ex_sql("select (etebar-if(min_etebar>0,min_etebar,0)) as etb from $c1_db.parvande left join $c1_db.user_parvande on (parvande_id=parvande.id) where user_id = ".$this->id,$q);
								if(isset($q[0]))
									$this->allEtebar = (int)$q[0]['etb'];
								break;
						}
					}
					else
					{
						$p = new profile_class($this->id);
						$this->allEtebar = $p->etebar-$p->min_etebar;
					}
					$this->company_group_id = -1;
					$this->is_master = false;
					$q = null;
					$mysql->ex_sql("select company_group_id,is_admin from company_user where user_id = $id",$q);
					if(isset($q[0]))
					{
						$this->company_group_id = (int)$q[0]['company_group_id'];
						$this->is_master = ((int)$q[0]['is_admin']==1);
					}
				}
			}
		}
		public function hasTerminal()
		{
			$my = new mysql_class;
                        $my->ex_sql("select `id` from terminals where user_id = ".$this->id,$q);
			$out = isset($q[0]);
			return($out);
		}
		public function resetPassword($email)
		{
			$out = FALSE;
			$my = new mysql_class;
			$my->ex_sql("select `user_id` as `id` from `profile` where `email` = '$email'",$q);
			if(isset($q[0]))
			{
				$newPass = rand(1,9999).'A';
				$npass = md5($newPass);
				$my->ex_sqlx("update `user` set `pass` = '$npass' where `id` = ".$q[0]['id']);
				$message = '<html><body dir="rtl">رمز عبور جدید شما عبار است از <br/>'.$newPass.'</body></html>';
				$e = new email_class($email,'دارما ، رمز عبور جدید',$message);
				$out = TRUE;
			}
			return($out);
		}
		function addLoginCount($user_id)
		{
			$my = new mysql_class;
			return($my->ex_sqlx("update user set login_count=login_count+1 where id=$user_id"));
		}
		function getIncomeUser()
		{
			$mysql = new mysql_class;
			$mysql->ex_sql("select * from `user` where `en` = -1 and `company_id` = ".$this->conf->company_id,$q);
			if(isset($q[0]))
			{
				$r = $q[0];
				$this->fname=$r['fname'];
				$this->lname=$r['lname'];
				$this->id=(int)$r['id'];
				$this->user=$r['user'];
				$this->pass=$r['pass'];
				$this->group_id=(int)$r['group_id'];
				$this->en = (int)$r['en'];
				$this->isEnable = (int)$r['isEnable'];
				$this->company_id = $r['company_id'];
				$this->login_count = $r['login_count'];
				$this->isOwn = ($this->conf->company_id == $this->company_id);
				$this->allEtebar = 0;
				if(!$this->isOwn)
				{
					switch((int)trim($this->company_id))
					{
						case 1:
							$c1_db = $this->conf->company_1;
							$mysql->doTransalet = FALSE;
							$mysql->ex_sql("select (etebar-min_etebar) as etb from $c1_db.parvande left join $c1_db.user_parvande on (parvande_id=parvande.id) where user_id = ".$this->id,$q);
							if(isset($q[0]))
								$this->allEtebar = (int)$q[0]['etb'];
							break;
					}
				}
				else
				{
					$p = new profile_class($this->id);
					$this->allEtebar = $p->etebar-$p->min_etebar;
				}
			}
		}
	}
?>
