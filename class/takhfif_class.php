<?php
	class takhfif_class
	{
		public function __construct($id=-1)
		{
			if((int)$id > 0)
			{
				$mysql = new mysql_class;
				$mysql->ex_sql("select * from `takhfif` where `id` = $id",$q);
				if(isset($q[0]))
				{
					$r = $q[0];
					$this->id=$r['id'];
					$this->name=$r['name'];
					$this->darsad=$r['darsad'];
					$this->en=$r['en'];
					$this->user_id=$r['user_id'];
					$this->regdate=$r['regdate'];
				}
			}
		}
		public function getModirTakhfif($kala_id,$user_id)
		{
			$my = new mysql_class;
			$kala_qu ='1=0';
			$user_qu ='1=0';
			$out = array('name'=>'','value'=>0);
			$arr = array('takhfif_k_u','takhfif_k_g','takhfif_g_u','takhfif_g_g');
			foreach($arr as $table)
			{
				$tt = explode('_',$table);
				$kala_qu = 'kala_id='.$kala_id;
				$user_qu = 'user_id='.$user_id;
				if($tt[1]=='g')
				{
					$kg = new kala_class($kala_id);
					$kala_qu = '`kala_miniGroup_id`='.$kg->kala_miniGroup_id;
				}
				if($tt[2]=='g')
				{
					$ug = new profile_class($user_id);
					$user_qu = isset($ug->user_daste_id)? '`user_group_id`='.$ug->user_daste_id:-1;
				}
				$q= null;
				$my->ex_sql("select `id`,`takhfif_id` from `$table` where $kala_qu and $user_qu",$q);
				//echo "select `id`,`takhfif_id` from `$table` where $kala_qu and $user_qu".'<br/>';
				if(isset($q[0]))
					break;
			}
			if(isset($q[0]))
			{
				$takhfif = new takhfif_class((int)$q[0]['takhfif_id']);
				$out = array('name'=>$takhfif->name,'value'=>$takhfif->darsad);
			}
			return($out);
		}
		function getBasteTakhfif($kala_id,$tedad)
		{
			$out = array('name'=>'تخفیف روی بسته','value'=>0);
			$kal = new kala_class($kala_id);
			if($kal->tedad_baste<=$tedad)
				$out['value'] = $kal->takhfif;
			return(array('main'=>$out,'saghfTakhfif'=>$kal->saghf_takhfif));
		}
		public function getTedadTakhfif($kala_id,$user_id,$tedad)
		{
//اشتباه است
// مهرداد
			$out = array('name'=>'تخفیف روی میزان خرید','value'=>0);
			$kala = new kala_class($kala_id);
			$my = new mysql_class;
			$factor = factor_class::loadByUser($user_id,TRUE);
			$last_id = factor_det_class::getLastTedad($user_id,$kala_id);
			if($factor!='')
			{
				$my->ex_sql("select sum(`tedad`) as `ts` from `factor_det` where `id`>$last_id and  `kala_id`=$kala_id and `factor_id` in ($factor) ",$q);
				if(isset($q[0]) && (int)$q[0]['ts']>$kala->tedad_kharid)
					$out['value'] = $kala->takhfif_tedad_kharid;
			}
			return($out);
		}
		public function get($kala_id,$user_id,$tedad)
		{
			$tmp_baste = takhfif_class::getBasteTakhfif($kala_id,$tedad);
			$modir = takhfif_class::getModirTakhfif($kala_id,$user_id);
			$baste = $tmp_baste['main'];
			$saghf = $tmp_baste['saghfTakhfif'];
			$tedad = takhfif_class::getTedadTakhfif($kala_id,$user_id,$tedad);
			$all = $modir['value']+$baste['value']+$tedad['value'];
			$takhfif = $saghf>=$all ? $all : $saghf ;
			$out = array('modirTakhfif'=>$modir,'basteTakhfif'=>$baste,'tedadTakhfif'=>$tedad,'saghfTakhfif'=>$saghf,'takhfif'=>$takhfif);
			return($out);
		}
	}
?>
