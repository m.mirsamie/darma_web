<?php
	class kala_class
	{
		public function __construct($id=-1)
		{
			if((int)$id > 0)
			{
				$mysql = new mysql_class;
				$mysql->ex_sql("select * from `kala` where `id` = $id",$q);
				if(isset($q[0]))
				{
					$r = $q[0];
					$this->id=(int)$r['id'];
					$this->kala_miniGroup_id=(int)$r['kala_miniGroup_id'];
					$this->name=$r['name'];
					$this->vahed_id=(int)$r['vahed_id'];
					$this->mojoodi=(int)$r['mojoodi'];
					$this->vazn=(int)$r['vazn'];
					$this->toz=$r['toz'];
					$this->pic=$r['pic'];
					$this->ghimat=(int)$r['ghimat'];
					$this->ghimat_user=$r['ghimat_user'];
					$this->tedad_baste=$r['tedad_baste'];
					$this->takhfif=$r['takhfif'];
					$this->saghf_takhfif=$r['saghf_takhfif'];
					$this->tedad_kharid=$r['tedad_kharid'];
					$this->takhfif_tedad_kharid=$r['takhfif_tedad_kharid'];
					$this->emtiz=$r['emtiz'];
					$this->special=$r['special'];
					$this->time_limit=$r['time_limit'];
					$this->taminkonande_id=$r['taminkonande_id'];
					$this->garanty_name=$r['garanty_name'];
					$this->garanty_addr=$r['garanty_addr'];
					$this->garanty_tell=$r['garanty_tell'];
					$this->brand_id=$r['brand_id'];
					$this->en=(int)$r['en'];
					$this->is_most_sale = (int)$r['is_most_sale'];
					$this->barcode=$r['barcode'];
					$this->novin_id=$r['novin_id'];
				}
			}
		}
		public function loadByBarcode($id)
		{
			if(trim($id) != '')
                        {
                                $mysql = new mysql_class;
                                $mysql->ex_sql("select * from `kala` where `barcode` = '$id'",$q);
                                if(isset($q[0]))
                                {
                                        $r = $q[0];
                                        $this->id=(int)$r['id'];
                                        $this->kala_miniGroup_id=(int)$r['kala_miniGroup_id'];
                                        $this->name=$r['name'];
                                        $this->vahed_id=(int)$r['vahed_id'];
                                        $this->mojoodi=(int)$r['mojoodi'];
                                        $this->vazn=(int)$r['vazn'];
                                        $this->toz=$r['toz'];
                                        $this->pic=$r['pic'];
                                        $this->ghimat=(int)$r['ghimat'];
                                        $this->ghimat_user=$r['ghimat_user'];
                                        $this->tedad_baste=$r['tedad_baste'];
                                        $this->takhfif=$r['takhfif'];
                                        $this->saghf_takhfif=$r['saghf_takhfif'];
                                        $this->tedad_kharid=$r['tedad_kharid'];
                                        $this->takhfif_tedad_kharid=$r['takhfif_tedad_kharid'];
                                        $this->emtiz=$r['emtiz'];
                                        $this->special=$r['special'];
                                        $this->time_limit=$r['time_limit'];
                                        $this->taminkonande_id=$r['taminkonande_id'];
                                        $this->garanty_name=$r['garanty_name'];
                                        $this->garanty_addr=$r['garanty_addr'];
                                        $this->garanty_tell=$r['garanty_tell'];
                                        $this->brand_id=$r['brand_id'];
                                        $this->en=(int)$r['en'];
                                        $this->is_most_sale = (int)$r['is_most_sale'];
                                        $this->barcode=$r['barcode'];
					$this->novin_id=$r['novin_id'];
                                }
                        }
		}
		public function refresh()
		{
			$id = $this->id;
			if((int)$id > 0)
			{
				$mysql = new mysql_class;
				$mysql->ex_sql("select * from `kala` where `id` = $id",$q);
				if(isset($q[0]))
				{
					$r = $q[0];
					$this->id=(int)$r['id'];
					$this->kala_miniGroup_id=(int)$r['kala_miniGroup_id'];
					$this->name=$r['name'];
					$this->vahed_id=(int)$r['vahed_id'];
					$this->mojoodi=(int)$r['mojoodi'];
					$this->vazn=(int)$r['vazn'];
					$this->toz=$r['toz'];
					$this->pic=$r['pic'];
					$this->ghimat=(int)$r['ghimat'];
					$this->ghimat_user=$r['ghimat_user'];
					$this->tedad_baste=$r['tedad_baste'];
					$this->takhfif=$r['takhfif'];
					$this->saghf_takhfif=$r['saghf_takhfif'];
					$this->tedad_kharid=$r['tedad_kharid'];
					$this->takhfif_tedad_kharid=$r['takhfif_tedad_kharid'];
					$this->emtiz=$r['emtiz'];
					$this->special=$r['special'];
					$this->time_limit=$r['time_limit'];
					$this->taminkonande_id=$r['taminkonande_id'];
					$this->garanty_name=$r['garanty_name'];
					$this->garanty_addr=$r['garanty_addr'];
					$this->garanty_tell=$r['garanty_tell'];
					$this->brand_id=$r['brand_id'];
					$this->en=(int)$r['en'];
					$this->is_most_sale = (int)$r['is_most_sale'];
					$this->barcode=$r['barcode'];
					$this->novin_id=$r['novin_id'];
				}
			}
		}
		public function setMojoodiNew($id,$m,$g_user,$g_darma,$is_en=TRUE)
		{
			$m = (int)$m;
			$id = (int)$id;
			$my = new mysql_class;
			$my->ex_sqlx("update `kala` set `novin_id`= $id,".(($m>=0)?" `mojoodi` = $m,":'')."`ghimat` = $g_user , `ghimat_user` = $g_user,`en`=".(($is_en)?'1':'-1')." where `id` = ".$this->id);
			$this->refresh();
		}
		public function getTimeLimit()
		{
			$out = array();
			$my = new mysql_class;
			$my->ex_sql("select `id`,`name`,`pic`,`ghimat`,`ghimat_user`,`time_limit` from `kala` where `time_limit` > '".date("Y-m-d H:i:s")."' and `en`=1 ",$q);
			foreach($q as $r)
				$out[] = array('id'=>(int)$r['id'],'name'=>$r['name'],'pic'=>file_exists($r['pic'])?$r['pic']:'','ghimat'=>$r['ghimat'],'ghimat_user'=>$r['ghimat_user'],'time_limit'=>strtotime($r['time_limit']));
			return($out);
		}
		public function loadByMiniGroup($kala_minGroup_id)
		{
			$out = array();
			$mysql = new mysql_class;
			$mysql->ex_sql("select * from `kala` where `kala_miniGroup_id`=$kala_minGroup_id and `en`=1 and `novin_id` > 0 order by `ghimat`",$q);
			foreach($q as $r)
			{
				$tmp = new kala_class(-1);
				$tmp->id=$r['id'];
				$tmp->kala_miniGroup_id=$r['kala_miniGroup_id'];
				$tmp->name=$r['name'];
				$tmp->vahed_id=$r['vahed_id'];
				$tmp->mojoodi=(int)$r['mojoodi'];
				$tmp->vazn=$r['vazn'];
				$tmp->toz=$r['toz'];
				$tmp->pic=$r['pic'];
				$tmp->ghimat=$r['ghimat'];
				$tmp->ghimat_user=$r['ghimat_user'];
				$tmp->tedad_baste=$r['tedad_baste'];
				$tmp->takhfif=$r['takhfif'];
				$tmp->saghf_takhfif=$r['saghf_takhfif'];
				$tmp->tedad_kharid=$r['tedad_kharid'];
				$tmp->takhfif_tedad_kharid=$r['takhfif_tedad_kharid'];
				$tmp->emtiz=$r['emtiz'];
				$tmp->special=$r['special'];
				$tmp->time_limit=$r['time_limit'];
				$tmp->taminkonande_id=$r['taminkonande_id'];
				$tmp->garanty_name=$r['garanty_name'];
				$tmp->garanty_addr=$r['garanty_addr'];
				$tmp->garanty_tell=$r['garanty_tell'];
				$tmp->brand_id=$r['brand_id'];
				$tmp->en=(int)$r['en'];
				$tmp->is_most_sale = (int)$r['is_most_sale'];
				$tmp->barcode=$r['barcode'];
				$tmp->novin_id=$r['novin_id'];
				$out[] = $tmp;
			}
			return($out);
		}
		public function loadLast($tedad,$arr,$kala_abarGroup_id=-1)
		{
			$out = array();
			$mysql = new mysql_class;
			$sh = '';
			foreach($arr as $ta)
				$sh.=($sh==''?'':',').$ta->id;
			$shart = ($sh=='')?' where ':" where `kala`.`id` not in ($sh) and ";
			if($kala_abarGroup_id>0)
					$qu = "select `kala`.`id`,`kala`.`novin_id`,`barcode`, `kala_miniGroup_id`, `kala`.`name`, `vahed_id`, `mojoodi`, `vazn`, `kala`.`pic`, `ghimat`, `ghimat_user`, `tedad_baste`, `takhfif`, `saghf_takhfif`, `tedad_kharid`, `takhfif_tedad_kharid`, `emtiz`, `special`, `time_limit`, `taminkonande_id`, `garanty_name`, `garanty_addr`, `garanty_tell`, `brand_id`,is_most_sale from `kala` left join `kala_miniGroup` on (`kala_miniGroup_id`=`kala_miniGroup`.`id`) left join `kala_group` on (`kala_miniGroup`.`kala_group_id`=`kala_group`.`id`) left join `kala_abarGroup` on (`kala_group`.`kala_abarGroup_id`=`kala_abarGroup`.`id`) $shart `kala`.`en`=1 and `kala_abarGroup`.`id`=$kala_abarGroup_id and `novin_id` > 0 order by `kala`.`id` desc limit $tedad";
			else
					$qu = "select * from `kala` $shart `en`=1 order by `id` desc limit $tedad";
			$mysql->ex_sql($qu,$q);
			foreach($q as $r)
			{
				$tmp = new kala_class(-1);
				$tmp->id=$r['id'];
				$tmp->kala_miniGroup_id=$r['kala_miniGroup_id'];
				$tmp->name=$r['name'];
				$tmp->vahed_id=$r['vahed_id'];
				$tmp->mojoodi=(int)$r['mojoodi'];
				$tmp->vazn=$r['vazn'];
				$tmp->toz='';
				$tmp->pic=$r['pic'];
				$tmp->ghimat=$r['ghimat'];
				$tmp->ghimat_user=$r['ghimat_user'];
				$tmp->tedad_baste=$r['tedad_baste'];
				$tmp->takhfif=$r['takhfif'];
				$tmp->saghf_takhfif=$r['saghf_takhfif'];
				$tmp->tedad_kharid=$r['tedad_kharid'];
				$tmp->takhfif_tedad_kharid=$r['takhfif_tedad_kharid'];
				$tmp->emtiz=$r['emtiz'];
				$tmp->special=$r['special'];
				$tmp->time_limit=$r['time_limit'];
				$tmp->taminkonande_id=$r['taminkonande_id'];
				$tmp->garanty_name=$r['garanty_name'];
				$tmp->garanty_addr=$r['garanty_addr'];
				$tmp->garanty_tell=$r['garanty_tell'];
				$tmp->brand_id=$r['brand_id'];
				$tmp->en=1;
				$tmp->is_most_sale = (int)$r['is_most_sale'];
				$tmp->barcode=$r['barcode'];
				$tmp->novin_id=$r['novin_id'];
				$out[] = $tmp;
			}
			return($out);
		}
		public function loadByBrand($brand_id=-1)
		{
			$brand_id = (int)$brand_id;
			$out = array();
			$my = new mysql_class;
			$my->ex_sql("select * from `kala` where `brand_id` = $brand_id and `en`=1 and `novin_id` > 0",$q);
			foreach($q as $r)
			{
				$tmp = new kala_class(-1);
				$tmp->id=$r['id'];
				$tmp->kala_miniGroup_id=$r['kala_miniGroup_id'];
				$tmp->name=$r['name'];
				$tmp->vahed_id=$r['vahed_id'];
				$tmp->mojoodi=(int)$r['mojoodi'];
				$tmp->vazn=$r['vazn'];
				$tmp->toz=$r['toz'];
				$tmp->pic=$r['pic'];
				$tmp->ghimat=$r['ghimat'];
				$tmp->ghimat_user=$r['ghimat_user'];
				$tmp->tedad_baste=$r['tedad_baste'];
				$tmp->takhfif=$r['takhfif'];
				$tmp->saghf_takhfif=$r['saghf_takhfif'];
				$tmp->tedad_kharid=$r['tedad_kharid'];
				$tmp->takhfif_tedad_kharid=$r['takhfif_tedad_kharid'];
				$tmp->emtiz=$r['emtiz'];
				$tmp->special=$r['special'];
				$tmp->time_limit=$r['time_limit'];
				$tmp->taminkonande_id=$r['taminkonande_id'];
				$tmp->garanty_name=$r['garanty_name'];
				$tmp->garanty_addr=$r['garanty_addr'];
				$tmp->garanty_tell=$r['garanty_tell'];
				$tmp->brand_id=$r['brand_id'];
				$tmp->en=(int)$r['en'];
				$tmp->is_most_sale = (int)$r['is_most_sale'];
				$tmp->barcode=$r['barcode'];
				$tmp->novin_id=$r['novin_id'];
				$out[] = $tmp;
			}
			return($out);
		}
		public function search($key)
		{
			$out = array();
			$my = new mysql_class;
			$my->ex_sql("select * from `kala` where `name` like '%$key%' and `en`=1 and `novin_id` > 0",$q);
			foreach($q as $r)
			{
				$tmp = new kala_class(-1);
				$tmp->id=$r['id'];
				$tmp->kala_miniGroup_id=$r['kala_miniGroup_id'];
				$tmp->name=$r['name'];
				$tmp->vahed_id=$r['vahed_id'];
				$tmp->mojoodi=(int)$r['mojoodi'];
				$tmp->vazn=$r['vazn'];
				$tmp->toz=$r['toz'];
				$tmp->pic=$r['pic'];
				$tmp->ghimat=$r['ghimat'];
				$tmp->ghimat_user=$r['ghimat_user'];
				$tmp->tedad_baste=$r['tedad_baste'];
				$tmp->takhfif=$r['takhfif'];
				$tmp->saghf_takhfif=$r['saghf_takhfif'];
				$tmp->tedad_kharid=$r['tedad_kharid'];
				$tmp->takhfif_tedad_kharid=$r['takhfif_tedad_kharid'];
				$tmp->emtiz=$r['emtiz'];
				$tmp->special=$r['special'];
				$tmp->time_limit=$r['time_limit'];
				$tmp->taminkonande_id=$r['taminkonande_id'];
				$tmp->garanty_name=$r['garanty_name'];
				$tmp->garanty_addr=$r['garanty_addr'];
				$tmp->garanty_tell=$r['garanty_tell'];
				$tmp->brand_id=$r['brand_id'];
				$tmp->en=(int)$r['en'];
				$tmp->is_most_sale = (int)$r['is_most_sale'];
				$tmp->barcode=$r['barcode'];
				$tmp->novin_id=$r['novin_id'];
				$out[] = $tmp;
			}
			return($out);
		}
		public function mostSale($count = 10)
		{
			$count = (int)$count;
			$lim = ($count>0)?" limit $count ":'';
			$out = array();
			$my = new mysql_class;
			$my->ex_sql("select `kala_id`,sum(`factor_det`.`tedad`) as `co` from `factor_det` left join `factor` on (`factor_id`=`factor`.`id`)  where `isTasfie` = 1 and `en`=1 and `novin_id` > 0 group by `kala_id` order by `co` desc $lim",$q);
			//$my->ex_sql("select id as kala_id,mojoodi as tedad from kala where is_most_sale>0 order by is_most_sale",$q);
			foreach($q as $r)
					$out[] = array('kala_id'=>(int)$r['kala_id'],'tedad'=>(int)$r['co']);
			return($out);
		}
		public function loadMostSale($count,$arr,$kala_abarGroup_id=-1)
		{
			$count = (int)$count;
			$lim = ($count>0)?" limit $count ":'';
			$out = array();
			$sh = '';
			foreach($arr as $ta)
				$sh.=($sh==''?'':',').$ta->id;
			$shart = ($sh=='')?'':" `kala`.`id` not in ($sh) and ";
			$my = new mysql_class;
			if($kala_abarGroup_id>0)
					$qu ="select `kala`.`id` as `kid`,`kala`.`novin_id` ,`kala`.`name`,`mojoodi`,`kala`.`pic`,`kala`.`ghimat`,`ghimat_user`,`tedad_baste`,`kala`.`takhfif`,is_most_sale from kala left join `kala_miniGroup` on (`kala_miniGroup_id`=`kala_miniGroup`.`id`) left join `kala_group` on (`kala_miniGroup`.`kala_group_id`=`kala_group`.`id`) left join `kala_abarGroup` on (`kala_group`.`kala_abarGroup_id`=`kala_abarGroup`.`id`) where $shart `kala`.`en`=1 and `kala_abarGroup`.`id`=$kala_abarGroup_id and is_most_sale>0 and `novin_id` > 0  order by is_most_sale $lim";
			else
				$qu ="select `kala`.`id` as `kid`,`kala`.`name`,`mojoodi`,`pic`,`kala`.`ghimat`,`ghimat_user`,`tedad_baste`,`kala`.`takhfif`,is_most_sale from kala where $shart is_most_sale>0 and `kala`.`en`=1 and `novin_id`>0 order by is_most_sale  $lim";
			$my->ex_sql($qu,$q);
			foreach($q as $r)
			{
		        $tmp = new kala_class(-1);
				$tmp->id=$r['kid'];
				$tmp->name=$r['name'];
				$tmp->mojoodi=(int)$r['mojoodi'];
				$tmp->pic=$r['pic'];
				$tmp->ghimat=$r['ghimat'];
				$tmp->ghimat_user=$r['ghimat_user'];
				$tmp->tedad_baste=$r['tedad_baste'];
				$tmp->takhfif=$r['takhfif'];
				$out[] = $tmp;
			}
                        return($out);
		}
		public function loadSpecial($count,$kala_abarGroup_id=-1)
		{
			$count = (int)$count;
			$lim = ($count>0)?" limit $count ":'';
			$out = array();
			$my = new mysql_class;
			if($kala_abarGroup_id>0)
					$qu = "select `kala`.`id` as `kid`,`kala`.`name`,`mojoodi`,`kala`.`pic`,`kala`.`ghimat`,`ghimat_user`,`tedad_baste`,`kala`.`takhfif` from `kala` left join `kala_miniGroup` on (`kala_miniGroup_id`=`kala_miniGroup`.`id`) left join `kala_group` on (`kala_miniGroup`.`kala_group_id`=`kala_group`.`id`) left join `kala_abarGroup` on (`kala_group`.`kala_abarGroup_id`=`kala_abarGroup`.`id`) where `kala`.`special`=1 and `kala`.`en`=1 and `kala_abarGroup`.`id`=$kala_abarGroup_id and `novin_id` > 0 order by `name` $lim";
			else
					$qu = "select `kala`.`id` as `kid`,`kala`.`name`,`mojoodi`,`kala`.`pic`,`kala`.`ghimat`,`ghimat_user`,`tedad_baste`,`kala`.`takhfif` from `kala` where `special`=1 and `en`=1 and `novin_id` > 0 order by `name` $lim";

            $my->ex_sql($qu,$q);
            foreach($q as $r)
			{
                                $tmp = new kala_class(-1);
				$tmp->id=$r['kid'];
				$tmp->name=$r['name'];
				$tmp->mojoodi=(int)$r['mojoodi'];
				$tmp->pic=$r['pic'];
				$tmp->ghimat=$r['ghimat'];
				$tmp->ghimat_user=$r['ghimat_user'];
				$tmp->tedad_baste=$r['tedad_baste'];
				$tmp->takhfif=$r['takhfif'];
				$out[] = $tmp;
			}
                        return($out);
		}
		public function mostRate($count = 10)
		{
				$count = (int)$count;
				$lim = ($count>0)?" limit $count ":'';
				$out = array();
				$my = new mysql_class;
				$my->ex_sql("select `id`,`emtiz` from `kala` where and `en`=1 and `novin_id` > 0 order by `emtiz` desc $lim",$q);
				foreach($q as $r)
									$out[] = array('kala_id'=>(int)$r['id'],'emtiaz'=>(int)$r['emtiz']);
				return($out);
		}
		public function almostDone($alertCount = 10)
		{
			$alertCount = (int)$alertCount;
			$out = array();
			$my = new mysql_class;
                        $my->ex_sql("select `id`,`mojoodi` from `kala` where `mojoodi` <= $alertCount and `en`=1 and `novin_id` > 0 order by `mojoodi`",$q);
			foreach($q as $r)
                                $out[] = array('kala_id'=>(int)$r['id'],'mojoodi'=>(int)$r['mojoodi']);
			return($out);
		}
		public function decMojoodi($inp)
		{
			$inp = (int)$inp;
			$my = new mysql_class;
                        $my->ex_sqlx("update `kala` set `mojoodi` = `mojoodi` - $inp where `id` = ".$this->id);
		}
		public function setMojoodi($ammount,$kala_id,$anbar_id)
		{
			$kala_id = (int)$kala_id;
			$anbar_id = (int)$anbar_id;
			$ammount = (int)$ammount;
			if($kala_id <= 0 && isset($this))
				$kala_id = (int)$this->id;
			$out = ($kala_id > 0 && $ammount >= 0);
			if($out)
			{
				$my = new mysql_class;
				$my->ex_sqlx("update kala set mojoodi = $ammount where id = $kala_id and anbar_id = $anbar_id");
			}
			return($out);
		}
		public function loadByParent($id,$parentTb)
		{
			$out = array();
			$id = (int)$id;
			$parentTb = trim($parentTb);
			$className = $parentTb.'_class';
			$parent = new $className($id);
			if(isset($parent->id))
			{
				$out = $parent->loadKalas();
			}
		}
		public function loadByAllParent($abar_id,$id,$mini_id)
		{
			$wer = array("novin_id > 0");
			$out = array();
			$id = (int)$id;
			$abar_id = (int)$abar_id;
			$mini_id = (int)$mini_id;
			$werC = '';
			if($abar_id > 0)
				$wer[] = 'kala_abarGroup_id='.$abar_id;
			if($id > 0)
				$wer[] = 'kala_Group_id='.$id;
			if($mini_id > 0)
				$wer[] = 'kala_miniGroup_id='.$mini_id;
			if(count($wer)>0)
				$werC = implode(' and ',$wer);
			if(trim($werC) != '')
				$werC = ' where '.$werC;
			$my = new mysql_class;
			$my->ex_sql("select kala.id,kala.name from kala left join kala_miniGroup on (kala_miniGroup_id=kala_miniGroup.id) left join kala_group on (kala_group_id=kala_group.id) left join kala_abarGroup on (kala_abarGroup_id=kala_abarGroup.id) $werC ",$q);
			foreach($q as $r)
				$out[] = array('id'=>$r['id'],'name'=>$r['name']);
			return($out);
		}
	}
?>
